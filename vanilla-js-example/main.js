const form = document.getElementById('form');
const app = document.getElementById('app');

form.onsubmit = submit;


// A list of offers to be shown to the user, such as a list of products in an e-commerce app or a list of service
// providers in a sharing economy app. This would typically be retrieved from the app's backend.
const OFFERS = [
    {
        name: "Paul",
        id: "provider-1"
    },
    {
        name: "Jane",
        id: "provider-2"
    },
    {
        name: "Ali",
        id: "provider-3"
    }
];


// Create markup
app.innerHTML = OFFERS.map(function (offer) {
    return (`<li class="offer">
        "${offer.name}" 
        <x-utu-recommendation id="recommendation" target-uuid="${offer.id}" style="marginTop: -20px;"></x-utu-recommendation> 
        <x-utu-feedback-form-popup source-uuid="user-1" target-uuid="${offer.id}" transaction-id="${offer.id}"></x-utu-feedback-form-popup>
        <br/><br/><br/><br/>
        <x-utu-feedback-details-popup target-uuid="${offer.id}"></x-utu-feedback-details-popup>
        </li>`);
}).join('');



function submit(event) {
    const apiKey = event.target.elements.apiKey.value;
    console.log(event.target.elements.apiKey.value);
    event.preventDefault();

    getToken.getToken(apiKey, "user-1").then(token => {
        console.log(token)
        window.dispatchEvent(new CustomEvent("utuIdentityDataReady", {detail: token}));
    });
}
