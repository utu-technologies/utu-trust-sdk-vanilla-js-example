# UTU Trust SDK

    
UTU’s Trust API seamlessly serves up personalized recommendations for trusted service providers on sharing platforms to drive your conversion, satisfaction, retention, and viral acquisition.

[More information about UTU](https://utu.io)

UTU Trust SDK provides web-components that access UTU’s Trust API and allows you simple integration with its services.

Find the [repository here](https://bitbucket.org/utu-technologies/utu-trust-sdk)

## Features

- Integration with UTU's Trust API
- Compatible with React, Angular, Vue and other popular UI libs and frameworks
- Shadow DOM
- Customized styles, which are not interfere with global CSS rules on a page

## Install

```bash
$ npm install @ututrust/web-components
```

## Examples

It is advised to review ```Quickstart Vanilla JS``` first before checking another samples.

### Quickstart Vanilla JS

Place ```<x-utu-root>``` custom tag. It is a parent component for recommendations:

```html
<script src="../../utu-web-components/dist/index.js"></script>

<!-- Once UTU SDK is included on your page, -->
<!-- it registers custom tags: <x-utu-root> and <x-utu-recommendation> -->

<!-- <x-utu-root> handles recommendations loading from API. -->

<x-utu-root api-key="[place your utu api key here]">
  <ul></ul>
</x-utu-root>
```

The next step is defining recommendations ```<x-utu-recommendation>```:

```html
<x-utu-root api-key="[place your utu api key here]">
  <ul>
    <li>
      <x-utu-recommendation recommendation-id="e541df40-74b6-478e-a7da-7a9e52778700" />
    </li>
  </ul>
</x-utu-root>
```
