(function (factory) {
  typeof define === 'function' && define.amd ? define(factory) :
  factory();
}((function () { 'use strict';

  var n$2,l$3,u$4,t$3,o$4,r$4,f$6,e$3={},c$4=[],s$3=/acit|ex(?:s|g|n|p|$)|rph|grid|ows|mnc|ntw|ine[ch]|zoo|^ord|itera/i;function a$3(n,l){for(var u in l)n[u]=l[u];return n}function h$2(n){var l=n.parentNode;l&&l.removeChild(n);}function v$2(l,u,i){var t,o,r,f={};for(r in u)"key"==r?t=u[r]:"ref"==r?o=u[r]:f[r]=u[r];if(arguments.length>2&&(f.children=arguments.length>3?n$2.call(arguments,2):i),"function"==typeof l&&null!=l.defaultProps)for(r in l.defaultProps)void 0===f[r]&&(f[r]=l.defaultProps[r]);return y$1(l,f,t,o,null)}function y$1(n,i,t,o,r){var f={type:n,props:i,key:t,ref:o,__k:null,__:null,__b:0,__e:null,__d:void 0,__c:null,__h:null,constructor:void 0,__v:null==r?++u$4:r};return null!=l$3.vnode&&l$3.vnode(f),f}function p$1(){return {current:null}}function d$2(n){return n.children}function _$1(n,l){this.props=n,this.context=l;}function k$2(n,l){if(null==l)return n.__?k$2(n.__,n.__.__k.indexOf(n)+1):null;for(var u;l<n.__k.length;l++)if(null!=(u=n.__k[l])&&null!=u.__e)return u.__e;return "function"==typeof n.type?k$2(n):null}function b$1(n){var l,u;if(null!=(n=n.__)&&null!=n.__c){for(n.__e=n.__c.base=null,l=0;l<n.__k.length;l++)if(null!=(u=n.__k[l])&&null!=u.__e){n.__e=n.__c.base=u.__e;break}return b$1(n)}}function m$1(n){(!n.__d&&(n.__d=!0)&&t$3.push(n)&&!g$2.__r++||r$4!==l$3.debounceRendering)&&((r$4=l$3.debounceRendering)||o$4)(g$2);}function g$2(){for(var n;g$2.__r=t$3.length;)n=t$3.sort(function(n,l){return n.__v.__b-l.__v.__b}),t$3=[],n.some(function(n){var l,u,i,t,o,r;n.__d&&(o=(t=(l=n).__v).__e,(r=l.__P)&&(u=[],(i=a$3({},t)).__v=t.__v+1,j$3(r,t,i,l.__n,void 0!==r.ownerSVGElement,null!=t.__h?[o]:null,u,null==o?k$2(t):o,t.__h),z$1(u,t),t.__e!=o&&b$1(t)));});}function w$2(n,l,u,i,t,o,r,f,s,a){var h,v,p,_,b,m,g,w=i&&i.__k||c$4,A=w.length;for(u.__k=[],h=0;h<l.length;h++)if(null!=(_=u.__k[h]=null==(_=l[h])||"boolean"==typeof _?null:"string"==typeof _||"number"==typeof _||"bigint"==typeof _?y$1(null,_,null,null,_):Array.isArray(_)?y$1(d$2,{children:_},null,null,null):_.__b>0?y$1(_.type,_.props,_.key,null,_.__v):_)){if(_.__=u,_.__b=u.__b+1,null===(p=w[h])||p&&_.key==p.key&&_.type===p.type)w[h]=void 0;else for(v=0;v<A;v++){if((p=w[v])&&_.key==p.key&&_.type===p.type){w[v]=void 0;break}p=null;}j$3(n,_,p=p||e$3,t,o,r,f,s,a),b=_.__e,(v=_.ref)&&p.ref!=v&&(g||(g=[]),p.ref&&g.push(p.ref,null,_),g.push(v,_.__c||b,_)),null!=b?(null==m&&(m=b),"function"==typeof _.type&&null!=_.__k&&_.__k===p.__k?_.__d=s=x$2(_,s,n):s=P$1(n,_,p,w,b,s),a||"option"!==u.type?"function"==typeof u.type&&(u.__d=s):n.value=""):s&&p.__e==s&&s.parentNode!=n&&(s=k$2(p));}for(u.__e=m,h=A;h--;)null!=w[h]&&("function"==typeof u.type&&null!=w[h].__e&&w[h].__e==u.__d&&(u.__d=k$2(i,h+1)),N$1(w[h],w[h]));if(g)for(h=0;h<g.length;h++)M$1(g[h],g[++h],g[++h]);}function x$2(n,l,u){var i,t;for(i=0;i<n.__k.length;i++)(t=n.__k[i])&&(t.__=n,l="function"==typeof t.type?x$2(t,l,u):P$1(u,t,t,n.__k,t.__e,l));return l}function A$2(n,l){return l=l||[],null==n||"boolean"==typeof n||(Array.isArray(n)?n.some(function(n){A$2(n,l);}):l.push(n)),l}function P$1(n,l,u,i,t,o){var r,f,e;if(void 0!==l.__d)r=l.__d,l.__d=void 0;else if(null==u||t!=o||null==t.parentNode)n:if(null==o||o.parentNode!==n)n.appendChild(t),r=null;else {for(f=o,e=0;(f=f.nextSibling)&&e<i.length;e+=2)if(f==t)break n;n.insertBefore(t,o),r=o;}return void 0!==r?r:t.nextSibling}function C$1(n,l,u,i,t){var o;for(o in u)"children"===o||"key"===o||o in l||H$1(n,o,null,u[o],i);for(o in l)t&&"function"!=typeof l[o]||"children"===o||"key"===o||"value"===o||"checked"===o||u[o]===l[o]||H$1(n,o,l[o],u[o],i);}function $$1(n,l,u){"-"===l[0]?n.setProperty(l,u):n[l]=null==u?"":"number"!=typeof u||s$3.test(l)?u:u+"px";}function H$1(n,l,u,i,t){var o;n:if("style"===l)if("string"==typeof u)n.style.cssText=u;else {if("string"==typeof i&&(n.style.cssText=i=""),i)for(l in i)u&&l in u||$$1(n.style,l,"");if(u)for(l in u)i&&u[l]===i[l]||$$1(n.style,l,u[l]);}else if("o"===l[0]&&"n"===l[1])o=l!==(l=l.replace(/Capture$/,"")),l=l.toLowerCase()in n?l.toLowerCase().slice(2):l.slice(2),n.l||(n.l={}),n.l[l+o]=u,u?i||n.addEventListener(l,o?T$2:I$1,o):n.removeEventListener(l,o?T$2:I$1,o);else if("dangerouslySetInnerHTML"!==l){if(t)l=l.replace(/xlink[H:h]/,"h").replace(/sName$/,"s");else if("href"!==l&&"list"!==l&&"form"!==l&&"tabIndex"!==l&&"download"!==l&&l in n)try{n[l]=null==u?"":u;break n}catch(n){}"function"==typeof u||(null!=u&&(!1!==u||"a"===l[0]&&"r"===l[1])?n.setAttribute(l,u):n.removeAttribute(l));}}function I$1(n){this.l[n.type+!1](l$3.event?l$3.event(n):n);}function T$2(n){this.l[n.type+!0](l$3.event?l$3.event(n):n);}function j$3(n,u,i,t,o,r,f,e,c){var s,h,v,y,p,k,b,m,g,x,A,P=u.type;if(void 0!==u.constructor)return null;null!=i.__h&&(c=i.__h,e=u.__e=i.__e,u.__h=null,r=[e]),(s=l$3.__b)&&s(u);try{n:if("function"==typeof P){if(m=u.props,g=(s=P.contextType)&&t[s.__c],x=s?g?g.props.value:s.__:t,i.__c?b=(h=u.__c=i.__c).__=h.__E:("prototype"in P&&P.prototype.render?u.__c=h=new P(m,x):(u.__c=h=new _$1(m,x),h.constructor=P,h.render=O$1),g&&g.sub(h),h.props=m,h.state||(h.state={}),h.context=x,h.__n=t,v=h.__d=!0,h.__h=[]),null==h.__s&&(h.__s=h.state),null!=P.getDerivedStateFromProps&&(h.__s==h.state&&(h.__s=a$3({},h.__s)),a$3(h.__s,P.getDerivedStateFromProps(m,h.__s))),y=h.props,p=h.state,v)null==P.getDerivedStateFromProps&&null!=h.componentWillMount&&h.componentWillMount(),null!=h.componentDidMount&&h.__h.push(h.componentDidMount);else {if(null==P.getDerivedStateFromProps&&m!==y&&null!=h.componentWillReceiveProps&&h.componentWillReceiveProps(m,x),!h.__e&&null!=h.shouldComponentUpdate&&!1===h.shouldComponentUpdate(m,h.__s,x)||u.__v===i.__v){h.props=m,h.state=h.__s,u.__v!==i.__v&&(h.__d=!1),h.__v=u,u.__e=i.__e,u.__k=i.__k,u.__k.forEach(function(n){n&&(n.__=u);}),h.__h.length&&f.push(h);break n}null!=h.componentWillUpdate&&h.componentWillUpdate(m,h.__s,x),null!=h.componentDidUpdate&&h.__h.push(function(){h.componentDidUpdate(y,p,k);});}h.context=x,h.props=m,h.state=h.__s,(s=l$3.__r)&&s(u),h.__d=!1,h.__v=u,h.__P=n,s=h.render(h.props,h.state,h.context),h.state=h.__s,null!=h.getChildContext&&(t=a$3(a$3({},t),h.getChildContext())),v||null==h.getSnapshotBeforeUpdate||(k=h.getSnapshotBeforeUpdate(y,p)),A=null!=s&&s.type===d$2&&null==s.key?s.props.children:s,w$2(n,Array.isArray(A)?A:[A],u,i,t,o,r,f,e,c),h.base=u.__e,u.__h=null,h.__h.length&&f.push(h),b&&(h.__E=h.__=null),h.__e=!1;}else null==r&&u.__v===i.__v?(u.__k=i.__k,u.__e=i.__e):u.__e=L$1(i.__e,u,i,t,o,r,f,c);(s=l$3.diffed)&&s(u);}catch(n){u.__v=null,(c||null!=r)&&(u.__e=e,u.__h=!!c,r[r.indexOf(e)]=null),l$3.__e(n,u,i);}}function z$1(n,u){l$3.__c&&l$3.__c(u,n),n.some(function(u){try{n=u.__h,u.__h=[],n.some(function(n){n.call(u);});}catch(n){l$3.__e(n,u.__v);}});}function L$1(l,u,i,t,o,r,f,c){var s,a,v,y=i.props,p=u.props,d=u.type,_=0;if("svg"===d&&(o=!0),null!=r)for(;_<r.length;_++)if((s=r[_])&&(s===l||(d?s.localName==d:3==s.nodeType))){l=s,r[_]=null;break}if(null==l){if(null===d)return document.createTextNode(p);l=o?document.createElementNS("http://www.w3.org/2000/svg",d):document.createElement(d,p.is&&p),r=null,c=!1;}if(null===d)y===p||c&&l.data===p||(l.data=p);else {if(r=r&&n$2.call(l.childNodes),a=(y=i.props||e$3).dangerouslySetInnerHTML,v=p.dangerouslySetInnerHTML,!c){if(null!=r)for(y={},_=0;_<l.attributes.length;_++)y[l.attributes[_].name]=l.attributes[_].value;(v||a)&&(v&&(a&&v.__html==a.__html||v.__html===l.innerHTML)||(l.innerHTML=v&&v.__html||""));}if(C$1(l,p,y,o,c),v)u.__k=[];else if(_=u.props.children,w$2(l,Array.isArray(_)?_:[_],u,i,t,o&&"foreignObject"!==d,r,f,r?r[0]:i.__k&&k$2(i,0),c),null!=r)for(_=r.length;_--;)null!=r[_]&&h$2(r[_]);c||("value"in p&&void 0!==(_=p.value)&&(_!==l.value||"progress"===d&&!_)&&H$1(l,"value",_,y.value,!1),"checked"in p&&void 0!==(_=p.checked)&&_!==l.checked&&H$1(l,"checked",_,y.checked,!1));}return l}function M$1(n,u,i){try{"function"==typeof n?n(u):n.current=u;}catch(n){l$3.__e(n,i);}}function N$1(n,u,i){var t,o;if(l$3.unmount&&l$3.unmount(n),(t=n.ref)&&(t.current&&t.current!==n.__e||M$1(t,null,u)),null!=(t=n.__c)){if(t.componentWillUnmount)try{t.componentWillUnmount();}catch(n){l$3.__e(n,u);}t.base=t.__P=null;}if(t=n.__k)for(o=0;o<t.length;o++)t[o]&&N$1(t[o],u,"function"!=typeof n.type);i||null==n.__e||h$2(n.__e),n.__e=n.__d=void 0;}function O$1(n,l,u){return this.constructor(n,u)}function S$1(u,i,t){var o,r,f;l$3.__&&l$3.__(u,i),r=(o="function"==typeof t)?null:t&&t.__k||i.__k,f=[],j$3(i,u=(!o&&t||i).__k=v$2(d$2,null,[u]),r||e$3,e$3,void 0!==i.ownerSVGElement,!o&&t?[t]:r?null:i.firstChild?n$2.call(i.childNodes):null,f,!o&&t?t:r?r.__e:i.firstChild,o),z$1(f,u);}function q$2(n,l){S$1(n,l,q$2);}function B$1(l,u,i){var t,o,r,f=a$3({},l.props);for(r in u)"key"==r?t=u[r]:"ref"==r?o=u[r]:f[r]=u[r];return arguments.length>2&&(f.children=arguments.length>3?n$2.call(arguments,2):i),y$1(l.type,f,t||l.key,o||l.ref,null)}function D$1(n,l){var u={__c:l="__cC"+f$6++,__:n,Consumer:function(n,l){return n.children(l)},Provider:function(n){var u,i;return this.getChildContext||(u=[],(i={})[l]=this,this.getChildContext=function(){return i},this.shouldComponentUpdate=function(n){this.props.value!==n.value&&u.some(m$1);},this.sub=function(n){u.push(n);var l=n.componentWillUnmount;n.componentWillUnmount=function(){u.splice(u.indexOf(n),1),l&&l.call(n);};}),n.children}};return u.Provider.__=u.Consumer.contextType=u}n$2=c$4.slice,l$3={__e:function(n,l){for(var u,i,t;l=l.__;)if((u=l.__c)&&!u.__)try{if((i=u.constructor)&&null!=i.getDerivedStateFromError&&(u.setState(i.getDerivedStateFromError(n)),t=u.__d),null!=u.componentDidCatch&&(u.componentDidCatch(n),t=u.__d),t)return u.__E=u}catch(l){n=l;}throw n}},u$4=0,_$1.prototype.setState=function(n,l){var u;u=null!=this.__s&&this.__s!==this.state?this.__s:this.__s=a$3({},this.state),"function"==typeof n&&(n=n(a$3({},u),this.props)),n&&a$3(u,n),null!=n&&this.__v&&(l&&this.__h.push(l),m$1(this));},_$1.prototype.forceUpdate=function(n){this.__v&&(this.__e=!0,n&&this.__h.push(n),m$1(this));},_$1.prototype.render=d$2,t$3=[],o$4="function"==typeof Promise?Promise.prototype.then.bind(Promise.resolve()):setTimeout,g$2.__r=0,f$6=0;

  function r$3() {
    return (r$3 = Object.assign || function (t) {
      for (var e = 1; e < arguments.length; e++) {
        var n = arguments[e];

        for (var o in n) Object.prototype.hasOwnProperty.call(n, o) && (t[o] = n[o]);
      }

      return t;
    }).apply(this, arguments);
  }

  function i$3(t) {
    this.getChildContext = function () {
      return t.context;
    };

    var e = t.children,
        n = function (t, e) {
      if (null == t) return {};
      var n,
          o,
          r = {},
          i = Object.keys(t);

      for (o = 0; o < i.length; o++) e.indexOf(n = i[o]) >= 0 || (r[n] = t[n]);

      return r;
    }(t, ["context", "children"]);

    return B$1(e, n);
  }

  function a$2() {
    var o = new CustomEvent("_preact", {
      detail: {},
      bubbles: !0,
      cancelable: !0
    });
    this.dispatchEvent(o), this._vdom = v$2(i$3, r$3({}, this._props, {
      context: o.detail.context
    }), function e(n, o) {
      if (3 === n.nodeType) return n.data;
      if (1 !== n.nodeType) return null;
      var r = [],
          i = {},
          a = 0,
          c = n.attributes,
          l = n.childNodes;

      for (a = c.length; a--;) "slot" !== c[a].name && (i[c[a].name] = c[a].value, i[s$2(c[a].name)] = c[a].value);

      for (a = l.length; a--;) {
        var p = e(l[a], null),
            d = l[a].slot;
        d ? i[d] = v$2(u$3, {
          name: d
        }, p) : r[a] = p;
      }

      var h = o ? v$2(u$3, null, r) : r;
      return v$2(o || n.nodeName.toLowerCase(), i, h);
    }(this, this._vdomComponent)), (this.hasAttribute("hydrate") ? q$2 : S$1)(this._vdom, this._root);
  }

  function s$2(t) {
    return t.replace(/-(\w)/g, function (t, e) {
      return e ? e.toUpperCase() : "";
    });
  }

  function c$3(t, e, r) {
    if (this._vdom) {
      var i = {};
      i[t] = r = null == r ? void 0 : r, i[s$2(t)] = r, this._vdom = B$1(this._vdom, i), S$1(this._vdom, this._root);
    }
  }

  function l$2() {
    S$1(this._vdom = null, this._root);
  }

  function u$3(e, n) {
    var o = this;
    return v$2("slot", r$3({}, e, {
      ref: function (t) {
        t ? (o.ref = t, o._listener || (o._listener = function (t) {
          t.stopPropagation(), t.detail.context = n;
        }, t.addEventListener("_preact", o._listener))) : o.ref.removeEventListener("_preact", o._listener);
      }
    }));
  }

  function register (t, e, n, o) {
    function r() {
      var e = Reflect.construct(HTMLElement, [], r);
      return e._vdomComponent = t, e._root = o && o.shadow ? e.attachShadow({
        mode: "open"
      }) : e, e;
    }

    return (r.prototype = Object.create(HTMLElement.prototype)).constructor = r, r.prototype.connectedCallback = a$2, r.prototype.attributeChangedCallback = c$3, r.prototype.disconnectedCallback = l$2, n = n || t.observedAttributes || Object.keys(t.propTypes || {}), r.observedAttributes = n, n.forEach(function (t) {
      Object.defineProperty(r.prototype, t, {
        get: function () {
          return this._vdom.props[t];
        },
        set: function (e) {
          this._vdom ? this.attributeChangedCallback(t, null, e) : (this._props || (this._props = {}), this._props[t] = e, this.connectedCallback());
          var n = typeof e;
          null != e && "string" !== n && "boolean" !== n && "number" !== n || this.setAttribute(t, e);
        }
      });
    }), customElements.define(e || t.tagName || t.displayName || t.name, r);
  }

  var t$2,u$2,r$2,o$3=0,i$2=[],c$2=l$3.__b,f$5=l$3.__r,e$2=l$3.diffed,a$1=l$3.__c,v$1=l$3.unmount;function m(t,r){l$3.__h&&l$3.__h(u$2,t,o$3||r),o$3=0;var i=u$2.__H||(u$2.__H={__:[],__h:[]});return t>=i.__.length&&i.__.push({}),i.__[t]}function l$1(n){return o$3=1,p(w$1,n)}function p(n,r,o){var i=m(t$2++,2);return i.t=n,i.__c||(i.__=[o?o(r):w$1(void 0,r),function(n){var t=i.t(i.__[0],n);i.__[0]!==t&&(i.__=[t,i.__[1]],i.__c.setState({}));}],i.__c=u$2),i.__}function y(r,o){var i=m(t$2++,3);!l$3.__s&&k$1(i.__H,o)&&(i.__=r,i.__H=o,u$2.__H.__h.push(i));}function h$1(r,o){var i=m(t$2++,4);!l$3.__s&&k$1(i.__H,o)&&(i.__=r,i.__H=o,u$2.__h.push(i));}function s$1(n){return o$3=5,d$1(function(){return {current:n}},[])}function _(n,t,u){o$3=6,h$1(function(){"function"==typeof n?n(t()):n&&(n.current=t());},null==u?u:u.concat(n));}function d$1(n,u){var r=m(t$2++,7);return k$1(r.__H,u)&&(r.__=n(),r.__H=u,r.__h=n),r.__}function A$1(n,t){return o$3=8,d$1(function(){return n},t)}function F$1(n){var r=u$2.context[n.__c],o=m(t$2++,9);return o.c=n,r?(null==o.__&&(o.__=!0,r.sub(u$2)),r.props.value):n.__}function T$1(t,u){l$3.useDebugValue&&l$3.useDebugValue(u?u(t):t);}function q$1(n){var r=m(t$2++,10),o=l$1();return r.__=n,u$2.componentDidCatch||(u$2.componentDidCatch=function(n){r.__&&r.__(n),o[1](n);}),[o[0],function(){o[1](void 0);}]}function x$1(){i$2.forEach(function(t){if(t.__P)try{t.__H.__h.forEach(g$1),t.__H.__h.forEach(j$2),t.__H.__h=[];}catch(u){t.__H.__h=[],l$3.__e(u,t.__v);}}),i$2=[];}l$3.__b=function(n){u$2=null,c$2&&c$2(n);},l$3.__r=function(n){f$5&&f$5(n),t$2=0;var r=(u$2=n.__c).__H;r&&(r.__h.forEach(g$1),r.__h.forEach(j$2),r.__h=[]);},l$3.diffed=function(t){e$2&&e$2(t);var o=t.__c;o&&o.__H&&o.__H.__h.length&&(1!==i$2.push(o)&&r$2===l$3.requestAnimationFrame||((r$2=l$3.requestAnimationFrame)||function(n){var t,u=function(){clearTimeout(r),b&&cancelAnimationFrame(t),setTimeout(n);},r=setTimeout(u,100);b&&(t=requestAnimationFrame(u));})(x$1)),u$2=void 0;},l$3.__c=function(t,u){u.some(function(t){try{t.__h.forEach(g$1),t.__h=t.__h.filter(function(n){return !n.__||j$2(n)});}catch(r){u.some(function(n){n.__h&&(n.__h=[]);}),u=[],l$3.__e(r,t.__v);}}),a$1&&a$1(t,u);},l$3.unmount=function(t){v$1&&v$1(t);var u=t.__c;if(u&&u.__H)try{u.__H.__.forEach(g$1);}catch(t){l$3.__e(t,u.__v);}};var b="function"==typeof requestAnimationFrame;function g$1(n){var t=u$2;"function"==typeof n.__c&&n.__c(),u$2=t;}function j$2(n){var t=u$2;n.__c=n.__(),u$2=t;}function k$1(n,t){return !n||n.length!==t.length||t.some(function(t,u){return t!==n[u]})}function w$1(n,t){return "function"==typeof t?t(n):t}

  function ownKeys$3(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);

      if (enumerableOnly) {
        symbols = symbols.filter(function (sym) {
          return Object.getOwnPropertyDescriptor(object, sym).enumerable;
        });
      }

      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread2(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys$3(Object(source), true).forEach(function (key) {
          _defineProperty$2(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys$3(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
    try {
      var info = gen[key](arg);
      var value = info.value;
    } catch (error) {
      reject(error);
      return;
    }

    if (info.done) {
      resolve(value);
    } else {
      Promise.resolve(value).then(_next, _throw);
    }
  }

  function _asyncToGenerator(fn) {
    return function () {
      var self = this,
          args = arguments;
      return new Promise(function (resolve, reject) {
        var gen = fn.apply(self, args);

        function _next(value) {
          asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
        }

        function _throw(err) {
          asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
        }

        _next(undefined);
      });
    };
  }

  function _defineProperty$2(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _extends$1() {
    _extends$1 = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends$1.apply(this, arguments);
  }

  function _objectWithoutPropertiesLoose(source, excluded) {
    if (source == null) return {};
    var target = {};
    var sourceKeys = Object.keys(source);
    var key, i;

    for (i = 0; i < sourceKeys.length; i++) {
      key = sourceKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      target[key] = source[key];
    }

    return target;
  }

  function _objectWithoutProperties(source, excluded) {
    if (source == null) return {};

    var target = _objectWithoutPropertiesLoose(source, excluded);

    var key, i;

    if (Object.getOwnPropertySymbols) {
      var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

      for (i = 0; i < sourceSymbolKeys.length; i++) {
        key = sourceSymbolKeys[i];
        if (excluded.indexOf(key) >= 0) continue;
        if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
        target[key] = source[key];
      }
    }

    return target;
  }

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _iterableToArrayLimit(arr, i) {
    var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"];

    if (_i == null) return;
    var _arr = [];
    var _n = true;
    var _d = false;

    var _s, _e;

    try {
      for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
  }

  function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;

    for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

    return arr2;
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

  function getDefaultExportFromCjs (x) {
  	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
  }

  function getAugmentedNamespace(n) {
  	if (n.__esModule) return n;
  	var a = Object.defineProperty({}, '__esModule', {value: true});
  	Object.keys(n).forEach(function (k) {
  		var d = Object.getOwnPropertyDescriptor(n, k);
  		Object.defineProperty(a, k, d.get ? d : {
  			enumerable: true,
  			get: function () {
  				return n[k];
  			}
  		});
  	});
  	return a;
  }

  function createCommonjsModule(fn) {
    var module = { exports: {} };
  	return fn(module, module.exports), module.exports;
  }

  /**
   * Copyright (c) 2014-present, Facebook, Inc.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE file in the root directory of this source tree.
   */

  createCommonjsModule(function (module) {
  var runtime = function (exports) {

    var Op = Object.prototype;
    var hasOwn = Op.hasOwnProperty;
    var undefined$1; // More compressible than void 0.

    var $Symbol = typeof Symbol === "function" ? Symbol : {};
    var iteratorSymbol = $Symbol.iterator || "@@iterator";
    var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
    var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

    function define(obj, key, value) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
      return obj[key];
    }

    try {
      // IE 8 has a broken Object.defineProperty that only works on DOM objects.
      define({}, "");
    } catch (err) {
      define = function (obj, key, value) {
        return obj[key] = value;
      };
    }

    function wrap(innerFn, outerFn, self, tryLocsList) {
      // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
      var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
      var generator = Object.create(protoGenerator.prototype);
      var context = new Context(tryLocsList || []); // The ._invoke method unifies the implementations of the .next,
      // .throw, and .return methods.

      generator._invoke = makeInvokeMethod(innerFn, self, context);
      return generator;
    }

    exports.wrap = wrap; // Try/catch helper to minimize deoptimizations. Returns a completion
    // record like context.tryEntries[i].completion. This interface could
    // have been (and was previously) designed to take a closure to be
    // invoked without arguments, but in all the cases we care about we
    // already have an existing method we want to call, so there's no need
    // to create a new function object. We can even get away with assuming
    // the method takes exactly one argument, since that happens to be true
    // in every case, so we don't have to touch the arguments object. The
    // only additional allocation required is the completion record, which
    // has a stable shape and so hopefully should be cheap to allocate.

    function tryCatch(fn, obj, arg) {
      try {
        return {
          type: "normal",
          arg: fn.call(obj, arg)
        };
      } catch (err) {
        return {
          type: "throw",
          arg: err
        };
      }
    }

    var GenStateSuspendedStart = "suspendedStart";
    var GenStateSuspendedYield = "suspendedYield";
    var GenStateExecuting = "executing";
    var GenStateCompleted = "completed"; // Returning this object from the innerFn has the same effect as
    // breaking out of the dispatch switch statement.

    var ContinueSentinel = {}; // Dummy constructor functions that we use as the .constructor and
    // .constructor.prototype properties for functions that return Generator
    // objects. For full spec compliance, you may wish to configure your
    // minifier not to mangle the names of these two functions.

    function Generator() {}

    function GeneratorFunction() {}

    function GeneratorFunctionPrototype() {} // This is a polyfill for %IteratorPrototype% for environments that
    // don't natively support it.


    var IteratorPrototype = {};
    define(IteratorPrototype, iteratorSymbol, function () {
      return this;
    });
    var getProto = Object.getPrototypeOf;
    var NativeIteratorPrototype = getProto && getProto(getProto(values([])));

    if (NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
      // This environment has a native %IteratorPrototype%; use it instead
      // of the polyfill.
      IteratorPrototype = NativeIteratorPrototype;
    }

    var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype);
    GeneratorFunction.prototype = GeneratorFunctionPrototype;
    define(Gp, "constructor", GeneratorFunctionPrototype);
    define(GeneratorFunctionPrototype, "constructor", GeneratorFunction);
    GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"); // Helper for defining the .next, .throw, and .return methods of the
    // Iterator interface in terms of a single ._invoke method.

    function defineIteratorMethods(prototype) {
      ["next", "throw", "return"].forEach(function (method) {
        define(prototype, method, function (arg) {
          return this._invoke(method, arg);
        });
      });
    }

    exports.isGeneratorFunction = function (genFun) {
      var ctor = typeof genFun === "function" && genFun.constructor;
      return ctor ? ctor === GeneratorFunction || // For the native GeneratorFunction constructor, the best we can
      // do is to check its .name property.
      (ctor.displayName || ctor.name) === "GeneratorFunction" : false;
    };

    exports.mark = function (genFun) {
      if (Object.setPrototypeOf) {
        Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
      } else {
        genFun.__proto__ = GeneratorFunctionPrototype;
        define(genFun, toStringTagSymbol, "GeneratorFunction");
      }

      genFun.prototype = Object.create(Gp);
      return genFun;
    }; // Within the body of any async function, `await x` is transformed to
    // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
    // `hasOwn.call(value, "__await")` to determine if the yielded value is
    // meant to be awaited.


    exports.awrap = function (arg) {
      return {
        __await: arg
      };
    };

    function AsyncIterator(generator, PromiseImpl) {
      function invoke(method, arg, resolve, reject) {
        var record = tryCatch(generator[method], generator, arg);

        if (record.type === "throw") {
          reject(record.arg);
        } else {
          var result = record.arg;
          var value = result.value;

          if (value && typeof value === "object" && hasOwn.call(value, "__await")) {
            return PromiseImpl.resolve(value.__await).then(function (value) {
              invoke("next", value, resolve, reject);
            }, function (err) {
              invoke("throw", err, resolve, reject);
            });
          }

          return PromiseImpl.resolve(value).then(function (unwrapped) {
            // When a yielded Promise is resolved, its final value becomes
            // the .value of the Promise<{value,done}> result for the
            // current iteration.
            result.value = unwrapped;
            resolve(result);
          }, function (error) {
            // If a rejected Promise was yielded, throw the rejection back
            // into the async generator function so it can be handled there.
            return invoke("throw", error, resolve, reject);
          });
        }
      }

      var previousPromise;

      function enqueue(method, arg) {
        function callInvokeWithMethodAndArg() {
          return new PromiseImpl(function (resolve, reject) {
            invoke(method, arg, resolve, reject);
          });
        }

        return previousPromise = // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, // Avoid propagating failures to Promises returned by later
        // invocations of the iterator.
        callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
      } // Define the unified helper method that is used to implement .next,
      // .throw, and .return (see defineIteratorMethods).


      this._invoke = enqueue;
    }

    defineIteratorMethods(AsyncIterator.prototype);
    define(AsyncIterator.prototype, asyncIteratorSymbol, function () {
      return this;
    });
    exports.AsyncIterator = AsyncIterator; // Note that simple async functions are implemented on top of
    // AsyncIterator objects; they just return a Promise for the value of
    // the final result produced by the iterator.

    exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) {
      if (PromiseImpl === void 0) PromiseImpl = Promise;
      var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl);
      return exports.isGeneratorFunction(outerFn) ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function (result) {
        return result.done ? result.value : iter.next();
      });
    };

    function makeInvokeMethod(innerFn, self, context) {
      var state = GenStateSuspendedStart;
      return function invoke(method, arg) {
        if (state === GenStateExecuting) {
          throw new Error("Generator is already running");
        }

        if (state === GenStateCompleted) {
          if (method === "throw") {
            throw arg;
          } // Be forgiving, per 25.3.3.3.3 of the spec:
          // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume


          return doneResult();
        }

        context.method = method;
        context.arg = arg;

        while (true) {
          var delegate = context.delegate;

          if (delegate) {
            var delegateResult = maybeInvokeDelegate(delegate, context);

            if (delegateResult) {
              if (delegateResult === ContinueSentinel) continue;
              return delegateResult;
            }
          }

          if (context.method === "next") {
            // Setting context._sent for legacy support of Babel's
            // function.sent implementation.
            context.sent = context._sent = context.arg;
          } else if (context.method === "throw") {
            if (state === GenStateSuspendedStart) {
              state = GenStateCompleted;
              throw context.arg;
            }

            context.dispatchException(context.arg);
          } else if (context.method === "return") {
            context.abrupt("return", context.arg);
          }

          state = GenStateExecuting;
          var record = tryCatch(innerFn, self, context);

          if (record.type === "normal") {
            // If an exception is thrown from innerFn, we leave state ===
            // GenStateExecuting and loop back for another invocation.
            state = context.done ? GenStateCompleted : GenStateSuspendedYield;

            if (record.arg === ContinueSentinel) {
              continue;
            }

            return {
              value: record.arg,
              done: context.done
            };
          } else if (record.type === "throw") {
            state = GenStateCompleted; // Dispatch the exception by looping back around to the
            // context.dispatchException(context.arg) call above.

            context.method = "throw";
            context.arg = record.arg;
          }
        }
      };
    } // Call delegate.iterator[context.method](context.arg) and handle the
    // result, either by returning a { value, done } result from the
    // delegate iterator, or by modifying context.method and context.arg,
    // setting context.delegate to null, and returning the ContinueSentinel.


    function maybeInvokeDelegate(delegate, context) {
      var method = delegate.iterator[context.method];

      if (method === undefined$1) {
        // A .throw or .return when the delegate iterator has no .throw
        // method always terminates the yield* loop.
        context.delegate = null;

        if (context.method === "throw") {
          // Note: ["return"] must be used for ES3 parsing compatibility.
          if (delegate.iterator["return"]) {
            // If the delegate iterator has a return method, give it a
            // chance to clean up.
            context.method = "return";
            context.arg = undefined$1;
            maybeInvokeDelegate(delegate, context);

            if (context.method === "throw") {
              // If maybeInvokeDelegate(context) changed context.method from
              // "return" to "throw", let that override the TypeError below.
              return ContinueSentinel;
            }
          }

          context.method = "throw";
          context.arg = new TypeError("The iterator does not provide a 'throw' method");
        }

        return ContinueSentinel;
      }

      var record = tryCatch(method, delegate.iterator, context.arg);

      if (record.type === "throw") {
        context.method = "throw";
        context.arg = record.arg;
        context.delegate = null;
        return ContinueSentinel;
      }

      var info = record.arg;

      if (!info) {
        context.method = "throw";
        context.arg = new TypeError("iterator result is not an object");
        context.delegate = null;
        return ContinueSentinel;
      }

      if (info.done) {
        // Assign the result of the finished delegate to the temporary
        // variable specified by delegate.resultName (see delegateYield).
        context[delegate.resultName] = info.value; // Resume execution at the desired location (see delegateYield).

        context.next = delegate.nextLoc; // If context.method was "throw" but the delegate handled the
        // exception, let the outer generator proceed normally. If
        // context.method was "next", forget context.arg since it has been
        // "consumed" by the delegate iterator. If context.method was
        // "return", allow the original .return call to continue in the
        // outer generator.

        if (context.method !== "return") {
          context.method = "next";
          context.arg = undefined$1;
        }
      } else {
        // Re-yield the result returned by the delegate method.
        return info;
      } // The delegate iterator is finished, so forget it and continue with
      // the outer generator.


      context.delegate = null;
      return ContinueSentinel;
    } // Define Generator.prototype.{next,throw,return} in terms of the
    // unified ._invoke helper method.


    defineIteratorMethods(Gp);
    define(Gp, toStringTagSymbol, "Generator"); // A Generator should always return itself as the iterator object when the
    // @@iterator function is called on it. Some browsers' implementations of the
    // iterator prototype chain incorrectly implement this, causing the Generator
    // object to not be returned from this call. This ensures that doesn't happen.
    // See https://github.com/facebook/regenerator/issues/274 for more details.

    define(Gp, iteratorSymbol, function () {
      return this;
    });
    define(Gp, "toString", function () {
      return "[object Generator]";
    });

    function pushTryEntry(locs) {
      var entry = {
        tryLoc: locs[0]
      };

      if (1 in locs) {
        entry.catchLoc = locs[1];
      }

      if (2 in locs) {
        entry.finallyLoc = locs[2];
        entry.afterLoc = locs[3];
      }

      this.tryEntries.push(entry);
    }

    function resetTryEntry(entry) {
      var record = entry.completion || {};
      record.type = "normal";
      delete record.arg;
      entry.completion = record;
    }

    function Context(tryLocsList) {
      // The root entry object (effectively a try statement without a catch
      // or a finally block) gives us a place to store values thrown from
      // locations where there is no enclosing try statement.
      this.tryEntries = [{
        tryLoc: "root"
      }];
      tryLocsList.forEach(pushTryEntry, this);
      this.reset(true);
    }

    exports.keys = function (object) {
      var keys = [];

      for (var key in object) {
        keys.push(key);
      }

      keys.reverse(); // Rather than returning an object with a next method, we keep
      // things simple and return the next function itself.

      return function next() {
        while (keys.length) {
          var key = keys.pop();

          if (key in object) {
            next.value = key;
            next.done = false;
            return next;
          }
        } // To avoid creating an additional object, we just hang the .value
        // and .done properties off the next function object itself. This
        // also ensures that the minifier will not anonymize the function.


        next.done = true;
        return next;
      };
    };

    function values(iterable) {
      if (iterable) {
        var iteratorMethod = iterable[iteratorSymbol];

        if (iteratorMethod) {
          return iteratorMethod.call(iterable);
        }

        if (typeof iterable.next === "function") {
          return iterable;
        }

        if (!isNaN(iterable.length)) {
          var i = -1,
              next = function next() {
            while (++i < iterable.length) {
              if (hasOwn.call(iterable, i)) {
                next.value = iterable[i];
                next.done = false;
                return next;
              }
            }

            next.value = undefined$1;
            next.done = true;
            return next;
          };

          return next.next = next;
        }
      } // Return an iterator with no values.


      return {
        next: doneResult
      };
    }

    exports.values = values;

    function doneResult() {
      return {
        value: undefined$1,
        done: true
      };
    }

    Context.prototype = {
      constructor: Context,
      reset: function (skipTempReset) {
        this.prev = 0;
        this.next = 0; // Resetting context._sent for legacy support of Babel's
        // function.sent implementation.

        this.sent = this._sent = undefined$1;
        this.done = false;
        this.delegate = null;
        this.method = "next";
        this.arg = undefined$1;
        this.tryEntries.forEach(resetTryEntry);

        if (!skipTempReset) {
          for (var name in this) {
            // Not sure about the optimal order of these conditions:
            if (name.charAt(0) === "t" && hasOwn.call(this, name) && !isNaN(+name.slice(1))) {
              this[name] = undefined$1;
            }
          }
        }
      },
      stop: function () {
        this.done = true;
        var rootEntry = this.tryEntries[0];
        var rootRecord = rootEntry.completion;

        if (rootRecord.type === "throw") {
          throw rootRecord.arg;
        }

        return this.rval;
      },
      dispatchException: function (exception) {
        if (this.done) {
          throw exception;
        }

        var context = this;

        function handle(loc, caught) {
          record.type = "throw";
          record.arg = exception;
          context.next = loc;

          if (caught) {
            // If the dispatched exception was caught by a catch block,
            // then let that catch block handle the exception normally.
            context.method = "next";
            context.arg = undefined$1;
          }

          return !!caught;
        }

        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
          var record = entry.completion;

          if (entry.tryLoc === "root") {
            // Exception thrown outside of any try block that could handle
            // it, so set the completion value of the entire function to
            // throw the exception.
            return handle("end");
          }

          if (entry.tryLoc <= this.prev) {
            var hasCatch = hasOwn.call(entry, "catchLoc");
            var hasFinally = hasOwn.call(entry, "finallyLoc");

            if (hasCatch && hasFinally) {
              if (this.prev < entry.catchLoc) {
                return handle(entry.catchLoc, true);
              } else if (this.prev < entry.finallyLoc) {
                return handle(entry.finallyLoc);
              }
            } else if (hasCatch) {
              if (this.prev < entry.catchLoc) {
                return handle(entry.catchLoc, true);
              }
            } else if (hasFinally) {
              if (this.prev < entry.finallyLoc) {
                return handle(entry.finallyLoc);
              }
            } else {
              throw new Error("try statement without catch or finally");
            }
          }
        }
      },
      abrupt: function (type, arg) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];

          if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) {
            var finallyEntry = entry;
            break;
          }
        }

        if (finallyEntry && (type === "break" || type === "continue") && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc) {
          // Ignore the finally entry if control is not jumping to a
          // location outside the try/catch block.
          finallyEntry = null;
        }

        var record = finallyEntry ? finallyEntry.completion : {};
        record.type = type;
        record.arg = arg;

        if (finallyEntry) {
          this.method = "next";
          this.next = finallyEntry.finallyLoc;
          return ContinueSentinel;
        }

        return this.complete(record);
      },
      complete: function (record, afterLoc) {
        if (record.type === "throw") {
          throw record.arg;
        }

        if (record.type === "break" || record.type === "continue") {
          this.next = record.arg;
        } else if (record.type === "return") {
          this.rval = this.arg = record.arg;
          this.method = "return";
          this.next = "end";
        } else if (record.type === "normal" && afterLoc) {
          this.next = afterLoc;
        }

        return ContinueSentinel;
      },
      finish: function (finallyLoc) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];

          if (entry.finallyLoc === finallyLoc) {
            this.complete(entry.completion, entry.afterLoc);
            resetTryEntry(entry);
            return ContinueSentinel;
          }
        }
      },
      "catch": function (tryLoc) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];

          if (entry.tryLoc === tryLoc) {
            var record = entry.completion;

            if (record.type === "throw") {
              var thrown = record.arg;
              resetTryEntry(entry);
            }

            return thrown;
          }
        } // The context.catch method must only be called with a location
        // argument that corresponds to a known catch block.


        throw new Error("illegal catch attempt");
      },
      delegateYield: function (iterable, resultName, nextLoc) {
        this.delegate = {
          iterator: values(iterable),
          resultName: resultName,
          nextLoc: nextLoc
        };

        if (this.method === "next") {
          // Deliberately forget the last sent value so that we don't
          // accidentally pass it on to the delegate.
          this.arg = undefined$1;
        }

        return ContinueSentinel;
      }
    }; // Regardless of whether this script is executing as a CommonJS module
    // or not, return the runtime object so that we can declare the variable
    // regeneratorRuntime in the outer scope, which allows this module to be
    // injected easily by `bin/regenerator --include-runtime script.js`.

    return exports;
  }( // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
  module.exports );

  try {
    regeneratorRuntime = runtime;
  } catch (accidentalStrictMode) {
    // This module should not be running in strict mode, so the above
    // assignment should always work unless something is misconfigured. Just
    // in case runtime.js accidentally runs in strict mode, in modern engines
    // we can explicitly access globalThis. In older engines we can escape
    // strict mode using a global Function call. This could conceivably fail
    // if a Content Security Policy forbids using Function, but in that case
    // the proper solution is to fix the accidental strict mode problem. If
    // you've misconfigured your bundler to force strict mode and applied a
    // CSP to forbid Function, and you're not willing to fix either of those
    // problems, please detail your unique predicament in a GitHub issue.
    if (typeof globalThis === "object") {
      globalThis.regeneratorRuntime = runtime;
    } else {
      Function("r", "regeneratorRuntime = r")(runtime);
    }
  }
  });

  var check = function (it) {
    return it && it.Math == Math && it;
  }; // https://github.com/zloirock/core-js/issues/86#issuecomment-115759028


  var global$1 = // eslint-disable-next-line es/no-global-this -- safe
  check(typeof globalThis == 'object' && globalThis) || check(typeof window == 'object' && window) || // eslint-disable-next-line no-restricted-globals -- safe
  check(typeof self == 'object' && self) || check(typeof commonjsGlobal == 'object' && commonjsGlobal) || // eslint-disable-next-line no-new-func -- fallback
  function () {
    return this;
  }() || Function('return this')();

  var fails = function (exec) {
    try {
      return !!exec();
    } catch (error) {
      return true;
    }
  };

  // Detect IE8's incomplete defineProperty implementation


  var descriptors = !fails(function () {
    // eslint-disable-next-line es/no-object-defineproperty -- required for testing
    return Object.defineProperty({}, 1, {
      get: function () {
        return 7;
      }
    })[1] != 7;
  });

  var $propertyIsEnumerable = {}.propertyIsEnumerable; // eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe

  var getOwnPropertyDescriptor$2 = Object.getOwnPropertyDescriptor; // Nashorn ~ JDK8 bug

  var NASHORN_BUG = getOwnPropertyDescriptor$2 && !$propertyIsEnumerable.call({
    1: 2
  }, 1); // `Object.prototype.propertyIsEnumerable` method implementation
  // https://tc39.es/ecma262/#sec-object.prototype.propertyisenumerable

  var f$4 = NASHORN_BUG ? function propertyIsEnumerable(V) {
    var descriptor = getOwnPropertyDescriptor$2(this, V);
    return !!descriptor && descriptor.enumerable;
  } : $propertyIsEnumerable;

  var objectPropertyIsEnumerable = {
  	f: f$4
  };

  var createPropertyDescriptor = function (bitmap, value) {
    return {
      enumerable: !(bitmap & 1),
      configurable: !(bitmap & 2),
      writable: !(bitmap & 4),
      value: value
    };
  };

  var toString$3 = {}.toString;

  var classofRaw = function (it) {
    return toString$3.call(it).slice(8, -1);
  };

  var split = ''.split; // fallback for non-array-like ES3 and non-enumerable old V8 strings

  var indexedObject = fails(function () {
    // throws an error in rhino, see https://github.com/mozilla/rhino/issues/346
    // eslint-disable-next-line no-prototype-builtins -- safe
    return !Object('z').propertyIsEnumerable(0);
  }) ? function (it) {
    return classofRaw(it) == 'String' ? split.call(it, '') : Object(it);
  } : Object;

  // `RequireObjectCoercible` abstract operation
  // https://tc39.es/ecma262/#sec-requireobjectcoercible
  var requireObjectCoercible = function (it) {
    if (it == undefined) throw TypeError("Can't call method on " + it);
    return it;
  };

  // toObject with fallback for non-array-like ES3 strings




  var toIndexedObject = function (it) {
    return indexedObject(requireObjectCoercible(it));
  };

  var isObject$2 = function (it) {
    return typeof it === 'object' ? it !== null : typeof it === 'function';
  };

  var aFunction$1 = function (variable) {
    return typeof variable == 'function' ? variable : undefined;
  };

  var getBuiltIn = function (namespace, method) {
    return arguments.length < 2 ? aFunction$1(global$1[namespace]) : global$1[namespace] && global$1[namespace][method];
  };

  var engineUserAgent = getBuiltIn('navigator', 'userAgent') || '';

  var process$1 = global$1.process;
  var Deno = global$1.Deno;
  var versions = process$1 && process$1.versions || Deno && Deno.version;
  var v8 = versions && versions.v8;
  var match$1, version;

  if (v8) {
    match$1 = v8.split('.');
    version = match$1[0] < 4 ? 1 : match$1[0] + match$1[1];
  } else if (engineUserAgent) {
    match$1 = engineUserAgent.match(/Edge\/(\d+)/);

    if (!match$1 || match$1[1] >= 74) {
      match$1 = engineUserAgent.match(/Chrome\/(\d+)/);
      if (match$1) version = match$1[1];
    }
  }

  var engineV8Version = version && +version;

  /* eslint-disable es/no-symbol -- required for testing */

  // eslint-disable-next-line es/no-object-getownpropertysymbols -- required for testing


  var nativeSymbol = !!Object.getOwnPropertySymbols && !fails(function () {
    var symbol = Symbol(); // Chrome 38 Symbol has incorrect toString conversion
    // `get-own-property-symbols` polyfill symbols converted to object are not Symbol instances

    return !String(symbol) || !(Object(symbol) instanceof Symbol) || // Chrome 38-40 symbols are not inherited from DOM collections prototypes to instances
    !Symbol.sham && engineV8Version && engineV8Version < 41;
  });

  /* eslint-disable es/no-symbol -- required for testing */

  var useSymbolAsUid = nativeSymbol && !Symbol.sham && typeof Symbol.iterator == 'symbol';

  var isSymbol$1 = useSymbolAsUid ? function (it) {
    return typeof it == 'symbol';
  } : function (it) {
    var $Symbol = getBuiltIn('Symbol');
    return typeof $Symbol == 'function' && Object(it) instanceof $Symbol;
  };

  // `OrdinaryToPrimitive` abstract operation
  // https://tc39.es/ecma262/#sec-ordinarytoprimitive


  var ordinaryToPrimitive = function (input, pref) {
    var fn, val;
    if (pref === 'string' && typeof (fn = input.toString) == 'function' && !isObject$2(val = fn.call(input))) return val;
    if (typeof (fn = input.valueOf) == 'function' && !isObject$2(val = fn.call(input))) return val;
    if (pref !== 'string' && typeof (fn = input.toString) == 'function' && !isObject$2(val = fn.call(input))) return val;
    throw TypeError("Can't convert object to primitive value");
  };

  var setGlobal = function (key, value) {
    try {
      // eslint-disable-next-line es/no-object-defineproperty -- safe
      Object.defineProperty(global$1, key, {
        value: value,
        configurable: true,
        writable: true
      });
    } catch (error) {
      global$1[key] = value;
    }

    return value;
  };

  var SHARED = '__core-js_shared__';
  var store$1 = global$1[SHARED] || setGlobal(SHARED, {});
  var sharedStore = store$1;

  var shared = createCommonjsModule(function (module) {
  (module.exports = function (key, value) {
    return sharedStore[key] || (sharedStore[key] = value !== undefined ? value : {});
  })('versions', []).push({
    version: '3.16.4',
    mode: 'global',
    copyright: '© 2021 Denis Pushkarev (zloirock.ru)'
  });
  });

  // `ToObject` abstract operation
  // https://tc39.es/ecma262/#sec-toobject


  var toObject$1 = function (argument) {
    return Object(requireObjectCoercible(argument));
  };

  var hasOwnProperty$1 = {}.hasOwnProperty;

  var has$8 = Object.hasOwn || function hasOwn(it, key) {
    return hasOwnProperty$1.call(toObject$1(it), key);
  };

  var id = 0;
  var postfix = Math.random();

  var uid = function (key) {
    return 'Symbol(' + String(key === undefined ? '' : key) + ')_' + (++id + postfix).toString(36);
  };

  var WellKnownSymbolsStore = shared('wks');
  var Symbol$1 = global$1.Symbol;
  var createWellKnownSymbol = useSymbolAsUid ? Symbol$1 : Symbol$1 && Symbol$1.withoutSetter || uid;

  var wellKnownSymbol = function (name) {
    if (!has$8(WellKnownSymbolsStore, name) || !(nativeSymbol || typeof WellKnownSymbolsStore[name] == 'string')) {
      if (nativeSymbol && has$8(Symbol$1, name)) {
        WellKnownSymbolsStore[name] = Symbol$1[name];
      } else {
        WellKnownSymbolsStore[name] = createWellKnownSymbol('Symbol.' + name);
      }
    }

    return WellKnownSymbolsStore[name];
  };

  var TO_PRIMITIVE = wellKnownSymbol('toPrimitive'); // `ToPrimitive` abstract operation
  // https://tc39.es/ecma262/#sec-toprimitive

  var toPrimitive = function (input, pref) {
    if (!isObject$2(input) || isSymbol$1(input)) return input;
    var exoticToPrim = input[TO_PRIMITIVE];
    var result;

    if (exoticToPrim !== undefined) {
      if (pref === undefined) pref = 'default';
      result = exoticToPrim.call(input, pref);
      if (!isObject$2(result) || isSymbol$1(result)) return result;
      throw TypeError("Can't convert object to primitive value");
    }

    if (pref === undefined) pref = 'number';
    return ordinaryToPrimitive(input, pref);
  };

  // `ToPropertyKey` abstract operation
  // https://tc39.es/ecma262/#sec-topropertykey


  var toPropertyKey = function (argument) {
    var key = toPrimitive(argument, 'string');
    return isSymbol$1(key) ? key : String(key);
  };

  var document$1 = global$1.document; // typeof document.createElement is 'object' in old IE

  var EXISTS = isObject$2(document$1) && isObject$2(document$1.createElement);

  var documentCreateElement = function (it) {
    return EXISTS ? document$1.createElement(it) : {};
  };

  // Thank's IE8 for his funny defineProperty


  var ie8DomDefine = !descriptors && !fails(function () {
    // eslint-disable-next-line es/no-object-defineproperty -- requied for testing
    return Object.defineProperty(documentCreateElement('div'), 'a', {
      get: function () {
        return 7;
      }
    }).a != 7;
  });

  // eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe


  var $getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor; // `Object.getOwnPropertyDescriptor` method
  // https://tc39.es/ecma262/#sec-object.getownpropertydescriptor

  var f$3 = descriptors ? $getOwnPropertyDescriptor : function getOwnPropertyDescriptor(O, P) {
    O = toIndexedObject(O);
    P = toPropertyKey(P);
    if (ie8DomDefine) try {
      return $getOwnPropertyDescriptor(O, P);
    } catch (error) {
      /* empty */
    }
    if (has$8(O, P)) return createPropertyDescriptor(!objectPropertyIsEnumerable.f.call(O, P), O[P]);
  };

  var objectGetOwnPropertyDescriptor = {
  	f: f$3
  };

  var anObject = function (it) {
    if (!isObject$2(it)) {
      throw TypeError(String(it) + ' is not an object');
    }

    return it;
  };

  // eslint-disable-next-line es/no-object-defineproperty -- safe


  var $defineProperty = Object.defineProperty; // `Object.defineProperty` method
  // https://tc39.es/ecma262/#sec-object.defineproperty

  var f$2 = descriptors ? $defineProperty : function defineProperty(O, P, Attributes) {
    anObject(O);
    P = toPropertyKey(P);
    anObject(Attributes);
    if (ie8DomDefine) try {
      return $defineProperty(O, P, Attributes);
    } catch (error) {
      /* empty */
    }
    if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported');
    if ('value' in Attributes) O[P] = Attributes.value;
    return O;
  };

  var objectDefineProperty = {
  	f: f$2
  };

  var createNonEnumerableProperty = descriptors ? function (object, key, value) {
    return objectDefineProperty.f(object, key, createPropertyDescriptor(1, value));
  } : function (object, key, value) {
    object[key] = value;
    return object;
  };

  var functionToString$1 = Function.toString; // this helper broken in `core-js@3.4.1-3.4.4`, so we can't use `shared` helper

  if (typeof sharedStore.inspectSource != 'function') {
    sharedStore.inspectSource = function (it) {
      return functionToString$1.call(it);
    };
  }

  var inspectSource = sharedStore.inspectSource;

  var WeakMap$2 = global$1.WeakMap;
  var nativeWeakMap = typeof WeakMap$2 === 'function' && /native code/.test(inspectSource(WeakMap$2));

  var keys$1 = shared('keys');

  var sharedKey = function (key) {
    return keys$1[key] || (keys$1[key] = uid(key));
  };

  var hiddenKeys$1 = {};

  var OBJECT_ALREADY_INITIALIZED = 'Object already initialized';
  var WeakMap$1 = global$1.WeakMap;
  var set$2, get$1, has$7;

  var enforce = function (it) {
    return has$7(it) ? get$1(it) : set$2(it, {});
  };

  var getterFor = function (TYPE) {
    return function (it) {
      var state;

      if (!isObject$2(it) || (state = get$1(it)).type !== TYPE) {
        throw TypeError('Incompatible receiver, ' + TYPE + ' required');
      }

      return state;
    };
  };

  if (nativeWeakMap || sharedStore.state) {
    var store = sharedStore.state || (sharedStore.state = new WeakMap$1());
    var wmget = store.get;
    var wmhas = store.has;
    var wmset = store.set;

    set$2 = function (it, metadata) {
      if (wmhas.call(store, it)) throw new TypeError(OBJECT_ALREADY_INITIALIZED);
      metadata.facade = it;
      wmset.call(store, it, metadata);
      return metadata;
    };

    get$1 = function (it) {
      return wmget.call(store, it) || {};
    };

    has$7 = function (it) {
      return wmhas.call(store, it);
    };
  } else {
    var STATE = sharedKey('state');
    hiddenKeys$1[STATE] = true;

    set$2 = function (it, metadata) {
      if (has$8(it, STATE)) throw new TypeError(OBJECT_ALREADY_INITIALIZED);
      metadata.facade = it;
      createNonEnumerableProperty(it, STATE, metadata);
      return metadata;
    };

    get$1 = function (it) {
      return has$8(it, STATE) ? it[STATE] : {};
    };

    has$7 = function (it) {
      return has$8(it, STATE);
    };
  }

  var internalState = {
    set: set$2,
    get: get$1,
    has: has$7,
    enforce: enforce,
    getterFor: getterFor
  };

  var redefine = createCommonjsModule(function (module) {
  var getInternalState = internalState.get;
  var enforceInternalState = internalState.enforce;
  var TEMPLATE = String(String).split('String');
  (module.exports = function (O, key, value, options) {
    var unsafe = options ? !!options.unsafe : false;
    var simple = options ? !!options.enumerable : false;
    var noTargetGet = options ? !!options.noTargetGet : false;
    var state;

    if (typeof value == 'function') {
      if (typeof key == 'string' && !has$8(value, 'name')) {
        createNonEnumerableProperty(value, 'name', key);
      }

      state = enforceInternalState(value);

      if (!state.source) {
        state.source = TEMPLATE.join(typeof key == 'string' ? key : '');
      }
    }

    if (O === global$1) {
      if (simple) O[key] = value;else setGlobal(key, value);
      return;
    } else if (!unsafe) {
      delete O[key];
    } else if (!noTargetGet && O[key]) {
      simple = true;
    }

    if (simple) O[key] = value;else createNonEnumerableProperty(O, key, value); // add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
  })(Function.prototype, 'toString', function toString() {
    return typeof this == 'function' && getInternalState(this).source || inspectSource(this);
  });
  });

  var ceil = Math.ceil;
  var floor$1 = Math.floor; // `ToInteger` abstract operation
  // https://tc39.es/ecma262/#sec-tointeger

  var toInteger = function (argument) {
    return isNaN(argument = +argument) ? 0 : (argument > 0 ? floor$1 : ceil)(argument);
  };

  var min$2 = Math.min; // `ToLength` abstract operation
  // https://tc39.es/ecma262/#sec-tolength

  var toLength = function (argument) {
    return argument > 0 ? min$2(toInteger(argument), 0x1FFFFFFFFFFFFF) : 0; // 2 ** 53 - 1 == 9007199254740991
  };

  var max = Math.max;
  var min$1 = Math.min; // Helper for a popular repeating case of the spec:
  // Let integer be ? ToInteger(index).
  // If integer < 0, let result be max((length + integer), 0); else let result be min(integer, length).

  var toAbsoluteIndex = function (index, length) {
    var integer = toInteger(index);
    return integer < 0 ? max(integer + length, 0) : min$1(integer, length);
  };

  // `Array.prototype.{ indexOf, includes }` methods implementation


  var createMethod$4 = function (IS_INCLUDES) {
    return function ($this, el, fromIndex) {
      var O = toIndexedObject($this);
      var length = toLength(O.length);
      var index = toAbsoluteIndex(fromIndex, length);
      var value; // Array#includes uses SameValueZero equality algorithm
      // eslint-disable-next-line no-self-compare -- NaN check

      if (IS_INCLUDES && el != el) while (length > index) {
        value = O[index++]; // eslint-disable-next-line no-self-compare -- NaN check

        if (value != value) return true; // Array#indexOf ignores holes, Array#includes - not
      } else for (; length > index; index++) {
        if ((IS_INCLUDES || index in O) && O[index] === el) return IS_INCLUDES || index || 0;
      }
      return !IS_INCLUDES && -1;
    };
  };

  var arrayIncludes = {
    // `Array.prototype.includes` method
    // https://tc39.es/ecma262/#sec-array.prototype.includes
    includes: createMethod$4(true),
    // `Array.prototype.indexOf` method
    // https://tc39.es/ecma262/#sec-array.prototype.indexof
    indexOf: createMethod$4(false)
  };

  var indexOf$1 = arrayIncludes.indexOf;



  var objectKeysInternal = function (object, names) {
    var O = toIndexedObject(object);
    var i = 0;
    var result = [];
    var key;

    for (key in O) !has$8(hiddenKeys$1, key) && has$8(O, key) && result.push(key); // Don't enum bug & hidden keys


    while (names.length > i) if (has$8(O, key = names[i++])) {
      ~indexOf$1(result, key) || result.push(key);
    }

    return result;
  };

  // IE8- don't enum bug keys
  var enumBugKeys = ['constructor', 'hasOwnProperty', 'isPrototypeOf', 'propertyIsEnumerable', 'toLocaleString', 'toString', 'valueOf'];

  var hiddenKeys = enumBugKeys.concat('length', 'prototype'); // `Object.getOwnPropertyNames` method
  // https://tc39.es/ecma262/#sec-object.getownpropertynames
  // eslint-disable-next-line es/no-object-getownpropertynames -- safe

  var f$1 = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
    return objectKeysInternal(O, hiddenKeys);
  };

  var objectGetOwnPropertyNames = {
  	f: f$1
  };

  // eslint-disable-next-line es/no-object-getownpropertysymbols -- safe
  var f = Object.getOwnPropertySymbols;

  var objectGetOwnPropertySymbols = {
  	f: f
  };

  // all object keys, includes non-enumerable and symbols


  var ownKeys$2 = getBuiltIn('Reflect', 'ownKeys') || function ownKeys(it) {
    var keys = objectGetOwnPropertyNames.f(anObject(it));
    var getOwnPropertySymbols = objectGetOwnPropertySymbols.f;
    return getOwnPropertySymbols ? keys.concat(getOwnPropertySymbols(it)) : keys;
  };

  var copyConstructorProperties = function (target, source) {
    var keys = ownKeys$2(source);
    var defineProperty = objectDefineProperty.f;
    var getOwnPropertyDescriptor = objectGetOwnPropertyDescriptor.f;

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      if (!has$8(target, key)) defineProperty(target, key, getOwnPropertyDescriptor(source, key));
    }
  };

  var replacement = /#|\.prototype\./;

  var isForced = function (feature, detection) {
    var value = data[normalize(feature)];
    return value == POLYFILL ? true : value == NATIVE ? false : typeof detection == 'function' ? fails(detection) : !!detection;
  };

  var normalize = isForced.normalize = function (string) {
    return String(string).replace(replacement, '.').toLowerCase();
  };

  var data = isForced.data = {};
  var NATIVE = isForced.NATIVE = 'N';
  var POLYFILL = isForced.POLYFILL = 'P';
  var isForced_1 = isForced;

  var getOwnPropertyDescriptor$1 = objectGetOwnPropertyDescriptor.f;










  /*
    options.target      - name of the target object
    options.global      - target is the global object
    options.stat        - export as static methods of target
    options.proto       - export as prototype methods of target
    options.real        - real prototype method for the `pure` version
    options.forced      - export even if the native feature is available
    options.bind        - bind methods to the target, required for the `pure` version
    options.wrap        - wrap constructors to preventing global pollution, required for the `pure` version
    options.unsafe      - use the simple assignment of property instead of delete + defineProperty
    options.sham        - add a flag to not completely full polyfills
    options.enumerable  - export as enumerable property
    options.noTargetGet - prevent calling a getter on target
  */


  var _export = function (options, source) {
    var TARGET = options.target;
    var GLOBAL = options.global;
    var STATIC = options.stat;
    var FORCED, target, key, targetProperty, sourceProperty, descriptor;

    if (GLOBAL) {
      target = global$1;
    } else if (STATIC) {
      target = global$1[TARGET] || setGlobal(TARGET, {});
    } else {
      target = (global$1[TARGET] || {}).prototype;
    }

    if (target) for (key in source) {
      sourceProperty = source[key];

      if (options.noTargetGet) {
        descriptor = getOwnPropertyDescriptor$1(target, key);
        targetProperty = descriptor && descriptor.value;
      } else targetProperty = target[key];

      FORCED = isForced_1(GLOBAL ? key : TARGET + (STATIC ? '.' : '#') + key, options.forced); // contained in target

      if (!FORCED && targetProperty !== undefined) {
        if (typeof sourceProperty === typeof targetProperty) continue;
        copyConstructorProperties(sourceProperty, targetProperty);
      } // add a flag to not completely full polyfills


      if (options.sham || targetProperty && targetProperty.sham) {
        createNonEnumerableProperty(sourceProperty, 'sham', true);
      } // extend global


      redefine(target, key, sourceProperty, options);
    }
  };

  // `IsArray` abstract operation
  // https://tc39.es/ecma262/#sec-isarray
  // eslint-disable-next-line es/no-array-isarray -- safe


  var isArray$5 = Array.isArray || function isArray(arg) {
    return classofRaw(arg) == 'Array';
  };

  var createProperty = function (object, key, value) {
    var propertyKey = toPropertyKey(key);
    if (propertyKey in object) objectDefineProperty.f(object, propertyKey, createPropertyDescriptor(0, value));else object[propertyKey] = value;
  };

  var SPECIES$3 = wellKnownSymbol('species'); // a part of `ArraySpeciesCreate` abstract operation
  // https://tc39.es/ecma262/#sec-arrayspeciescreate

  var arraySpeciesConstructor = function (originalArray) {
    var C;

    if (isArray$5(originalArray)) {
      C = originalArray.constructor; // cross-realm fallback

      if (typeof C == 'function' && (C === Array || isArray$5(C.prototype))) C = undefined;else if (isObject$2(C)) {
        C = C[SPECIES$3];
        if (C === null) C = undefined;
      }
    }

    return C === undefined ? Array : C;
  };

  // `ArraySpeciesCreate` abstract operation
  // https://tc39.es/ecma262/#sec-arrayspeciescreate


  var arraySpeciesCreate = function (originalArray, length) {
    return new (arraySpeciesConstructor(originalArray))(length === 0 ? 0 : length);
  };

  var SPECIES$2 = wellKnownSymbol('species');

  var arrayMethodHasSpeciesSupport = function (METHOD_NAME) {
    // We can't use this feature detection in V8 since it causes
    // deoptimization and serious performance degradation
    // https://github.com/zloirock/core-js/issues/677
    return engineV8Version >= 51 || !fails(function () {
      var array = [];
      var constructor = array.constructor = {};

      constructor[SPECIES$2] = function () {
        return {
          foo: 1
        };
      };

      return array[METHOD_NAME](Boolean).foo !== 1;
    });
  };

  var IS_CONCAT_SPREADABLE = wellKnownSymbol('isConcatSpreadable');
  var MAX_SAFE_INTEGER = 0x1FFFFFFFFFFFFF;
  var MAXIMUM_ALLOWED_INDEX_EXCEEDED = 'Maximum allowed index exceeded'; // We can't use this feature detection in V8 since it causes
  // deoptimization and serious performance degradation
  // https://github.com/zloirock/core-js/issues/679

  var IS_CONCAT_SPREADABLE_SUPPORT = engineV8Version >= 51 || !fails(function () {
    var array = [];
    array[IS_CONCAT_SPREADABLE] = false;
    return array.concat()[0] !== array;
  });
  var SPECIES_SUPPORT = arrayMethodHasSpeciesSupport('concat');

  var isConcatSpreadable = function (O) {
    if (!isObject$2(O)) return false;
    var spreadable = O[IS_CONCAT_SPREADABLE];
    return spreadable !== undefined ? !!spreadable : isArray$5(O);
  };

  var FORCED$1 = !IS_CONCAT_SPREADABLE_SUPPORT || !SPECIES_SUPPORT; // `Array.prototype.concat` method
  // https://tc39.es/ecma262/#sec-array.prototype.concat
  // with adding support of @@isConcatSpreadable and @@species

  _export({
    target: 'Array',
    proto: true,
    forced: FORCED$1
  }, {
    // eslint-disable-next-line no-unused-vars -- required for `.length`
    concat: function concat(arg) {
      var O = toObject$1(this);
      var A = arraySpeciesCreate(O, 0);
      var n = 0;
      var i, k, length, len, E;

      for (i = -1, length = arguments.length; i < length; i++) {
        E = i === -1 ? O : arguments[i];

        if (isConcatSpreadable(E)) {
          len = toLength(E.length);
          if (n + len > MAX_SAFE_INTEGER) throw TypeError(MAXIMUM_ALLOWED_INDEX_EXCEEDED);

          for (k = 0; k < len; k++, n++) if (k in E) createProperty(A, n, E[k]);
        } else {
          if (n >= MAX_SAFE_INTEGER) throw TypeError(MAXIMUM_ALLOWED_INDEX_EXCEEDED);
          createProperty(A, n++, E);
        }
      }

      A.length = n;
      return A;
    }
  });

  var toString$2 = function (argument) {
    if (isSymbol$1(argument)) throw TypeError('Cannot convert a Symbol value to a string');
    return String(argument);
  };

  // `RegExp.prototype.flags` getter implementation
  // https://tc39.es/ecma262/#sec-get-regexp.prototype.flags


  var regexpFlags = function () {
    var that = anObject(this);
    var result = '';
    if (that.global) result += 'g';
    if (that.ignoreCase) result += 'i';
    if (that.multiline) result += 'm';
    if (that.dotAll) result += 's';
    if (that.unicode) result += 'u';
    if (that.sticky) result += 'y';
    return result;
  };

  // babel-minify and Closure Compiler transpiles RegExp('a', 'y') -> /a/y and it causes SyntaxError


  var $RegExp$2 = global$1.RegExp;
  var UNSUPPORTED_Y$2 = fails(function () {
    var re = $RegExp$2('a', 'y');
    re.lastIndex = 2;
    return re.exec('abcd') != null;
  });
  var BROKEN_CARET = fails(function () {
    // https://bugzilla.mozilla.org/show_bug.cgi?id=773687
    var re = $RegExp$2('^r', 'gy');
    re.lastIndex = 2;
    return re.exec('str') != null;
  });

  var regexpStickyHelpers = {
  	UNSUPPORTED_Y: UNSUPPORTED_Y$2,
  	BROKEN_CARET: BROKEN_CARET
  };

  // `Object.keys` method
  // https://tc39.es/ecma262/#sec-object.keys
  // eslint-disable-next-line es/no-object-keys -- safe


  var objectKeys = Object.keys || function keys(O) {
    return objectKeysInternal(O, enumBugKeys);
  };

  // `Object.defineProperties` method
  // https://tc39.es/ecma262/#sec-object.defineproperties
  // eslint-disable-next-line es/no-object-defineproperties -- safe


  var objectDefineProperties = descriptors ? Object.defineProperties : function defineProperties(O, Properties) {
    anObject(O);
    var keys = objectKeys(Properties);
    var length = keys.length;
    var index = 0;
    var key;

    while (length > index) objectDefineProperty.f(O, key = keys[index++], Properties[key]);

    return O;
  };

  var html = getBuiltIn('document', 'documentElement');

  /* global ActiveXObject -- old IE, WSH */

  var GT = '>';
  var LT = '<';
  var PROTOTYPE = 'prototype';
  var SCRIPT = 'script';
  var IE_PROTO = sharedKey('IE_PROTO');

  var EmptyConstructor = function () {
    /* empty */
  };

  var scriptTag = function (content) {
    return LT + SCRIPT + GT + content + LT + '/' + SCRIPT + GT;
  }; // Create object with fake `null` prototype: use ActiveX Object with cleared prototype


  var NullProtoObjectViaActiveX = function (activeXDocument) {
    activeXDocument.write(scriptTag(''));
    activeXDocument.close();
    var temp = activeXDocument.parentWindow.Object;
    activeXDocument = null; // avoid memory leak

    return temp;
  }; // Create object with fake `null` prototype: use iframe Object with cleared prototype


  var NullProtoObjectViaIFrame = function () {
    // Thrash, waste and sodomy: IE GC bug
    var iframe = documentCreateElement('iframe');
    var JS = 'java' + SCRIPT + ':';
    var iframeDocument;
    iframe.style.display = 'none';
    html.appendChild(iframe); // https://github.com/zloirock/core-js/issues/475

    iframe.src = String(JS);
    iframeDocument = iframe.contentWindow.document;
    iframeDocument.open();
    iframeDocument.write(scriptTag('document.F=Object'));
    iframeDocument.close();
    return iframeDocument.F;
  }; // Check for document.domain and active x support
  // No need to use active x approach when document.domain is not set
  // see https://github.com/es-shims/es5-shim/issues/150
  // variation of https://github.com/kitcambridge/es5-shim/commit/4f738ac066346
  // avoid IE GC bug


  var activeXDocument;

  var NullProtoObject = function () {
    try {
      activeXDocument = new ActiveXObject('htmlfile');
    } catch (error) {
      /* ignore */
    }

    NullProtoObject = typeof document != 'undefined' ? document.domain && activeXDocument ? NullProtoObjectViaActiveX(activeXDocument) // old IE
    : NullProtoObjectViaIFrame() : NullProtoObjectViaActiveX(activeXDocument); // WSH

    var length = enumBugKeys.length;

    while (length--) delete NullProtoObject[PROTOTYPE][enumBugKeys[length]];

    return NullProtoObject();
  };

  hiddenKeys$1[IE_PROTO] = true; // `Object.create` method
  // https://tc39.es/ecma262/#sec-object.create

  var objectCreate = Object.create || function create(O, Properties) {
    var result;

    if (O !== null) {
      EmptyConstructor[PROTOTYPE] = anObject(O);
      result = new EmptyConstructor();
      EmptyConstructor[PROTOTYPE] = null; // add "__proto__" for Object.getPrototypeOf polyfill

      result[IE_PROTO] = O;
    } else result = NullProtoObject();

    return Properties === undefined ? result : objectDefineProperties(result, Properties);
  };

  // babel-minify and Closure Compiler transpiles RegExp('.', 's') -> /./s and it causes SyntaxError


  var $RegExp$1 = global$1.RegExp;
  var regexpUnsupportedDotAll = fails(function () {
    var re = $RegExp$1('.', 's');
    return !(re.dotAll && re.exec('\n') && re.flags === 's');
  });

  // babel-minify and Closure Compiler transpiles RegExp('(?<a>b)', 'g') -> /(?<a>b)/g and it causes SyntaxError


  var $RegExp = global$1.RegExp;
  var regexpUnsupportedNcg = fails(function () {
    var re = $RegExp('(?<a>b)', 'g');
    return re.exec('b').groups.a !== 'b' || 'b'.replace(re, '$<a>c') !== 'bc';
  });

  /* eslint-disable regexp/no-empty-capturing-group, regexp/no-empty-group, regexp/no-lazy-ends -- testing */

  /* eslint-disable regexp/no-useless-quantifier -- testing */











  var getInternalState = internalState.get;





  var nativeExec = RegExp.prototype.exec;
  var nativeReplace = shared('native-string-replace', String.prototype.replace);
  var patchedExec = nativeExec;

  var UPDATES_LAST_INDEX_WRONG = function () {
    var re1 = /a/;
    var re2 = /b*/g;
    nativeExec.call(re1, 'a');
    nativeExec.call(re2, 'a');
    return re1.lastIndex !== 0 || re2.lastIndex !== 0;
  }();

  var UNSUPPORTED_Y$1 = regexpStickyHelpers.UNSUPPORTED_Y || regexpStickyHelpers.BROKEN_CARET; // nonparticipating capturing group, copied from es5-shim's String#split patch.

  var NPCG_INCLUDED = /()??/.exec('')[1] !== undefined;
  var PATCH = UPDATES_LAST_INDEX_WRONG || NPCG_INCLUDED || UNSUPPORTED_Y$1 || regexpUnsupportedDotAll || regexpUnsupportedNcg;

  if (PATCH) {
    // eslint-disable-next-line max-statements -- TODO
    patchedExec = function exec(string) {
      var re = this;
      var state = getInternalState(re);
      var str = toString$2(string);
      var raw = state.raw;
      var result, reCopy, lastIndex, match, i, object, group;

      if (raw) {
        raw.lastIndex = re.lastIndex;
        result = patchedExec.call(raw, str);
        re.lastIndex = raw.lastIndex;
        return result;
      }

      var groups = state.groups;
      var sticky = UNSUPPORTED_Y$1 && re.sticky;
      var flags = regexpFlags.call(re);
      var source = re.source;
      var charsAdded = 0;
      var strCopy = str;

      if (sticky) {
        flags = flags.replace('y', '');

        if (flags.indexOf('g') === -1) {
          flags += 'g';
        }

        strCopy = str.slice(re.lastIndex); // Support anchored sticky behavior.

        if (re.lastIndex > 0 && (!re.multiline || re.multiline && str.charAt(re.lastIndex - 1) !== '\n')) {
          source = '(?: ' + source + ')';
          strCopy = ' ' + strCopy;
          charsAdded++;
        } // ^(? + rx + ) is needed, in combination with some str slicing, to
        // simulate the 'y' flag.


        reCopy = new RegExp('^(?:' + source + ')', flags);
      }

      if (NPCG_INCLUDED) {
        reCopy = new RegExp('^' + source + '$(?!\\s)', flags);
      }

      if (UPDATES_LAST_INDEX_WRONG) lastIndex = re.lastIndex;
      match = nativeExec.call(sticky ? reCopy : re, strCopy);

      if (sticky) {
        if (match) {
          match.input = match.input.slice(charsAdded);
          match[0] = match[0].slice(charsAdded);
          match.index = re.lastIndex;
          re.lastIndex += match[0].length;
        } else re.lastIndex = 0;
      } else if (UPDATES_LAST_INDEX_WRONG && match) {
        re.lastIndex = re.global ? match.index + match[0].length : lastIndex;
      }

      if (NPCG_INCLUDED && match && match.length > 1) {
        // Fix browsers whose `exec` methods don't consistently return `undefined`
        // for NPCG, like IE8. NOTE: This doesn' work for /(.?)?/
        nativeReplace.call(match[0], reCopy, function () {
          for (i = 1; i < arguments.length - 2; i++) {
            if (arguments[i] === undefined) match[i] = undefined;
          }
        });
      }

      if (match && groups) {
        match.groups = object = objectCreate(null);

        for (i = 0; i < groups.length; i++) {
          group = groups[i];
          object[group[0]] = match[group[1]];
        }
      }

      return match;
    };
  }

  var regexpExec = patchedExec;

  // `RegExp.prototype.exec` method
  // https://tc39.es/ecma262/#sec-regexp.prototype.exec


  _export({
    target: 'RegExp',
    proto: true,
    forced: /./.exec !== regexpExec
  }, {
    exec: regexpExec
  });

  var SPECIES$1 = wellKnownSymbol('species');
  var RegExpPrototype = RegExp.prototype;

  var fixRegexpWellKnownSymbolLogic = function (KEY, exec, FORCED, SHAM) {
    var SYMBOL = wellKnownSymbol(KEY);
    var DELEGATES_TO_SYMBOL = !fails(function () {
      // String methods call symbol-named RegEp methods
      var O = {};

      O[SYMBOL] = function () {
        return 7;
      };

      return ''[KEY](O) != 7;
    });
    var DELEGATES_TO_EXEC = DELEGATES_TO_SYMBOL && !fails(function () {
      // Symbol-named RegExp methods call .exec
      var execCalled = false;
      var re = /a/;

      if (KEY === 'split') {
        // We can't use real regex here since it causes deoptimization
        // and serious performance degradation in V8
        // https://github.com/zloirock/core-js/issues/306
        re = {}; // RegExp[@@split] doesn't call the regex's exec method, but first creates
        // a new one. We need to return the patched regex when creating the new one.

        re.constructor = {};

        re.constructor[SPECIES$1] = function () {
          return re;
        };

        re.flags = '';
        re[SYMBOL] = /./[SYMBOL];
      }

      re.exec = function () {
        execCalled = true;
        return null;
      };

      re[SYMBOL]('');
      return !execCalled;
    });

    if (!DELEGATES_TO_SYMBOL || !DELEGATES_TO_EXEC || FORCED) {
      var nativeRegExpMethod = /./[SYMBOL];
      var methods = exec(SYMBOL, ''[KEY], function (nativeMethod, regexp, str, arg2, forceStringMethod) {
        var $exec = regexp.exec;

        if ($exec === regexpExec || $exec === RegExpPrototype.exec) {
          if (DELEGATES_TO_SYMBOL && !forceStringMethod) {
            // The native String method already delegates to @@method (this
            // polyfilled function), leasing to infinite recursion.
            // We avoid it by directly calling the native @@method method.
            return {
              done: true,
              value: nativeRegExpMethod.call(regexp, str, arg2)
            };
          }

          return {
            done: true,
            value: nativeMethod.call(str, regexp, arg2)
          };
        }

        return {
          done: false
        };
      });
      redefine(String.prototype, KEY, methods[0]);
      redefine(RegExpPrototype, SYMBOL, methods[1]);
    }

    if (SHAM) createNonEnumerableProperty(RegExpPrototype[SYMBOL], 'sham', true);
  };

  var MATCH = wellKnownSymbol('match'); // `IsRegExp` abstract operation
  // https://tc39.es/ecma262/#sec-isregexp

  var isRegexp = function (it) {
    var isRegExp;
    return isObject$2(it) && ((isRegExp = it[MATCH]) !== undefined ? !!isRegExp : classofRaw(it) == 'RegExp');
  };

  var aFunction = function (it) {
    if (typeof it != 'function') {
      throw TypeError(String(it) + ' is not a function');
    }

    return it;
  };

  var SPECIES = wellKnownSymbol('species'); // `SpeciesConstructor` abstract operation
  // https://tc39.es/ecma262/#sec-speciesconstructor

  var speciesConstructor = function (O, defaultConstructor) {
    var C = anObject(O).constructor;
    var S;
    return C === undefined || (S = anObject(C)[SPECIES]) == undefined ? defaultConstructor : aFunction(S);
  };

  // `String.prototype.codePointAt` methods implementation


  var createMethod$3 = function (CONVERT_TO_STRING) {
    return function ($this, pos) {
      var S = toString$2(requireObjectCoercible($this));
      var position = toInteger(pos);
      var size = S.length;
      var first, second;
      if (position < 0 || position >= size) return CONVERT_TO_STRING ? '' : undefined;
      first = S.charCodeAt(position);
      return first < 0xD800 || first > 0xDBFF || position + 1 === size || (second = S.charCodeAt(position + 1)) < 0xDC00 || second > 0xDFFF ? CONVERT_TO_STRING ? S.charAt(position) : first : CONVERT_TO_STRING ? S.slice(position, position + 2) : (first - 0xD800 << 10) + (second - 0xDC00) + 0x10000;
    };
  };

  var stringMultibyte = {
    // `String.prototype.codePointAt` method
    // https://tc39.es/ecma262/#sec-string.prototype.codepointat
    codeAt: createMethod$3(false),
    // `String.prototype.at` method
    // https://github.com/mathiasbynens/String.prototype.at
    charAt: createMethod$3(true)
  };

  var charAt = stringMultibyte.charAt; // `AdvanceStringIndex` abstract operation
  // https://tc39.es/ecma262/#sec-advancestringindex


  var advanceStringIndex = function (S, index, unicode) {
    return index + (unicode ? charAt(S, index).length : 1);
  };

  // `RegExpExec` abstract operation
  // https://tc39.es/ecma262/#sec-regexpexec


  var regexpExecAbstract = function (R, S) {
    var exec = R.exec;

    if (typeof exec === 'function') {
      var result = exec.call(R, S);

      if (typeof result !== 'object') {
        throw TypeError('RegExp exec method returned something other than an Object or null');
      }

      return result;
    }

    if (classofRaw(R) !== 'RegExp') {
      throw TypeError('RegExp#exec called on incompatible receiver');
    }

    return regexpExec.call(R, S);
  };

  var UNSUPPORTED_Y = regexpStickyHelpers.UNSUPPORTED_Y;
  var arrayPush = [].push;
  var min = Math.min;
  var MAX_UINT32 = 0xFFFFFFFF; // Chrome 51 has a buggy "split" implementation when RegExp#exec !== nativeExec
  // Weex JS has frozen built-in prototypes, so use try / catch wrapper

  var SPLIT_WORKS_WITH_OVERWRITTEN_EXEC = !fails(function () {
    // eslint-disable-next-line regexp/no-empty-group -- required for testing
    var re = /(?:)/;
    var originalExec = re.exec;

    re.exec = function () {
      return originalExec.apply(this, arguments);
    };

    var result = 'ab'.split(re);
    return result.length !== 2 || result[0] !== 'a' || result[1] !== 'b';
  }); // @@split logic

  fixRegexpWellKnownSymbolLogic('split', function (SPLIT, nativeSplit, maybeCallNative) {
    var internalSplit;

    if ('abbc'.split(/(b)*/)[1] == 'c' || // eslint-disable-next-line regexp/no-empty-group -- required for testing
    'test'.split(/(?:)/, -1).length != 4 || 'ab'.split(/(?:ab)*/).length != 2 || '.'.split(/(.?)(.?)/).length != 4 || // eslint-disable-next-line regexp/no-empty-capturing-group, regexp/no-empty-group -- required for testing
    '.'.split(/()()/).length > 1 || ''.split(/.?/).length) {
      // based on es5-shim implementation, need to rework it
      internalSplit = function (separator, limit) {
        var string = toString$2(requireObjectCoercible(this));
        var lim = limit === undefined ? MAX_UINT32 : limit >>> 0;
        if (lim === 0) return [];
        if (separator === undefined) return [string]; // If `separator` is not a regex, use native split

        if (!isRegexp(separator)) {
          return nativeSplit.call(string, separator, lim);
        }

        var output = [];
        var flags = (separator.ignoreCase ? 'i' : '') + (separator.multiline ? 'm' : '') + (separator.unicode ? 'u' : '') + (separator.sticky ? 'y' : '');
        var lastLastIndex = 0; // Make `global` and avoid `lastIndex` issues by working with a copy

        var separatorCopy = new RegExp(separator.source, flags + 'g');
        var match, lastIndex, lastLength;

        while (match = regexpExec.call(separatorCopy, string)) {
          lastIndex = separatorCopy.lastIndex;

          if (lastIndex > lastLastIndex) {
            output.push(string.slice(lastLastIndex, match.index));
            if (match.length > 1 && match.index < string.length) arrayPush.apply(output, match.slice(1));
            lastLength = match[0].length;
            lastLastIndex = lastIndex;
            if (output.length >= lim) break;
          }

          if (separatorCopy.lastIndex === match.index) separatorCopy.lastIndex++; // Avoid an infinite loop
        }

        if (lastLastIndex === string.length) {
          if (lastLength || !separatorCopy.test('')) output.push('');
        } else output.push(string.slice(lastLastIndex));

        return output.length > lim ? output.slice(0, lim) : output;
      }; // Chakra, V8

    } else if ('0'.split(undefined, 0).length) {
      internalSplit = function (separator, limit) {
        return separator === undefined && limit === 0 ? [] : nativeSplit.call(this, separator, limit);
      };
    } else internalSplit = nativeSplit;

    return [// `String.prototype.split` method
    // https://tc39.es/ecma262/#sec-string.prototype.split
    function split(separator, limit) {
      var O = requireObjectCoercible(this);
      var splitter = separator == undefined ? undefined : separator[SPLIT];
      return splitter !== undefined ? splitter.call(separator, O, limit) : internalSplit.call(toString$2(O), separator, limit);
    }, // `RegExp.prototype[@@split]` method
    // https://tc39.es/ecma262/#sec-regexp.prototype-@@split
    //
    // NOTE: This cannot be properly polyfilled in engines that don't support
    // the 'y' flag.
    function (string, limit) {
      var rx = anObject(this);
      var S = toString$2(string);
      var res = maybeCallNative(internalSplit, rx, S, limit, internalSplit !== nativeSplit);
      if (res.done) return res.value;
      var C = speciesConstructor(rx, RegExp);
      var unicodeMatching = rx.unicode;
      var flags = (rx.ignoreCase ? 'i' : '') + (rx.multiline ? 'm' : '') + (rx.unicode ? 'u' : '') + (UNSUPPORTED_Y ? 'g' : 'y'); // ^(? + rx + ) is needed, in combination with some S slicing, to
      // simulate the 'y' flag.

      var splitter = new C(UNSUPPORTED_Y ? '^(?:' + rx.source + ')' : rx, flags);
      var lim = limit === undefined ? MAX_UINT32 : limit >>> 0;
      if (lim === 0) return [];
      if (S.length === 0) return regexpExecAbstract(splitter, S) === null ? [S] : [];
      var p = 0;
      var q = 0;
      var A = [];

      while (q < S.length) {
        splitter.lastIndex = UNSUPPORTED_Y ? 0 : q;
        var z = regexpExecAbstract(splitter, UNSUPPORTED_Y ? S.slice(q) : S);
        var e;

        if (z === null || (e = min(toLength(splitter.lastIndex + (UNSUPPORTED_Y ? q : 0)), S.length)) === p) {
          q = advanceStringIndex(S, q, unicodeMatching);
        } else {
          A.push(S.slice(p, q));
          if (A.length === lim) return A;

          for (var i = 1; i <= z.length - 1; i++) {
            A.push(z[i]);
            if (A.length === lim) return A;
          }

          q = p = e;
        }
      }

      A.push(S.slice(p));
      return A;
    }];
  }, !SPLIT_WORKS_WITH_OVERWRITTEN_EXEC, UNSUPPORTED_Y);

  var bind = function bind(fn, thisArg) {
    return function wrap() {
      var args = new Array(arguments.length);

      for (var i = 0; i < args.length; i++) {
        args[i] = arguments[i];
      }

      return fn.apply(thisArg, args);
    };
  };

  /*global toString:true*/
  // utils is a library of generic helper functions non-specific to axios


  var toString$1 = Object.prototype.toString;
  /**
   * Determine if a value is an Array
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if value is an Array, otherwise false
   */

  function isArray$4(val) {
    return toString$1.call(val) === '[object Array]';
  }
  /**
   * Determine if a value is undefined
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if the value is undefined, otherwise false
   */


  function isUndefined$1(val) {
    return typeof val === 'undefined';
  }
  /**
   * Determine if a value is a Buffer
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if value is a Buffer, otherwise false
   */


  function isBuffer$1(val) {
    return val !== null && !isUndefined$1(val) && val.constructor !== null && !isUndefined$1(val.constructor) && typeof val.constructor.isBuffer === 'function' && val.constructor.isBuffer(val);
  }
  /**
   * Determine if a value is an ArrayBuffer
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if value is an ArrayBuffer, otherwise false
   */


  function isArrayBuffer(val) {
    return toString$1.call(val) === '[object ArrayBuffer]';
  }
  /**
   * Determine if a value is a FormData
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if value is an FormData, otherwise false
   */


  function isFormData(val) {
    return typeof FormData !== 'undefined' && val instanceof FormData;
  }
  /**
   * Determine if a value is a view on an ArrayBuffer
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
   */


  function isArrayBufferView(val) {
    var result;

    if (typeof ArrayBuffer !== 'undefined' && ArrayBuffer.isView) {
      result = ArrayBuffer.isView(val);
    } else {
      result = val && val.buffer && val.buffer instanceof ArrayBuffer;
    }

    return result;
  }
  /**
   * Determine if a value is a String
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if value is a String, otherwise false
   */


  function isString$2(val) {
    return typeof val === 'string';
  }
  /**
   * Determine if a value is a Number
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if value is a Number, otherwise false
   */


  function isNumber$1(val) {
    return typeof val === 'number';
  }
  /**
   * Determine if a value is an Object
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if value is an Object, otherwise false
   */


  function isObject$1(val) {
    return val !== null && typeof val === 'object';
  }
  /**
   * Determine if a value is a plain Object
   *
   * @param {Object} val The value to test
   * @return {boolean} True if value is a plain Object, otherwise false
   */


  function isPlainObject(val) {
    if (toString$1.call(val) !== '[object Object]') {
      return false;
    }

    var prototype = Object.getPrototypeOf(val);
    return prototype === null || prototype === Object.prototype;
  }
  /**
   * Determine if a value is a Date
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if value is a Date, otherwise false
   */


  function isDate$1(val) {
    return toString$1.call(val) === '[object Date]';
  }
  /**
   * Determine if a value is a File
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if value is a File, otherwise false
   */


  function isFile(val) {
    return toString$1.call(val) === '[object File]';
  }
  /**
   * Determine if a value is a Blob
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if value is a Blob, otherwise false
   */


  function isBlob(val) {
    return toString$1.call(val) === '[object Blob]';
  }
  /**
   * Determine if a value is a Function
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if value is a Function, otherwise false
   */


  function isFunction$1(val) {
    return toString$1.call(val) === '[object Function]';
  }
  /**
   * Determine if a value is a Stream
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if value is a Stream, otherwise false
   */


  function isStream(val) {
    return isObject$1(val) && isFunction$1(val.pipe);
  }
  /**
   * Determine if a value is a URLSearchParams object
   *
   * @param {Object} val The value to test
   * @returns {boolean} True if value is a URLSearchParams object, otherwise false
   */


  function isURLSearchParams(val) {
    return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
  }
  /**
   * Trim excess whitespace off the beginning and end of a string
   *
   * @param {String} str The String to trim
   * @returns {String} The String freed of excess whitespace
   */


  function trim$1(str) {
    return str.replace(/^\s*/, '').replace(/\s*$/, '');
  }
  /**
   * Determine if we're running in a standard browser environment
   *
   * This allows axios to run in a web worker, and react-native.
   * Both environments support XMLHttpRequest, but not fully standard globals.
   *
   * web workers:
   *  typeof window -> undefined
   *  typeof document -> undefined
   *
   * react-native:
   *  navigator.product -> 'ReactNative'
   * nativescript
   *  navigator.product -> 'NativeScript' or 'NS'
   */


  function isStandardBrowserEnv() {
    if (typeof navigator !== 'undefined' && (navigator.product === 'ReactNative' || navigator.product === 'NativeScript' || navigator.product === 'NS')) {
      return false;
    }

    return typeof window !== 'undefined' && typeof document !== 'undefined';
  }
  /**
   * Iterate over an Array or an Object invoking a function for each item.
   *
   * If `obj` is an Array callback will be called passing
   * the value, index, and complete array for each item.
   *
   * If 'obj' is an Object callback will be called passing
   * the value, key, and complete object for each property.
   *
   * @param {Object|Array} obj The object to iterate
   * @param {Function} fn The callback to invoke for each item
   */


  function forEach(obj, fn) {
    // Don't bother if no value provided
    if (obj === null || typeof obj === 'undefined') {
      return;
    } // Force an array if not already something iterable


    if (typeof obj !== 'object') {
      /*eslint no-param-reassign:0*/
      obj = [obj];
    }

    if (isArray$4(obj)) {
      // Iterate over array values
      for (var i = 0, l = obj.length; i < l; i++) {
        fn.call(null, obj[i], i, obj);
      }
    } else {
      // Iterate over object keys
      for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
          fn.call(null, obj[key], key, obj);
        }
      }
    }
  }
  /**
   * Accepts varargs expecting each argument to be an object, then
   * immutably merges the properties of each object and returns result.
   *
   * When multiple objects contain the same key the later object in
   * the arguments list will take precedence.
   *
   * Example:
   *
   * ```js
   * var result = merge({foo: 123}, {foo: 456});
   * console.log(result.foo); // outputs 456
   * ```
   *
   * @param {Object} obj1 Object to merge
   * @returns {Object} Result of all merge properties
   */


  function merge$1() {
    var result = {};

    function assignValue(val, key) {
      if (isPlainObject(result[key]) && isPlainObject(val)) {
        result[key] = merge$1(result[key], val);
      } else if (isPlainObject(val)) {
        result[key] = merge$1({}, val);
      } else if (isArray$4(val)) {
        result[key] = val.slice();
      } else {
        result[key] = val;
      }
    }

    for (var i = 0, l = arguments.length; i < l; i++) {
      forEach(arguments[i], assignValue);
    }

    return result;
  }
  /**
   * Extends object a by mutably adding to it the properties of object b.
   *
   * @param {Object} a The object to be extended
   * @param {Object} b The object to copy properties from
   * @param {Object} thisArg The object to bind function to
   * @return {Object} The resulting value of object a
   */


  function extend(a, b, thisArg) {
    forEach(b, function assignValue(val, key) {
      if (thisArg && typeof val === 'function') {
        a[key] = bind(val, thisArg);
      } else {
        a[key] = val;
      }
    });
    return a;
  }
  /**
   * Remove byte order marker. This catches EF BB BF (the UTF-8 BOM)
   *
   * @param {string} content with BOM
   * @return {string} content value without BOM
   */


  function stripBOM(content) {
    if (content.charCodeAt(0) === 0xFEFF) {
      content = content.slice(1);
    }

    return content;
  }

  var utils$1 = {
    isArray: isArray$4,
    isArrayBuffer: isArrayBuffer,
    isBuffer: isBuffer$1,
    isFormData: isFormData,
    isArrayBufferView: isArrayBufferView,
    isString: isString$2,
    isNumber: isNumber$1,
    isObject: isObject$1,
    isPlainObject: isPlainObject,
    isUndefined: isUndefined$1,
    isDate: isDate$1,
    isFile: isFile,
    isBlob: isBlob,
    isFunction: isFunction$1,
    isStream: isStream,
    isURLSearchParams: isURLSearchParams,
    isStandardBrowserEnv: isStandardBrowserEnv,
    forEach: forEach,
    merge: merge$1,
    extend: extend,
    trim: trim$1,
    stripBOM: stripBOM
  };

  function encode$2(val) {
    return encodeURIComponent(val).replace(/%3A/gi, ':').replace(/%24/g, '$').replace(/%2C/gi, ',').replace(/%20/g, '+').replace(/%5B/gi, '[').replace(/%5D/gi, ']');
  }
  /**
   * Build a URL by appending params to the end
   *
   * @param {string} url The base of the url (e.g., http://www.google.com)
   * @param {object} [params] The params to be appended
   * @returns {string} The formatted url
   */


  var buildURL = function buildURL(url, params, paramsSerializer) {
    /*eslint no-param-reassign:0*/
    if (!params) {
      return url;
    }

    var serializedParams;

    if (paramsSerializer) {
      serializedParams = paramsSerializer(params);
    } else if (utils$1.isURLSearchParams(params)) {
      serializedParams = params.toString();
    } else {
      var parts = [];
      utils$1.forEach(params, function serialize(val, key) {
        if (val === null || typeof val === 'undefined') {
          return;
        }

        if (utils$1.isArray(val)) {
          key = key + '[]';
        } else {
          val = [val];
        }

        utils$1.forEach(val, function parseValue(v) {
          if (utils$1.isDate(v)) {
            v = v.toISOString();
          } else if (utils$1.isObject(v)) {
            v = JSON.stringify(v);
          }

          parts.push(encode$2(key) + '=' + encode$2(v));
        });
      });
      serializedParams = parts.join('&');
    }

    if (serializedParams) {
      var hashmarkIndex = url.indexOf('#');

      if (hashmarkIndex !== -1) {
        url = url.slice(0, hashmarkIndex);
      }

      url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
    }

    return url;
  };

  function InterceptorManager() {
    this.handlers = [];
  }
  /**
   * Add a new interceptor to the stack
   *
   * @param {Function} fulfilled The function to handle `then` for a `Promise`
   * @param {Function} rejected The function to handle `reject` for a `Promise`
   *
   * @return {Number} An ID used to remove interceptor later
   */


  InterceptorManager.prototype.use = function use(fulfilled, rejected) {
    this.handlers.push({
      fulfilled: fulfilled,
      rejected: rejected
    });
    return this.handlers.length - 1;
  };
  /**
   * Remove an interceptor from the stack
   *
   * @param {Number} id The ID that was returned by `use`
   */


  InterceptorManager.prototype.eject = function eject(id) {
    if (this.handlers[id]) {
      this.handlers[id] = null;
    }
  };
  /**
   * Iterate over all the registered interceptors
   *
   * This method is particularly useful for skipping over any
   * interceptors that may have become `null` calling `eject`.
   *
   * @param {Function} fn The function to call for each interceptor
   */


  InterceptorManager.prototype.forEach = function forEach(fn) {
    utils$1.forEach(this.handlers, function forEachHandler(h) {
      if (h !== null) {
        fn(h);
      }
    });
  };

  var InterceptorManager_1 = InterceptorManager;

  /**
   * Transform the data for a request or a response
   *
   * @param {Object|String} data The data to be transformed
   * @param {Array} headers The headers for the request or response
   * @param {Array|Function} fns A single function or Array of functions
   * @returns {*} The resulting transformed data
   */


  var transformData = function transformData(data, headers, fns) {
    /*eslint no-param-reassign:0*/
    utils$1.forEach(fns, function transform(fn) {
      data = fn(data, headers);
    });
    return data;
  };

  var isCancel = function isCancel(value) {
    return !!(value && value.__CANCEL__);
  };

  var normalizeHeaderName = function normalizeHeaderName(headers, normalizedName) {
    utils$1.forEach(headers, function processHeader(value, name) {
      if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
        headers[normalizedName] = value;
        delete headers[name];
      }
    });
  };

  /**
   * Update an Error with the specified config, error code, and response.
   *
   * @param {Error} error The error to update.
   * @param {Object} config The config.
   * @param {string} [code] The error code (for example, 'ECONNABORTED').
   * @param {Object} [request] The request.
   * @param {Object} [response] The response.
   * @returns {Error} The error.
   */

  var enhanceError = function enhanceError(error, config, code, request, response) {
    error.config = config;

    if (code) {
      error.code = code;
    }

    error.request = request;
    error.response = response;
    error.isAxiosError = true;

    error.toJSON = function toJSON() {
      return {
        // Standard
        message: this.message,
        name: this.name,
        // Microsoft
        description: this.description,
        number: this.number,
        // Mozilla
        fileName: this.fileName,
        lineNumber: this.lineNumber,
        columnNumber: this.columnNumber,
        stack: this.stack,
        // Axios
        config: this.config,
        code: this.code
      };
    };

    return error;
  };

  /**
   * Create an Error with the specified message, config, error code, request and response.
   *
   * @param {string} message The error message.
   * @param {Object} config The config.
   * @param {string} [code] The error code (for example, 'ECONNABORTED').
   * @param {Object} [request] The request.
   * @param {Object} [response] The response.
   * @returns {Error} The created error.
   */


  var createError = function createError(message, config, code, request, response) {
    var error = new Error(message);
    return enhanceError(error, config, code, request, response);
  };

  /**
   * Resolve or reject a Promise based on response status.
   *
   * @param {Function} resolve A function that resolves the promise.
   * @param {Function} reject A function that rejects the promise.
   * @param {object} response The response.
   */


  var settle = function settle(resolve, reject, response) {
    var validateStatus = response.config.validateStatus;

    if (!response.status || !validateStatus || validateStatus(response.status)) {
      resolve(response);
    } else {
      reject(createError('Request failed with status code ' + response.status, response.config, null, response.request, response));
    }
  };

  var cookies = utils$1.isStandardBrowserEnv() ? // Standard browser envs support document.cookie
  function standardBrowserEnv() {
    return {
      write: function write(name, value, expires, path, domain, secure) {
        var cookie = [];
        cookie.push(name + '=' + encodeURIComponent(value));

        if (utils$1.isNumber(expires)) {
          cookie.push('expires=' + new Date(expires).toGMTString());
        }

        if (utils$1.isString(path)) {
          cookie.push('path=' + path);
        }

        if (utils$1.isString(domain)) {
          cookie.push('domain=' + domain);
        }

        if (secure === true) {
          cookie.push('secure');
        }

        document.cookie = cookie.join('; ');
      },
      read: function read(name) {
        var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
        return match ? decodeURIComponent(match[3]) : null;
      },
      remove: function remove(name) {
        this.write(name, '', Date.now() - 86400000);
      }
    };
  }() : // Non standard browser env (web workers, react-native) lack needed support.
  function nonStandardBrowserEnv() {
    return {
      write: function write() {},
      read: function read() {
        return null;
      },
      remove: function remove() {}
    };
  }();

  /**
   * Determines whether the specified URL is absolute
   *
   * @param {string} url The URL to test
   * @returns {boolean} True if the specified URL is absolute, otherwise false
   */

  var isAbsoluteURL = function isAbsoluteURL(url) {
    // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
    // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
    // by any combination of letters, digits, plus, period, or hyphen.
    return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
  };

  /**
   * Creates a new URL by combining the specified URLs
   *
   * @param {string} baseURL The base URL
   * @param {string} relativeURL The relative URL
   * @returns {string} The combined URL
   */

  var combineURLs = function combineURLs(baseURL, relativeURL) {
    return relativeURL ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '') : baseURL;
  };

  /**
   * Creates a new URL by combining the baseURL with the requestedURL,
   * only when the requestedURL is not already an absolute URL.
   * If the requestURL is absolute, this function returns the requestedURL untouched.
   *
   * @param {string} baseURL The base URL
   * @param {string} requestedURL Absolute or relative URL to combine
   * @returns {string} The combined full path
   */


  var buildFullPath = function buildFullPath(baseURL, requestedURL) {
    if (baseURL && !isAbsoluteURL(requestedURL)) {
      return combineURLs(baseURL, requestedURL);
    }

    return requestedURL;
  };

  // Headers whose duplicates are ignored by node
  // c.f. https://nodejs.org/api/http.html#http_message_headers


  var ignoreDuplicateOf = ['age', 'authorization', 'content-length', 'content-type', 'etag', 'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since', 'last-modified', 'location', 'max-forwards', 'proxy-authorization', 'referer', 'retry-after', 'user-agent'];
  /**
   * Parse headers into an object
   *
   * ```
   * Date: Wed, 27 Aug 2014 08:58:49 GMT
   * Content-Type: application/json
   * Connection: keep-alive
   * Transfer-Encoding: chunked
   * ```
   *
   * @param {String} headers Headers needing to be parsed
   * @returns {Object} Headers parsed into an object
   */

  var parseHeaders = function parseHeaders(headers) {
    var parsed = {};
    var key;
    var val;
    var i;

    if (!headers) {
      return parsed;
    }

    utils$1.forEach(headers.split('\n'), function parser(line) {
      i = line.indexOf(':');
      key = utils$1.trim(line.substr(0, i)).toLowerCase();
      val = utils$1.trim(line.substr(i + 1));

      if (key) {
        if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
          return;
        }

        if (key === 'set-cookie') {
          parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
        } else {
          parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
        }
      }
    });
    return parsed;
  };

  var isURLSameOrigin = utils$1.isStandardBrowserEnv() ? // Standard browser envs have full support of the APIs needed to test
  // whether the request URL is of the same origin as current location.
  function standardBrowserEnv() {
    var msie = /(msie|trident)/i.test(navigator.userAgent);
    var urlParsingNode = document.createElement('a');
    var originURL;
    /**
    * Parse a URL to discover it's components
    *
    * @param {String} url The URL to be parsed
    * @returns {Object}
    */

    function resolveURL(url) {
      var href = url;

      if (msie) {
        // IE needs attribute set twice to normalize properties
        urlParsingNode.setAttribute('href', href);
        href = urlParsingNode.href;
      }

      urlParsingNode.setAttribute('href', href); // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils

      return {
        href: urlParsingNode.href,
        protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
        host: urlParsingNode.host,
        search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
        hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
        hostname: urlParsingNode.hostname,
        port: urlParsingNode.port,
        pathname: urlParsingNode.pathname.charAt(0) === '/' ? urlParsingNode.pathname : '/' + urlParsingNode.pathname
      };
    }

    originURL = resolveURL(window.location.href);
    /**
    * Determine if a URL shares the same origin as the current location
    *
    * @param {String} requestURL The URL to test
    * @returns {boolean} True if URL shares the same origin, otherwise false
    */

    return function isURLSameOrigin(requestURL) {
      var parsed = utils$1.isString(requestURL) ? resolveURL(requestURL) : requestURL;
      return parsed.protocol === originURL.protocol && parsed.host === originURL.host;
    };
  }() : // Non standard browser envs (web workers, react-native) lack needed support.
  function nonStandardBrowserEnv() {
    return function isURLSameOrigin() {
      return true;
    };
  }();

  var xhr = function xhrAdapter(config) {
    return new Promise(function dispatchXhrRequest(resolve, reject) {
      var requestData = config.data;
      var requestHeaders = config.headers;

      if (utils$1.isFormData(requestData)) {
        delete requestHeaders['Content-Type']; // Let the browser set it
      }

      var request = new XMLHttpRequest(); // HTTP basic authentication

      if (config.auth) {
        var username = config.auth.username || '';
        var password = config.auth.password ? unescape(encodeURIComponent(config.auth.password)) : '';
        requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
      }

      var fullPath = buildFullPath(config.baseURL, config.url);
      request.open(config.method.toUpperCase(), buildURL(fullPath, config.params, config.paramsSerializer), true); // Set the request timeout in MS

      request.timeout = config.timeout; // Listen for ready state

      request.onreadystatechange = function handleLoad() {
        if (!request || request.readyState !== 4) {
          return;
        } // The request errored out and we didn't get a response, this will be
        // handled by onerror instead
        // With one exception: request that using file: protocol, most browsers
        // will return status as 0 even though it's a successful request


        if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
          return;
        } // Prepare the response


        var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
        var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
        var response = {
          data: responseData,
          status: request.status,
          statusText: request.statusText,
          headers: responseHeaders,
          config: config,
          request: request
        };
        settle(resolve, reject, response); // Clean up request

        request = null;
      }; // Handle browser request cancellation (as opposed to a manual cancellation)


      request.onabort = function handleAbort() {
        if (!request) {
          return;
        }

        reject(createError('Request aborted', config, 'ECONNABORTED', request)); // Clean up request

        request = null;
      }; // Handle low level network errors


      request.onerror = function handleError() {
        // Real errors are hidden from us by the browser
        // onerror should only fire if it's a network error
        reject(createError('Network Error', config, null, request)); // Clean up request

        request = null;
      }; // Handle timeout


      request.ontimeout = function handleTimeout() {
        var timeoutErrorMessage = 'timeout of ' + config.timeout + 'ms exceeded';

        if (config.timeoutErrorMessage) {
          timeoutErrorMessage = config.timeoutErrorMessage;
        }

        reject(createError(timeoutErrorMessage, config, 'ECONNABORTED', request)); // Clean up request

        request = null;
      }; // Add xsrf header
      // This is only done if running in a standard browser environment.
      // Specifically not if we're in a web worker, or react-native.


      if (utils$1.isStandardBrowserEnv()) {
        // Add xsrf header
        var xsrfValue = (config.withCredentials || isURLSameOrigin(fullPath)) && config.xsrfCookieName ? cookies.read(config.xsrfCookieName) : undefined;

        if (xsrfValue) {
          requestHeaders[config.xsrfHeaderName] = xsrfValue;
        }
      } // Add headers to the request


      if ('setRequestHeader' in request) {
        utils$1.forEach(requestHeaders, function setRequestHeader(val, key) {
          if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
            // Remove Content-Type if data is undefined
            delete requestHeaders[key];
          } else {
            // Otherwise add header to the request
            request.setRequestHeader(key, val);
          }
        });
      } // Add withCredentials to request if needed


      if (!utils$1.isUndefined(config.withCredentials)) {
        request.withCredentials = !!config.withCredentials;
      } // Add responseType to request if needed


      if (config.responseType) {
        try {
          request.responseType = config.responseType;
        } catch (e) {
          // Expected DOMException thrown by browsers not compatible XMLHttpRequest Level 2.
          // But, this can be suppressed for 'json' type as it can be parsed by default 'transformResponse' function.
          if (config.responseType !== 'json') {
            throw e;
          }
        }
      } // Handle progress if needed


      if (typeof config.onDownloadProgress === 'function') {
        request.addEventListener('progress', config.onDownloadProgress);
      } // Not all browsers support upload events


      if (typeof config.onUploadProgress === 'function' && request.upload) {
        request.upload.addEventListener('progress', config.onUploadProgress);
      }

      if (config.cancelToken) {
        // Handle cancellation
        config.cancelToken.promise.then(function onCanceled(cancel) {
          if (!request) {
            return;
          }

          request.abort();
          reject(cancel); // Clean up request

          request = null;
        });
      }

      if (!requestData) {
        requestData = null;
      } // Send the request


      request.send(requestData);
    });
  };

  var DEFAULT_CONTENT_TYPE = {
    'Content-Type': 'application/x-www-form-urlencoded'
  };

  function setContentTypeIfUnset(headers, value) {
    if (!utils$1.isUndefined(headers) && utils$1.isUndefined(headers['Content-Type'])) {
      headers['Content-Type'] = value;
    }
  }

  function getDefaultAdapter() {
    var adapter;

    if (typeof XMLHttpRequest !== 'undefined') {
      // For browsers use XHR adapter
      adapter = xhr;
    } else if (typeof process !== 'undefined' && Object.prototype.toString.call(process) === '[object process]') {
      // For node use HTTP adapter
      adapter = xhr;
    }

    return adapter;
  }

  var defaults$2 = {
    adapter: getDefaultAdapter(),
    transformRequest: [function transformRequest(data, headers) {
      normalizeHeaderName(headers, 'Accept');
      normalizeHeaderName(headers, 'Content-Type');

      if (utils$1.isFormData(data) || utils$1.isArrayBuffer(data) || utils$1.isBuffer(data) || utils$1.isStream(data) || utils$1.isFile(data) || utils$1.isBlob(data)) {
        return data;
      }

      if (utils$1.isArrayBufferView(data)) {
        return data.buffer;
      }

      if (utils$1.isURLSearchParams(data)) {
        setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
        return data.toString();
      }

      if (utils$1.isObject(data)) {
        setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
        return JSON.stringify(data);
      }

      return data;
    }],
    transformResponse: [function transformResponse(data) {
      /*eslint no-param-reassign:0*/
      if (typeof data === 'string') {
        try {
          data = JSON.parse(data);
        } catch (e) {
          /* Ignore */
        }
      }

      return data;
    }],

    /**
     * A timeout in milliseconds to abort a request. If set to 0 (default) a
     * timeout is not created.
     */
    timeout: 0,
    xsrfCookieName: 'XSRF-TOKEN',
    xsrfHeaderName: 'X-XSRF-TOKEN',
    maxContentLength: -1,
    maxBodyLength: -1,
    validateStatus: function validateStatus(status) {
      return status >= 200 && status < 300;
    }
  };
  defaults$2.headers = {
    common: {
      'Accept': 'application/json, text/plain, */*'
    }
  };
  utils$1.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
    defaults$2.headers[method] = {};
  });
  utils$1.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
    defaults$2.headers[method] = utils$1.merge(DEFAULT_CONTENT_TYPE);
  });
  var defaults_1 = defaults$2;

  /**
   * Throws a `Cancel` if cancellation has been requested.
   */


  function throwIfCancellationRequested(config) {
    if (config.cancelToken) {
      config.cancelToken.throwIfRequested();
    }
  }
  /**
   * Dispatch a request to the server using the configured adapter.
   *
   * @param {object} config The config that is to be used for the request
   * @returns {Promise} The Promise to be fulfilled
   */


  var dispatchRequest = function dispatchRequest(config) {
    throwIfCancellationRequested(config); // Ensure headers exist

    config.headers = config.headers || {}; // Transform request data

    config.data = transformData(config.data, config.headers, config.transformRequest); // Flatten headers

    config.headers = utils$1.merge(config.headers.common || {}, config.headers[config.method] || {}, config.headers);
    utils$1.forEach(['delete', 'get', 'head', 'post', 'put', 'patch', 'common'], function cleanHeaderConfig(method) {
      delete config.headers[method];
    });
    var adapter = config.adapter || defaults_1.adapter;
    return adapter(config).then(function onAdapterResolution(response) {
      throwIfCancellationRequested(config); // Transform response data

      response.data = transformData(response.data, response.headers, config.transformResponse);
      return response;
    }, function onAdapterRejection(reason) {
      if (!isCancel(reason)) {
        throwIfCancellationRequested(config); // Transform response data

        if (reason && reason.response) {
          reason.response.data = transformData(reason.response.data, reason.response.headers, config.transformResponse);
        }
      }

      return Promise.reject(reason);
    });
  };

  /**
   * Config-specific merge-function which creates a new config-object
   * by merging two configuration objects together.
   *
   * @param {Object} config1
   * @param {Object} config2
   * @returns {Object} New object resulting from merging config2 to config1
   */


  var mergeConfig = function mergeConfig(config1, config2) {
    // eslint-disable-next-line no-param-reassign
    config2 = config2 || {};
    var config = {};
    var valueFromConfig2Keys = ['url', 'method', 'data'];
    var mergeDeepPropertiesKeys = ['headers', 'auth', 'proxy', 'params'];
    var defaultToConfig2Keys = ['baseURL', 'transformRequest', 'transformResponse', 'paramsSerializer', 'timeout', 'timeoutMessage', 'withCredentials', 'adapter', 'responseType', 'xsrfCookieName', 'xsrfHeaderName', 'onUploadProgress', 'onDownloadProgress', 'decompress', 'maxContentLength', 'maxBodyLength', 'maxRedirects', 'transport', 'httpAgent', 'httpsAgent', 'cancelToken', 'socketPath', 'responseEncoding'];
    var directMergeKeys = ['validateStatus'];

    function getMergedValue(target, source) {
      if (utils$1.isPlainObject(target) && utils$1.isPlainObject(source)) {
        return utils$1.merge(target, source);
      } else if (utils$1.isPlainObject(source)) {
        return utils$1.merge({}, source);
      } else if (utils$1.isArray(source)) {
        return source.slice();
      }

      return source;
    }

    function mergeDeepProperties(prop) {
      if (!utils$1.isUndefined(config2[prop])) {
        config[prop] = getMergedValue(config1[prop], config2[prop]);
      } else if (!utils$1.isUndefined(config1[prop])) {
        config[prop] = getMergedValue(undefined, config1[prop]);
      }
    }

    utils$1.forEach(valueFromConfig2Keys, function valueFromConfig2(prop) {
      if (!utils$1.isUndefined(config2[prop])) {
        config[prop] = getMergedValue(undefined, config2[prop]);
      }
    });
    utils$1.forEach(mergeDeepPropertiesKeys, mergeDeepProperties);
    utils$1.forEach(defaultToConfig2Keys, function defaultToConfig2(prop) {
      if (!utils$1.isUndefined(config2[prop])) {
        config[prop] = getMergedValue(undefined, config2[prop]);
      } else if (!utils$1.isUndefined(config1[prop])) {
        config[prop] = getMergedValue(undefined, config1[prop]);
      }
    });
    utils$1.forEach(directMergeKeys, function merge(prop) {
      if (prop in config2) {
        config[prop] = getMergedValue(config1[prop], config2[prop]);
      } else if (prop in config1) {
        config[prop] = getMergedValue(undefined, config1[prop]);
      }
    });
    var axiosKeys = valueFromConfig2Keys.concat(mergeDeepPropertiesKeys).concat(defaultToConfig2Keys).concat(directMergeKeys);
    var otherKeys = Object.keys(config1).concat(Object.keys(config2)).filter(function filterAxiosKeys(key) {
      return axiosKeys.indexOf(key) === -1;
    });
    utils$1.forEach(otherKeys, mergeDeepProperties);
    return config;
  };

  /**
   * Create a new instance of Axios
   *
   * @param {Object} instanceConfig The default config for the instance
   */


  function Axios(instanceConfig) {
    this.defaults = instanceConfig;
    this.interceptors = {
      request: new InterceptorManager_1(),
      response: new InterceptorManager_1()
    };
  }
  /**
   * Dispatch a request
   *
   * @param {Object} config The config specific for this request (merged with this.defaults)
   */


  Axios.prototype.request = function request(config) {
    /*eslint no-param-reassign:0*/
    // Allow for axios('example/url'[, config]) a la fetch API
    if (typeof config === 'string') {
      config = arguments[1] || {};
      config.url = arguments[0];
    } else {
      config = config || {};
    }

    config = mergeConfig(this.defaults, config); // Set config.method

    if (config.method) {
      config.method = config.method.toLowerCase();
    } else if (this.defaults.method) {
      config.method = this.defaults.method.toLowerCase();
    } else {
      config.method = 'get';
    } // Hook up interceptors middleware


    var chain = [dispatchRequest, undefined];
    var promise = Promise.resolve(config);
    this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
      chain.unshift(interceptor.fulfilled, interceptor.rejected);
    });
    this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
      chain.push(interceptor.fulfilled, interceptor.rejected);
    });

    while (chain.length) {
      promise = promise.then(chain.shift(), chain.shift());
    }

    return promise;
  };

  Axios.prototype.getUri = function getUri(config) {
    config = mergeConfig(this.defaults, config);
    return buildURL(config.url, config.params, config.paramsSerializer).replace(/^\?/, '');
  }; // Provide aliases for supported request methods


  utils$1.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
    /*eslint func-names:0*/
    Axios.prototype[method] = function (url, config) {
      return this.request(mergeConfig(config || {}, {
        method: method,
        url: url,
        data: (config || {}).data
      }));
    };
  });
  utils$1.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
    /*eslint func-names:0*/
    Axios.prototype[method] = function (url, data, config) {
      return this.request(mergeConfig(config || {}, {
        method: method,
        url: url,
        data: data
      }));
    };
  });
  var Axios_1 = Axios;

  /**
   * A `Cancel` is an object that is thrown when an operation is canceled.
   *
   * @class
   * @param {string=} message The message.
   */

  function Cancel(message) {
    this.message = message;
  }

  Cancel.prototype.toString = function toString() {
    return 'Cancel' + (this.message ? ': ' + this.message : '');
  };

  Cancel.prototype.__CANCEL__ = true;
  var Cancel_1 = Cancel;

  /**
   * A `CancelToken` is an object that can be used to request cancellation of an operation.
   *
   * @class
   * @param {Function} executor The executor function.
   */


  function CancelToken(executor) {
    if (typeof executor !== 'function') {
      throw new TypeError('executor must be a function.');
    }

    var resolvePromise;
    this.promise = new Promise(function promiseExecutor(resolve) {
      resolvePromise = resolve;
    });
    var token = this;
    executor(function cancel(message) {
      if (token.reason) {
        // Cancellation has already been requested
        return;
      }

      token.reason = new Cancel_1(message);
      resolvePromise(token.reason);
    });
  }
  /**
   * Throws a `Cancel` if cancellation has been requested.
   */


  CancelToken.prototype.throwIfRequested = function throwIfRequested() {
    if (this.reason) {
      throw this.reason;
    }
  };
  /**
   * Returns an object that contains a new `CancelToken` and a function that, when called,
   * cancels the `CancelToken`.
   */


  CancelToken.source = function source() {
    var cancel;
    var token = new CancelToken(function executor(c) {
      cancel = c;
    });
    return {
      token: token,
      cancel: cancel
    };
  };

  var CancelToken_1 = CancelToken;

  /**
   * Syntactic sugar for invoking a function and expanding an array for arguments.
   *
   * Common use case would be to use `Function.prototype.apply`.
   *
   *  ```js
   *  function f(x, y, z) {}
   *  var args = [1, 2, 3];
   *  f.apply(null, args);
   *  ```
   *
   * With `spread` this example can be re-written.
   *
   *  ```js
   *  spread(function(x, y, z) {})([1, 2, 3]);
   *  ```
   *
   * @param {Function} callback
   * @returns {Function}
   */

  var spread = function spread(callback) {
    return function wrap(arr) {
      return callback.apply(null, arr);
    };
  };

  /**
   * Determines whether the payload is an error thrown by Axios
   *
   * @param {*} payload The value to test
   * @returns {boolean} True if the payload is an error thrown by Axios, otherwise false
   */

  var isAxiosError = function isAxiosError(payload) {
    return typeof payload === 'object' && payload.isAxiosError === true;
  };

  /**
   * Create an instance of Axios
   *
   * @param {Object} defaultConfig The default config for the instance
   * @return {Axios} A new instance of Axios
   */


  function createInstance(defaultConfig) {
    var context = new Axios_1(defaultConfig);
    var instance = bind(Axios_1.prototype.request, context); // Copy axios.prototype to instance

    utils$1.extend(instance, Axios_1.prototype, context); // Copy context to instance

    utils$1.extend(instance, context);
    return instance;
  } // Create the default instance to be exported


  var axios$1 = createInstance(defaults_1); // Expose Axios class to allow class inheritance

  axios$1.Axios = Axios_1; // Factory for creating new instances

  axios$1.create = function create(instanceConfig) {
    return createInstance(mergeConfig(axios$1.defaults, instanceConfig));
  }; // Expose Cancel & CancelToken


  axios$1.Cancel = Cancel_1;
  axios$1.CancelToken = CancelToken_1;
  axios$1.isCancel = isCancel; // Expose all/spread

  axios$1.all = function all(promises) {
    return Promise.all(promises);
  };

  axios$1.spread = spread; // Expose isAxiosError

  axios$1.isAxiosError = isAxiosError;
  var axios_1 = axios$1; // Allow use of default import syntax in TypeScript

  var _default$1 = axios$1;
  axios_1.default = _default$1;

  var axios = axios_1;

  /* eslint complexity: [2, 18], max-statements: [2, 33] */

  var shams = function hasSymbols() {
    if (typeof Symbol !== 'function' || typeof Object.getOwnPropertySymbols !== 'function') {
      return false;
    }

    if (typeof Symbol.iterator === 'symbol') {
      return true;
    }

    var obj = {};
    var sym = Symbol('test');
    var symObj = Object(sym);

    if (typeof sym === 'string') {
      return false;
    }

    if (Object.prototype.toString.call(sym) !== '[object Symbol]') {
      return false;
    }

    if (Object.prototype.toString.call(symObj) !== '[object Symbol]') {
      return false;
    } // temp disabled per https://github.com/ljharb/object.assign/issues/17
    // if (sym instanceof Symbol) { return false; }
    // temp disabled per https://github.com/WebReflection/get-own-property-symbols/issues/4
    // if (!(symObj instanceof Symbol)) { return false; }
    // if (typeof Symbol.prototype.toString !== 'function') { return false; }
    // if (String(sym) !== Symbol.prototype.toString.call(sym)) { return false; }


    var symVal = 42;
    obj[sym] = symVal;

    for (sym in obj) {
      return false;
    } // eslint-disable-line no-restricted-syntax, no-unreachable-loop


    if (typeof Object.keys === 'function' && Object.keys(obj).length !== 0) {
      return false;
    }

    if (typeof Object.getOwnPropertyNames === 'function' && Object.getOwnPropertyNames(obj).length !== 0) {
      return false;
    }

    var syms = Object.getOwnPropertySymbols(obj);

    if (syms.length !== 1 || syms[0] !== sym) {
      return false;
    }

    if (!Object.prototype.propertyIsEnumerable.call(obj, sym)) {
      return false;
    }

    if (typeof Object.getOwnPropertyDescriptor === 'function') {
      var descriptor = Object.getOwnPropertyDescriptor(obj, sym);

      if (descriptor.value !== symVal || descriptor.enumerable !== true) {
        return false;
      }
    }

    return true;
  };

  var origSymbol = typeof Symbol !== 'undefined' && Symbol;



  var hasSymbols$1 = function hasNativeSymbols() {
    if (typeof origSymbol !== 'function') {
      return false;
    }

    if (typeof Symbol !== 'function') {
      return false;
    }

    if (typeof origSymbol('foo') !== 'symbol') {
      return false;
    }

    if (typeof Symbol('bar') !== 'symbol') {
      return false;
    }

    return shams();
  };

  /* eslint no-invalid-this: 1 */

  var ERROR_MESSAGE = 'Function.prototype.bind called on incompatible ';
  var slice = Array.prototype.slice;
  var toStr$1 = Object.prototype.toString;
  var funcType = '[object Function]';

  var implementation = function bind(that) {
    var target = this;

    if (typeof target !== 'function' || toStr$1.call(target) !== funcType) {
      throw new TypeError(ERROR_MESSAGE + target);
    }

    var args = slice.call(arguments, 1);
    var bound;

    var binder = function () {
      if (this instanceof bound) {
        var result = target.apply(this, args.concat(slice.call(arguments)));

        if (Object(result) === result) {
          return result;
        }

        return this;
      } else {
        return target.apply(that, args.concat(slice.call(arguments)));
      }
    };

    var boundLength = Math.max(0, target.length - args.length);
    var boundArgs = [];

    for (var i = 0; i < boundLength; i++) {
      boundArgs.push('$' + i);
    }

    bound = Function('binder', 'return function (' + boundArgs.join(',') + '){ return binder.apply(this,arguments); }')(binder);

    if (target.prototype) {
      var Empty = function Empty() {};

      Empty.prototype = target.prototype;
      bound.prototype = new Empty();
      Empty.prototype = null;
    }

    return bound;
  };

  var functionBind = Function.prototype.bind || implementation;

  var src = functionBind.call(Function.call, Object.prototype.hasOwnProperty);

  var undefined$1;
  var $SyntaxError = SyntaxError;
  var $Function = Function;
  var $TypeError$1 = TypeError; // eslint-disable-next-line consistent-return

  var getEvalledConstructor = function (expressionSyntax) {
    try {
      return $Function('"use strict"; return (' + expressionSyntax + ').constructor;')();
    } catch (e) {}
  };

  var $gOPD = Object.getOwnPropertyDescriptor;

  if ($gOPD) {
    try {
      $gOPD({}, '');
    } catch (e) {
      $gOPD = null; // this is IE 8, which has a broken gOPD
    }
  }

  var throwTypeError = function () {
    throw new $TypeError$1();
  };

  var ThrowTypeError = $gOPD ? function () {
    try {
      // eslint-disable-next-line no-unused-expressions, no-caller, no-restricted-properties
      arguments.callee; // IE 8 does not throw here

      return throwTypeError;
    } catch (calleeThrows) {
      try {
        // IE 8 throws on Object.getOwnPropertyDescriptor(arguments, '')
        return $gOPD(arguments, 'callee').get;
      } catch (gOPDthrows) {
        return throwTypeError;
      }
    }
  }() : throwTypeError;

  var hasSymbols = hasSymbols$1();

  var getProto = Object.getPrototypeOf || function (x) {
    return x.__proto__;
  }; // eslint-disable-line no-proto


  var needsEval = {};
  var TypedArray = typeof Uint8Array === 'undefined' ? undefined$1 : getProto(Uint8Array);
  var INTRINSICS = {
    '%AggregateError%': typeof AggregateError === 'undefined' ? undefined$1 : AggregateError,
    '%Array%': Array,
    '%ArrayBuffer%': typeof ArrayBuffer === 'undefined' ? undefined$1 : ArrayBuffer,
    '%ArrayIteratorPrototype%': hasSymbols ? getProto([][Symbol.iterator]()) : undefined$1,
    '%AsyncFromSyncIteratorPrototype%': undefined$1,
    '%AsyncFunction%': needsEval,
    '%AsyncGenerator%': needsEval,
    '%AsyncGeneratorFunction%': needsEval,
    '%AsyncIteratorPrototype%': needsEval,
    '%Atomics%': typeof Atomics === 'undefined' ? undefined$1 : Atomics,
    '%BigInt%': typeof BigInt === 'undefined' ? undefined$1 : BigInt,
    '%Boolean%': Boolean,
    '%DataView%': typeof DataView === 'undefined' ? undefined$1 : DataView,
    '%Date%': Date,
    '%decodeURI%': decodeURI,
    '%decodeURIComponent%': decodeURIComponent,
    '%encodeURI%': encodeURI,
    '%encodeURIComponent%': encodeURIComponent,
    '%Error%': Error,
    '%eval%': eval,
    // eslint-disable-line no-eval
    '%EvalError%': EvalError,
    '%Float32Array%': typeof Float32Array === 'undefined' ? undefined$1 : Float32Array,
    '%Float64Array%': typeof Float64Array === 'undefined' ? undefined$1 : Float64Array,
    '%FinalizationRegistry%': typeof FinalizationRegistry === 'undefined' ? undefined$1 : FinalizationRegistry,
    '%Function%': $Function,
    '%GeneratorFunction%': needsEval,
    '%Int8Array%': typeof Int8Array === 'undefined' ? undefined$1 : Int8Array,
    '%Int16Array%': typeof Int16Array === 'undefined' ? undefined$1 : Int16Array,
    '%Int32Array%': typeof Int32Array === 'undefined' ? undefined$1 : Int32Array,
    '%isFinite%': isFinite,
    '%isNaN%': isNaN,
    '%IteratorPrototype%': hasSymbols ? getProto(getProto([][Symbol.iterator]())) : undefined$1,
    '%JSON%': typeof JSON === 'object' ? JSON : undefined$1,
    '%Map%': typeof Map === 'undefined' ? undefined$1 : Map,
    '%MapIteratorPrototype%': typeof Map === 'undefined' || !hasSymbols ? undefined$1 : getProto(new Map()[Symbol.iterator]()),
    '%Math%': Math,
    '%Number%': Number,
    '%Object%': Object,
    '%parseFloat%': parseFloat,
    '%parseInt%': parseInt,
    '%Promise%': typeof Promise === 'undefined' ? undefined$1 : Promise,
    '%Proxy%': typeof Proxy === 'undefined' ? undefined$1 : Proxy,
    '%RangeError%': RangeError,
    '%ReferenceError%': ReferenceError,
    '%Reflect%': typeof Reflect === 'undefined' ? undefined$1 : Reflect,
    '%RegExp%': RegExp,
    '%Set%': typeof Set === 'undefined' ? undefined$1 : Set,
    '%SetIteratorPrototype%': typeof Set === 'undefined' || !hasSymbols ? undefined$1 : getProto(new Set()[Symbol.iterator]()),
    '%SharedArrayBuffer%': typeof SharedArrayBuffer === 'undefined' ? undefined$1 : SharedArrayBuffer,
    '%String%': String,
    '%StringIteratorPrototype%': hasSymbols ? getProto(''[Symbol.iterator]()) : undefined$1,
    '%Symbol%': hasSymbols ? Symbol : undefined$1,
    '%SyntaxError%': $SyntaxError,
    '%ThrowTypeError%': ThrowTypeError,
    '%TypedArray%': TypedArray,
    '%TypeError%': $TypeError$1,
    '%Uint8Array%': typeof Uint8Array === 'undefined' ? undefined$1 : Uint8Array,
    '%Uint8ClampedArray%': typeof Uint8ClampedArray === 'undefined' ? undefined$1 : Uint8ClampedArray,
    '%Uint16Array%': typeof Uint16Array === 'undefined' ? undefined$1 : Uint16Array,
    '%Uint32Array%': typeof Uint32Array === 'undefined' ? undefined$1 : Uint32Array,
    '%URIError%': URIError,
    '%WeakMap%': typeof WeakMap === 'undefined' ? undefined$1 : WeakMap,
    '%WeakRef%': typeof WeakRef === 'undefined' ? undefined$1 : WeakRef,
    '%WeakSet%': typeof WeakSet === 'undefined' ? undefined$1 : WeakSet
  };

  var doEval = function doEval(name) {
    var value;

    if (name === '%AsyncFunction%') {
      value = getEvalledConstructor('async function () {}');
    } else if (name === '%GeneratorFunction%') {
      value = getEvalledConstructor('function* () {}');
    } else if (name === '%AsyncGeneratorFunction%') {
      value = getEvalledConstructor('async function* () {}');
    } else if (name === '%AsyncGenerator%') {
      var fn = doEval('%AsyncGeneratorFunction%');

      if (fn) {
        value = fn.prototype;
      }
    } else if (name === '%AsyncIteratorPrototype%') {
      var gen = doEval('%AsyncGenerator%');

      if (gen) {
        value = getProto(gen.prototype);
      }
    }

    INTRINSICS[name] = value;
    return value;
  };

  var LEGACY_ALIASES = {
    '%ArrayBufferPrototype%': ['ArrayBuffer', 'prototype'],
    '%ArrayPrototype%': ['Array', 'prototype'],
    '%ArrayProto_entries%': ['Array', 'prototype', 'entries'],
    '%ArrayProto_forEach%': ['Array', 'prototype', 'forEach'],
    '%ArrayProto_keys%': ['Array', 'prototype', 'keys'],
    '%ArrayProto_values%': ['Array', 'prototype', 'values'],
    '%AsyncFunctionPrototype%': ['AsyncFunction', 'prototype'],
    '%AsyncGenerator%': ['AsyncGeneratorFunction', 'prototype'],
    '%AsyncGeneratorPrototype%': ['AsyncGeneratorFunction', 'prototype', 'prototype'],
    '%BooleanPrototype%': ['Boolean', 'prototype'],
    '%DataViewPrototype%': ['DataView', 'prototype'],
    '%DatePrototype%': ['Date', 'prototype'],
    '%ErrorPrototype%': ['Error', 'prototype'],
    '%EvalErrorPrototype%': ['EvalError', 'prototype'],
    '%Float32ArrayPrototype%': ['Float32Array', 'prototype'],
    '%Float64ArrayPrototype%': ['Float64Array', 'prototype'],
    '%FunctionPrototype%': ['Function', 'prototype'],
    '%Generator%': ['GeneratorFunction', 'prototype'],
    '%GeneratorPrototype%': ['GeneratorFunction', 'prototype', 'prototype'],
    '%Int8ArrayPrototype%': ['Int8Array', 'prototype'],
    '%Int16ArrayPrototype%': ['Int16Array', 'prototype'],
    '%Int32ArrayPrototype%': ['Int32Array', 'prototype'],
    '%JSONParse%': ['JSON', 'parse'],
    '%JSONStringify%': ['JSON', 'stringify'],
    '%MapPrototype%': ['Map', 'prototype'],
    '%NumberPrototype%': ['Number', 'prototype'],
    '%ObjectPrototype%': ['Object', 'prototype'],
    '%ObjProto_toString%': ['Object', 'prototype', 'toString'],
    '%ObjProto_valueOf%': ['Object', 'prototype', 'valueOf'],
    '%PromisePrototype%': ['Promise', 'prototype'],
    '%PromiseProto_then%': ['Promise', 'prototype', 'then'],
    '%Promise_all%': ['Promise', 'all'],
    '%Promise_reject%': ['Promise', 'reject'],
    '%Promise_resolve%': ['Promise', 'resolve'],
    '%RangeErrorPrototype%': ['RangeError', 'prototype'],
    '%ReferenceErrorPrototype%': ['ReferenceError', 'prototype'],
    '%RegExpPrototype%': ['RegExp', 'prototype'],
    '%SetPrototype%': ['Set', 'prototype'],
    '%SharedArrayBufferPrototype%': ['SharedArrayBuffer', 'prototype'],
    '%StringPrototype%': ['String', 'prototype'],
    '%SymbolPrototype%': ['Symbol', 'prototype'],
    '%SyntaxErrorPrototype%': ['SyntaxError', 'prototype'],
    '%TypedArrayPrototype%': ['TypedArray', 'prototype'],
    '%TypeErrorPrototype%': ['TypeError', 'prototype'],
    '%Uint8ArrayPrototype%': ['Uint8Array', 'prototype'],
    '%Uint8ClampedArrayPrototype%': ['Uint8ClampedArray', 'prototype'],
    '%Uint16ArrayPrototype%': ['Uint16Array', 'prototype'],
    '%Uint32ArrayPrototype%': ['Uint32Array', 'prototype'],
    '%URIErrorPrototype%': ['URIError', 'prototype'],
    '%WeakMapPrototype%': ['WeakMap', 'prototype'],
    '%WeakSetPrototype%': ['WeakSet', 'prototype']
  };





  var $concat = functionBind.call(Function.call, Array.prototype.concat);
  var $spliceApply = functionBind.call(Function.apply, Array.prototype.splice);
  var $replace = functionBind.call(Function.call, String.prototype.replace);
  var $strSlice = functionBind.call(Function.call, String.prototype.slice);
  /* adapted from https://github.com/lodash/lodash/blob/4.17.15/dist/lodash.js#L6735-L6744 */

  var rePropName = /[^%.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|%$))/g;
  var reEscapeChar = /\\(\\)?/g;
  /** Used to match backslashes in property paths. */

  var stringToPath$1 = function stringToPath(string) {
    var first = $strSlice(string, 0, 1);
    var last = $strSlice(string, -1);

    if (first === '%' && last !== '%') {
      throw new $SyntaxError('invalid intrinsic syntax, expected closing `%`');
    } else if (last === '%' && first !== '%') {
      throw new $SyntaxError('invalid intrinsic syntax, expected opening `%`');
    }

    var result = [];
    $replace(string, rePropName, function (match, number, quote, subString) {
      result[result.length] = quote ? $replace(subString, reEscapeChar, '$1') : number || match;
    });
    return result;
  };
  /* end adaptation */


  var getBaseIntrinsic = function getBaseIntrinsic(name, allowMissing) {
    var intrinsicName = name;
    var alias;

    if (src(LEGACY_ALIASES, intrinsicName)) {
      alias = LEGACY_ALIASES[intrinsicName];
      intrinsicName = '%' + alias[0] + '%';
    }

    if (src(INTRINSICS, intrinsicName)) {
      var value = INTRINSICS[intrinsicName];

      if (value === needsEval) {
        value = doEval(intrinsicName);
      }

      if (typeof value === 'undefined' && !allowMissing) {
        throw new $TypeError$1('intrinsic ' + name + ' exists, but is not available. Please file an issue!');
      }

      return {
        alias: alias,
        name: intrinsicName,
        value: value
      };
    }

    throw new $SyntaxError('intrinsic ' + name + ' does not exist!');
  };

  var getIntrinsic = function GetIntrinsic(name, allowMissing) {
    if (typeof name !== 'string' || name.length === 0) {
      throw new $TypeError$1('intrinsic name must be a non-empty string');
    }

    if (arguments.length > 1 && typeof allowMissing !== 'boolean') {
      throw new $TypeError$1('"allowMissing" argument must be a boolean');
    }

    var parts = stringToPath$1(name);
    var intrinsicBaseName = parts.length > 0 ? parts[0] : '';
    var intrinsic = getBaseIntrinsic('%' + intrinsicBaseName + '%', allowMissing);
    var intrinsicRealName = intrinsic.name;
    var value = intrinsic.value;
    var skipFurtherCaching = false;
    var alias = intrinsic.alias;

    if (alias) {
      intrinsicBaseName = alias[0];
      $spliceApply(parts, $concat([0, 1], alias));
    }

    for (var i = 1, isOwn = true; i < parts.length; i += 1) {
      var part = parts[i];
      var first = $strSlice(part, 0, 1);
      var last = $strSlice(part, -1);

      if ((first === '"' || first === "'" || first === '`' || last === '"' || last === "'" || last === '`') && first !== last) {
        throw new $SyntaxError('property names with quotes must have matching quotes');
      }

      if (part === 'constructor' || !isOwn) {
        skipFurtherCaching = true;
      }

      intrinsicBaseName += '.' + part;
      intrinsicRealName = '%' + intrinsicBaseName + '%';

      if (src(INTRINSICS, intrinsicRealName)) {
        value = INTRINSICS[intrinsicRealName];
      } else if (value != null) {
        if (!(part in value)) {
          if (!allowMissing) {
            throw new $TypeError$1('base intrinsic for ' + name + ' exists, but the property is not available.');
          }

          return void undefined$1;
        }

        if ($gOPD && i + 1 >= parts.length) {
          var desc = $gOPD(value, part);
          isOwn = !!desc; // By convention, when a data property is converted to an accessor
          // property to emulate a data property that does not suffer from
          // the override mistake, that accessor's getter is marked with
          // an `originalValue` property. Here, when we detect this, we
          // uphold the illusion by pretending to see that original data
          // property, i.e., returning the value rather than the getter
          // itself.

          if (isOwn && 'get' in desc && !('originalValue' in desc.get)) {
            value = desc.get;
          } else {
            value = value[part];
          }
        } else {
          isOwn = src(value, part);
          value = value[part];
        }

        if (isOwn && !skipFurtherCaching) {
          INTRINSICS[intrinsicRealName] = value;
        }
      }
    }

    return value;
  };

  var callBind = createCommonjsModule(function (module) {





  var $apply = getIntrinsic('%Function.prototype.apply%');
  var $call = getIntrinsic('%Function.prototype.call%');
  var $reflectApply = getIntrinsic('%Reflect.apply%', true) || functionBind.call($call, $apply);
  var $gOPD = getIntrinsic('%Object.getOwnPropertyDescriptor%', true);
  var $defineProperty = getIntrinsic('%Object.defineProperty%', true);
  var $max = getIntrinsic('%Math.max%');

  if ($defineProperty) {
    try {
      $defineProperty({}, 'a', {
        value: 1
      });
    } catch (e) {
      // IE 8 has a broken defineProperty
      $defineProperty = null;
    }
  }

  module.exports = function callBind(originalFunction) {
    var func = $reflectApply(functionBind, $call, arguments);

    if ($gOPD && $defineProperty) {
      var desc = $gOPD(func, 'length');

      if (desc.configurable) {
        // original length, plus the receiver, minus any additional arguments (after the receiver)
        $defineProperty(func, 'length', {
          value: 1 + $max(0, originalFunction.length - (arguments.length - 1))
        });
      }
    }

    return func;
  };

  var applyBind = function applyBind() {
    return $reflectApply(functionBind, $apply, arguments);
  };

  if ($defineProperty) {
    $defineProperty(module.exports, 'apply', {
      value: applyBind
    });
  } else {
    module.exports.apply = applyBind;
  }
  });

  var $indexOf = callBind(getIntrinsic('String.prototype.indexOf'));

  var callBound = function callBoundIntrinsic(name, allowMissing) {
    var intrinsic = getIntrinsic(name, !!allowMissing);

    if (typeof intrinsic === 'function' && $indexOf(name, '.prototype.') > -1) {
      return callBind(intrinsic);
    }

    return intrinsic;
  };

  var _nodeResolve_empty = {};

  var _nodeResolve_empty$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': _nodeResolve_empty
  });

  var require$$0$1 = /*@__PURE__*/getAugmentedNamespace(_nodeResolve_empty$1);

  var hasMap = typeof Map === 'function' && Map.prototype;
  var mapSizeDescriptor = Object.getOwnPropertyDescriptor && hasMap ? Object.getOwnPropertyDescriptor(Map.prototype, 'size') : null;
  var mapSize = hasMap && mapSizeDescriptor && typeof mapSizeDescriptor.get === 'function' ? mapSizeDescriptor.get : null;
  var mapForEach = hasMap && Map.prototype.forEach;
  var hasSet = typeof Set === 'function' && Set.prototype;
  var setSizeDescriptor = Object.getOwnPropertyDescriptor && hasSet ? Object.getOwnPropertyDescriptor(Set.prototype, 'size') : null;
  var setSize = hasSet && setSizeDescriptor && typeof setSizeDescriptor.get === 'function' ? setSizeDescriptor.get : null;
  var setForEach = hasSet && Set.prototype.forEach;
  var hasWeakMap = typeof WeakMap === 'function' && WeakMap.prototype;
  var weakMapHas = hasWeakMap ? WeakMap.prototype.has : null;
  var hasWeakSet = typeof WeakSet === 'function' && WeakSet.prototype;
  var weakSetHas = hasWeakSet ? WeakSet.prototype.has : null;
  var hasWeakRef = typeof WeakRef === 'function' && WeakRef.prototype;
  var weakRefDeref = hasWeakRef ? WeakRef.prototype.deref : null;
  var booleanValueOf = Boolean.prototype.valueOf;
  var objectToString = Object.prototype.toString;
  var functionToString = Function.prototype.toString;
  var match = String.prototype.match;
  var bigIntValueOf = typeof BigInt === 'function' ? BigInt.prototype.valueOf : null;
  var gOPS = Object.getOwnPropertySymbols;
  var symToString = typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol' ? Symbol.prototype.toString : null;
  var hasShammedSymbols = typeof Symbol === 'function' && typeof Symbol.iterator === 'object';
  var isEnumerable = Object.prototype.propertyIsEnumerable;
  var gPO = (typeof Reflect === 'function' ? Reflect.getPrototypeOf : Object.getPrototypeOf) || ([].__proto__ === Array.prototype // eslint-disable-line no-proto
  ? function (O) {
    return O.__proto__; // eslint-disable-line no-proto
  } : null);

  var inspectCustom = require$$0$1.custom;

  var inspectSymbol = inspectCustom && isSymbol(inspectCustom) ? inspectCustom : null;
  var toStringTag = typeof Symbol === 'function' && typeof Symbol.toStringTag !== 'undefined' ? Symbol.toStringTag : null;

  var objectInspect = function inspect_(obj, options, depth, seen) {
    var opts = options || {};

    if (has$6(opts, 'quoteStyle') && opts.quoteStyle !== 'single' && opts.quoteStyle !== 'double') {
      throw new TypeError('option "quoteStyle" must be "single" or "double"');
    }

    if (has$6(opts, 'maxStringLength') && (typeof opts.maxStringLength === 'number' ? opts.maxStringLength < 0 && opts.maxStringLength !== Infinity : opts.maxStringLength !== null)) {
      throw new TypeError('option "maxStringLength", if provided, must be a positive integer, Infinity, or `null`');
    }

    var customInspect = has$6(opts, 'customInspect') ? opts.customInspect : true;

    if (typeof customInspect !== 'boolean' && customInspect !== 'symbol') {
      throw new TypeError('option "customInspect", if provided, must be `true`, `false`, or `\'symbol\'`');
    }

    if (has$6(opts, 'indent') && opts.indent !== null && opts.indent !== '\t' && !(parseInt(opts.indent, 10) === opts.indent && opts.indent > 0)) {
      throw new TypeError('options "indent" must be "\\t", an integer > 0, or `null`');
    }

    if (typeof obj === 'undefined') {
      return 'undefined';
    }

    if (obj === null) {
      return 'null';
    }

    if (typeof obj === 'boolean') {
      return obj ? 'true' : 'false';
    }

    if (typeof obj === 'string') {
      return inspectString(obj, opts);
    }

    if (typeof obj === 'number') {
      if (obj === 0) {
        return Infinity / obj > 0 ? '0' : '-0';
      }

      return String(obj);
    }

    if (typeof obj === 'bigint') {
      return String(obj) + 'n';
    }

    var maxDepth = typeof opts.depth === 'undefined' ? 5 : opts.depth;

    if (typeof depth === 'undefined') {
      depth = 0;
    }

    if (depth >= maxDepth && maxDepth > 0 && typeof obj === 'object') {
      return isArray$3(obj) ? '[Array]' : '[Object]';
    }

    var indent = getIndent(opts, depth);

    if (typeof seen === 'undefined') {
      seen = [];
    } else if (indexOf(seen, obj) >= 0) {
      return '[Circular]';
    }

    function inspect(value, from, noIndent) {
      if (from) {
        seen = seen.slice();
        seen.push(from);
      }

      if (noIndent) {
        var newOpts = {
          depth: opts.depth
        };

        if (has$6(opts, 'quoteStyle')) {
          newOpts.quoteStyle = opts.quoteStyle;
        }

        return inspect_(value, newOpts, depth + 1, seen);
      }

      return inspect_(value, opts, depth + 1, seen);
    }

    if (typeof obj === 'function') {
      var name = nameOf(obj);
      var keys = arrObjKeys(obj, inspect);
      return '[Function' + (name ? ': ' + name : ' (anonymous)') + ']' + (keys.length > 0 ? ' { ' + keys.join(', ') + ' }' : '');
    }

    if (isSymbol(obj)) {
      var symString = hasShammedSymbols ? String(obj).replace(/^(Symbol\(.*\))_[^)]*$/, '$1') : symToString.call(obj);
      return typeof obj === 'object' && !hasShammedSymbols ? markBoxed(symString) : symString;
    }

    if (isElement(obj)) {
      var s = '<' + String(obj.nodeName).toLowerCase();
      var attrs = obj.attributes || [];

      for (var i = 0; i < attrs.length; i++) {
        s += ' ' + attrs[i].name + '=' + wrapQuotes(quote(attrs[i].value), 'double', opts);
      }

      s += '>';

      if (obj.childNodes && obj.childNodes.length) {
        s += '...';
      }

      s += '</' + String(obj.nodeName).toLowerCase() + '>';
      return s;
    }

    if (isArray$3(obj)) {
      if (obj.length === 0) {
        return '[]';
      }

      var xs = arrObjKeys(obj, inspect);

      if (indent && !singleLineValues(xs)) {
        return '[' + indentedJoin(xs, indent) + ']';
      }

      return '[ ' + xs.join(', ') + ' ]';
    }

    if (isError(obj)) {
      var parts = arrObjKeys(obj, inspect);

      if (parts.length === 0) {
        return '[' + String(obj) + ']';
      }

      return '{ [' + String(obj) + '] ' + parts.join(', ') + ' }';
    }

    if (typeof obj === 'object' && customInspect) {
      if (inspectSymbol && typeof obj[inspectSymbol] === 'function') {
        return obj[inspectSymbol]();
      } else if (customInspect !== 'symbol' && typeof obj.inspect === 'function') {
        return obj.inspect();
      }
    }

    if (isMap(obj)) {
      var mapParts = [];
      mapForEach.call(obj, function (value, key) {
        mapParts.push(inspect(key, obj, true) + ' => ' + inspect(value, obj));
      });
      return collectionOf('Map', mapSize.call(obj), mapParts, indent);
    }

    if (isSet(obj)) {
      var setParts = [];
      setForEach.call(obj, function (value) {
        setParts.push(inspect(value, obj));
      });
      return collectionOf('Set', setSize.call(obj), setParts, indent);
    }

    if (isWeakMap(obj)) {
      return weakCollectionOf('WeakMap');
    }

    if (isWeakSet(obj)) {
      return weakCollectionOf('WeakSet');
    }

    if (isWeakRef(obj)) {
      return weakCollectionOf('WeakRef');
    }

    if (isNumber(obj)) {
      return markBoxed(inspect(Number(obj)));
    }

    if (isBigInt(obj)) {
      return markBoxed(inspect(bigIntValueOf.call(obj)));
    }

    if (isBoolean$1(obj)) {
      return markBoxed(booleanValueOf.call(obj));
    }

    if (isString$1(obj)) {
      return markBoxed(inspect(String(obj)));
    }

    if (!isDate(obj) && !isRegExp$1(obj)) {
      var ys = arrObjKeys(obj, inspect);
      var isPlainObject = gPO ? gPO(obj) === Object.prototype : obj instanceof Object || obj.constructor === Object;
      var protoTag = obj instanceof Object ? '' : 'null prototype';
      var stringTag = !isPlainObject && toStringTag && Object(obj) === obj && toStringTag in obj ? toStr(obj).slice(8, -1) : protoTag ? 'Object' : '';
      var constructorTag = isPlainObject || typeof obj.constructor !== 'function' ? '' : obj.constructor.name ? obj.constructor.name + ' ' : '';
      var tag = constructorTag + (stringTag || protoTag ? '[' + [].concat(stringTag || [], protoTag || []).join(': ') + '] ' : '');

      if (ys.length === 0) {
        return tag + '{}';
      }

      if (indent) {
        return tag + '{' + indentedJoin(ys, indent) + '}';
      }

      return tag + '{ ' + ys.join(', ') + ' }';
    }

    return String(obj);
  };

  function wrapQuotes(s, defaultStyle, opts) {
    var quoteChar = (opts.quoteStyle || defaultStyle) === 'double' ? '"' : "'";
    return quoteChar + s + quoteChar;
  }

  function quote(s) {
    return String(s).replace(/"/g, '&quot;');
  }

  function isArray$3(obj) {
    return toStr(obj) === '[object Array]' && (!toStringTag || !(typeof obj === 'object' && toStringTag in obj));
  }

  function isDate(obj) {
    return toStr(obj) === '[object Date]' && (!toStringTag || !(typeof obj === 'object' && toStringTag in obj));
  }

  function isRegExp$1(obj) {
    return toStr(obj) === '[object RegExp]' && (!toStringTag || !(typeof obj === 'object' && toStringTag in obj));
  }

  function isError(obj) {
    return toStr(obj) === '[object Error]' && (!toStringTag || !(typeof obj === 'object' && toStringTag in obj));
  }

  function isString$1(obj) {
    return toStr(obj) === '[object String]' && (!toStringTag || !(typeof obj === 'object' && toStringTag in obj));
  }

  function isNumber(obj) {
    return toStr(obj) === '[object Number]' && (!toStringTag || !(typeof obj === 'object' && toStringTag in obj));
  }

  function isBoolean$1(obj) {
    return toStr(obj) === '[object Boolean]' && (!toStringTag || !(typeof obj === 'object' && toStringTag in obj));
  } // Symbol and BigInt do have Symbol.toStringTag by spec, so that can't be used to eliminate false positives


  function isSymbol(obj) {
    if (hasShammedSymbols) {
      return obj && typeof obj === 'object' && obj instanceof Symbol;
    }

    if (typeof obj === 'symbol') {
      return true;
    }

    if (!obj || typeof obj !== 'object' || !symToString) {
      return false;
    }

    try {
      symToString.call(obj);
      return true;
    } catch (e) {}

    return false;
  }

  function isBigInt(obj) {
    if (!obj || typeof obj !== 'object' || !bigIntValueOf) {
      return false;
    }

    try {
      bigIntValueOf.call(obj);
      return true;
    } catch (e) {}

    return false;
  }

  var hasOwn = Object.prototype.hasOwnProperty || function (key) {
    return key in this;
  };

  function has$6(obj, key) {
    return hasOwn.call(obj, key);
  }

  function toStr(obj) {
    return objectToString.call(obj);
  }

  function nameOf(f) {
    if (f.name) {
      return f.name;
    }

    var m = match.call(functionToString.call(f), /^function\s*([\w$]+)/);

    if (m) {
      return m[1];
    }

    return null;
  }

  function indexOf(xs, x) {
    if (xs.indexOf) {
      return xs.indexOf(x);
    }

    for (var i = 0, l = xs.length; i < l; i++) {
      if (xs[i] === x) {
        return i;
      }
    }

    return -1;
  }

  function isMap(x) {
    if (!mapSize || !x || typeof x !== 'object') {
      return false;
    }

    try {
      mapSize.call(x);

      try {
        setSize.call(x);
      } catch (s) {
        return true;
      }

      return x instanceof Map; // core-js workaround, pre-v2.5.0
    } catch (e) {}

    return false;
  }

  function isWeakMap(x) {
    if (!weakMapHas || !x || typeof x !== 'object') {
      return false;
    }

    try {
      weakMapHas.call(x, weakMapHas);

      try {
        weakSetHas.call(x, weakSetHas);
      } catch (s) {
        return true;
      }

      return x instanceof WeakMap; // core-js workaround, pre-v2.5.0
    } catch (e) {}

    return false;
  }

  function isWeakRef(x) {
    if (!weakRefDeref || !x || typeof x !== 'object') {
      return false;
    }

    try {
      weakRefDeref.call(x);
      return true;
    } catch (e) {}

    return false;
  }

  function isSet(x) {
    if (!setSize || !x || typeof x !== 'object') {
      return false;
    }

    try {
      setSize.call(x);

      try {
        mapSize.call(x);
      } catch (m) {
        return true;
      }

      return x instanceof Set; // core-js workaround, pre-v2.5.0
    } catch (e) {}

    return false;
  }

  function isWeakSet(x) {
    if (!weakSetHas || !x || typeof x !== 'object') {
      return false;
    }

    try {
      weakSetHas.call(x, weakSetHas);

      try {
        weakMapHas.call(x, weakMapHas);
      } catch (s) {
        return true;
      }

      return x instanceof WeakSet; // core-js workaround, pre-v2.5.0
    } catch (e) {}

    return false;
  }

  function isElement(x) {
    if (!x || typeof x !== 'object') {
      return false;
    }

    if (typeof HTMLElement !== 'undefined' && x instanceof HTMLElement) {
      return true;
    }

    return typeof x.nodeName === 'string' && typeof x.getAttribute === 'function';
  }

  function inspectString(str, opts) {
    if (str.length > opts.maxStringLength) {
      var remaining = str.length - opts.maxStringLength;
      var trailer = '... ' + remaining + ' more character' + (remaining > 1 ? 's' : '');
      return inspectString(str.slice(0, opts.maxStringLength), opts) + trailer;
    } // eslint-disable-next-line no-control-regex


    var s = str.replace(/(['\\])/g, '\\$1').replace(/[\x00-\x1f]/g, lowbyte);
    return wrapQuotes(s, 'single', opts);
  }

  function lowbyte(c) {
    var n = c.charCodeAt(0);
    var x = {
      8: 'b',
      9: 't',
      10: 'n',
      12: 'f',
      13: 'r'
    }[n];

    if (x) {
      return '\\' + x;
    }

    return '\\x' + (n < 0x10 ? '0' : '') + n.toString(16).toUpperCase();
  }

  function markBoxed(str) {
    return 'Object(' + str + ')';
  }

  function weakCollectionOf(type) {
    return type + ' { ? }';
  }

  function collectionOf(type, size, entries, indent) {
    var joinedEntries = indent ? indentedJoin(entries, indent) : entries.join(', ');
    return type + ' (' + size + ') {' + joinedEntries + '}';
  }

  function singleLineValues(xs) {
    for (var i = 0; i < xs.length; i++) {
      if (indexOf(xs[i], '\n') >= 0) {
        return false;
      }
    }

    return true;
  }

  function getIndent(opts, depth) {
    var baseIndent;

    if (opts.indent === '\t') {
      baseIndent = '\t';
    } else if (typeof opts.indent === 'number' && opts.indent > 0) {
      baseIndent = Array(opts.indent + 1).join(' ');
    } else {
      return null;
    }

    return {
      base: baseIndent,
      prev: Array(depth + 1).join(baseIndent)
    };
  }

  function indentedJoin(xs, indent) {
    if (xs.length === 0) {
      return '';
    }

    var lineJoiner = '\n' + indent.prev + indent.base;
    return lineJoiner + xs.join(',' + lineJoiner) + '\n' + indent.prev;
  }

  function arrObjKeys(obj, inspect) {
    var isArr = isArray$3(obj);
    var xs = [];

    if (isArr) {
      xs.length = obj.length;

      for (var i = 0; i < obj.length; i++) {
        xs[i] = has$6(obj, i) ? inspect(obj[i], obj) : '';
      }
    }

    var syms = typeof gOPS === 'function' ? gOPS(obj) : [];
    var symMap;

    if (hasShammedSymbols) {
      symMap = {};

      for (var k = 0; k < syms.length; k++) {
        symMap['$' + syms[k]] = syms[k];
      }
    }

    for (var key in obj) {
      // eslint-disable-line no-restricted-syntax
      if (!has$6(obj, key)) {
        continue;
      } // eslint-disable-line no-restricted-syntax, no-continue


      if (isArr && String(Number(key)) === key && key < obj.length) {
        continue;
      } // eslint-disable-line no-restricted-syntax, no-continue


      if (hasShammedSymbols && symMap['$' + key] instanceof Symbol) {
        // this is to prevent shammed Symbols, which are stored as strings, from being included in the string key section
        continue; // eslint-disable-line no-restricted-syntax, no-continue
      } else if (/[^\w$]/.test(key)) {
        xs.push(inspect(key, obj) + ': ' + inspect(obj[key], obj));
      } else {
        xs.push(key + ': ' + inspect(obj[key], obj));
      }
    }

    if (typeof gOPS === 'function') {
      for (var j = 0; j < syms.length; j++) {
        if (isEnumerable.call(obj, syms[j])) {
          xs.push('[' + inspect(syms[j]) + ']: ' + inspect(obj[syms[j]], obj));
        }
      }
    }

    return xs;
  }

  var $TypeError = getIntrinsic('%TypeError%');
  var $WeakMap = getIntrinsic('%WeakMap%', true);
  var $Map = getIntrinsic('%Map%', true);
  var $weakMapGet = callBound('WeakMap.prototype.get', true);
  var $weakMapSet = callBound('WeakMap.prototype.set', true);
  var $weakMapHas = callBound('WeakMap.prototype.has', true);
  var $mapGet = callBound('Map.prototype.get', true);
  var $mapSet = callBound('Map.prototype.set', true);
  var $mapHas = callBound('Map.prototype.has', true);
  /*
   * This function traverses the list returning the node corresponding to the
   * given key.
   *
   * That node is also moved to the head of the list, so that if it's accessed
   * again we don't need to traverse the whole list. By doing so, all the recently
   * used nodes can be accessed relatively quickly.
   */

  var listGetNode = function (list, key) {
    // eslint-disable-line consistent-return
    for (var prev = list, curr; (curr = prev.next) !== null; prev = curr) {
      if (curr.key === key) {
        prev.next = curr.next;
        curr.next = list.next;
        list.next = curr; // eslint-disable-line no-param-reassign

        return curr;
      }
    }
  };

  var listGet = function (objects, key) {
    var node = listGetNode(objects, key);
    return node && node.value;
  };

  var listSet = function (objects, key, value) {
    var node = listGetNode(objects, key);

    if (node) {
      node.value = value;
    } else {
      // Prepend the new node to the beginning of the list
      objects.next = {
        // eslint-disable-line no-param-reassign
        key: key,
        next: objects.next,
        value: value
      };
    }
  };

  var listHas = function (objects, key) {
    return !!listGetNode(objects, key);
  };

  var sideChannel = function getSideChannel() {
    var $wm;
    var $m;
    var $o;
    var channel = {
      assert: function (key) {
        if (!channel.has(key)) {
          throw new $TypeError('Side channel does not contain ' + objectInspect(key));
        }
      },
      get: function (key) {
        // eslint-disable-line consistent-return
        if ($WeakMap && key && (typeof key === 'object' || typeof key === 'function')) {
          if ($wm) {
            return $weakMapGet($wm, key);
          }
        } else if ($Map) {
          if ($m) {
            return $mapGet($m, key);
          }
        } else {
          if ($o) {
            // eslint-disable-line no-lonely-if
            return listGet($o, key);
          }
        }
      },
      has: function (key) {
        if ($WeakMap && key && (typeof key === 'object' || typeof key === 'function')) {
          if ($wm) {
            return $weakMapHas($wm, key);
          }
        } else if ($Map) {
          if ($m) {
            return $mapHas($m, key);
          }
        } else {
          if ($o) {
            // eslint-disable-line no-lonely-if
            return listHas($o, key);
          }
        }

        return false;
      },
      set: function (key, value) {
        if ($WeakMap && key && (typeof key === 'object' || typeof key === 'function')) {
          if (!$wm) {
            $wm = new $WeakMap();
          }

          $weakMapSet($wm, key, value);
        } else if ($Map) {
          if (!$m) {
            $m = new $Map();
          }

          $mapSet($m, key, value);
        } else {
          if (!$o) {
            /*
             * Initialize the linked list as an empty node, so that we don't have
             * to special-case handling of the first node: we can always refer to
             * it as (previous node).next, instead of something like (list).head
             */
            $o = {
              key: {},
              next: null
            };
          }

          listSet($o, key, value);
        }
      }
    };
    return channel;
  };

  var replace = String.prototype.replace;
  var percentTwenties = /%20/g;
  var Format = {
    RFC1738: 'RFC1738',
    RFC3986: 'RFC3986'
  };
  var formats = {
    'default': Format.RFC3986,
    formatters: {
      RFC1738: function (value) {
        return replace.call(value, percentTwenties, '+');
      },
      RFC3986: function (value) {
        return String(value);
      }
    },
    RFC1738: Format.RFC1738,
    RFC3986: Format.RFC3986
  };

  var has$5 = Object.prototype.hasOwnProperty;
  var isArray$2 = Array.isArray;

  var hexTable = function () {
    var array = [];

    for (var i = 0; i < 256; ++i) {
      array.push('%' + ((i < 16 ? '0' : '') + i.toString(16)).toUpperCase());
    }

    return array;
  }();

  var compactQueue = function compactQueue(queue) {
    while (queue.length > 1) {
      var item = queue.pop();
      var obj = item.obj[item.prop];

      if (isArray$2(obj)) {
        var compacted = [];

        for (var j = 0; j < obj.length; ++j) {
          if (typeof obj[j] !== 'undefined') {
            compacted.push(obj[j]);
          }
        }

        item.obj[item.prop] = compacted;
      }
    }
  };

  var arrayToObject = function arrayToObject(source, options) {
    var obj = options && options.plainObjects ? Object.create(null) : {};

    for (var i = 0; i < source.length; ++i) {
      if (typeof source[i] !== 'undefined') {
        obj[i] = source[i];
      }
    }

    return obj;
  };

  var merge = function merge(target, source, options) {
    /* eslint no-param-reassign: 0 */
    if (!source) {
      return target;
    }

    if (typeof source !== 'object') {
      if (isArray$2(target)) {
        target.push(source);
      } else if (target && typeof target === 'object') {
        if (options && (options.plainObjects || options.allowPrototypes) || !has$5.call(Object.prototype, source)) {
          target[source] = true;
        }
      } else {
        return [target, source];
      }

      return target;
    }

    if (!target || typeof target !== 'object') {
      return [target].concat(source);
    }

    var mergeTarget = target;

    if (isArray$2(target) && !isArray$2(source)) {
      mergeTarget = arrayToObject(target, options);
    }

    if (isArray$2(target) && isArray$2(source)) {
      source.forEach(function (item, i) {
        if (has$5.call(target, i)) {
          var targetItem = target[i];

          if (targetItem && typeof targetItem === 'object' && item && typeof item === 'object') {
            target[i] = merge(targetItem, item, options);
          } else {
            target.push(item);
          }
        } else {
          target[i] = item;
        }
      });
      return target;
    }

    return Object.keys(source).reduce(function (acc, key) {
      var value = source[key];

      if (has$5.call(acc, key)) {
        acc[key] = merge(acc[key], value, options);
      } else {
        acc[key] = value;
      }

      return acc;
    }, mergeTarget);
  };

  var assign = function assignSingleSource(target, source) {
    return Object.keys(source).reduce(function (acc, key) {
      acc[key] = source[key];
      return acc;
    }, target);
  };

  var decode$1 = function (str, decoder, charset) {
    var strWithoutPlus = str.replace(/\+/g, ' ');

    if (charset === 'iso-8859-1') {
      // unescape never throws, no try...catch needed:
      return strWithoutPlus.replace(/%[0-9a-f]{2}/gi, unescape);
    } // utf-8


    try {
      return decodeURIComponent(strWithoutPlus);
    } catch (e) {
      return strWithoutPlus;
    }
  };

  var encode$1 = function encode(str, defaultEncoder, charset, kind, format) {
    // This code was originally written by Brian White (mscdex) for the io.js core querystring library.
    // It has been adapted here for stricter adherence to RFC 3986
    if (str.length === 0) {
      return str;
    }

    var string = str;

    if (typeof str === 'symbol') {
      string = Symbol.prototype.toString.call(str);
    } else if (typeof str !== 'string') {
      string = String(str);
    }

    if (charset === 'iso-8859-1') {
      return escape(string).replace(/%u[0-9a-f]{4}/gi, function ($0) {
        return '%26%23' + parseInt($0.slice(2), 16) + '%3B';
      });
    }

    var out = '';

    for (var i = 0; i < string.length; ++i) {
      var c = string.charCodeAt(i);

      if (c === 0x2D // -
      || c === 0x2E // .
      || c === 0x5F // _
      || c === 0x7E // ~
      || c >= 0x30 && c <= 0x39 // 0-9
      || c >= 0x41 && c <= 0x5A // a-z
      || c >= 0x61 && c <= 0x7A // A-Z
      || format === formats.RFC1738 && (c === 0x28 || c === 0x29) // ( )
      ) {
        out += string.charAt(i);
        continue;
      }

      if (c < 0x80) {
        out = out + hexTable[c];
        continue;
      }

      if (c < 0x800) {
        out = out + (hexTable[0xC0 | c >> 6] + hexTable[0x80 | c & 0x3F]);
        continue;
      }

      if (c < 0xD800 || c >= 0xE000) {
        out = out + (hexTable[0xE0 | c >> 12] + hexTable[0x80 | c >> 6 & 0x3F] + hexTable[0x80 | c & 0x3F]);
        continue;
      }

      i += 1;
      c = 0x10000 + ((c & 0x3FF) << 10 | string.charCodeAt(i) & 0x3FF);
      out += hexTable[0xF0 | c >> 18] + hexTable[0x80 | c >> 12 & 0x3F] + hexTable[0x80 | c >> 6 & 0x3F] + hexTable[0x80 | c & 0x3F];
    }

    return out;
  };

  var compact$1 = function compact(value) {
    var queue = [{
      obj: {
        o: value
      },
      prop: 'o'
    }];
    var refs = [];

    for (var i = 0; i < queue.length; ++i) {
      var item = queue[i];
      var obj = item.obj[item.prop];
      var keys = Object.keys(obj);

      for (var j = 0; j < keys.length; ++j) {
        var key = keys[j];
        var val = obj[key];

        if (typeof val === 'object' && val !== null && refs.indexOf(val) === -1) {
          queue.push({
            obj: obj,
            prop: key
          });
          refs.push(val);
        }
      }
    }

    compactQueue(queue);
    return value;
  };

  var isRegExp = function isRegExp(obj) {
    return Object.prototype.toString.call(obj) === '[object RegExp]';
  };

  var isBuffer = function isBuffer(obj) {
    if (!obj || typeof obj !== 'object') {
      return false;
    }

    return !!(obj.constructor && obj.constructor.isBuffer && obj.constructor.isBuffer(obj));
  };

  var combine = function combine(a, b) {
    return [].concat(a, b);
  };

  var maybeMap = function maybeMap(val, fn) {
    if (isArray$2(val)) {
      var mapped = [];

      for (var i = 0; i < val.length; i += 1) {
        mapped.push(fn(val[i]));
      }

      return mapped;
    }

    return fn(val);
  };

  var utils = {
    arrayToObject: arrayToObject,
    assign: assign,
    combine: combine,
    compact: compact$1,
    decode: decode$1,
    encode: encode$1,
    isBuffer: isBuffer,
    isRegExp: isRegExp,
    maybeMap: maybeMap,
    merge: merge
  };

  var has$4 = Object.prototype.hasOwnProperty;
  var arrayPrefixGenerators = {
    brackets: function brackets(prefix) {
      return prefix + '[]';
    },
    comma: 'comma',
    indices: function indices(prefix, key) {
      return prefix + '[' + key + ']';
    },
    repeat: function repeat(prefix) {
      return prefix;
    }
  };
  var isArray$1 = Array.isArray;
  var push$1 = Array.prototype.push;

  var pushToArray = function (arr, valueOrArray) {
    push$1.apply(arr, isArray$1(valueOrArray) ? valueOrArray : [valueOrArray]);
  };

  var toISO = Date.prototype.toISOString;
  var defaultFormat = formats['default'];
  var defaults$1 = {
    addQueryPrefix: false,
    allowDots: false,
    charset: 'utf-8',
    charsetSentinel: false,
    delimiter: '&',
    encode: true,
    encoder: utils.encode,
    encodeValuesOnly: false,
    format: defaultFormat,
    formatter: formats.formatters[defaultFormat],
    // deprecated
    indices: false,
    serializeDate: function serializeDate(date) {
      return toISO.call(date);
    },
    skipNulls: false,
    strictNullHandling: false
  };

  var isNonNullishPrimitive = function isNonNullishPrimitive(v) {
    return typeof v === 'string' || typeof v === 'number' || typeof v === 'boolean' || typeof v === 'symbol' || typeof v === 'bigint';
  };

  var stringify$1 = function stringify(object, prefix, generateArrayPrefix, strictNullHandling, skipNulls, encoder, filter, sort, allowDots, serializeDate, format, formatter, encodeValuesOnly, charset, sideChannel$1) {
    var obj = object;

    if (sideChannel$1.has(object)) {
      throw new RangeError('Cyclic object value');
    }

    if (typeof filter === 'function') {
      obj = filter(prefix, obj);
    } else if (obj instanceof Date) {
      obj = serializeDate(obj);
    } else if (generateArrayPrefix === 'comma' && isArray$1(obj)) {
      obj = utils.maybeMap(obj, function (value) {
        if (value instanceof Date) {
          return serializeDate(value);
        }

        return value;
      });
    }

    if (obj === null) {
      if (strictNullHandling) {
        return encoder && !encodeValuesOnly ? encoder(prefix, defaults$1.encoder, charset, 'key', format) : prefix;
      }

      obj = '';
    }

    if (isNonNullishPrimitive(obj) || utils.isBuffer(obj)) {
      if (encoder) {
        var keyValue = encodeValuesOnly ? prefix : encoder(prefix, defaults$1.encoder, charset, 'key', format);
        return [formatter(keyValue) + '=' + formatter(encoder(obj, defaults$1.encoder, charset, 'value', format))];
      }

      return [formatter(prefix) + '=' + formatter(String(obj))];
    }

    var values = [];

    if (typeof obj === 'undefined') {
      return values;
    }

    var objKeys;

    if (generateArrayPrefix === 'comma' && isArray$1(obj)) {
      // we need to join elements in
      objKeys = [{
        value: obj.length > 0 ? obj.join(',') || null : undefined
      }];
    } else if (isArray$1(filter)) {
      objKeys = filter;
    } else {
      var keys = Object.keys(obj);
      objKeys = sort ? keys.sort(sort) : keys;
    }

    for (var i = 0; i < objKeys.length; ++i) {
      var key = objKeys[i];
      var value = typeof key === 'object' && key.value !== undefined ? key.value : obj[key];

      if (skipNulls && value === null) {
        continue;
      }

      var keyPrefix = isArray$1(obj) ? typeof generateArrayPrefix === 'function' ? generateArrayPrefix(prefix, key) : prefix : prefix + (allowDots ? '.' + key : '[' + key + ']');
      sideChannel$1.set(object, true);
      var valueSideChannel = sideChannel();
      pushToArray(values, stringify(value, keyPrefix, generateArrayPrefix, strictNullHandling, skipNulls, encoder, filter, sort, allowDots, serializeDate, format, formatter, encodeValuesOnly, charset, valueSideChannel));
    }

    return values;
  };

  var normalizeStringifyOptions = function normalizeStringifyOptions(opts) {
    if (!opts) {
      return defaults$1;
    }

    if (opts.encoder !== null && opts.encoder !== undefined && typeof opts.encoder !== 'function') {
      throw new TypeError('Encoder has to be a function.');
    }

    var charset = opts.charset || defaults$1.charset;

    if (typeof opts.charset !== 'undefined' && opts.charset !== 'utf-8' && opts.charset !== 'iso-8859-1') {
      throw new TypeError('The charset option must be either utf-8, iso-8859-1, or undefined');
    }

    var format = formats['default'];

    if (typeof opts.format !== 'undefined') {
      if (!has$4.call(formats.formatters, opts.format)) {
        throw new TypeError('Unknown format option provided.');
      }

      format = opts.format;
    }

    var formatter = formats.formatters[format];
    var filter = defaults$1.filter;

    if (typeof opts.filter === 'function' || isArray$1(opts.filter)) {
      filter = opts.filter;
    }

    return {
      addQueryPrefix: typeof opts.addQueryPrefix === 'boolean' ? opts.addQueryPrefix : defaults$1.addQueryPrefix,
      allowDots: typeof opts.allowDots === 'undefined' ? defaults$1.allowDots : !!opts.allowDots,
      charset: charset,
      charsetSentinel: typeof opts.charsetSentinel === 'boolean' ? opts.charsetSentinel : defaults$1.charsetSentinel,
      delimiter: typeof opts.delimiter === 'undefined' ? defaults$1.delimiter : opts.delimiter,
      encode: typeof opts.encode === 'boolean' ? opts.encode : defaults$1.encode,
      encoder: typeof opts.encoder === 'function' ? opts.encoder : defaults$1.encoder,
      encodeValuesOnly: typeof opts.encodeValuesOnly === 'boolean' ? opts.encodeValuesOnly : defaults$1.encodeValuesOnly,
      filter: filter,
      format: format,
      formatter: formatter,
      serializeDate: typeof opts.serializeDate === 'function' ? opts.serializeDate : defaults$1.serializeDate,
      skipNulls: typeof opts.skipNulls === 'boolean' ? opts.skipNulls : defaults$1.skipNulls,
      sort: typeof opts.sort === 'function' ? opts.sort : null,
      strictNullHandling: typeof opts.strictNullHandling === 'boolean' ? opts.strictNullHandling : defaults$1.strictNullHandling
    };
  };

  var stringify_1 = function (object, opts) {
    var obj = object;
    var options = normalizeStringifyOptions(opts);
    var objKeys;
    var filter;

    if (typeof options.filter === 'function') {
      filter = options.filter;
      obj = filter('', obj);
    } else if (isArray$1(options.filter)) {
      filter = options.filter;
      objKeys = filter;
    }

    var keys = [];

    if (typeof obj !== 'object' || obj === null) {
      return '';
    }

    var arrayFormat;

    if (opts && opts.arrayFormat in arrayPrefixGenerators) {
      arrayFormat = opts.arrayFormat;
    } else if (opts && 'indices' in opts) {
      arrayFormat = opts.indices ? 'indices' : 'repeat';
    } else {
      arrayFormat = 'indices';
    }

    var generateArrayPrefix = arrayPrefixGenerators[arrayFormat];

    if (!objKeys) {
      objKeys = Object.keys(obj);
    }

    if (options.sort) {
      objKeys.sort(options.sort);
    }

    var sideChannel$1 = sideChannel();

    for (var i = 0; i < objKeys.length; ++i) {
      var key = objKeys[i];

      if (options.skipNulls && obj[key] === null) {
        continue;
      }

      pushToArray(keys, stringify$1(obj[key], key, generateArrayPrefix, options.strictNullHandling, options.skipNulls, options.encode ? options.encoder : null, options.filter, options.sort, options.allowDots, options.serializeDate, options.format, options.formatter, options.encodeValuesOnly, options.charset, sideChannel$1));
    }

    var joined = keys.join(options.delimiter);
    var prefix = options.addQueryPrefix === true ? '?' : '';

    if (options.charsetSentinel) {
      if (options.charset === 'iso-8859-1') {
        // encodeURIComponent('&#10003;'), the "numeric entity" representation of a checkmark
        prefix += 'utf8=%26%2310003%3B&';
      } else {
        // encodeURIComponent('✓')
        prefix += 'utf8=%E2%9C%93&';
      }
    }

    return joined.length > 0 ? prefix + joined : '';
  };

  var has$3 = Object.prototype.hasOwnProperty;
  var isArray = Array.isArray;
  var defaults = {
    allowDots: false,
    allowPrototypes: false,
    allowSparse: false,
    arrayLimit: 20,
    charset: 'utf-8',
    charsetSentinel: false,
    comma: false,
    decoder: utils.decode,
    delimiter: '&',
    depth: 5,
    ignoreQueryPrefix: false,
    interpretNumericEntities: false,
    parameterLimit: 1000,
    parseArrays: true,
    plainObjects: false,
    strictNullHandling: false
  };

  var interpretNumericEntities = function (str) {
    return str.replace(/&#(\d+);/g, function ($0, numberStr) {
      return String.fromCharCode(parseInt(numberStr, 10));
    });
  };

  var parseArrayValue = function (val, options) {
    if (val && typeof val === 'string' && options.comma && val.indexOf(',') > -1) {
      return val.split(',');
    }

    return val;
  }; // This is what browsers will submit when the ✓ character occurs in an
  // application/x-www-form-urlencoded body and the encoding of the page containing
  // the form is iso-8859-1, or when the submitted form has an accept-charset
  // attribute of iso-8859-1. Presumably also with other charsets that do not contain
  // the ✓ character, such as us-ascii.


  var isoSentinel = 'utf8=%26%2310003%3B'; // encodeURIComponent('&#10003;')
  // These are the percent-encoded utf-8 octets representing a checkmark, indicating that the request actually is utf-8 encoded.

  var charsetSentinel = 'utf8=%E2%9C%93'; // encodeURIComponent('✓')

  var parseValues = function parseQueryStringValues(str, options) {
    var obj = {};
    var cleanStr = options.ignoreQueryPrefix ? str.replace(/^\?/, '') : str;
    var limit = options.parameterLimit === Infinity ? undefined : options.parameterLimit;
    var parts = cleanStr.split(options.delimiter, limit);
    var skipIndex = -1; // Keep track of where the utf8 sentinel was found

    var i;
    var charset = options.charset;

    if (options.charsetSentinel) {
      for (i = 0; i < parts.length; ++i) {
        if (parts[i].indexOf('utf8=') === 0) {
          if (parts[i] === charsetSentinel) {
            charset = 'utf-8';
          } else if (parts[i] === isoSentinel) {
            charset = 'iso-8859-1';
          }

          skipIndex = i;
          i = parts.length; // The eslint settings do not allow break;
        }
      }
    }

    for (i = 0; i < parts.length; ++i) {
      if (i === skipIndex) {
        continue;
      }

      var part = parts[i];
      var bracketEqualsPos = part.indexOf(']=');
      var pos = bracketEqualsPos === -1 ? part.indexOf('=') : bracketEqualsPos + 1;
      var key, val;

      if (pos === -1) {
        key = options.decoder(part, defaults.decoder, charset, 'key');
        val = options.strictNullHandling ? null : '';
      } else {
        key = options.decoder(part.slice(0, pos), defaults.decoder, charset, 'key');
        val = utils.maybeMap(parseArrayValue(part.slice(pos + 1), options), function (encodedVal) {
          return options.decoder(encodedVal, defaults.decoder, charset, 'value');
        });
      }

      if (val && options.interpretNumericEntities && charset === 'iso-8859-1') {
        val = interpretNumericEntities(val);
      }

      if (part.indexOf('[]=') > -1) {
        val = isArray(val) ? [val] : val;
      }

      if (has$3.call(obj, key)) {
        obj[key] = utils.combine(obj[key], val);
      } else {
        obj[key] = val;
      }
    }

    return obj;
  };

  var parseObject = function (chain, val, options, valuesParsed) {
    var leaf = valuesParsed ? val : parseArrayValue(val, options);

    for (var i = chain.length - 1; i >= 0; --i) {
      var obj;
      var root = chain[i];

      if (root === '[]' && options.parseArrays) {
        obj = [].concat(leaf);
      } else {
        obj = options.plainObjects ? Object.create(null) : {};
        var cleanRoot = root.charAt(0) === '[' && root.charAt(root.length - 1) === ']' ? root.slice(1, -1) : root;
        var index = parseInt(cleanRoot, 10);

        if (!options.parseArrays && cleanRoot === '') {
          obj = {
            0: leaf
          };
        } else if (!isNaN(index) && root !== cleanRoot && String(index) === cleanRoot && index >= 0 && options.parseArrays && index <= options.arrayLimit) {
          obj = [];
          obj[index] = leaf;
        } else {
          obj[cleanRoot] = leaf;
        }
      }

      leaf = obj;
    }

    return leaf;
  };

  var parseKeys = function parseQueryStringKeys(givenKey, val, options, valuesParsed) {
    if (!givenKey) {
      return;
    } // Transform dot notation to bracket notation


    var key = options.allowDots ? givenKey.replace(/\.([^.[]+)/g, '[$1]') : givenKey; // The regex chunks

    var brackets = /(\[[^[\]]*])/;
    var child = /(\[[^[\]]*])/g; // Get the parent

    var segment = options.depth > 0 && brackets.exec(key);
    var parent = segment ? key.slice(0, segment.index) : key; // Stash the parent if it exists

    var keys = [];

    if (parent) {
      // If we aren't using plain objects, optionally prefix keys that would overwrite object prototype properties
      if (!options.plainObjects && has$3.call(Object.prototype, parent)) {
        if (!options.allowPrototypes) {
          return;
        }
      }

      keys.push(parent);
    } // Loop through children appending to the array until we hit depth


    var i = 0;

    while (options.depth > 0 && (segment = child.exec(key)) !== null && i < options.depth) {
      i += 1;

      if (!options.plainObjects && has$3.call(Object.prototype, segment[1].slice(1, -1))) {
        if (!options.allowPrototypes) {
          return;
        }
      }

      keys.push(segment[1]);
    } // If there's a remainder, just add whatever is left


    if (segment) {
      keys.push('[' + key.slice(segment.index) + ']');
    }

    return parseObject(keys, val, options, valuesParsed);
  };

  var normalizeParseOptions = function normalizeParseOptions(opts) {
    if (!opts) {
      return defaults;
    }

    if (opts.decoder !== null && opts.decoder !== undefined && typeof opts.decoder !== 'function') {
      throw new TypeError('Decoder has to be a function.');
    }

    if (typeof opts.charset !== 'undefined' && opts.charset !== 'utf-8' && opts.charset !== 'iso-8859-1') {
      throw new TypeError('The charset option must be either utf-8, iso-8859-1, or undefined');
    }

    var charset = typeof opts.charset === 'undefined' ? defaults.charset : opts.charset;
    return {
      allowDots: typeof opts.allowDots === 'undefined' ? defaults.allowDots : !!opts.allowDots,
      allowPrototypes: typeof opts.allowPrototypes === 'boolean' ? opts.allowPrototypes : defaults.allowPrototypes,
      allowSparse: typeof opts.allowSparse === 'boolean' ? opts.allowSparse : defaults.allowSparse,
      arrayLimit: typeof opts.arrayLimit === 'number' ? opts.arrayLimit : defaults.arrayLimit,
      charset: charset,
      charsetSentinel: typeof opts.charsetSentinel === 'boolean' ? opts.charsetSentinel : defaults.charsetSentinel,
      comma: typeof opts.comma === 'boolean' ? opts.comma : defaults.comma,
      decoder: typeof opts.decoder === 'function' ? opts.decoder : defaults.decoder,
      delimiter: typeof opts.delimiter === 'string' || utils.isRegExp(opts.delimiter) ? opts.delimiter : defaults.delimiter,
      // eslint-disable-next-line no-implicit-coercion, no-extra-parens
      depth: typeof opts.depth === 'number' || opts.depth === false ? +opts.depth : defaults.depth,
      ignoreQueryPrefix: opts.ignoreQueryPrefix === true,
      interpretNumericEntities: typeof opts.interpretNumericEntities === 'boolean' ? opts.interpretNumericEntities : defaults.interpretNumericEntities,
      parameterLimit: typeof opts.parameterLimit === 'number' ? opts.parameterLimit : defaults.parameterLimit,
      parseArrays: opts.parseArrays !== false,
      plainObjects: typeof opts.plainObjects === 'boolean' ? opts.plainObjects : defaults.plainObjects,
      strictNullHandling: typeof opts.strictNullHandling === 'boolean' ? opts.strictNullHandling : defaults.strictNullHandling
    };
  };

  var parse$1 = function (str, opts) {
    var options = normalizeParseOptions(opts);

    if (str === '' || str === null || typeof str === 'undefined') {
      return options.plainObjects ? Object.create(null) : {};
    }

    var tempObj = typeof str === 'string' ? parseValues(str, options) : str;
    var obj = options.plainObjects ? Object.create(null) : {}; // Iterate over the keys and setup the new object

    var keys = Object.keys(tempObj);

    for (var i = 0; i < keys.length; ++i) {
      var key = keys[i];
      var newObj = parseKeys(key, tempObj[key], options, typeof str === 'string');
      obj = utils.merge(obj, newObj, options);
    }

    if (options.allowSparse === true) {
      return obj;
    }

    return utils.compact(obj);
  };

  var lib = {
    formats: formats,
    parse: parse$1,
    stringify: stringify_1
  };

  /*
   *  base64.js
   *
   *  Licensed under the BSD 3-Clause License.
   *    http://opensource.org/licenses/BSD-3-Clause
   *
   *  References:
   *    http://en.wikipedia.org/wiki/Base64
   */

  var base64 = createCommonjsModule(function (module, exports) {

  (function (global, factory) {
    module.exports = factory(global) ;
  })(typeof self !== 'undefined' ? self : typeof window !== 'undefined' ? window : typeof commonjsGlobal !== 'undefined' ? commonjsGlobal : commonjsGlobal, function (global) {

    global = global || {};
    var _Base64 = global.Base64;
    var version = "2.6.4"; // constants

    var b64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

    var b64tab = function (bin) {
      var t = {};

      for (var i = 0, l = bin.length; i < l; i++) t[bin.charAt(i)] = i;

      return t;
    }(b64chars);

    var fromCharCode = String.fromCharCode; // encoder stuff

    var cb_utob = function (c) {
      if (c.length < 2) {
        var cc = c.charCodeAt(0);
        return cc < 0x80 ? c : cc < 0x800 ? fromCharCode(0xc0 | cc >>> 6) + fromCharCode(0x80 | cc & 0x3f) : fromCharCode(0xe0 | cc >>> 12 & 0x0f) + fromCharCode(0x80 | cc >>> 6 & 0x3f) + fromCharCode(0x80 | cc & 0x3f);
      } else {
        var cc = 0x10000 + (c.charCodeAt(0) - 0xD800) * 0x400 + (c.charCodeAt(1) - 0xDC00);
        return fromCharCode(0xf0 | cc >>> 18 & 0x07) + fromCharCode(0x80 | cc >>> 12 & 0x3f) + fromCharCode(0x80 | cc >>> 6 & 0x3f) + fromCharCode(0x80 | cc & 0x3f);
      }
    };

    var re_utob = /[\uD800-\uDBFF][\uDC00-\uDFFFF]|[^\x00-\x7F]/g;

    var utob = function (u) {
      return u.replace(re_utob, cb_utob);
    };

    var cb_encode = function (ccc) {
      var padlen = [0, 2, 1][ccc.length % 3],
          ord = ccc.charCodeAt(0) << 16 | (ccc.length > 1 ? ccc.charCodeAt(1) : 0) << 8 | (ccc.length > 2 ? ccc.charCodeAt(2) : 0),
          chars = [b64chars.charAt(ord >>> 18), b64chars.charAt(ord >>> 12 & 63), padlen >= 2 ? '=' : b64chars.charAt(ord >>> 6 & 63), padlen >= 1 ? '=' : b64chars.charAt(ord & 63)];
      return chars.join('');
    };

    var btoa = global.btoa && typeof global.btoa == 'function' ? function (b) {
      return global.btoa(b);
    } : function (b) {
      if (b.match(/[^\x00-\xFF]/)) throw new RangeError('The string contains invalid characters.');
      return b.replace(/[\s\S]{1,3}/g, cb_encode);
    };

    var _encode = function (u) {
      return btoa(utob(String(u)));
    };

    var mkUriSafe = function (b64) {
      return b64.replace(/[+\/]/g, function (m0) {
        return m0 == '+' ? '-' : '_';
      }).replace(/=/g, '');
    };

    var encode = function (u, urisafe) {
      return urisafe ? mkUriSafe(_encode(u)) : _encode(u);
    };

    var encodeURI = function (u) {
      return encode(u, true);
    };

    var fromUint8Array;
    if (global.Uint8Array) fromUint8Array = function (a, urisafe) {
      // return btoa(fromCharCode.apply(null, a));
      var b64 = '';

      for (var i = 0, l = a.length; i < l; i += 3) {
        var a0 = a[i],
            a1 = a[i + 1],
            a2 = a[i + 2];
        var ord = a0 << 16 | a1 << 8 | a2;
        b64 += b64chars.charAt(ord >>> 18) + b64chars.charAt(ord >>> 12 & 63) + (typeof a1 != 'undefined' ? b64chars.charAt(ord >>> 6 & 63) : '=') + (typeof a2 != 'undefined' ? b64chars.charAt(ord & 63) : '=');
      }

      return urisafe ? mkUriSafe(b64) : b64;
    }; // decoder stuff

    var re_btou = /[\xC0-\xDF][\x80-\xBF]|[\xE0-\xEF][\x80-\xBF]{2}|[\xF0-\xF7][\x80-\xBF]{3}/g;

    var cb_btou = function (cccc) {
      switch (cccc.length) {
        case 4:
          var cp = (0x07 & cccc.charCodeAt(0)) << 18 | (0x3f & cccc.charCodeAt(1)) << 12 | (0x3f & cccc.charCodeAt(2)) << 6 | 0x3f & cccc.charCodeAt(3),
              offset = cp - 0x10000;
          return fromCharCode((offset >>> 10) + 0xD800) + fromCharCode((offset & 0x3FF) + 0xDC00);

        case 3:
          return fromCharCode((0x0f & cccc.charCodeAt(0)) << 12 | (0x3f & cccc.charCodeAt(1)) << 6 | 0x3f & cccc.charCodeAt(2));

        default:
          return fromCharCode((0x1f & cccc.charCodeAt(0)) << 6 | 0x3f & cccc.charCodeAt(1));
      }
    };

    var btou = function (b) {
      return b.replace(re_btou, cb_btou);
    };

    var cb_decode = function (cccc) {
      var len = cccc.length,
          padlen = len % 4,
          n = (len > 0 ? b64tab[cccc.charAt(0)] << 18 : 0) | (len > 1 ? b64tab[cccc.charAt(1)] << 12 : 0) | (len > 2 ? b64tab[cccc.charAt(2)] << 6 : 0) | (len > 3 ? b64tab[cccc.charAt(3)] : 0),
          chars = [fromCharCode(n >>> 16), fromCharCode(n >>> 8 & 0xff), fromCharCode(n & 0xff)];
      chars.length -= [0, 0, 2, 1][padlen];
      return chars.join('');
    };

    var _atob = global.atob && typeof global.atob == 'function' ? function (a) {
      return global.atob(a);
    } : function (a) {
      return a.replace(/\S{1,4}/g, cb_decode);
    };

    var atob = function (a) {
      return _atob(String(a).replace(/[^A-Za-z0-9\+\/]/g, ''));
    };

    var _decode = function (a) {
      return btou(_atob(a));
    };

    var _fromURI = function (a) {
      return String(a).replace(/[-_]/g, function (m0) {
        return m0 == '-' ? '+' : '/';
      }).replace(/[^A-Za-z0-9\+\/]/g, '');
    };

    var decode = function (a) {
      return _decode(_fromURI(a));
    };

    var toUint8Array;
    if (global.Uint8Array) toUint8Array = function (a) {
      return Uint8Array.from(atob(_fromURI(a)), function (c) {
        return c.charCodeAt(0);
      });
    };

    var noConflict = function () {
      var Base64 = global.Base64;
      global.Base64 = _Base64;
      return Base64;
    }; // export Base64


    global.Base64 = {
      VERSION: version,
      atob: atob,
      btoa: btoa,
      fromBase64: decode,
      toBase64: encode,
      utob: utob,
      encode: encode,
      encodeURI: encodeURI,
      btou: btou,
      decode: decode,
      noConflict: noConflict,
      fromUint8Array: fromUint8Array,
      toUint8Array: toUint8Array
    }; // if ES5 is available, make Base64.extendString() available

    if (typeof Object.defineProperty === 'function') {
      var noEnum = function (v) {
        return {
          value: v,
          enumerable: false,
          writable: true,
          configurable: true
        };
      };

      global.Base64.extendString = function () {
        Object.defineProperty(String.prototype, 'fromBase64', noEnum(function () {
          return decode(this);
        }));
        Object.defineProperty(String.prototype, 'toBase64', noEnum(function (urisafe) {
          return encode(this, urisafe);
        }));
        Object.defineProperty(String.prototype, 'toBase64URI', noEnum(function () {
          return encode(this, true);
        }));
      };
    } //
    // export Base64 to the namespace
    //


    if (global['Meteor']) {
      // Meteor.js
      Base64 = global.Base64;
    } // module.exports and AMD are mutually exclusive.
    // module.exports has precedence.


    if (module.exports) {
      module.exports.Base64 = global.Base64;
    } // that's it!


    return {
      Base64: global.Base64
    };
  });
  });

  /**
   * Check if we're required to add a port number.
   *
   * @see https://url.spec.whatwg.org/#default-port
   * @param {Number|String} port Port number we need to check
   * @param {String} protocol Protocol we need to check against.
   * @returns {Boolean} Is it a default port for the given protocol
   * @api private
   */

  var requiresPort = function required(port, protocol) {
    protocol = protocol.split(':')[0];
    port = +port;
    if (!port) return false;

    switch (protocol) {
      case 'http':
      case 'ws':
        return port !== 80;

      case 'https':
      case 'wss':
        return port !== 443;

      case 'ftp':
        return port !== 21;

      case 'gopher':
        return port !== 70;

      case 'file':
        return false;
    }

    return port !== 0;
  };

  var has$2 = Object.prototype.hasOwnProperty,
      undef;
  /**
   * Decode a URI encoded string.
   *
   * @param {String} input The URI encoded string.
   * @returns {String|Null} The decoded string.
   * @api private
   */

  function decode(input) {
    try {
      return decodeURIComponent(input.replace(/\+/g, ' '));
    } catch (e) {
      return null;
    }
  }
  /**
   * Attempts to encode a given input.
   *
   * @param {String} input The string that needs to be encoded.
   * @returns {String|Null} The encoded string.
   * @api private
   */


  function encode(input) {
    try {
      return encodeURIComponent(input);
    } catch (e) {
      return null;
    }
  }
  /**
   * Simple query string parser.
   *
   * @param {String} query The query string that needs to be parsed.
   * @returns {Object}
   * @api public
   */


  function querystring(query) {
    var parser = /([^=?#&]+)=?([^&]*)/g,
        result = {},
        part;

    while (part = parser.exec(query)) {
      var key = decode(part[1]),
          value = decode(part[2]); //
      // Prevent overriding of existing properties. This ensures that build-in
      // methods like `toString` or __proto__ are not overriden by malicious
      // querystrings.
      //
      // In the case if failed decoding, we want to omit the key/value pairs
      // from the result.
      //

      if (key === null || value === null || key in result) continue;
      result[key] = value;
    }

    return result;
  }
  /**
   * Transform a query string to an object.
   *
   * @param {Object} obj Object that should be transformed.
   * @param {String} prefix Optional prefix.
   * @returns {String}
   * @api public
   */


  function querystringify(obj, prefix) {
    prefix = prefix || '';
    var pairs = [],
        value,
        key; //
    // Optionally prefix with a '?' if needed
    //

    if ('string' !== typeof prefix) prefix = '?';

    for (key in obj) {
      if (has$2.call(obj, key)) {
        value = obj[key]; //
        // Edge cases where we actually want to encode the value to an empty
        // string instead of the stringified value.
        //

        if (!value && (value === null || value === undef || isNaN(value))) {
          value = '';
        }

        key = encode(key);
        value = encode(value); //
        // If we failed to encode the strings, we should bail out as we don't
        // want to add invalid strings to the query.
        //

        if (key === null || value === null) continue;
        pairs.push(key + '=' + value);
      }
    }

    return pairs.length ? prefix + pairs.join('&') : '';
  } //
  // Expose the module.
  //


  var stringify = querystringify;
  var parse = querystring;

  var querystringify_1 = {
  	stringify: stringify,
  	parse: parse
  };

  var slashes = /^[A-Za-z][A-Za-z0-9+-.]*:\/\//,
      protocolre = /^([a-z][a-z0-9.+-]*:)?(\/\/)?([\\/]+)?([\S\s]*)/i,
      windowsDriveLetter = /^[a-zA-Z]:/,
      whitespace$1 = '[\\x09\\x0A\\x0B\\x0C\\x0D\\x20\\xA0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u202F\\u205F\\u3000\\u2028\\u2029\\uFEFF]',
      left = new RegExp('^' + whitespace$1 + '+');
  /**
   * Trim a given string.
   *
   * @param {String} str String to trim.
   * @public
   */


  function trimLeft(str) {
    return (str ? str : '').toString().replace(left, '');
  }
  /**
   * These are the parse rules for the URL parser, it informs the parser
   * about:
   *
   * 0. The char it Needs to parse, if it's a string it should be done using
   *    indexOf, RegExp using exec and NaN means set as current value.
   * 1. The property we should set when parsing this value.
   * 2. Indication if it's backwards or forward parsing, when set as number it's
   *    the value of extra chars that should be split off.
   * 3. Inherit from location if non existing in the parser.
   * 4. `toLowerCase` the resulting value.
   */


  var rules = [['#', 'hash'], // Extract from the back.
  ['?', 'query'], // Extract from the back.
  function sanitize(address, url) {
    // Sanitize what is left of the address
    return isSpecial(url.protocol) ? address.replace(/\\/g, '/') : address;
  }, ['/', 'pathname'], // Extract from the back.
  ['@', 'auth', 1], // Extract from the front.
  [NaN, 'host', undefined, 1, 1], // Set left over value.
  [/:(\d+)$/, 'port', undefined, 1], // RegExp the back.
  [NaN, 'hostname', undefined, 1, 1] // Set left over.
  ];
  /**
   * These properties should not be copied or inherited from. This is only needed
   * for all non blob URL's as a blob URL does not include a hash, only the
   * origin.
   *
   * @type {Object}
   * @private
   */

  var ignore = {
    hash: 1,
    query: 1
  };
  /**
   * The location object differs when your code is loaded through a normal page,
   * Worker or through a worker using a blob. And with the blobble begins the
   * trouble as the location object will contain the URL of the blob, not the
   * location of the page where our code is loaded in. The actual origin is
   * encoded in the `pathname` so we can thankfully generate a good "default"
   * location from it so we can generate proper relative URL's again.
   *
   * @param {Object|String} loc Optional default location object.
   * @returns {Object} lolcation object.
   * @public
   */

  function lolcation(loc) {
    var globalVar;
    if (typeof window !== 'undefined') globalVar = window;else if (typeof commonjsGlobal !== 'undefined') globalVar = commonjsGlobal;else if (typeof self !== 'undefined') globalVar = self;else globalVar = {};
    var location = globalVar.location || {};
    loc = loc || location;
    var finaldestination = {},
        type = typeof loc,
        key;

    if ('blob:' === loc.protocol) {
      finaldestination = new Url(unescape(loc.pathname), {});
    } else if ('string' === type) {
      finaldestination = new Url(loc, {});

      for (key in ignore) delete finaldestination[key];
    } else if ('object' === type) {
      for (key in loc) {
        if (key in ignore) continue;
        finaldestination[key] = loc[key];
      }

      if (finaldestination.slashes === undefined) {
        finaldestination.slashes = slashes.test(loc.href);
      }
    }

    return finaldestination;
  }
  /**
   * Check whether a protocol scheme is special.
   *
   * @param {String} The protocol scheme of the URL
   * @return {Boolean} `true` if the protocol scheme is special, else `false`
   * @private
   */


  function isSpecial(scheme) {
    return scheme === 'file:' || scheme === 'ftp:' || scheme === 'http:' || scheme === 'https:' || scheme === 'ws:' || scheme === 'wss:';
  }
  /**
   * @typedef ProtocolExtract
   * @type Object
   * @property {String} protocol Protocol matched in the URL, in lowercase.
   * @property {Boolean} slashes `true` if protocol is followed by "//", else `false`.
   * @property {String} rest Rest of the URL that is not part of the protocol.
   */

  /**
   * Extract protocol information from a URL with/without double slash ("//").
   *
   * @param {String} address URL we want to extract from.
   * @param {Object} location
   * @return {ProtocolExtract} Extracted information.
   * @private
   */


  function extractProtocol(address, location) {
    address = trimLeft(address);
    location = location || {};
    var match = protocolre.exec(address);
    var protocol = match[1] ? match[1].toLowerCase() : '';
    var forwardSlashes = !!match[2];
    var otherSlashes = !!match[3];
    var slashesCount = 0;
    var rest;

    if (forwardSlashes) {
      if (otherSlashes) {
        rest = match[2] + match[3] + match[4];
        slashesCount = match[2].length + match[3].length;
      } else {
        rest = match[2] + match[4];
        slashesCount = match[2].length;
      }
    } else {
      if (otherSlashes) {
        rest = match[3] + match[4];
        slashesCount = match[3].length;
      } else {
        rest = match[4];
      }
    }

    if (protocol === 'file:') {
      if (slashesCount >= 2) {
        rest = rest.slice(2);
      }
    } else if (isSpecial(protocol)) {
      rest = match[4];
    } else if (protocol) {
      if (forwardSlashes) {
        rest = rest.slice(2);
      }
    } else if (slashesCount >= 2 && isSpecial(location.protocol)) {
      rest = match[4];
    }

    return {
      protocol: protocol,
      slashes: forwardSlashes || isSpecial(protocol),
      slashesCount: slashesCount,
      rest: rest
    };
  }
  /**
   * Resolve a relative URL pathname against a base URL pathname.
   *
   * @param {String} relative Pathname of the relative URL.
   * @param {String} base Pathname of the base URL.
   * @return {String} Resolved pathname.
   * @private
   */


  function resolve(relative, base) {
    if (relative === '') return base;
    var path = (base || '/').split('/').slice(0, -1).concat(relative.split('/')),
        i = path.length,
        last = path[i - 1],
        unshift = false,
        up = 0;

    while (i--) {
      if (path[i] === '.') {
        path.splice(i, 1);
      } else if (path[i] === '..') {
        path.splice(i, 1);
        up++;
      } else if (up) {
        if (i === 0) unshift = true;
        path.splice(i, 1);
        up--;
      }
    }

    if (unshift) path.unshift('');
    if (last === '.' || last === '..') path.push('');
    return path.join('/');
  }
  /**
   * The actual URL instance. Instead of returning an object we've opted-in to
   * create an actual constructor as it's much more memory efficient and
   * faster and it pleases my OCD.
   *
   * It is worth noting that we should not use `URL` as class name to prevent
   * clashes with the global URL instance that got introduced in browsers.
   *
   * @constructor
   * @param {String} address URL we want to parse.
   * @param {Object|String} [location] Location defaults for relative paths.
   * @param {Boolean|Function} [parser] Parser for the query string.
   * @private
   */


  function Url(address, location, parser) {
    address = trimLeft(address);

    if (!(this instanceof Url)) {
      return new Url(address, location, parser);
    }

    var relative,
        extracted,
        parse,
        instruction,
        index,
        key,
        instructions = rules.slice(),
        type = typeof location,
        url = this,
        i = 0; //
    // The following if statements allows this module two have compatibility with
    // 2 different API:
    //
    // 1. Node.js's `url.parse` api which accepts a URL, boolean as arguments
    //    where the boolean indicates that the query string should also be parsed.
    //
    // 2. The `URL` interface of the browser which accepts a URL, object as
    //    arguments. The supplied object will be used as default values / fall-back
    //    for relative paths.
    //

    if ('object' !== type && 'string' !== type) {
      parser = location;
      location = null;
    }

    if (parser && 'function' !== typeof parser) parser = querystringify_1.parse;
    location = lolcation(location); //
    // Extract protocol information before running the instructions.
    //

    extracted = extractProtocol(address || '', location);
    relative = !extracted.protocol && !extracted.slashes;
    url.slashes = extracted.slashes || relative && location.slashes;
    url.protocol = extracted.protocol || location.protocol || '';
    address = extracted.rest; //
    // When the authority component is absent the URL starts with a path
    // component.
    //

    if (extracted.protocol === 'file:' && (extracted.slashesCount !== 2 || windowsDriveLetter.test(address)) || !extracted.slashes && (extracted.protocol || extracted.slashesCount < 2 || !isSpecial(url.protocol))) {
      instructions[3] = [/(.*)/, 'pathname'];
    }

    for (; i < instructions.length; i++) {
      instruction = instructions[i];

      if (typeof instruction === 'function') {
        address = instruction(address, url);
        continue;
      }

      parse = instruction[0];
      key = instruction[1];

      if (parse !== parse) {
        url[key] = address;
      } else if ('string' === typeof parse) {
        if (~(index = address.indexOf(parse))) {
          if ('number' === typeof instruction[2]) {
            url[key] = address.slice(0, index);
            address = address.slice(index + instruction[2]);
          } else {
            url[key] = address.slice(index);
            address = address.slice(0, index);
          }
        }
      } else if (index = parse.exec(address)) {
        url[key] = index[1];
        address = address.slice(0, index.index);
      }

      url[key] = url[key] || (relative && instruction[3] ? location[key] || '' : ''); //
      // Hostname, host and protocol should be lowercased so they can be used to
      // create a proper `origin`.
      //

      if (instruction[4]) url[key] = url[key].toLowerCase();
    } //
    // Also parse the supplied query string in to an object. If we're supplied
    // with a custom parser as function use that instead of the default build-in
    // parser.
    //


    if (parser) url.query = parser(url.query); //
    // If the URL is relative, resolve the pathname against the base URL.
    //

    if (relative && location.slashes && url.pathname.charAt(0) !== '/' && (url.pathname !== '' || location.pathname !== '')) {
      url.pathname = resolve(url.pathname, location.pathname);
    } //
    // Default to a / for pathname if none exists. This normalizes the URL
    // to always have a /
    //


    if (url.pathname.charAt(0) !== '/' && isSpecial(url.protocol)) {
      url.pathname = '/' + url.pathname;
    } //
    // We should not add port numbers if they are already the default port number
    // for a given protocol. As the host also contains the port number we're going
    // override it with the hostname which contains no port number.
    //


    if (!requiresPort(url.port, url.protocol)) {
      url.host = url.hostname;
      url.port = '';
    } //
    // Parse down the `auth` for the username and password.
    //


    url.username = url.password = '';

    if (url.auth) {
      instruction = url.auth.split(':');
      url.username = instruction[0] || '';
      url.password = instruction[1] || '';
    }

    url.origin = url.protocol !== 'file:' && isSpecial(url.protocol) && url.host ? url.protocol + '//' + url.host : 'null'; //
    // The href is just the compiled result.
    //

    url.href = url.toString();
  }
  /**
   * This is convenience method for changing properties in the URL instance to
   * insure that they all propagate correctly.
   *
   * @param {String} part          Property we need to adjust.
   * @param {Mixed} value          The newly assigned value.
   * @param {Boolean|Function} fn  When setting the query, it will be the function
   *                               used to parse the query.
   *                               When setting the protocol, double slash will be
   *                               removed from the final url if it is true.
   * @returns {URL} URL instance for chaining.
   * @public
   */


  function set$1(part, value, fn) {
    var url = this;

    switch (part) {
      case 'query':
        if ('string' === typeof value && value.length) {
          value = (fn || querystringify_1.parse)(value);
        }

        url[part] = value;
        break;

      case 'port':
        url[part] = value;

        if (!requiresPort(value, url.protocol)) {
          url.host = url.hostname;
          url[part] = '';
        } else if (value) {
          url.host = url.hostname + ':' + value;
        }

        break;

      case 'hostname':
        url[part] = value;
        if (url.port) value += ':' + url.port;
        url.host = value;
        break;

      case 'host':
        url[part] = value;

        if (/:\d+$/.test(value)) {
          value = value.split(':');
          url.port = value.pop();
          url.hostname = value.join(':');
        } else {
          url.hostname = value;
          url.port = '';
        }

        break;

      case 'protocol':
        url.protocol = value.toLowerCase();
        url.slashes = !fn;
        break;

      case 'pathname':
      case 'hash':
        if (value) {
          var char = part === 'pathname' ? '/' : '#';
          url[part] = value.charAt(0) !== char ? char + value : value;
        } else {
          url[part] = value;
        }

        break;

      default:
        url[part] = value;
    }

    for (var i = 0; i < rules.length; i++) {
      var ins = rules[i];
      if (ins[4]) url[ins[1]] = url[ins[1]].toLowerCase();
    }

    url.origin = url.protocol !== 'file:' && isSpecial(url.protocol) && url.host ? url.protocol + '//' + url.host : 'null';
    url.href = url.toString();
    return url;
  }
  /**
   * Transform the properties back in to a valid and full URL string.
   *
   * @param {Function} stringify Optional query stringify function.
   * @returns {String} Compiled version of the URL.
   * @public
   */


  function toString(stringify) {
    if (!stringify || 'function' !== typeof stringify) stringify = querystringify_1.stringify;
    var query,
        url = this,
        protocol = url.protocol;
    if (protocol && protocol.charAt(protocol.length - 1) !== ':') protocol += ':';
    var result = protocol + (url.slashes || isSpecial(url.protocol) ? '//' : '');

    if (url.username) {
      result += url.username;
      if (url.password) result += ':' + url.password;
      result += '@';
    }

    result += url.host + url.pathname;
    query = 'object' === typeof url.query ? stringify(url.query) : url.query;
    if (query) result += '?' !== query.charAt(0) ? '?' + query : query;
    if (url.hash) result += url.hash;
    return result;
  }

  Url.prototype = {
    set: set$1,
    toString: toString
  }; //
  // Expose the URL parser and some additional properties that might be useful for
  // others or testing.
  //

  Url.extractProtocol = extractProtocol;
  Url.location = lolcation;
  Url.trimLeft = trimLeft;
  Url.qs = querystringify_1;
  var urlParse = Url;

  function _typeof$1(obj) {
    "@babel/helpers - typeof";

    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof$1 = function _typeof(obj) {
        return typeof obj;
      };
    } else {
      _typeof$1 = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof$1(obj);
  }

  function _classCallCheck$7(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _inherits$2(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf$1(subClass, superClass);
  }

  function _createSuper$1(Derived) {
    return function () {
      var Super = _getPrototypeOf$1(Derived),
          result;

      if (_isNativeReflectConstruct$1()) {
        var NewTarget = _getPrototypeOf$1(this).constructor;

        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }

      return _possibleConstructorReturn$2(this, result);
    };
  }

  function _possibleConstructorReturn$2(self, call) {
    if (call && (_typeof$1(call) === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized$1(self);
  }

  function _assertThisInitialized$1(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _wrapNativeSuper(Class) {
    var _cache = typeof Map === "function" ? new Map() : undefined;

    _wrapNativeSuper = function _wrapNativeSuper(Class) {
      if (Class === null || !_isNativeFunction(Class)) return Class;

      if (typeof Class !== "function") {
        throw new TypeError("Super expression must either be null or a function");
      }

      if (typeof _cache !== "undefined") {
        if (_cache.has(Class)) return _cache.get(Class);

        _cache.set(Class, Wrapper);
      }

      function Wrapper() {
        return _construct(Class, arguments, _getPrototypeOf$1(this).constructor);
      }

      Wrapper.prototype = Object.create(Class.prototype, {
        constructor: {
          value: Wrapper,
          enumerable: false,
          writable: true,
          configurable: true
        }
      });
      return _setPrototypeOf$1(Wrapper, Class);
    };

    return _wrapNativeSuper(Class);
  }

  function _construct(Parent, args, Class) {
    if (_isNativeReflectConstruct$1()) {
      _construct = Reflect.construct;
    } else {
      _construct = function _construct(Parent, args, Class) {
        var a = [null];
        a.push.apply(a, args);
        var Constructor = Function.bind.apply(Parent, a);
        var instance = new Constructor();
        if (Class) _setPrototypeOf$1(instance, Class.prototype);
        return instance;
      };
    }

    return _construct.apply(null, arguments);
  }

  function _isNativeReflectConstruct$1() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  function _isNativeFunction(fn) {
    return Function.toString.call(fn).indexOf("[native code]") !== -1;
  }

  function _setPrototypeOf$1(o, p) {
    _setPrototypeOf$1 = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf$1(o, p);
  }

  function _getPrototypeOf$1(o) {
    _getPrototypeOf$1 = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf$1(o);
  }

  var DetailedError = /*#__PURE__*/function (_Error) {
    _inherits$2(DetailedError, _Error);

    var _super = _createSuper$1(DetailedError);

    function DetailedError(message) {
      var _this;

      var causingErr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var req = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var res = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

      _classCallCheck$7(this, DetailedError);

      _this = _super.call(this, message);
      _this.originalRequest = req;
      _this.originalResponse = res;
      _this.causingError = causingErr;

      if (causingErr != null) {
        message += ", caused by ".concat(causingErr.toString());
      }

      if (req != null) {
        var requestId = req.getHeader('X-Request-ID') || 'n/a';
        var method = req.getMethod();
        var url = req.getURL();
        var status = res ? res.getStatus() : 'n/a';
        var body = res ? res.getBody() || '' : 'n/a';
        message += ", originated from request (method: ".concat(method, ", url: ").concat(url, ", response code: ").concat(status, ", response text: ").concat(body, ", request id: ").concat(requestId, ")");
      }

      _this.message = message;
      return _this;
    }

    return DetailedError;
  }( /*#__PURE__*/_wrapNativeSuper(Error));

  /* eslint no-console: "off" */
  function log$1(msg) {
    return;
  }

  /**
   * Generate a UUID v4 based on random numbers. We intentioanlly use the less
   * secure Math.random function here since the more secure crypto.getRandomNumbers
   * is not available on all platforms.
   * This is not a problem for us since we use the UUID only for generating a
   * request ID, so we can correlate server logs to client errors.
   *
   * This function is taken from following site:
   * https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
   *
   * @return {string} The generate UUID
   */
  function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0,
          v = c == 'x' ? r : r & 0x3 | 0x8;
      return v.toString(16);
    });
  }

  function ownKeys$1(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread$1(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys$1(Object(source), true).forEach(function (key) {
          _defineProperty$1(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys$1(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty$1(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _classCallCheck$6(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties$5(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass$6(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties$5(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties$5(Constructor, staticProps);
    return Constructor;
  }
  var defaultOptions$1 = {
    endpoint: null,
    uploadUrl: null,
    metadata: {},
    fingerprint: null,
    uploadSize: null,
    onProgress: null,
    onChunkComplete: null,
    onSuccess: null,
    onError: null,
    _onUploadUrlAvailable: null,
    overridePatchMethod: false,
    headers: {},
    addRequestId: false,
    onBeforeRequest: null,
    onAfterResponse: null,
    onShouldRetry: null,
    chunkSize: Infinity,
    retryDelays: [0, 1000, 3000, 5000],
    parallelUploads: 1,
    storeFingerprintForResuming: true,
    removeFingerprintOnSuccess: false,
    uploadLengthDeferred: false,
    uploadDataDuringCreation: false,
    urlStorage: null,
    fileReader: null,
    httpStack: null
  };

  var BaseUpload = /*#__PURE__*/function () {
    function BaseUpload(file, options) {
      _classCallCheck$6(this, BaseUpload); // Warn about removed options from previous versions


      this.options = options; // The storage module used to store URLs

      this._urlStorage = this.options.urlStorage; // The underlying File/Blob object

      this.file = file; // The URL against which the file will be uploaded

      this.url = null; // The underlying request object for the current PATCH request

      this._req = null; // The fingerpinrt for the current file (set after start())

      this._fingerprint = null; // The key that the URL storage returned when saving an URL with a fingerprint,

      this._urlStorageKey = null; // The offset used in the current PATCH request

      this._offset = null; // True if the current PATCH request has been aborted

      this._aborted = false; // The file's size in bytes

      this._size = null; // The Source object which will wrap around the given file and provides us
      // with a unified interface for getting its size and slice chunks from its
      // content allowing us to easily handle Files, Blobs, Buffers and Streams.

      this._source = null; // The current count of attempts which have been made. Zero indicates none.

      this._retryAttempt = 0; // The timeout's ID which is used to delay the next retry

      this._retryTimeout = null; // The offset of the remote upload before the latest attempt was started.

      this._offsetBeforeRetry = 0; // An array of BaseUpload instances which are used for uploading the different
      // parts, if the parallelUploads option is used.

      this._parallelUploads = null; // An array of upload URLs which are used for uploading the different
      // parts, if the parallelUploads option is used.

      this._parallelUploadUrls = null;
    }
    /**
     * Use the Termination extension to delete an upload from the server by sending a DELETE
     * request to the specified upload URL. This is only possible if the server supports the
     * Termination extension. If the `options.retryDelays` property is set, the method will
     * also retry if an error ocurrs.
     *
     * @param {String} url The upload's URL which will be terminated.
     * @param {object} options Optional options for influencing HTTP requests.
     * @return {Promise} The Promise will be resolved/rejected when the requests finish.
     */


    _createClass$6(BaseUpload, [{
      key: "findPreviousUploads",
      value: function findPreviousUploads() {
        var _this = this;

        return this.options.fingerprint(this.file, this.options).then(function (fingerprint) {
          return _this._urlStorage.findUploadsByFingerprint(fingerprint);
        });
      }
    }, {
      key: "resumeFromPreviousUpload",
      value: function resumeFromPreviousUpload(previousUpload) {
        this.url = previousUpload.uploadUrl || null;
        this._parallelUploadUrls = previousUpload.parallelUploadUrls || null;
        this._urlStorageKey = previousUpload.urlStorageKey;
      }
    }, {
      key: "start",
      value: function start() {
        var _this2 = this;

        var file = this.file;

        if (!file) {
          this._emitError(new Error('tus: no file or stream to upload provided'));

          return;
        }

        if (!this.options.endpoint && !this.options.uploadUrl) {
          this._emitError(new Error('tus: neither an endpoint or an upload URL is provided'));

          return;
        }

        var retryDelays = this.options.retryDelays;

        if (retryDelays != null && Object.prototype.toString.call(retryDelays) !== '[object Array]') {
          this._emitError(new Error('tus: the `retryDelays` option must either be an array or null'));

          return;
        }

        if (this.options.parallelUploads > 1) {
          // Test which options are incompatible with parallel uploads.
          ['uploadUrl', 'uploadSize', 'uploadLengthDeferred'].forEach(function (optionName) {
            if (_this2.options[optionName]) {
              _this2._emitError(new Error("tus: cannot use the ".concat(optionName, " option when parallelUploads is enabled")));
            }
          });
        }

        this.options.fingerprint(file, this.options).then(function (fingerprint) {

          _this2._fingerprint = fingerprint;

          if (_this2._source) {
            return _this2._source;
          }

          return _this2.options.fileReader.openFile(file, _this2.options.chunkSize);
        }).then(function (source) {
          _this2._source = source; // If the upload was configured to use multiple requests or if we resume from
          // an upload which used multiple requests, we start a parallel upload.

          if (_this2.options.parallelUploads > 1 || _this2._parallelUploadUrls != null) {
            _this2._startParallelUpload();
          } else {
            _this2._startSingleUpload();
          }
        })["catch"](function (err) {
          _this2._emitError(err);
        });
      }
      /**
       * Initiate the uploading procedure for a parallelized upload, where one file is split into
       * multiple request which are run in parallel.
       *
       * @api private
       */

    }, {
      key: "_startParallelUpload",
      value: function _startParallelUpload() {
        var _this3 = this;

        var totalSize = this._size = this._source.size;
        var totalProgress = 0;
        this._parallelUploads = [];
        var partCount = this._parallelUploadUrls != null ? this._parallelUploadUrls.length : this.options.parallelUploads; // The input file will be split into multiple slices which are uploaded in separate
        // requests. Here we generate the start and end position for the slices.

        var parts = splitSizeIntoParts(this._source.size, partCount, this._parallelUploadUrls); // Create an empty list for storing the upload URLs

        this._parallelUploadUrls = new Array(parts.length); // Generate a promise for each slice that will be resolve if the respective
        // upload is completed.

        var uploads = parts.map(function (part, index) {
          var lastPartProgress = 0;
          return _this3._source.slice(part.start, part.end).then(function (_ref) {
            var value = _ref.value;
            return new Promise(function (resolve, reject) {
              // Merge with the user supplied options but overwrite some values.
              var options = _objectSpread$1({}, _this3.options, {
                // If available, the partial upload should be resumed from a previous URL.
                uploadUrl: part.uploadUrl || null,
                // We take manually care of resuming for partial uploads, so they should
                // not be stored in the URL storage.
                storeFingerprintForResuming: false,
                removeFingerprintOnSuccess: false,
                // Reset the parallelUploads option to not cause recursion.
                parallelUploads: 1,
                metadata: {},
                // Add the header to indicate the this is a partial upload.
                headers: _objectSpread$1({}, _this3.options.headers, {
                  'Upload-Concat': 'partial'
                }),
                // Reject or resolve the promise if the upload errors or completes.
                onSuccess: resolve,
                onError: reject,
                // Based in the progress for this partial upload, calculate the progress
                // for the entire final upload.
                onProgress: function onProgress(newPartProgress) {
                  totalProgress = totalProgress - lastPartProgress + newPartProgress;
                  lastPartProgress = newPartProgress;

                  _this3._emitProgress(totalProgress, totalSize);
                },
                // Wait until every partial upload has an upload URL, so we can add
                // them to the URL storage.
                _onUploadUrlAvailable: function _onUploadUrlAvailable() {
                  _this3._parallelUploadUrls[index] = upload.url; // Test if all uploads have received an URL

                  if (_this3._parallelUploadUrls.filter(function (u) {
                    return !!u;
                  }).length === parts.length) {
                    _this3._saveUploadInUrlStorage();
                  }
                }
              });

              var upload = new BaseUpload(value, options);
              upload.start(); // Store the upload in an array, so we can later abort them if necessary.

              _this3._parallelUploads.push(upload);
            });
          });
        });
        var req; // Wait until all partial uploads are finished and we can send the POST request for
        // creating the final upload.

        Promise.all(uploads).then(function () {
          req = _this3._openRequest('POST', _this3.options.endpoint);
          req.setHeader('Upload-Concat', "final;".concat(_this3._parallelUploadUrls.join(' '))); // Add metadata if values have been added

          var metadata = encodeMetadata(_this3.options.metadata);

          if (metadata !== '') {
            req.setHeader('Upload-Metadata', metadata);
          }

          return _this3._sendRequest(req, null);
        }).then(function (res) {
          if (!inStatusCategory(res.getStatus(), 200)) {
            _this3._emitHttpError(req, res, 'tus: unexpected response while creating upload');

            return;
          }

          var location = res.getHeader('Location');

          if (location == null) {
            _this3._emitHttpError(req, res, 'tus: invalid or missing Location header');

            return;
          }

          _this3.url = resolveUrl(_this3.options.endpoint, location);
          log$1("Created upload at ".concat(_this3.url));

          _this3._emitSuccess();
        })["catch"](function (err) {
          _this3._emitError(err);
        });
      }
      /**
       * Initiate the uploading procedure for a non-parallel upload. Here the entire file is
       * uploaded in a sequential matter.
       *
       * @api private
       */

    }, {
      key: "_startSingleUpload",
      value: function _startSingleUpload() {
        // First, we look at the uploadLengthDeferred option.
        // Next, we check if the caller has supplied a manual upload size.
        // Finally, we try to use the calculated size from the source object.
        if (this.options.uploadLengthDeferred) {
          this._size = null;
        } else if (this.options.uploadSize != null) {
          this._size = +this.options.uploadSize;

          if (isNaN(this._size)) {
            this._emitError(new Error('tus: cannot convert `uploadSize` option into a number'));

            return;
          }
        } else {
          this._size = this._source.size;

          if (this._size == null) {
            this._emitError(new Error("tus: cannot automatically derive upload's size from input and must be specified manually using the `uploadSize` option"));

            return;
          }
        } // Reset the aborted flag when the upload is started or else the
        // _performUpload will stop before sending a request if the upload has been
        // aborted previously.


        this._aborted = false; // The upload had been started previously and we should reuse this URL.

        if (this.url != null) {
          log$1("Resuming upload from previous URL: ".concat(this.url));

          this._resumeUpload();

          return;
        } // A URL has manually been specified, so we try to resume


        if (this.options.uploadUrl != null) {
          log$1("Resuming upload from provided URL: ".concat(this.options.url));
          this.url = this.options.uploadUrl;

          this._resumeUpload();

          return;
        } // An upload has not started for the file yet, so we start a new one

        this._createUpload();
      }
      /**
       * Abort any running request and stop the current upload. After abort is called, no event
       * handler will be invoked anymore. You can use the `start` method to resume the upload
       * again.
       * If `shouldTerminate` is true, the `terminate` function will be called to remove the
       * current upload from the server.
       *
       * @param {boolean} shouldTerminate True if the upload should be deleted from the server.
       * @return {Promise} The Promise will be resolved/rejected when the requests finish.
       */

    }, {
      key: "abort",
      value: function abort(shouldTerminate) {
        var _this4 = this; // Count the number of arguments to see if a callback is being provided in the old style required by tus-js-client 1.x, then throw an error if it is.
        // `arguments` is a JavaScript built-in variable that contains all of the function's arguments.


        if (arguments.length > 1 && typeof arguments[1] === 'function') {
          throw new Error('tus: the abort function does not accept a callback since v2 anymore; please use the returned Promise instead');
        } // Stop any parallel partial uploads, that have been started in _startParallelUploads.


        if (this._parallelUploads != null) {
          this._parallelUploads.forEach(function (upload) {
            upload.abort(shouldTerminate);
          });
        } // Stop any current running request.


        if (this._req !== null) {
          this._req.abort();

          this._source.close();
        }

        this._aborted = true; // Stop any timeout used for initiating a retry.

        if (this._retryTimeout != null) {
          clearTimeout(this._retryTimeout);
          this._retryTimeout = null;
        }

        if (!shouldTerminate || this.url == null) {
          return Promise.resolve();
        }

        return BaseUpload.terminate(this.url, this.options) // Remove entry from the URL storage since the upload URL is no longer valid.
        .then(function () {
          return _this4._removeFromUrlStorage();
        });
      }
    }, {
      key: "_emitHttpError",
      value: function _emitHttpError(req, res, message, causingErr) {
        this._emitError(new DetailedError(message, causingErr, req, res));
      }
    }, {
      key: "_emitError",
      value: function _emitError(err) {
        var _this5 = this; // Do not emit errors, e.g. from aborted HTTP requests, if the upload has been stopped.


        if (this._aborted) return; // Check if we should retry, when enabled, before sending the error to the user.

        if (this.options.retryDelays != null) {
          // We will reset the attempt counter if
          // - we were already able to connect to the server (offset != null) and
          // - we were able to upload a small chunk of data to the server
          var shouldResetDelays = this._offset != null && this._offset > this._offsetBeforeRetry;

          if (shouldResetDelays) {
            this._retryAttempt = 0;
          }

          if (shouldRetry(err, this._retryAttempt, this.options)) {
            var delay = this.options.retryDelays[this._retryAttempt++];
            this._offsetBeforeRetry = this._offset;
            this._retryTimeout = setTimeout(function () {
              _this5.start();
            }, delay);
            return;
          }
        }

        if (typeof this.options.onError === 'function') {
          this.options.onError(err);
        } else {
          throw err;
        }
      }
      /**
       * Publishes notification if the upload has been successfully completed.
       *
       * @api private
       */

    }, {
      key: "_emitSuccess",
      value: function _emitSuccess() {
        if (this.options.removeFingerprintOnSuccess) {
          // Remove stored fingerprint and corresponding endpoint. This causes
          // new uploads of the same file to be treated as a different file.
          this._removeFromUrlStorage();
        }

        if (typeof this.options.onSuccess === 'function') {
          this.options.onSuccess();
        }
      }
      /**
       * Publishes notification when data has been sent to the server. This
       * data may not have been accepted by the server yet.
       *
       * @param {number} bytesSent  Number of bytes sent to the server.
       * @param {number} bytesTotal Total number of bytes to be sent to the server.
       * @api private
       */

    }, {
      key: "_emitProgress",
      value: function _emitProgress(bytesSent, bytesTotal) {
        if (typeof this.options.onProgress === 'function') {
          this.options.onProgress(bytesSent, bytesTotal);
        }
      }
      /**
       * Publishes notification when a chunk of data has been sent to the server
       * and accepted by the server.
       * @param {number} chunkSize  Size of the chunk that was accepted by the server.
       * @param {number} bytesAccepted Total number of bytes that have been
       *                                accepted by the server.
       * @param {number} bytesTotal Total number of bytes to be sent to the server.
       * @api private
       */

    }, {
      key: "_emitChunkComplete",
      value: function _emitChunkComplete(chunkSize, bytesAccepted, bytesTotal) {
        if (typeof this.options.onChunkComplete === 'function') {
          this.options.onChunkComplete(chunkSize, bytesAccepted, bytesTotal);
        }
      }
      /**
       * Create a new upload using the creation extension by sending a POST
       * request to the endpoint. After successful creation the file will be
       * uploaded
       *
       * @api private
       */

    }, {
      key: "_createUpload",
      value: function _createUpload() {
        var _this6 = this;

        if (!this.options.endpoint) {
          this._emitError(new Error('tus: unable to create upload because no endpoint is provided'));

          return;
        }

        var req = this._openRequest('POST', this.options.endpoint);

        if (this.options.uploadLengthDeferred) {
          req.setHeader('Upload-Defer-Length', 1);
        } else {
          req.setHeader('Upload-Length', this._size);
        } // Add metadata if values have been added


        var metadata = encodeMetadata(this.options.metadata);

        if (metadata !== '') {
          req.setHeader('Upload-Metadata', metadata);
        }

        var promise;

        if (this.options.uploadDataDuringCreation && !this.options.uploadLengthDeferred) {
          this._offset = 0;
          promise = this._addChunkToRequest(req);
        } else {
          promise = this._sendRequest(req, null);
        }

        promise.then(function (res) {
          if (!inStatusCategory(res.getStatus(), 200)) {
            _this6._emitHttpError(req, res, 'tus: unexpected response while creating upload');

            return;
          }

          var location = res.getHeader('Location');

          if (location == null) {
            _this6._emitHttpError(req, res, 'tus: invalid or missing Location header');

            return;
          }

          _this6.url = resolveUrl(_this6.options.endpoint, location);
          log$1("Created upload at ".concat(_this6.url));

          if (typeof _this6.options._onUploadUrlAvailable === 'function') {
            _this6.options._onUploadUrlAvailable();
          }

          if (_this6._size === 0) {
            // Nothing to upload and file was successfully created
            _this6._emitSuccess();

            _this6._source.close();

            return;
          }

          _this6._saveUploadInUrlStorage();

          if (_this6.options.uploadDataDuringCreation) {
            _this6._handleUploadResponse(req, res);
          } else {
            _this6._offset = 0;

            _this6._performUpload();
          }
        })["catch"](function (err) {
          _this6._emitHttpError(req, null, 'tus: failed to create upload', err);
        });
      }
      /*
       * Try to resume an existing upload. First a HEAD request will be sent
       * to retrieve the offset. If the request fails a new upload will be
       * created. In the case of a successful response the file will be uploaded.
       *
       * @api private
       */

    }, {
      key: "_resumeUpload",
      value: function _resumeUpload() {
        var _this7 = this;

        var req = this._openRequest('HEAD', this.url);

        var promise = this._sendRequest(req, null);

        promise.then(function (res) {
          var status = res.getStatus();

          if (!inStatusCategory(status, 200)) {
            if (inStatusCategory(status, 400)) {
              // Remove stored fingerprint and corresponding endpoint,
              // on client errors since the file can not be found
              _this7._removeFromUrlStorage();
            } // If the upload is locked (indicated by the 423 Locked status code), we
            // emit an error instead of directly starting a new upload. This way the
            // retry logic can catch the error and will retry the upload. An upload
            // is usually locked for a short period of time and will be available
            // afterwards.


            if (status === 423) {
              _this7._emitHttpError(req, res, 'tus: upload is currently locked; retry later');

              return;
            }

            if (!_this7.options.endpoint) {
              // Don't attempt to create a new upload if no endpoint is provided.
              _this7._emitHttpError(req, res, 'tus: unable to resume upload (new upload cannot be created without an endpoint)');

              return;
            } // Try to create a new upload


            _this7.url = null;

            _this7._createUpload();

            return;
          }

          var offset = parseInt(res.getHeader('Upload-Offset'), 10);

          if (isNaN(offset)) {
            _this7._emitHttpError(req, res, 'tus: invalid or missing offset value');

            return;
          }

          var length = parseInt(res.getHeader('Upload-Length'), 10);

          if (isNaN(length) && !_this7.options.uploadLengthDeferred) {
            _this7._emitHttpError(req, res, 'tus: invalid or missing length value');

            return;
          }

          if (typeof _this7.options._onUploadUrlAvailable === 'function') {
            _this7.options._onUploadUrlAvailable();
          } // Upload has already been completed and we do not need to send additional
          // data to the server


          if (offset === length) {
            _this7._emitProgress(length, length);

            _this7._emitSuccess();

            return;
          }

          _this7._offset = offset;

          _this7._performUpload();
        })["catch"](function (err) {
          _this7._emitHttpError(req, null, 'tus: failed to resume upload', err);
        });
      }
      /**
       * Start uploading the file using PATCH requests. The file will be divided
       * into chunks as specified in the chunkSize option. During the upload
       * the onProgress event handler may be invoked multiple times.
       *
       * @api private
       */

    }, {
      key: "_performUpload",
      value: function _performUpload() {
        var _this8 = this; // If the upload has been aborted, we will not send the next PATCH request.
        // This is important if the abort method was called during a callback, such
        // as onChunkComplete or onProgress.


        if (this._aborted) {
          return;
        }

        var req; // Some browser and servers may not support the PATCH method. For those
        // cases, you can tell tus-js-client to use a POST request with the
        // X-HTTP-Method-Override header for simulating a PATCH request.

        if (this.options.overridePatchMethod) {
          req = this._openRequest('POST', this.url);
          req.setHeader('X-HTTP-Method-Override', 'PATCH');
        } else {
          req = this._openRequest('PATCH', this.url);
        }

        req.setHeader('Upload-Offset', this._offset);

        var promise = this._addChunkToRequest(req);

        promise.then(function (res) {
          if (!inStatusCategory(res.getStatus(), 200)) {
            _this8._emitHttpError(req, res, 'tus: unexpected response while uploading chunk');

            return;
          }

          _this8._handleUploadResponse(req, res);
        })["catch"](function (err) {
          // Don't emit an error if the upload was aborted manually
          if (_this8._aborted) {
            return;
          }

          _this8._emitHttpError(req, null, "tus: failed to upload chunk at offset ".concat(_this8._offset), err);
        });
      }
      /**
       * _addChunktoRequest reads a chunk from the source and sends it using the
       * supplied request object. It will not handle the response.
       *
       * @api private
       */

    }, {
      key: "_addChunkToRequest",
      value: function _addChunkToRequest(req) {
        var _this9 = this;

        var start = this._offset;
        var end = this._offset + this.options.chunkSize;
        req.setProgressHandler(function (bytesSent) {
          _this9._emitProgress(start + bytesSent, _this9._size);
        });
        req.setHeader('Content-Type', 'application/offset+octet-stream'); // The specified chunkSize may be Infinity or the calcluated end position
        // may exceed the file's size. In both cases, we limit the end position to
        // the input's total size for simpler calculations and correctness.

        if ((end === Infinity || end > this._size) && !this.options.uploadLengthDeferred) {
          end = this._size;
        }

        return this._source.slice(start, end).then(function (_ref2) {
          var value = _ref2.value,
              done = _ref2.done; // If the upload length is deferred, the upload size was not specified during
          // upload creation. So, if the file reader is done reading, we know the total
          // upload size and can tell the tus server.

          if (_this9.options.uploadLengthDeferred && done) {
            _this9._size = _this9._offset + (value && value.size ? value.size : 0);
            req.setHeader('Upload-Length', _this9._size);
          }

          if (value === null) {
            return _this9._sendRequest(req);
          }

          _this9._emitProgress(_this9._offset, _this9._size);

          return _this9._sendRequest(req, value);
        });
      }
      /**
       * _handleUploadResponse is used by requests that haven been sent using _addChunkToRequest
       * and already have received a response.
       *
       * @api private
       */

    }, {
      key: "_handleUploadResponse",
      value: function _handleUploadResponse(req, res) {
        var offset = parseInt(res.getHeader('Upload-Offset'), 10);

        if (isNaN(offset)) {
          this._emitHttpError(req, res, 'tus: invalid or missing offset value');

          return;
        }

        this._emitProgress(offset, this._size);

        this._emitChunkComplete(offset - this._offset, offset, this._size);

        this._offset = offset;

        if (offset == this._size) {
          // Yay, finally done :)
          this._emitSuccess();

          this._source.close();

          return;
        }

        this._performUpload();
      }
      /**
       * Create a new HTTP request object with the given method and URL.
       *
       * @api private
       */

    }, {
      key: "_openRequest",
      value: function _openRequest(method, url) {
        var req = openRequest(method, url, this.options);
        this._req = req;
        return req;
      }
      /**
       * Remove the entry in the URL storage, if it has been saved before.
       *
       * @api private
       */

    }, {
      key: "_removeFromUrlStorage",
      value: function _removeFromUrlStorage() {
        var _this10 = this;

        if (!this._urlStorageKey) return;

        this._urlStorage.removeUpload(this._urlStorageKey)["catch"](function (err) {
          _this10._emitError(err);
        });

        this._urlStorageKey = null;
      }
      /**
       * Add the upload URL to the URL storage, if possible.
       *
       * @api private
       */

    }, {
      key: "_saveUploadInUrlStorage",
      value: function _saveUploadInUrlStorage() {
        var _this11 = this; // Only if a fingerprint was calculated for the input (i.e. not a stream), we can store the upload URL.


        if (!this.options.storeFingerprintForResuming || !this._fingerprint) {
          return;
        }

        var storedUpload = {
          size: this._size,
          metadata: this.options.metadata,
          creationTime: new Date().toString()
        };

        if (this._parallelUploads) {
          // Save multiple URLs if the parallelUploads option is used ...
          storedUpload.parallelUploadUrls = this._parallelUploadUrls;
        } else {
          // ... otherwise we just save the one available URL.
          storedUpload.uploadUrl = this.url;
        }

        this._urlStorage.addUpload(this._fingerprint, storedUpload).then(function (urlStorageKey) {
          return _this11._urlStorageKey = urlStorageKey;
        })["catch"](function (err) {
          _this11._emitError(err);
        });
      }
      /**
       * Send a request with the provided body.
       *
       * @api private
       */

    }, {
      key: "_sendRequest",
      value: function _sendRequest(req) {
        var body = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
        return sendRequest(req, body, this.options);
      }
    }], [{
      key: "terminate",
      value: function terminate(url, options) {
        // Count the number of arguments to see if a callback is being provided as the last
        // argument in the old style required by tus-js-client 1.x, then throw an error if it is.
        // `arguments` is a JavaScript built-in variable that contains all of the function's arguments.
        if (arguments.length > 1 && typeof arguments[arguments.length - 1] === 'function') {
          throw new Error('tus: the terminate function does not accept a callback since v2 anymore; please use the returned Promise instead');
        } // Note that in order for the trick above to work, a default value cannot be set for `options`,
        // so the check below replaces the old default `{}`.


        if (options === undefined) {
          options = {};
        }

        var req = openRequest('DELETE', url, options);
        return sendRequest(req, null, options).then(function (res) {
          // A 204 response indicates a successfull request
          if (res.getStatus() === 204) {
            return;
          }

          throw new DetailedError('tus: unexpected response while terminating upload', null, req, res);
        })["catch"](function (err) {
          if (!(err instanceof DetailedError)) {
            err = new DetailedError('tus: failed to terminate upload', err, req, null);
          }

          if (!shouldRetry(err, 0, options)) {
            throw err;
          } // Instead of keeping track of the retry attempts, we remove the first element from the delays
          // array. If the array is empty, all retry attempts are used up and we will bubble up the error.
          // We recursively call the terminate function will removing elements from the retryDelays array.


          var delay = options.retryDelays[0];
          var remainingDelays = options.retryDelays.slice(1);

          var newOptions = _objectSpread$1({}, options, {
            retryDelays: remainingDelays
          });

          return new Promise(function (resolve) {
            return setTimeout(resolve, delay);
          }).then(function () {
            return BaseUpload.terminate(url, newOptions);
          });
        });
      }
    }]);

    return BaseUpload;
  }();

  function encodeMetadata(metadata) {
    var encoded = [];

    for (var key in metadata) {
      encoded.push("".concat(key, " ").concat(base64.Base64.encode(metadata[key])));
    }

    return encoded.join(',');
  }
  /**
   * Checks whether a given status is in the range of the expected category.
   * For example, only a status between 200 and 299 will satisfy the category 200.
   *
   * @api private
   */


  function inStatusCategory(status, category) {
    return status >= category && status < category + 100;
  }
  /**
   * Create a new HTTP request with the specified method and URL.
   * The necessary headers that are included in every request
   * will be added, including the request ID.
   *
   * @api private
   */


  function openRequest(method, url, options) {
    var req = options.httpStack.createRequest(method, url);
    req.setHeader('Tus-Resumable', '1.0.0');
    var headers = options.headers || {};

    for (var name in headers) {
      req.setHeader(name, headers[name]);
    }

    if (options.addRequestId) {
      var requestId = uuid();
      req.setHeader('X-Request-ID', requestId);
    }

    return req;
  }
  /**
   * Send a request with the provided body while invoking the onBeforeRequest
   * and onAfterResponse callbacks.
   *
   * @api private
   */


  function sendRequest(req, body, options) {
    var onBeforeRequestPromise = typeof options.onBeforeRequest === 'function' ? Promise.resolve(options.onBeforeRequest(req)) : Promise.resolve();
    return onBeforeRequestPromise.then(function () {
      return req.send(body).then(function (res) {
        var onAfterResponsePromise = typeof options.onAfterResponse === 'function' ? Promise.resolve(options.onAfterResponse(req, res)) : Promise.resolve();
        return onAfterResponsePromise.then(function () {
          return res;
        });
      });
    });
  }
  /**
   * Checks whether the browser running this code has internet access.
   * This function will always return true in the node.js environment
   *
   * @api private
   */


  function isOnline() {
    var online = true;

    if (typeof window !== 'undefined' && 'navigator' in window && window.navigator.onLine === false) {
      online = false;
    }

    return online;
  }
  /**
   * Checks whether or not it is ok to retry a request.
   * @param {Error} err the error returned from the last request
   * @param {number} retryAttempt the number of times the request has already been retried
   * @param {object} options tus Upload options
   *
   * @api private
   */


  function shouldRetry(err, retryAttempt, options) {
    // We only attempt a retry if
    // - retryDelays option is set
    // - we didn't exceed the maxium number of retries, yet, and
    // - this error was caused by a request or it's response and
    // - the error is server error (i.e. not a status 4xx except a 409 or 423) or
    // a onShouldRetry is specified and returns true
    // - the browser does not indicate that we are offline
    if (options.retryDelays == null || retryAttempt >= options.retryDelays.length || err.originalRequest == null) {
      return false;
    }

    if (options && typeof options.onShouldRetry === 'function') {
      return options.onShouldRetry(err, retryAttempt, options);
    }

    var status = err.originalResponse ? err.originalResponse.getStatus() : 0;
    return (!inStatusCategory(status, 400) || status === 409 || status === 423) && isOnline();
  }
  /**
   * Resolve a relative link given the origin as source. For example,
   * if a HTTP request to http://example.com/files/ returns a Location
   * header with the value /upload/abc, the resolved URL will be:
   * http://example.com/upload/abc
   */


  function resolveUrl(origin, link) {
    return new urlParse(link, origin).toString();
  }
  /**
   * Calculate the start and end positions for the parts if an upload
   * is split into multiple parallel requests.
   *
   * @param {number} totalSize The byte size of the upload, which will be split.
   * @param {number} partCount The number in how many parts the upload will be split.
   * @param {string[]} previousUrls The upload URLs for previous parts.
   * @return {object[]}
   * @api private
   */


  function splitSizeIntoParts(totalSize, partCount, previousUrls) {
    var partSize = Math.floor(totalSize / partCount);
    var parts = [];

    for (var i = 0; i < partCount; i++) {
      parts.push({
        start: partSize * i,
        end: partSize * (i + 1)
      });
    }

    parts[partCount - 1].end = totalSize; // Attach URLs from previous uploads, if available.

    if (previousUrls) {
      parts.forEach(function (part, index) {
        part.uploadUrl = previousUrls[index] || null;
      });
    }

    return parts;
  }

  BaseUpload.defaultOptions = defaultOptions$1;

  function _classCallCheck$5(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties$4(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass$5(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties$4(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties$4(Constructor, staticProps);
    return Constructor;
  }
  /* eslint no-unused-vars: "off" */


  var NoopUrlStorage = /*#__PURE__*/function () {
    function NoopUrlStorage() {
      _classCallCheck$5(this, NoopUrlStorage);
    }

    _createClass$5(NoopUrlStorage, [{
      key: "listAllUploads",
      value: function listAllUploads() {
        return Promise.resolve([]);
      }
    }, {
      key: "findUploadsByFingerprint",
      value: function findUploadsByFingerprint(fingerprint) {
        return Promise.resolve([]);
      }
    }, {
      key: "removeUpload",
      value: function removeUpload(urlStorageKey) {
        return Promise.resolve();
      }
    }, {
      key: "addUpload",
      value: function addUpload(fingerprint, upload) {
        return Promise.resolve(null);
      }
    }]);

    return NoopUrlStorage;
  }();

  function _classCallCheck$4(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties$3(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass$4(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties$3(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties$3(Constructor, staticProps);
    return Constructor;
  }
  /* global window, localStorage */


  var hasStorage = false;

  try {
    hasStorage = 'localStorage' in window; // Attempt to store and read entries from the local storage to detect Private
    // Mode on Safari on iOS (see #49)

    var key$1 = 'tusSupport';
    localStorage.setItem(key$1, localStorage.getItem(key$1));
  } catch (e) {
    // If we try to access localStorage inside a sandboxed iframe, a SecurityError
    // is thrown. When in private mode on iOS Safari, a QuotaExceededError is
    // thrown (see #49)
    if (e.code === e.SECURITY_ERR || e.code === e.QUOTA_EXCEEDED_ERR) {
      hasStorage = false;
    } else {
      throw e;
    }
  }

  var canStoreURLs = hasStorage;
  var WebStorageUrlStorage = /*#__PURE__*/function () {
    function WebStorageUrlStorage() {
      _classCallCheck$4(this, WebStorageUrlStorage);
    }

    _createClass$4(WebStorageUrlStorage, [{
      key: "findAllUploads",
      value: function findAllUploads() {
        var results = this._findEntries('tus::');

        return Promise.resolve(results);
      }
    }, {
      key: "findUploadsByFingerprint",
      value: function findUploadsByFingerprint(fingerprint) {
        var results = this._findEntries("tus::".concat(fingerprint, "::"));

        return Promise.resolve(results);
      }
    }, {
      key: "removeUpload",
      value: function removeUpload(urlStorageKey) {
        localStorage.removeItem(urlStorageKey);
        return Promise.resolve();
      }
    }, {
      key: "addUpload",
      value: function addUpload(fingerprint, upload) {
        var id = Math.round(Math.random() * 1e12);
        var key = "tus::".concat(fingerprint, "::").concat(id);
        localStorage.setItem(key, JSON.stringify(upload));
        return Promise.resolve(key);
      }
    }, {
      key: "_findEntries",
      value: function _findEntries(prefix) {
        var results = [];

        for (var i = 0; i < localStorage.length; i++) {
          var _key = localStorage.key(i);

          if (_key.indexOf(prefix) !== 0) continue;

          try {
            var upload = JSON.parse(localStorage.getItem(_key));
            upload.urlStorageKey = _key;
            results.push(upload);
          } catch (e) {// The JSON parse error is intentionally ignored here, so a malformed
            // entry in the storage cannot prevent an upload.
          }
        }

        return results;
      }
    }]);

    return WebStorageUrlStorage;
  }();

  function _classCallCheck$3(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties$2(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass$3(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties$2(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties$2(Constructor, staticProps);
    return Constructor;
  }
  /* global window */


  var XHRHttpStack = /*#__PURE__*/function () {
    function XHRHttpStack() {
      _classCallCheck$3(this, XHRHttpStack);
    }

    _createClass$3(XHRHttpStack, [{
      key: "createRequest",
      value: function createRequest(method, url) {
        return new Request(method, url);
      }
    }, {
      key: "getName",
      value: function getName() {
        return 'XHRHttpStack';
      }
    }]);

    return XHRHttpStack;
  }();

  var Request = /*#__PURE__*/function () {
    function Request(method, url) {
      _classCallCheck$3(this, Request);

      this._xhr = new XMLHttpRequest();

      this._xhr.open(method, url, true);

      this._method = method;
      this._url = url;
      this._headers = {};
    }

    _createClass$3(Request, [{
      key: "getMethod",
      value: function getMethod() {
        return this._method;
      }
    }, {
      key: "getURL",
      value: function getURL() {
        return this._url;
      }
    }, {
      key: "setHeader",
      value: function setHeader(header, value) {
        this._xhr.setRequestHeader(header, value);

        this._headers[header] = value;
      }
    }, {
      key: "getHeader",
      value: function getHeader(header) {
        return this._headers[header];
      }
    }, {
      key: "setProgressHandler",
      value: function setProgressHandler(progressHandler) {
        // Test support for progress events before attaching an event listener
        if (!('upload' in this._xhr)) {
          return;
        }

        this._xhr.upload.onprogress = function (e) {
          if (!e.lengthComputable) {
            return;
          }

          progressHandler(e.loaded);
        };
      }
    }, {
      key: "send",
      value: function send() {
        var _this = this;

        var body = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        return new Promise(function (resolve, reject) {
          _this._xhr.onload = function () {
            resolve(new Response(_this._xhr));
          };

          _this._xhr.onerror = function (err) {
            reject(err);
          };

          _this._xhr.send(body);
        });
      }
    }, {
      key: "abort",
      value: function abort() {
        this._xhr.abort();

        return Promise.resolve();
      }
    }, {
      key: "getUnderlyingObject",
      value: function getUnderlyingObject() {
        return this._xhr;
      }
    }]);

    return Request;
  }();

  var Response = /*#__PURE__*/function () {
    function Response(xhr) {
      _classCallCheck$3(this, Response);

      this._xhr = xhr;
    }

    _createClass$3(Response, [{
      key: "getStatus",
      value: function getStatus() {
        return this._xhr.status;
      }
    }, {
      key: "getHeader",
      value: function getHeader(header) {
        return this._xhr.getResponseHeader(header);
      }
    }, {
      key: "getBody",
      value: function getBody() {
        return this._xhr.responseText;
      }
    }, {
      key: "getUnderlyingObject",
      value: function getUnderlyingObject() {
        return this._xhr;
      }
    }]);

    return Response;
  }();

  var isReactNative = function isReactNative() {
    return typeof navigator !== 'undefined' && typeof navigator.product === 'string' && navigator.product.toLowerCase() === 'reactnative';
  };

  /**
   * uriToBlob resolves a URI to a Blob object. This is used for
   * React Native to retrieve a file (identified by a file://
   * URI) as a blob.
   */
  function uriToBlob(uri) {
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';

      xhr.onload = function () {
        var blob = xhr.response;
        resolve(blob);
      };

      xhr.onerror = function (err) {
        reject(err);
      };

      xhr.open('GET', uri);
      xhr.send();
    });
  }

  var isCordova = function isCordova() {
    return typeof window != 'undefined' && (typeof window.PhoneGap != 'undefined' || typeof window.Cordova != 'undefined' || typeof window.cordova != 'undefined');
  };

  /**
   * readAsByteArray converts a File object to a Uint8Array.
   * This function is only used on the Apache Cordova platform.
   * See https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/index.html#read-a-file
   */
  function readAsByteArray(chunk) {
    return new Promise(function (resolve, reject) {
      var reader = new FileReader();

      reader.onload = function () {
        var value = new Uint8Array(reader.result);
        resolve({
          value: value
        });
      };

      reader.onerror = function (err) {
        reject(err);
      };

      reader.readAsArrayBuffer(chunk);
    });
  }

  function _classCallCheck$2(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties$1(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass$2(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties$1(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties$1(Constructor, staticProps);
    return Constructor;
  }

  var FileSource = /*#__PURE__*/function () {
    // Make this.size a method
    function FileSource(file) {
      _classCallCheck$2(this, FileSource);

      this._file = file;
      this.size = file.size;
    }

    _createClass$2(FileSource, [{
      key: "slice",
      value: function slice(start, end) {
        // In Apache Cordova applications, a File must be resolved using
        // FileReader instances, see
        // https://cordova.apache.org/docs/en/8.x/reference/cordova-plugin-file/index.html#read-a-file
        if (isCordova()) {
          return readAsByteArray(this._file.slice(start, end));
        }

        var value = this._file.slice(start, end);

        return Promise.resolve({
          value: value
        });
      }
    }, {
      key: "close",
      value: function close() {// Nothing to do here since we don't need to release any resources.
      }
    }]);

    return FileSource;
  }();

  var StreamSource = /*#__PURE__*/function () {
    function StreamSource(reader, chunkSize) {
      _classCallCheck$2(this, StreamSource);

      this._chunkSize = chunkSize;
      this._buffer = undefined;
      this._bufferOffset = 0;
      this._reader = reader;
      this._done = false;
    }

    _createClass$2(StreamSource, [{
      key: "slice",
      value: function slice(start, end) {
        if (start < this._bufferOffset) {
          return Promise.reject(new Error("Requested data is before the reader's current offset"));
        }

        return this._readUntilEnoughDataOrDone(start, end);
      }
    }, {
      key: "_readUntilEnoughDataOrDone",
      value: function _readUntilEnoughDataOrDone(start, end) {
        var _this = this;

        var hasEnoughData = end <= this._bufferOffset + len(this._buffer);

        if (this._done || hasEnoughData) {
          var value = this._getDataFromBuffer(start, end);

          var done = value == null ? this._done : false;
          return Promise.resolve({
            value: value,
            done: done
          });
        }

        return this._reader.read().then(function (_ref) {
          var value = _ref.value,
              done = _ref.done;

          if (done) {
            _this._done = true;
          } else if (_this._buffer === undefined) {
            _this._buffer = value;
          } else {
            _this._buffer = concat(_this._buffer, value);
          }

          return _this._readUntilEnoughDataOrDone(start, end);
        });
      }
    }, {
      key: "_getDataFromBuffer",
      value: function _getDataFromBuffer(start, end) {
        // Remove data from buffer before `start`.
        // Data might be reread from the buffer if an upload fails, so we can only
        // safely delete data when it comes *before* what is currently being read.
        if (start > this._bufferOffset) {
          this._buffer = this._buffer.slice(start - this._bufferOffset);
          this._bufferOffset = start;
        } // If the buffer is empty after removing old data, all data has been read.


        var hasAllDataBeenRead = len(this._buffer) === 0;

        if (this._done && hasAllDataBeenRead) {
          return null;
        } // We already removed data before `start`, so we just return the first
        // chunk from the buffer.


        return this._buffer.slice(0, end - start);
      }
    }, {
      key: "close",
      value: function close() {
        if (this._reader.cancel) {
          this._reader.cancel();
        }
      }
    }]);

    return StreamSource;
  }();

  function len(blobOrArray) {
    if (blobOrArray === undefined) return 0;
    if (blobOrArray.size !== undefined) return blobOrArray.size;
    return blobOrArray.length;
  }
  /*
    Typed arrays and blobs don't have a concat method.
    This function helps StreamSource accumulate data to reach chunkSize.
  */


  function concat(a, b) {
    if (a.concat) {
      // Is `a` an Array?
      return a.concat(b);
    }

    if (a instanceof Blob) {
      return new Blob([a, b], {
        type: a.type
      });
    }

    if (a.set) {
      // Is `a` a typed array?
      var c = new a.constructor(a.length + b.length);
      c.set(a);
      c.set(b, a.length);
      return c;
    }

    throw new Error('Unknown data type');
  }

  var FileReader$1 = /*#__PURE__*/function () {
    function FileReader() {
      _classCallCheck$2(this, FileReader);
    }

    _createClass$2(FileReader, [{
      key: "openFile",
      value: function openFile(input, chunkSize) {
        // In React Native, when user selects a file, instead of a File or Blob,
        // you usually get a file object {} with a uri property that contains
        // a local path to the file. We use XMLHttpRequest to fetch
        // the file blob, before uploading with tus.
        if (isReactNative() && input && typeof input.uri !== 'undefined') {
          return uriToBlob(input.uri).then(function (blob) {
            return new FileSource(blob);
          })["catch"](function (err) {
            throw new Error("tus: cannot fetch `file.uri` as Blob, make sure the uri is correct and accessible. ".concat(err));
          });
        } // Since we emulate the Blob type in our tests (not all target browsers
        // support it), we cannot use `instanceof` for testing whether the input value
        // can be handled. Instead, we simply check is the slice() function and the
        // size property are available.


        if (typeof input.slice === 'function' && typeof input.size !== 'undefined') {
          return Promise.resolve(new FileSource(input));
        }

        if (typeof input.read === 'function') {
          chunkSize = +chunkSize;

          if (!isFinite(chunkSize)) {
            return Promise.reject(new Error('cannot create source for stream without a finite value for the `chunkSize` option'));
          }

          return Promise.resolve(new StreamSource(input, chunkSize));
        }

        return Promise.reject(new Error('source object may only be an instance of File, Blob, or Reader in this environment'));
      }
    }]);

    return FileReader;
  }();

  /**
   * Generate a fingerprint for a file which will be used the store the endpoint
   *
   * @param {File} file
   * @param {Object} options
   * @param {Function} callback
   */

  function fingerprint(file, options) {
    if (isReactNative()) {
      return Promise.resolve(reactNativeFingerprint(file, options));
    }

    return Promise.resolve(['tus-br', file.name, file.type, file.size, file.lastModified, options.endpoint].join('-'));
  }

  function reactNativeFingerprint(file, options) {
    var exifHash = file.exif ? hashCode(JSON.stringify(file.exif)) : 'noexif';
    return ['tus-rn', file.name || 'noname', file.size || 'nosize', exifHash, options.endpoint].join('/');
  }

  function hashCode(str) {
    // from https://stackoverflow.com/a/8831937/151666
    var hash = 0;

    if (str.length === 0) {
      return hash;
    }

    for (var i = 0; i < str.length; i++) {
      var _char = str.charCodeAt(i);

      hash = (hash << 5) - hash + _char;
      hash &= hash; // Convert to 32bit integer
    }

    return hash;
  }

  function _typeof(obj) {
    "@babel/helpers - typeof";

    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function _typeof(obj) {
        return typeof obj;
      };
    } else {
      _typeof = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof(obj);
  }

  function _classCallCheck$1(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass$1(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  function _inherits$1(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };

    return _setPrototypeOf(o, p);
  }

  function _createSuper(Derived) {
    return function () {
      var Super = _getPrototypeOf(Derived),
          result;

      if (_isNativeReflectConstruct()) {
        var NewTarget = _getPrototypeOf(this).constructor;

        result = Reflect.construct(Super, arguments, NewTarget);
      } else {
        result = Super.apply(this, arguments);
      }

      return _possibleConstructorReturn$1(this, result);
    };
  }

  function _possibleConstructorReturn$1(self, call) {
    if (call && (_typeof(call) === "object" || typeof call === "function")) {
      return call;
    }

    return _assertThisInitialized(self);
  }

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return self;
  }

  function _isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  var defaultOptions = _objectSpread({}, BaseUpload.defaultOptions, {
    httpStack: new XHRHttpStack(),
    fileReader: new FileReader$1(),
    urlStorage: canStoreURLs ? new WebStorageUrlStorage() : new NoopUrlStorage(),
    fingerprint: fingerprint
  });

  var Upload = /*#__PURE__*/function (_BaseUpload) {
    _inherits$1(Upload, _BaseUpload);

    var _super = _createSuper(Upload);

    function Upload() {
      var file = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      _classCallCheck$1(this, Upload);

      options = _objectSpread({}, defaultOptions, {}, options);
      return _super.call(this, file, options);
    }

    _createClass$1(Upload, null, [{
      key: "terminate",
      value: function terminate(url, options, cb) {
        options = _objectSpread({}, defaultOptions, {}, options);
        return BaseUpload.terminate(url, options, cb);
      }
    }]);

    return Upload;
  }(BaseUpload);

  // Names that are part of the external SDK interface, i.e. those which our clients use.
  // Names of all provided x-tags.
  var TAG_ROOT = "x-utu-root";
  var TAG_RECOMMENDATION = "x-utu-recommendation";
  var TAG_FEEDBACK_FORM = "x-utu-feedback-form";
  var TAG_FEEDBACK_FORM_POPUP = "x-utu-feedback-form-popup";
  var TAG_FEEDBACK_DETAILS_POPUP = "x-utu-feedback-details-popup"; // Attribute names of all attributes provided by all TAGs.

  var ATTR_API_URL = "api-url";
  var ATTR_SOURCE_UUID = "source-uuid";
  var ATTR_TARGET_TYPE = "target-type";
  var ATTR_TARGET_UUIDS = "target-uuids";
  var ATTR_TARGET_UUID = "target-uuid";
  var ATTR_TRANSACTION_ID = "transaction-id"; // API resource paths

  var API_RANKING = "/ranking";
  var API_FEEDBACK = "/feedback";
  var API_BADGES = "/badges";
  var API_FEEDBACK_SUMMARY = "/feedbackSummary"; // Events

  var EVENT_UTU_IDENTITY_DATA_READY = "utuIdentityDataReady"; // Local Storage Keys

  var LOCAL_STORAGE_KEY_UTU_IDENTITY_DATA = "utu-identity-data";

  function e$1(e) {
    this.message = e;
  }

  e$1.prototype = new Error(), e$1.prototype.name = "InvalidCharacterError";

  var r$1 = "undefined" != typeof window && window.atob && window.atob.bind(window) || function (r) {
    var t = String(r).replace(/=+$/, "");
    if (t.length % 4 == 1) throw new e$1("'atob' failed: The string to be decoded is not correctly encoded.");

    for (var n, o, a = 0, i = 0, c = ""; o = t.charAt(i++); ~o && (n = a % 4 ? 64 * n + o : o, a++ % 4) ? c += String.fromCharCode(255 & n >> (-2 * a & 6)) : 0) o = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(o);

    return c;
  };

  function t$1(e) {
    var t = e.replace(/-/g, "+").replace(/_/g, "/");

    switch (t.length % 4) {
      case 0:
        break;

      case 2:
        t += "==";
        break;

      case 3:
        t += "=";
        break;

      default:
        throw "Illegal base64url string!";
    }

    try {
      return function (e) {
        return decodeURIComponent(r$1(e).replace(/(.)/g, function (e, r) {
          var t = r.charCodeAt(0).toString(16).toUpperCase();
          return t.length < 2 && (t = "0" + t), "%" + t;
        }));
      }(t);
    } catch (e) {
      return r$1(t);
    }
  }

  function n$1(e) {
    this.message = e;
  }

  function o$2(e, r) {
    if ("string" != typeof e) throw new n$1("Invalid token specified");
    var o = !0 === (r = r || {}).header ? 0 : 1;

    try {
      return JSON.parse(t$1(e.split(".")[o]));
    } catch (e) {
      throw new n$1("Invalid token specified: " + e.message);
    }
  }

  n$1.prototype = new Error(), n$1.prototype.name = "InvalidTokenError";

  function useAuth() {
    var _useState = l$1(getAccessTokenFromLocalStorage()),
        _useState2 = _slicedToArray(_useState, 2),
        accessToken = _useState2[0],
        setAccessToken = _useState2[1];

    y(function () {
      function onUtuTokensReady(event) {
        var access_token = event.detail.access_token;
        setAccessToken(access_token);
      }

      window.addEventListener(EVENT_UTU_IDENTITY_DATA_READY, onUtuTokensReady);
      return function () {
        window.removeEventListener(EVENT_UTU_IDENTITY_DATA_READY, onUtuTokensReady);
      };
    }, []);
    return {
      accessToken: accessToken
    };
  }

  function getAccessTokenFromLocalStorage() {
    var item = localStorage.getItem(LOCAL_STORAGE_KEY_UTU_IDENTITY_DATA);

    if (item) {
      var _JSON$parse = JSON.parse(item),
          access_token = _JSON$parse.access_token;

      var _ref = o$2(access_token),
          exp = _ref.exp;

      if (isValid(exp)) {
        return access_token;
      }
    }

    return null;
  }

  function isValid(exp) {
    return Date.now() < exp * 1000;
  }

  function useRankingApi(apiUrl, sourceUuid, targetType) {
    var _useAuth = useAuth(),
        accessToken = _useAuth.accessToken;

    var _useState = l$1(undefined),
        _useState2 = _slicedToArray(_useState, 2),
        rankingItems = _useState2[0],
        setRankingItems = _useState2[1];

    var queryParams = lib.stringify({
      sourceCriteria: JSON.stringify(createEntityCriteria(sourceUuid)),
      targetType: targetType
    });
    y(function () {
      function f() {
        return _f.apply(this, arguments);
      }

      function _f() {
        _f = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
          var response;
          return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _context.next = 2;
                  return axios.get("".concat(apiUrl).concat(API_RANKING, "?").concat(queryParams), withAuthorizationHeader(accessToken));

                case 2:
                  response = _context.sent;
                  setRankingItems(response.data.result);

                case 4:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee);
        }));
        return _f.apply(this, arguments);
      }

      f();
    }, [setRankingItems, apiUrl, queryParams, accessToken]);
    return {
      rankingItems: rankingItems
    };
  }
  function useFeedbackApi(apiUrl, sourceUuid, targetUuid, transactionId) {
    var _useAuth2 = useAuth(),
        accessToken = _useAuth2.accessToken;

    var sourceCriteria = createEntityCriteria(sourceUuid);
    var targetCriteria = createEntityCriteria(targetUuid);

    var sendFeedback = function sendFeedback(feedbackData) {
      return axios.post("".concat(apiUrl).concat(API_FEEDBACK), {
        sourceCriteria: sourceCriteria,
        targetCriteria: targetCriteria,
        transactionId: transactionId,
        data: feedbackData
      }, withAuthorizationHeader(accessToken)).then(function () {
        console.log("successfully uploaded to utu-api");
      }).catch(function (err) {
        console.log(err);
      });
    };

    return {
      sendFeedback: sendFeedback
    };
  }
  function useBadgesApi(apiUrl) {
    var _useAuth3 = useAuth(),
        accessToken = _useAuth3.accessToken;

    var _useState3 = l$1([]),
        _useState4 = _slicedToArray(_useState3, 2),
        badgeSets = _useState4[0],
        setBadgeSets = _useState4[1];

    y(function () {
      function f() {
        return _f2.apply(this, arguments);
      }

      function _f2() {
        _f2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
          var response;
          return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  _context2.next = 2;
                  return axios.get("".concat(apiUrl).concat(API_BADGES), withAuthorizationHeader(accessToken));

                case 2:
                  response = _context2.sent;
                  setBadgeSets(response.data.result);

                case 4:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2);
        }));
        return _f2.apply(this, arguments);
      }

      f();
    }, [setBadgeSets, apiUrl, accessToken]);
    return {
      badgeSets: badgeSets
    };
  }
  function useFeedbackSummaryApi(apiUrl, targetUuid) {
    var _useAuth4 = useAuth(),
        accessToken = _useAuth4.accessToken;

    var _useState5 = l$1(undefined),
        _useState6 = _slicedToArray(_useState5, 2),
        feedbackSummary = _useState6[0],
        setFeedbackSummary = _useState6[1];

    var queryParams = lib.stringify({
      targetCriteria: JSON.stringify(createEntityCriteria(targetUuid))
    });
    y(function () {
      function f() {
        return _f3.apply(this, arguments);
      }

      function _f3() {
        _f3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
          var response;
          return regeneratorRuntime.wrap(function _callee3$(_context3) {
            while (1) {
              switch (_context3.prev = _context3.next) {
                case 0:
                  _context3.next = 2;
                  return axios.get("".concat(apiUrl).concat(API_FEEDBACK_SUMMARY, "?").concat(queryParams), withAuthorizationHeader(accessToken));

                case 2:
                  response = _context3.sent;
                  setFeedbackSummary(response.data.result.data);

                case 4:
                case "end":
                  return _context3.stop();
              }
            }
          }, _callee3);
        }));
        return _f3.apply(this, arguments);
      }

      f();
    }, [setFeedbackSummary, apiUrl, queryParams, accessToken]);
    return {
      feedbackSummary: feedbackSummary
    };
  }
  function useUploadVideoApi(source) {
    var _useAuth5 = useAuth(),
        accessToken = _useAuth5.accessToken;

    var _useState7 = l$1({
      uploading: false
    }),
        _useState8 = _slicedToArray(_useState7, 2),
        status = _useState8[0],
        setStatus = _useState8[1];

    y(function () {
      if (!source) return;
      var wrappedReader = {
        read: function read() {
          var result = source.read();
          return result;
        }
      };
      var options = {
        resume: false,
        endpoint: "https://stage-ca.ututrust.com/files",
        headers: getAuthorizationHeader(accessToken),
        chunkSize: 28 * 1024,
        uploadLengthDeferred: true,
        retryDelays: [0, 3000, 5000, 10000, 20000],
        metadata: {
          filename: "webcam.webm",
          filetype: "video/webm"
        },
        onError: function onError() {
          setStatus({
            uploading: false,
            errorMessage: "video was not successfully uploaded "
          });
        },
        onSuccess: function onSuccess() {
          var publishedVideoUrl = getPublishedVideoUrl(upload.url);
          var checkVideoInterval = setInterval(function () {
            axios.head(publishedVideoUrl).then(function () {
              setStatus({
                uploading: false,
                publishedVideoUrl: publishedVideoUrl
              });
              clearInterval(checkVideoInterval);
            }).catch(function () {
              return setStatus({
                errorMessage: "Video not yet available."
              });
            });
          }, 500);
          setStatus({
            successMessage: "video was successfully uploaded"
          });
        }
      };
      var upload = new Upload(wrappedReader, options); // Start the upload

      upload.start();
      setStatus({
        uploading: true
      });
    }, [accessToken, source]);
    return status;
  }

  function getPublishedVideoUrl(uploadUrl) {
    var videoId = uploadUrl.split("/").pop();
    return "".concat("https://tus-test-dev-003.s3.eu-central-1.amazonaws.com", "/").concat(videoId);
  }

  function createEntityCriteria(uuid) {
    return {
      ids: {
        uuid: uuid
      }
    };
  }

  function withAuthorizationHeader(accessToken) {
    var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    return _objectSpread2(_objectSpread2({}, config), {}, {
      headers: _objectSpread2(_objectSpread2({}, config.headers || {}), getAuthorizationHeader(accessToken))
    });
  }

  function getAuthorizationHeader(accessToken) {
    return {
      "Authorization": "Bearer ".concat(accessToken)
    };
  }

  function getBaseProps(props, componentName) {
    var apiUrl = props[ATTR_API_URL] || "https://stage-api.ututrust.com";

    if (!apiUrl) {
      throw new Error("".concat(ATTR_API_URL, " attribute of ").concat(componentName, " was not set"));
    }

    return {
      apiUrl: apiUrl
    };
  }
  function BaseComponent(_ref) {
    var children = _ref.children,
        forwardedRef = _ref.forwardedRef,
        className = _ref.className,
        style = _ref.style,
        excludeBootstrap = _ref.excludeBootstrap,
        excludeFonts = _ref.excludeFonts;
    return v$2("div", {
      ref: forwardedRef,
      className: className
    }, v$2("style", null, !excludeFonts && v$2(d$2, null, "@import \"https://fonts.googleapis.com/css2?family=Roboto:wght@300;500&display=swap\";"), !excludeBootstrap && v$2(d$2, null, "@import \"https://getbootstrap.com/docs/5.0/dist/css/bootstrap-reboot.min.css\"; @import \"https://getbootstrap.com/docs/5.0/dist/css/bootstrap-grid.min.css\"; @import \"https://getbootstrap.com/docs/5.0/dist/css/bootstrap-utilities.min.css\";"), style), children);
  }

  function styleInject(css, ref) {
    if (ref === void 0) ref = {};
    var insertAt = ref.insertAt;

    if (!css || typeof document === 'undefined') {
      return;
    }

    var head = document.head || document.getElementsByTagName('head')[0];
    var style = document.createElement('style');
    style.type = 'text/css';

    if (insertAt === 'top') {
      if (head.firstChild) {
        head.insertBefore(style, head.firstChild);
      } else {
        head.appendChild(style);
      }
    } else {
      head.appendChild(style);
    }

    if (style.styleSheet) {
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
  }

  var css_248z$7 = "::slotted(ul) {\n  list-style: none;\n  padding: 0;\n  margin: 0; }\n";
  styleInject(css_248z$7);

  function Root(props) {
    var _getBaseProps = getBaseProps(props, TAG_ROOT),
        apiUrl = _getBaseProps.apiUrl;

    var sourceUuid = props[ATTR_SOURCE_UUID];
    var targetType = props[ATTR_TARGET_TYPE];

    var _useRankingApi = useRankingApi(apiUrl, sourceUuid, targetType),
        rankingItems = _useRankingApi.rankingItems;

    var ref = s$1();
    y(function () {
      if (ref.current && rankingItems) {
        ref.current.data = rankingItems;
        setTimeout(function () {
          ref.current.dispatchEvent(new CustomEvent("loaded", {
            composed: true,
            detail: {
              data: rankingItems
            }
          }));
        }, 0);
      }
    }, [ref, rankingItems]);
    return v$2(BaseComponent, {
      forwardedRef: ref,
      className: "recommendation-root",
      style: css_248z$7,
      excludeBootstrap: true,
      excludeFonts: true
    }, v$2("slot", null));
  }

  // optional / simple context binding


  var functionBindContext = function (fn, that, length) {
    aFunction(fn);
    if (that === undefined) return fn;

    switch (length) {
      case 0:
        return function () {
          return fn.call(that);
        };

      case 1:
        return function (a) {
          return fn.call(that, a);
        };

      case 2:
        return function (a, b) {
          return fn.call(that, a, b);
        };

      case 3:
        return function (a, b, c) {
          return fn.call(that, a, b, c);
        };
    }

    return function () {
      return fn.apply(that, arguments);
    };
  };

  var push = [].push; // `Array.prototype.{ forEach, map, filter, some, every, find, findIndex, filterReject }` methods implementation

  var createMethod$2 = function (TYPE) {
    var IS_MAP = TYPE == 1;
    var IS_FILTER = TYPE == 2;
    var IS_SOME = TYPE == 3;
    var IS_EVERY = TYPE == 4;
    var IS_FIND_INDEX = TYPE == 6;
    var IS_FILTER_REJECT = TYPE == 7;
    var NO_HOLES = TYPE == 5 || IS_FIND_INDEX;
    return function ($this, callbackfn, that, specificCreate) {
      var O = toObject$1($this);
      var self = indexedObject(O);
      var boundFunction = functionBindContext(callbackfn, that, 3);
      var length = toLength(self.length);
      var index = 0;
      var create = specificCreate || arraySpeciesCreate;
      var target = IS_MAP ? create($this, length) : IS_FILTER || IS_FILTER_REJECT ? create($this, 0) : undefined;
      var value, result;

      for (; length > index; index++) if (NO_HOLES || index in self) {
        value = self[index];
        result = boundFunction(value, index, O);

        if (TYPE) {
          if (IS_MAP) target[index] = result; // map
          else if (result) switch (TYPE) {
            case 3:
              return true;
            // some

            case 5:
              return value;
            // find

            case 6:
              return index;
            // findIndex

            case 2:
              push.call(target, value);
            // filter
          } else switch (TYPE) {
            case 4:
              return false;
            // every

            case 7:
              push.call(target, value);
            // filterReject
          }
        }
      }

      return IS_FIND_INDEX ? -1 : IS_SOME || IS_EVERY ? IS_EVERY : target;
    };
  };

  var arrayIteration = {
    // `Array.prototype.forEach` method
    // https://tc39.es/ecma262/#sec-array.prototype.foreach
    forEach: createMethod$2(0),
    // `Array.prototype.map` method
    // https://tc39.es/ecma262/#sec-array.prototype.map
    map: createMethod$2(1),
    // `Array.prototype.filter` method
    // https://tc39.es/ecma262/#sec-array.prototype.filter
    filter: createMethod$2(2),
    // `Array.prototype.some` method
    // https://tc39.es/ecma262/#sec-array.prototype.some
    some: createMethod$2(3),
    // `Array.prototype.every` method
    // https://tc39.es/ecma262/#sec-array.prototype.every
    every: createMethod$2(4),
    // `Array.prototype.find` method
    // https://tc39.es/ecma262/#sec-array.prototype.find
    find: createMethod$2(5),
    // `Array.prototype.findIndex` method
    // https://tc39.es/ecma262/#sec-array.prototype.findIndex
    findIndex: createMethod$2(6),
    // `Array.prototype.filterReject` method
    // https://github.com/tc39/proposal-array-filtering
    filterReject: createMethod$2(7)
  };

  var UNSCOPABLES = wellKnownSymbol('unscopables');
  var ArrayPrototype = Array.prototype; // Array.prototype[@@unscopables]
  // https://tc39.es/ecma262/#sec-array.prototype-@@unscopables

  if (ArrayPrototype[UNSCOPABLES] == undefined) {
    objectDefineProperty.f(ArrayPrototype, UNSCOPABLES, {
      configurable: true,
      value: objectCreate(null)
    });
  } // add a key to Array.prototype[@@unscopables]


  var addToUnscopables = function (key) {
    ArrayPrototype[UNSCOPABLES][key] = true;
  };

  var $find = arrayIteration.find;



  var FIND = 'find';
  var SKIPS_HOLES = true; // Shouldn't skip holes

  if (FIND in []) Array(1)[FIND](function () {
    SKIPS_HOLES = false;
  }); // `Array.prototype.find` method
  // https://tc39.es/ecma262/#sec-array.prototype.find

  _export({
    target: 'Array',
    proto: true,
    forced: SKIPS_HOLES
  }, {
    find: function find(callbackfn
    /* , that = undefined */
    ) {
      return $find(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
    }
  }); // https://tc39.es/ecma262/#sec-array.prototype-@@unscopables

  addToUnscopables(FIND);

  var $map = arrayIteration.map;



  var HAS_SPECIES_SUPPORT = arrayMethodHasSpeciesSupport('map'); // `Array.prototype.map` method
  // https://tc39.es/ecma262/#sec-array.prototype.map
  // with adding support of @@species

  _export({
    target: 'Array',
    proto: true,
    forced: !HAS_SPECIES_SUPPORT
  }, {
    map: function map(callbackfn
    /* , thisArg */
    ) {
      return $map(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
    }
  });

  var css_248z$6 = ":host {\n  margin: 0;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";\n  font-size: 14px;\n  font-weight: 300;\n  line-height: 1.5;\n  color: #000;\n  -webkit-text-size-adjust: 100%;\n  -webkit-tap-highlight-color: rgba(0, 0, 0, 0); }\n\n.utu-section {\n  border-bottom: 1px solid rgba(0, 0, 0, 0.15);\n  background: #fff;\n  padding: 18px; }\n\nh3 {\n  text-transform: capitalize;\n  font-weight: 500;\n  font-size: 14px; }\n\n.btn {\n  background-color: #FFDD33;\n  padding: 12px;\n  border-radius: 10px;\n  border: none;\n  font-size: 14px;\n  font-weight: 500;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\"; }\n\n.icon-btn {\n  border: none;\n  background: none;\n  padding: 0;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";\n  font-weight: 500;\n  font-size: 14px; }\n  .icon-btn svg {\n    width: 60px;\n    height: 60px; }\n\ntextarea {\n  resize: vertical;\n  min-height: 189px;\n  border-radius: 10px;\n  border: none;\n  background-color: rgba(255, 221, 51, 0.15);\n  padding: 16px;\n  font-weight: 300; }\n\n.error {\n  color: #e80054; }\n\n.utu-recommendation {\n  height: 70px; }\n  .utu-recommendation .list-unstyled {\n    padding-left: 0;\n    list-style: none; }\n  .utu-recommendation .summary-image-list {\n    padding-left: 20px; }\n    .utu-recommendation .summary-image-list .summary-image-item {\n      height: 60px;\n      margin-left: -20px; }\n";
  styleInject(css_248z$6);

  function getRoot(ref) {
    if (ref && ref.current) {
      var shadowRoot = ref.current.getRootNode();
      var rootContainer = shadowRoot.host.closest(TAG_ROOT);

      if (rootContainer) {
        return rootContainer.shadowRoot.firstChild;
      }
    }

    return null;
  }

  function getValueFromRoot(ref, targetUuid) {
    var root = getRoot(ref);

    if (root && root.data) {
      return root.data.find(function (item) {
        return item.entity.uuid === targetUuid;
      });
    }

    return null;
  }

  function Recommendation(props) {
    var ref = s$1();
    var targetUuid = props[ATTR_TARGET_UUID];

    var _useState = l$1(getValueFromRoot(ref, targetUuid)),
        _useState2 = _slicedToArray(_useState, 2),
        rankingItem = _useState2[0],
        setRankingItem = _useState2[1];

    y(function () {
      function loaded() {
        setRankingItem(getValueFromRoot(ref, targetUuid));
      }

      if (ref.current && targetUuid) {
        var root = getRoot(ref);

        if (root) {
          root.addEventListener("loaded", loaded);
        }
      }

      return function () {
        var root = getRoot(ref);

        if (root) {
          root.removeEventListener("loaded", loaded);
        }
      };
    }, [ref, targetUuid]);
    return v$2(BaseComponent, {
      style: css_248z$6,
      forwardedRef: ref,
      className: "utu-recommendation utu-section d-flex flex-column align-items-center pt-4 pb-4"
    }, rankingItem && v$2(d$2, null, v$2("div", null, v$2("ul", {
      className: "summary-image-list d-flex justify-content-center list-unstyled mb-4 mt-4"
    }, rankingItem.summaryImages.map(function (image) {
      return v$2("li", {
        key: image
      }, v$2("img", {
        className: "summary-image-item rounded-circle",
        src: image,
        alt: "summary"
      }));
    })), v$2("div", {
      className: "mb-3"
    }, rankingItem.summaryText))));
  }

  var css_248z$5 = ":host {\n  margin: 0;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";\n  font-size: 14px;\n  font-weight: 300;\n  line-height: 1.5;\n  color: #000;\n  -webkit-text-size-adjust: 100%;\n  -webkit-tap-highlight-color: rgba(0, 0, 0, 0); }\n\n.utu-section {\n  border-bottom: 1px solid rgba(0, 0, 0, 0.15);\n  background: #fff;\n  padding: 18px; }\n\nh3 {\n  text-transform: capitalize;\n  font-weight: 500;\n  font-size: 14px; }\n\n.btn {\n  background-color: #FFDD33;\n  padding: 12px;\n  border-radius: 10px;\n  border: none;\n  font-size: 14px;\n  font-weight: 500;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\"; }\n\n.icon-btn {\n  border: none;\n  background: none;\n  padding: 0;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";\n  font-weight: 500;\n  font-size: 14px; }\n  .icon-btn svg {\n    width: 60px;\n    height: 60px; }\n\ntextarea {\n  resize: vertical;\n  min-height: 189px;\n  border-radius: 10px;\n  border: none;\n  background-color: rgba(255, 221, 51, 0.15);\n  padding: 16px;\n  font-weight: 300; }\n\n.error {\n  color: #e80054; }\n";
  styleInject(css_248z$5);

  var css_248z$4 = ":host {\n  margin: 0;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";\n  font-size: 14px;\n  font-weight: 300;\n  line-height: 1.5;\n  color: #000;\n  -webkit-text-size-adjust: 100%;\n  -webkit-tap-highlight-color: rgba(0, 0, 0, 0); }\n\n.utu-section {\n  border-bottom: 1px solid rgba(0, 0, 0, 0.15);\n  background: #fff;\n  padding: 18px; }\n\nh3 {\n  text-transform: capitalize;\n  font-weight: 500;\n  font-size: 14px; }\n\n.btn {\n  background-color: #FFDD33;\n  padding: 12px;\n  border-radius: 10px;\n  border: none;\n  font-size: 14px;\n  font-weight: 500;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\"; }\n\n.icon-btn {\n  border: none;\n  background: none;\n  padding: 0;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";\n  font-weight: 500;\n  font-size: 14px; }\n  .icon-btn svg {\n    width: 60px;\n    height: 60px; }\n\ntextarea {\n  resize: vertical;\n  min-height: 189px;\n  border-radius: 10px;\n  border: none;\n  background-color: rgba(255, 221, 51, 0.15);\n  padding: 16px;\n  font-weight: 300; }\n\n.error {\n  color: #e80054; }\n\nimg.badge_img {\n  height: 60px;\n  width: 60px; }\n\nbutton.badge_img {\n  height: 80px;\n  width: 80px; }\n\n.button {\n  cursor: pointer; }\n\n.button:hover {\n  background-color: #fff; }\n\n.button:active {\n  transform: translateY(4px); }\n\n.btn:disabled {\n  opacity: 0.1; }\n\n.badge_disable {\n  height: 50px;\n  width: 50px;\n  opacity: 0.1; }\n";
  styleInject(css_248z$4);

  var _excluded = ["id", "badges"];
  function BadgeSet(_ref) {
    var id = _ref.id,
        badges = _ref.badges,
        props = _objectWithoutProperties(_ref, _excluded);

    var _useState = l$1(""),
        _useState2 = _slicedToArray(_useState, 2),
        selectedQualifier = _useState2[0],
        setSelectedQualifier = _useState2[1];

    var isSelected = function isSelected(qualifier) {
      return selectedQualifier === qualifier;
    };

    var _getBaseProps = getBaseProps(props, TAG_FEEDBACK_FORM),
        apiUrl = _getBaseProps.apiUrl;

    var _useFeedbackApi = useFeedbackApi(apiUrl, props[ATTR_SOURCE_UUID], props[ATTR_TARGET_UUID], props[ATTR_TRANSACTION_ID]),
        sendFeedback = _useFeedbackApi.sendFeedback;

    return v$2(BaseComponent, {
      style: css_248z$4
    }, v$2("div", {
      key: id,
      className: " pt-4 pb-4 utu-section"
    }, v$2("h3", {
      className: "mt-3"
    }, id), v$2("div", {
      className: "d-flex justify-content-around mt-4 mb-3 pt-2"
    }, badges.map(function (_ref2) {
      var qualifier = _ref2.qualifier,
          data = _ref2.data;
      return v$2("button", {
        className: isSelected(qualifier) ? "badge_disable icon-btn" : "badge_img icon-btn",
        type: "button",
        onClick: function onClick() {
          var qualifierToSet = isSelected(qualifier) ? "neutral" : qualifier;
          sendFeedback({
            badges: _defineProperty$2({}, id, qualifierToSet)
          });
          setSelectedQualifier(qualifierToSet);
        }
      }, v$2("div", {
        className: "text-center px-2"
      }, v$2("img", {
        src: data.image,
        className: "badge_img"
      }), v$2("p", {
        className: "mt-3"
      }, data.label)));
    }))));
  }

  function Badges(props) {
    var _getBaseProps = getBaseProps(props, TAG_FEEDBACK_FORM),
        apiUrl = _getBaseProps.apiUrl;

    var _useBadgesApi = useBadgesApi(apiUrl),
        badgeSets = _useBadgesApi.badgeSets;

    return v$2(BaseComponent, {
      className: "utu-badges d-flex flex-row justify-content-around",
      excludeBootstrap: true,
      excludeFonts: true
    }, badgeSets.map(function (_ref) {
      var id = _ref.id,
          badges = _ref.badges;
      return v$2("div", null, v$2(BadgeSet, _extends$1({}, props, {
        id: id,
        badges: badges
      })));
    }));
  }

  function S(n,t){for(var e in t)n[e]=t[e];return n}function C(n,t){for(var e in n)if("__source"!==e&&!(e in t))return !0;for(var r in t)if("__source"!==r&&n[r]!==t[r])return !0;return !1}function E(n){this.props=n;}function g(n,t){function e(n){var e=this.props.ref,r=e==n.ref;return !r&&e&&(e.call?e(null):e.current=null),t?!t(this.props,n)||!r:C(this.props,n)}function r(t){return this.shouldComponentUpdate=e,v$2(n,t)}return r.displayName="Memo("+(n.displayName||n.name)+")",r.prototype.isReactComponent=!0,r.__f=!0,r}(E.prototype=new _$1).isPureReactComponent=!0,E.prototype.shouldComponentUpdate=function(n,t){return C(this.props,n)||C(this.state,t)};var w=l$3.__b;l$3.__b=function(n){n.type&&n.type.__f&&n.ref&&(n.props.ref=n.ref,n.ref=null),w&&w(n);};var R="undefined"!=typeof Symbol&&Symbol.for&&Symbol.for("react.forward_ref")||3911;function x(n){function t(t,e){var r=S({},t);return delete r.ref,n(r,(e=t.ref||e)&&("object"!=typeof e||"current"in e)?e:null)}return t.$$typeof=R,t.render=t,t.prototype.isReactComponent=t.__f=!0,t.displayName="ForwardRef("+(n.displayName||n.name)+")",t}var N=function(n,t){return null==n?null:A$2(A$2(n).map(t))},k={map:N,forEach:N,count:function(n){return n?A$2(n).length:0},only:function(n){var t=A$2(n);if(1!==t.length)throw "Children.only";return t[0]},toArray:A$2},A=l$3.__e;l$3.__e=function(n,t,e){if(n.then)for(var r,u=t;u=u.__;)if((r=u.__c)&&r.__c)return null==t.__e&&(t.__e=e.__e,t.__k=e.__k),r.__c(n,t);A(n,t,e);};var O=l$3.unmount;function L(){this.__u=0,this.t=null,this.__b=null;}function U(n){var t=n.__.__c;return t&&t.__e&&t.__e(n)}function F(n){var t,e,r;function u(u){if(t||(t=n()).then(function(n){e=n.default||n;},function(n){r=n;}),r)throw r;if(!e)throw t;return v$2(e,u)}return u.displayName="Lazy",u.__f=!0,u}function M(){this.u=null,this.o=null;}l$3.unmount=function(n){var t=n.__c;t&&t.__R&&t.__R(),t&&!0===n.__h&&(n.type=null),O&&O(n);},(L.prototype=new _$1).__c=function(n,t){var e=t.__c,r=this;null==r.t&&(r.t=[]),r.t.push(e);var u=U(r.__v),o=!1,i=function(){o||(o=!0,e.__R=null,u?u(l):l());};e.__R=i;var l=function(){if(!--r.__u){if(r.state.__e){var n=r.state.__e;r.__v.__k[0]=function n(t,e,r){return t&&(t.__v=null,t.__k=t.__k&&t.__k.map(function(t){return n(t,e,r)}),t.__c&&t.__c.__P===e&&(t.__e&&r.insertBefore(t.__e,t.__d),t.__c.__e=!0,t.__c.__P=r)),t}(n,n.__c.__P,n.__c.__O);}var t;for(r.setState({__e:r.__b=null});t=r.t.pop();)t.forceUpdate();}},f=!0===t.__h;r.__u++||f||r.setState({__e:r.__b=r.__v.__k[0]}),n.then(i,i);},L.prototype.componentWillUnmount=function(){this.t=[];},L.prototype.render=function(n,t){if(this.__b){if(this.__v.__k){var e=document.createElement("div"),r=this.__v.__k[0].__c;this.__v.__k[0]=function n(t,e,r){return t&&(t.__c&&t.__c.__H&&(t.__c.__H.__.forEach(function(n){"function"==typeof n.__c&&n.__c();}),t.__c.__H=null),null!=(t=S({},t)).__c&&(t.__c.__P===r&&(t.__c.__P=e),t.__c=null),t.__k=t.__k&&t.__k.map(function(t){return n(t,e,r)})),t}(this.__b,e,r.__O=r.__P);}this.__b=null;}var u=t.__e&&v$2(d$2,null,n.fallback);return u&&(u.__h=null),[v$2(d$2,null,t.__e?null:n.children),u]};var T=function(n,t,e){if(++e[1]===e[0]&&n.o.delete(t),n.props.revealOrder&&("t"!==n.props.revealOrder[0]||!n.o.size))for(e=n.u;e;){for(;e.length>3;)e.pop()();if(e[1]<e[0])break;n.u=e=e[2];}};function D(n){return this.getChildContext=function(){return n.context},n.children}function I(n){var t=this,e=n.i;t.componentWillUnmount=function(){S$1(null,t.l),t.l=null,t.i=null;},t.i&&t.i!==e&&t.componentWillUnmount(),n.__v?(t.l||(t.i=e,t.l={nodeType:1,parentNode:e,childNodes:[],appendChild:function(n){this.childNodes.push(n),t.i.appendChild(n);},insertBefore:function(n,e){this.childNodes.push(n),t.i.appendChild(n);},removeChild:function(n){this.childNodes.splice(this.childNodes.indexOf(n)>>>1,1),t.i.removeChild(n);}}),S$1(v$2(D,{context:t.context},n.__v),t.l)):t.l&&t.componentWillUnmount();}function W(n,t){return v$2(I,{__v:n,i:t})}(M.prototype=new _$1).__e=function(n){var t=this,e=U(t.__v),r=t.o.get(n);return r[0]++,function(u){var o=function(){t.props.revealOrder?(r.push(u),T(t,n,r)):u();};e?e(o):o();}},M.prototype.render=function(n){this.u=null,this.o=new Map;var t=A$2(n.children);n.revealOrder&&"b"===n.revealOrder[0]&&t.reverse();for(var e=t.length;e--;)this.o.set(t[e],this.u=[1,0,this.u]);return n.children},M.prototype.componentDidUpdate=M.prototype.componentDidMount=function(){var n=this;this.o.forEach(function(t,e){T(n,e,t);});};var j$1="undefined"!=typeof Symbol&&Symbol.for&&Symbol.for("react.element")||60103,P=/^(?:accent|alignment|arabic|baseline|cap|clip(?!PathU)|color|fill|flood|font|glyph(?!R)|horiz|marker(?!H|W|U)|overline|paint|stop|strikethrough|stroke|text(?!L)|underline|unicode|units|v|vector|vert|word|writing|x(?!C))[A-Z]/,V=function(n){return ("undefined"!=typeof Symbol&&"symbol"==typeof Symbol()?/fil|che|rad/i:/fil|che|ra/i).test(n)};function z(n,t,e){return null==t.__k&&(t.textContent=""),S$1(n,t),"function"==typeof e&&e(),n?n.__c:null}function B(n,t,e){return q$2(n,t),"function"==typeof e&&e(),n?n.__c:null}_$1.prototype.isReactComponent={},["componentWillMount","componentWillReceiveProps","componentWillUpdate"].forEach(function(n){Object.defineProperty(_$1.prototype,n,{configurable:!0,get:function(){return this["UNSAFE_"+n]},set:function(t){Object.defineProperty(this,n,{configurable:!0,writable:!0,value:t});}});});var H=l$3.event;function Z(){}function Y(){return this.cancelBubble}function $(){return this.defaultPrevented}l$3.event=function(n){return H&&(n=H(n)),n.persist=Z,n.isPropagationStopped=Y,n.isDefaultPrevented=$,n.nativeEvent=n};var q,G={configurable:!0,get:function(){return this.class}},J=l$3.vnode;l$3.vnode=function(n){var t=n.type,e=n.props,r=e;if("string"==typeof t){for(var u in r={},e){var o=e[u];"value"===u&&"defaultValue"in e&&null==o||("defaultValue"===u&&"value"in e&&null==e.value?u="value":"download"===u&&!0===o?o="":/ondoubleclick/i.test(u)?u="ondblclick":/^onchange(textarea|input)/i.test(u+t)&&!V(e.type)?u="oninput":/^on(Ani|Tra|Tou|BeforeInp)/.test(u)?u=u.toLowerCase():P.test(u)?u=u.replace(/[A-Z0-9]/,"-$&").toLowerCase():null===o&&(o=void 0),r[u]=o);}"select"==t&&r.multiple&&Array.isArray(r.value)&&(r.value=A$2(e.children).forEach(function(n){n.props.selected=-1!=r.value.indexOf(n.props.value);})),"select"==t&&null!=r.defaultValue&&(r.value=A$2(e.children).forEach(function(n){n.props.selected=r.multiple?-1!=r.defaultValue.indexOf(n.props.value):r.defaultValue==n.props.value;})),n.props=r;}t&&e.class!=e.className&&(G.enumerable="className"in e,null!=e.className&&(r.class=e.className),Object.defineProperty(r,"className",G)),n.$$typeof=j$1,J&&J(n);};var K=l$3.__r;l$3.__r=function(n){K&&K(n),q=n.__c;};var Q={ReactCurrentDispatcher:{current:{readContext:function(n){return q.__n[n.__c].props.value}}}},X="17.0.2";function nn(n){return v$2.bind(null,n)}function tn(n){return !!n&&n.$$typeof===j$1}function en(n){return tn(n)?B$1.apply(null,arguments):n}function rn(n){return !!n.__k&&(S$1(null,n),!0)}function un(n){return n&&(n.base||1===n.nodeType&&n)||null}var on=function(n,t){return n(t)},ln=function(n,t){return n(t)},fn=d$2;var compat = {useState:l$1,useReducer:p,useEffect:y,useLayoutEffect:h$1,useRef:s$1,useImperativeHandle:_,useMemo:d$1,useCallback:A$1,useContext:F$1,useDebugValue:T$1,version:"17.0.2",Children:k,render:z,hydrate:B,unmountComponentAtNode:rn,createPortal:W,createElement:v$2,createContext:D$1,createFactory:nn,cloneElement:en,createRef:p$1,Fragment:d$2,isValidElement:tn,findDOMNode:un,Component:_$1,PureComponent:E,memo:g,forwardRef:x,flushSync:ln,unstable_batchedUpdates:on,StrictMode:d$2,Suspense:L,SuspenseList:M,lazy:F,__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED:Q};

  var compat$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': compat,
    version: X,
    Children: k,
    render: z,
    hydrate: B,
    unmountComponentAtNode: rn,
    createPortal: W,
    createFactory: nn,
    cloneElement: en,
    isValidElement: tn,
    findDOMNode: un,
    PureComponent: E,
    memo: g,
    forwardRef: x,
    flushSync: ln,
    unstable_batchedUpdates: on,
    StrictMode: fn,
    Suspense: L,
    SuspenseList: M,
    lazy: F,
    __SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED: Q,
    createElement: v$2,
    createContext: D$1,
    createRef: p$1,
    Fragment: d$2,
    Component: _$1,
    useState: l$1,
    useReducer: p,
    useEffect: y,
    useLayoutEffect: h$1,
    useRef: s$1,
    useImperativeHandle: _,
    useMemo: d$1,
    useCallback: A$1,
    useContext: F$1,
    useDebugValue: T$1,
    useErrorBoundary: q$1
  });

  var isHTMLElement = value => value instanceof HTMLElement;

  const EVENTS = {
    BLUR: 'blur',
    CHANGE: 'change',
    INPUT: 'input'
  };
  const VALIDATION_MODE = {
    onBlur: 'onBlur',
    onChange: 'onChange',
    onSubmit: 'onSubmit',
    onTouched: 'onTouched',
    all: 'all'
  };
  const SELECT = 'select';
  const UNDEFINED = 'undefined';
  const INPUT_VALIDATION_RULES = {
    max: 'max',
    min: 'min',
    maxLength: 'maxLength',
    minLength: 'minLength',
    pattern: 'pattern',
    required: 'required',
    validate: 'validate'
  };

  function attachEventListeners({
    ref
  }, shouldAttachChangeEvent, handleChange) {
    if (isHTMLElement(ref) && handleChange) {
      ref.addEventListener(shouldAttachChangeEvent ? EVENTS.CHANGE : EVENTS.INPUT, handleChange);
      ref.addEventListener(EVENTS.BLUR, handleChange);
    }
  }

  var isNullOrUndefined = value => value == null;

  const isObjectType = value => typeof value === 'object';

  var isObject = value => !isNullOrUndefined(value) && !Array.isArray(value) && isObjectType(value) && !(value instanceof Date);

  var isKey = value => /^\w*$/.test(value);

  var compact = value => value.filter(Boolean);

  var stringToPath = input => compact(input.replace(/["|']/g, '').replace(/\[/g, '.').replace(/\]/g, '').split('.'));

  function set(object, path, value) {
    let index = -1;
    const tempPath = isKey(path) ? [path] : stringToPath(path);
    const length = tempPath.length;
    const lastIndex = length - 1;

    while (++index < length) {
      const key = tempPath[index];
      let newValue = value;

      if (index !== lastIndex) {
        const objValue = object[key];
        newValue = isObject(objValue) || Array.isArray(objValue) ? objValue : !isNaN(+tempPath[index + 1]) ? [] : {};
      }

      object[key] = newValue;
      object = object[key];
    }

    return object;
  }

  var transformToNestObject = (data, value = {}) => {
    for (const key in data) {
      !isKey(key) ? set(value, key, data[key]) : value[key] = data[key];
    }

    return value;
  };

  var isUndefined = val => val === undefined;

  var get = (obj = {}, path, defaultValue) => {
    const result = compact(path.split(/[,[\].]+?/)).reduce((result, key) => isNullOrUndefined(result) ? result : result[key], obj);
    return isUndefined(result) || result === obj ? isUndefined(obj[path]) ? defaultValue : obj[path] : result;
  };

  var focusOnErrorField = (fields, fieldErrors) => {
    for (const key in fields) {
      if (get(fieldErrors, key)) {
        const field = fields[key];

        if (field) {
          if (field.ref.focus && isUndefined(field.ref.focus())) {
            break;
          } else if (field.options) {
            field.options[0].ref.focus();
            break;
          }
        }
      }
    }
  };

  var removeAllEventListeners = (ref, validateWithStateUpdate) => {
    if (isHTMLElement(ref) && ref.removeEventListener) {
      ref.removeEventListener(EVENTS.INPUT, validateWithStateUpdate);
      ref.removeEventListener(EVENTS.CHANGE, validateWithStateUpdate);
      ref.removeEventListener(EVENTS.BLUR, validateWithStateUpdate);
    }
  };

  const defaultReturn = {
    isValid: false,
    value: null
  };

  var getRadioValue = options => Array.isArray(options) ? options.reduce((previous, option) => option && option.ref.checked ? {
    isValid: true,
    value: option.ref.value
  } : previous, defaultReturn) : defaultReturn;

  var getMultipleSelectValue = options => [...options].filter(({
    selected
  }) => selected).map(({
    value
  }) => value);

  var isRadioInput = element => element.type === 'radio';

  var isFileInput = element => element.type === 'file';

  var isCheckBoxInput = element => element.type === 'checkbox';

  var isMultipleSelect = element => element.type === `${SELECT}-multiple`;

  const defaultResult = {
    value: false,
    isValid: false
  };
  const validResult = {
    value: true,
    isValid: true
  };

  var getCheckboxValue = options => {
    if (Array.isArray(options)) {
      if (options.length > 1) {
        const values = options.filter(option => option && option.ref.checked).map(({
          ref: {
            value
          }
        }) => value);
        return {
          value: values,
          isValid: !!values.length
        };
      }

      const {
        checked,
        value,
        attributes
      } = options[0].ref;
      return checked ? attributes && !isUndefined(attributes.value) ? isUndefined(value) || value === '' ? validResult : {
        value: value,
        isValid: true
      } : validResult : defaultResult;
    }

    return defaultResult;
  };

  function getFieldValue(fieldsRef, name, shallowFieldsStateRef, excludeDisabled, shouldKeepRawValue) {
    const field = fieldsRef.current[name];

    if (field) {
      const {
        ref: {
          value,
          disabled
        },
        ref,
        valueAsNumber,
        valueAsDate,
        setValueAs
      } = field;

      if (disabled && excludeDisabled) {
        return;
      }

      if (isFileInput(ref)) {
        return ref.files;
      }

      if (isRadioInput(ref)) {
        return getRadioValue(field.options).value;
      }

      if (isMultipleSelect(ref)) {
        return getMultipleSelectValue(ref.options);
      }

      if (isCheckBoxInput(ref)) {
        return getCheckboxValue(field.options).value;
      }

      return shouldKeepRawValue ? value : valueAsNumber ? value === '' ? NaN : +value : valueAsDate ? ref.valueAsDate : setValueAs ? setValueAs(value) : value;
    }

    if (shallowFieldsStateRef) {
      return get(shallowFieldsStateRef.current, name);
    }
  }

  function isDetached(element) {
    if (!element) {
      return true;
    }

    if (!(element instanceof HTMLElement) || element.nodeType === Node.DOCUMENT_NODE) {
      return false;
    }

    return isDetached(element.parentNode);
  }

  var isEmptyObject = value => isObject(value) && !Object.keys(value).length;

  var isBoolean = value => typeof value === 'boolean';

  function baseGet(object, updatePath) {
    const length = updatePath.slice(0, -1).length;
    let index = 0;

    while (index < length) {
      object = isUndefined(object) ? index++ : object[updatePath[index++]];
    }

    return object;
  }

  function unset(object, path) {
    const updatePath = isKey(path) ? [path] : stringToPath(path);
    const childObject = updatePath.length == 1 ? object : baseGet(object, updatePath);
    const key = updatePath[updatePath.length - 1];
    let previousObjRef;

    if (childObject) {
      delete childObject[key];
    }

    for (let k = 0; k < updatePath.slice(0, -1).length; k++) {
      let index = -1;
      let objectRef;
      const currentPaths = updatePath.slice(0, -(k + 1));
      const currentPathsLength = currentPaths.length - 1;

      if (k > 0) {
        previousObjRef = object;
      }

      while (++index < currentPaths.length) {
        const item = currentPaths[index];
        objectRef = objectRef ? objectRef[item] : object[item];

        if (currentPathsLength === index && (isObject(objectRef) && isEmptyObject(objectRef) || Array.isArray(objectRef) && !objectRef.filter(data => isObject(data) && !isEmptyObject(data) || isBoolean(data)).length)) {
          previousObjRef ? delete previousObjRef[item] : delete object[item];
        }

        previousObjRef = objectRef;
      }
    }

    return object;
  }

  const isSameRef = (fieldValue, ref) => fieldValue && fieldValue.ref === ref;

  function findRemovedFieldAndRemoveListener(fieldsRef, handleChange, field, shallowFieldsStateRef, shouldUnregister, forceDelete) {
    const {
      ref,
      ref: {
        name
      }
    } = field;
    const fieldRef = fieldsRef.current[name];

    if (!shouldUnregister) {
      const value = getFieldValue(fieldsRef, name, shallowFieldsStateRef);
      !isUndefined(value) && set(shallowFieldsStateRef.current, name, value);
    }

    if (!ref.type || !fieldRef) {
      delete fieldsRef.current[name];
      return;
    }

    if (isRadioInput(ref) || isCheckBoxInput(ref)) {
      if (Array.isArray(fieldRef.options) && fieldRef.options.length) {
        compact(fieldRef.options).forEach((option = {}, index) => {
          if (isDetached(option.ref) && isSameRef(option, option.ref) || forceDelete) {
            removeAllEventListeners(option.ref, handleChange);
            unset(fieldRef.options, `[${index}]`);
          }
        });

        if (fieldRef.options && !compact(fieldRef.options).length) {
          delete fieldsRef.current[name];
        }
      } else {
        delete fieldsRef.current[name];
      }
    } else if (isDetached(ref) && isSameRef(fieldRef, ref) || forceDelete) {
      removeAllEventListeners(ref, handleChange);
      delete fieldsRef.current[name];
    }
  }

  var isPrimitive = value => isNullOrUndefined(value) || !isObjectType(value);

  function deepMerge(target, source) {
    if (isPrimitive(target) || isPrimitive(source)) {
      return source;
    }

    for (const key in source) {
      const targetValue = target[key];
      const sourceValue = source[key];

      try {
        target[key] = isObject(targetValue) && isObject(sourceValue) || Array.isArray(targetValue) && Array.isArray(sourceValue) ? deepMerge(targetValue, sourceValue) : sourceValue;
      } catch (_a) {}
    }

    return target;
  }

  function deepEqual(object1, object2, isErrorObject) {
    if (isPrimitive(object1) || isPrimitive(object2) || object1 instanceof Date || object2 instanceof Date) {
      return object1 === object2;
    }

    if (!tn(object1)) {
      const keys1 = Object.keys(object1);
      const keys2 = Object.keys(object2);

      if (keys1.length !== keys2.length) {
        return false;
      }

      for (const key of keys1) {
        const val1 = object1[key];

        if (!(isErrorObject && key === 'ref')) {
          const val2 = object2[key];

          if ((isObject(val1) || Array.isArray(val1)) && (isObject(val2) || Array.isArray(val2)) ? !deepEqual(val1, val2, isErrorObject) : val1 !== val2) {
            return false;
          }
        }
      }
    }

    return true;
  }

  function setDirtyFields(values, defaultValues, dirtyFields, parentNode, parentName) {
    let index = -1;

    while (++index < values.length) {
      for (const key in values[index]) {
        if (Array.isArray(values[index][key])) {
          !dirtyFields[index] && (dirtyFields[index] = {});
          dirtyFields[index][key] = [];
          setDirtyFields(values[index][key], get(defaultValues[index] || {}, key, []), dirtyFields[index][key], dirtyFields[index], key);
        } else {
          deepEqual(get(defaultValues[index] || {}, key), values[index][key]) ? set(dirtyFields[index] || {}, key) : dirtyFields[index] = Object.assign(Object.assign({}, dirtyFields[index]), {
            [key]: true
          });
        }
      }

      parentNode && !dirtyFields.length && delete parentNode[parentName];
    }

    return dirtyFields;
  }

  var setFieldArrayDirtyFields = (values, defaultValues, dirtyFields) => deepMerge(setDirtyFields(values, defaultValues, dirtyFields.slice(0, values.length)), setDirtyFields(defaultValues, values, dirtyFields.slice(0, values.length)));

  var isString = value => typeof value === 'string';

  var getFieldsValues = (fieldsRef, shallowFieldsState, shouldUnregister, excludeDisabled, search) => {
    const output = {};

    for (const name in fieldsRef.current) {
      if (isUndefined(search) || (isString(search) ? name.startsWith(search) : Array.isArray(search) && search.find(data => name.startsWith(data)))) {
        output[name] = getFieldValue(fieldsRef, name, undefined, excludeDisabled);
      }
    }

    return shouldUnregister ? transformToNestObject(output) : deepMerge(shallowFieldsState, transformToNestObject(output));
  };

  var isErrorStateChanged = ({
    errors,
    name,
    error,
    validFields,
    fieldsWithValidation
  }) => {
    const isValid = isUndefined(error);
    const previousError = get(errors, name);
    return isValid && !!previousError || !isValid && !deepEqual(previousError, error, true) || isValid && get(fieldsWithValidation, name) && !get(validFields, name);
  };

  var isRegex = value => value instanceof RegExp;

  var getValueAndMessage = validationData => isObject(validationData) && !isRegex(validationData) ? validationData : {
    value: validationData,
    message: ''
  };

  var isFunction = value => typeof value === 'function';

  var isMessage = value => isString(value) || tn(value);

  function getValidateError(result, ref, type = 'validate') {
    if (isMessage(result) || isBoolean(result) && !result) {
      return {
        type,
        message: isMessage(result) ? result : '',
        ref
      };
    }
  }

  var appendErrors = (name, validateAllFieldCriteria, errors, type, message) => validateAllFieldCriteria ? Object.assign(Object.assign({}, errors[name]), {
    types: Object.assign(Object.assign({}, errors[name] && errors[name].types ? errors[name].types : {}), {
      [type]: message || true
    })
  }) : {};

  var validateField = async (fieldsRef, validateAllFieldCriteria, {
    ref,
    ref: {
      value
    },
    options,
    required,
    maxLength,
    minLength,
    min,
    max,
    pattern,
    validate
  }, shallowFieldsStateRef) => {
    const name = ref.name;
    const error = {};
    const isRadio = isRadioInput(ref);
    const isCheckBox = isCheckBoxInput(ref);
    const isRadioOrCheckbox = isRadio || isCheckBox;
    const isEmpty = value === '';
    const appendErrorsCurry = appendErrors.bind(null, name, validateAllFieldCriteria, error);

    const getMinMaxMessage = (exceedMax, maxLengthMessage, minLengthMessage, maxType = INPUT_VALIDATION_RULES.maxLength, minType = INPUT_VALIDATION_RULES.minLength) => {
      const message = exceedMax ? maxLengthMessage : minLengthMessage;
      error[name] = Object.assign({
        type: exceedMax ? maxType : minType,
        message,
        ref
      }, exceedMax ? appendErrorsCurry(maxType, message) : appendErrorsCurry(minType, message));
    };

    if (required && (!isRadio && !isCheckBox && (isEmpty || isNullOrUndefined(value)) || isBoolean(value) && !value || isCheckBox && !getCheckboxValue(options).isValid || isRadio && !getRadioValue(options).isValid)) {
      const {
        value,
        message
      } = isMessage(required) ? {
        value: !!required,
        message: required
      } : getValueAndMessage(required);

      if (value) {
        error[name] = Object.assign({
          type: INPUT_VALIDATION_RULES.required,
          message,
          ref: isRadioOrCheckbox ? ((fieldsRef.current[name].options || [])[0] || {}).ref : ref
        }, appendErrorsCurry(INPUT_VALIDATION_RULES.required, message));

        if (!validateAllFieldCriteria) {
          return error;
        }
      }
    }

    if ((!isNullOrUndefined(min) || !isNullOrUndefined(max)) && value !== '') {
      let exceedMax;
      let exceedMin;
      const maxOutput = getValueAndMessage(max);
      const minOutput = getValueAndMessage(min);

      if (!isNaN(value)) {
        const valueNumber = ref.valueAsNumber || parseFloat(value);

        if (!isNullOrUndefined(maxOutput.value)) {
          exceedMax = valueNumber > maxOutput.value;
        }

        if (!isNullOrUndefined(minOutput.value)) {
          exceedMin = valueNumber < minOutput.value;
        }
      } else {
        const valueDate = ref.valueAsDate || new Date(value);

        if (isString(maxOutput.value)) {
          exceedMax = valueDate > new Date(maxOutput.value);
        }

        if (isString(minOutput.value)) {
          exceedMin = valueDate < new Date(minOutput.value);
        }
      }

      if (exceedMax || exceedMin) {
        getMinMaxMessage(!!exceedMax, maxOutput.message, minOutput.message, INPUT_VALIDATION_RULES.max, INPUT_VALIDATION_RULES.min);

        if (!validateAllFieldCriteria) {
          return error;
        }
      }
    }

    if (isString(value) && !isEmpty && (maxLength || minLength)) {
      const maxLengthOutput = getValueAndMessage(maxLength);
      const minLengthOutput = getValueAndMessage(minLength);
      const exceedMax = !isNullOrUndefined(maxLengthOutput.value) && value.length > maxLengthOutput.value;
      const exceedMin = !isNullOrUndefined(minLengthOutput.value) && value.length < minLengthOutput.value;

      if (exceedMax || exceedMin) {
        getMinMaxMessage(exceedMax, maxLengthOutput.message, minLengthOutput.message);

        if (!validateAllFieldCriteria) {
          return error;
        }
      }
    }

    if (isString(value) && pattern && !isEmpty) {
      const {
        value: patternValue,
        message
      } = getValueAndMessage(pattern);

      if (isRegex(patternValue) && !patternValue.test(value)) {
        error[name] = Object.assign({
          type: INPUT_VALIDATION_RULES.pattern,
          message,
          ref
        }, appendErrorsCurry(INPUT_VALIDATION_RULES.pattern, message));

        if (!validateAllFieldCriteria) {
          return error;
        }
      }
    }

    if (validate) {
      const fieldValue = getFieldValue(fieldsRef, name, shallowFieldsStateRef, false, true);
      const validateRef = isRadioOrCheckbox && options ? options[0].ref : ref;

      if (isFunction(validate)) {
        const result = await validate(fieldValue);
        const validateError = getValidateError(result, validateRef);

        if (validateError) {
          error[name] = Object.assign(Object.assign({}, validateError), appendErrorsCurry(INPUT_VALIDATION_RULES.validate, validateError.message));

          if (!validateAllFieldCriteria) {
            return error;
          }
        }
      } else if (isObject(validate)) {
        let validationResult = {};

        for (const [key, validateFunction] of Object.entries(validate)) {
          if (!isEmptyObject(validationResult) && !validateAllFieldCriteria) {
            break;
          }

          const validateResult = await validateFunction(fieldValue);
          const validateError = getValidateError(validateResult, validateRef, key);

          if (validateError) {
            validationResult = Object.assign(Object.assign({}, validateError), appendErrorsCurry(key, validateError.message));

            if (validateAllFieldCriteria) {
              error[name] = validationResult;
            }
          }
        }

        if (!isEmptyObject(validationResult)) {
          error[name] = Object.assign({
            ref: validateRef
          }, validationResult);

          if (!validateAllFieldCriteria) {
            return error;
          }
        }
      }
    }

    return error;
  };

  const getPath = (rootPath, values, paths = []) => {
    for (const property in values) {
      const rootName = rootPath + (isObject(values) ? `.${property}` : `[${property}]`);
      isPrimitive(values[property]) ? paths.push(rootName) : getPath(rootName, values[property], paths);
    }

    return paths;
  };

  var assignWatchFields = (fieldValues, fieldName, watchFields, inputValue, isSingleField) => {
    let value = undefined;
    watchFields.add(fieldName);

    if (!isEmptyObject(fieldValues)) {
      value = get(fieldValues, fieldName);

      if (isObject(value) || Array.isArray(value)) {
        getPath(fieldName, value).forEach(name => watchFields.add(name));
      }
    }

    return isUndefined(value) ? isSingleField ? inputValue : get(inputValue, fieldName) : value;
  };

  var skipValidation = ({
    isOnBlur,
    isOnChange,
    isOnTouch,
    isTouched,
    isReValidateOnBlur,
    isReValidateOnChange,
    isBlurEvent,
    isSubmitted,
    isOnAll
  }) => {
    if (isOnAll) {
      return false;
    } else if (!isSubmitted && isOnTouch) {
      return !(isTouched || isBlurEvent);
    } else if (isSubmitted ? isReValidateOnBlur : isOnBlur) {
      return !isBlurEvent;
    } else if (isSubmitted ? isReValidateOnChange : isOnChange) {
      return isBlurEvent;
    }

    return true;
  };

  var getFieldArrayParentName = name => name.substring(0, name.indexOf('['));

  const isMatchFieldArrayName = (name, searchName) => RegExp(`^${searchName}([|.)\\d+`.replace(/\[/g, '\\[').replace(/\]/g, '\\]')).test(name);

  var isNameInFieldArray = (names, name) => [...names].some(current => isMatchFieldArrayName(name, current));

  var isSelectInput = element => element.type === `${SELECT}-one`;

  function onDomRemove(fieldsRef, removeFieldEventListenerAndRef) {
    const observer = new MutationObserver(() => {
      for (const field of Object.values(fieldsRef.current)) {
        if (field && field.options) {
          for (const option of field.options) {
            if (option && option.ref && isDetached(option.ref)) {
              removeFieldEventListenerAndRef(field);
            }
          }
        } else if (field && isDetached(field.ref)) {
          removeFieldEventListenerAndRef(field);
        }
      }
    });
    observer.observe(window.document, {
      childList: true,
      subtree: true
    });
    return observer;
  }

  var isWeb = typeof window !== UNDEFINED && typeof document !== UNDEFINED;

  function cloneObject(data) {
    var _a;

    let copy;

    if (isPrimitive(data) || isWeb && (data instanceof File || isHTMLElement(data))) {
      return data;
    }

    if (!['Set', 'Map', 'Object', 'Date', 'Array'].includes((_a = data.constructor) === null || _a === void 0 ? void 0 : _a.name)) {
      return data;
    }

    if (data instanceof Date) {
      copy = new Date(data.getTime());
      return copy;
    }

    if (data instanceof Set) {
      copy = new Set();

      for (const item of data) {
        copy.add(item);
      }

      return copy;
    }

    if (data instanceof Map) {
      copy = new Map();

      for (const key of data.keys()) {
        copy.set(key, cloneObject(data.get(key)));
      }

      return copy;
    }

    copy = Array.isArray(data) ? [] : {};

    for (const key in data) {
      copy[key] = cloneObject(data[key]);
    }

    return copy;
  }

  var modeChecker = mode => ({
    isOnSubmit: !mode || mode === VALIDATION_MODE.onSubmit,
    isOnBlur: mode === VALIDATION_MODE.onBlur,
    isOnChange: mode === VALIDATION_MODE.onChange,
    isOnAll: mode === VALIDATION_MODE.all,
    isOnTouch: mode === VALIDATION_MODE.onTouched
  });

  var isRadioOrCheckboxFunction = ref => isRadioInput(ref) || isCheckBoxInput(ref);

  const isWindowUndefined = typeof window === UNDEFINED;
  const isProxyEnabled = isWeb ? 'Proxy' in window : typeof Proxy !== UNDEFINED;

  function useForm({
    mode = VALIDATION_MODE.onSubmit,
    reValidateMode = VALIDATION_MODE.onChange,
    resolver,
    context,
    defaultValues = {},
    shouldFocusError = true,
    shouldUnregister = true,
    criteriaMode
  } = {}) {
    const fieldsRef = s$1({});
    const fieldArrayDefaultValuesRef = s$1({});
    const fieldArrayValuesRef = s$1({});
    const watchFieldsRef = s$1(new Set());
    const useWatchFieldsRef = s$1({});
    const useWatchRenderFunctionsRef = s$1({});
    const fieldsWithValidationRef = s$1({});
    const validFieldsRef = s$1({});
    const defaultValuesRef = s$1(defaultValues);
    const isUnMount = s$1(false);
    const isWatchAllRef = s$1(false);
    const handleChangeRef = s$1();
    const shallowFieldsStateRef = s$1({});
    const resetFieldArrayFunctionRef = s$1({});
    const contextRef = s$1(context);
    const resolverRef = s$1(resolver);
    const fieldArrayNamesRef = s$1(new Set());
    const modeRef = s$1(modeChecker(mode));
    const {
      isOnSubmit,
      isOnTouch
    } = modeRef.current;
    const isValidateAllFieldCriteria = criteriaMode === VALIDATION_MODE.all;
    const [formState, setFormState] = l$1({
      isDirty: false,
      isValidating: false,
      dirtyFields: {},
      isSubmitted: false,
      submitCount: 0,
      touched: {},
      isSubmitting: false,
      isSubmitSuccessful: false,
      isValid: !isOnSubmit,
      errors: {}
    });
    const readFormStateRef = s$1({
      isDirty: !isProxyEnabled,
      dirtyFields: !isProxyEnabled,
      touched: !isProxyEnabled || isOnTouch,
      isValidating: !isProxyEnabled,
      isSubmitting: !isProxyEnabled,
      isValid: !isProxyEnabled
    });
    const formStateRef = s$1(formState);
    const observerRef = s$1();
    const {
      isOnBlur: isReValidateOnBlur,
      isOnChange: isReValidateOnChange
    } = s$1(modeChecker(reValidateMode)).current;
    contextRef.current = context;
    resolverRef.current = resolver;
    formStateRef.current = formState;
    shallowFieldsStateRef.current = shouldUnregister ? {} : isEmptyObject(shallowFieldsStateRef.current) ? cloneObject(defaultValues) : shallowFieldsStateRef.current;
    const updateFormState = A$1((state = {}) => {
      if (!isUnMount.current) {
        formStateRef.current = Object.assign(Object.assign({}, formStateRef.current), state);
        setFormState(formStateRef.current);
      }
    }, []);

    const updateIsValidating = () => readFormStateRef.current.isValidating && updateFormState({
      isValidating: true
    });

    const shouldRenderBaseOnError = A$1((name, error, shouldRender = false, state = {}, isValid) => {
      let shouldReRender = shouldRender || isErrorStateChanged({
        errors: formStateRef.current.errors,
        error,
        name,
        validFields: validFieldsRef.current,
        fieldsWithValidation: fieldsWithValidationRef.current
      });
      const previousError = get(formStateRef.current.errors, name);

      if (error) {
        unset(validFieldsRef.current, name);
        shouldReRender = shouldReRender || !previousError || !deepEqual(previousError, error, true);
        set(formStateRef.current.errors, name, error);
      } else {
        if (get(fieldsWithValidationRef.current, name) || resolverRef.current) {
          set(validFieldsRef.current, name, true);
          shouldReRender = shouldReRender || previousError;
        }

        unset(formStateRef.current.errors, name);
      }

      if (shouldReRender && !isNullOrUndefined(shouldRender) || !isEmptyObject(state) || readFormStateRef.current.isValidating) {
        updateFormState(Object.assign(Object.assign(Object.assign({}, state), resolverRef.current ? {
          isValid: !!isValid
        } : {}), {
          isValidating: false
        }));
      }
    }, []);
    const setFieldValue = A$1((name, rawValue) => {
      const {
        ref,
        options
      } = fieldsRef.current[name];
      const value = isWeb && isHTMLElement(ref) && isNullOrUndefined(rawValue) ? '' : rawValue;

      if (isRadioInput(ref)) {
        (options || []).forEach(({
          ref: radioRef
        }) => radioRef.checked = radioRef.value === value);
      } else if (isFileInput(ref) && !isString(value)) {
        ref.files = value;
      } else if (isMultipleSelect(ref)) {
        [...ref.options].forEach(selectRef => selectRef.selected = value.includes(selectRef.value));
      } else if (isCheckBoxInput(ref) && options) {
        options.length > 1 ? options.forEach(({
          ref: checkboxRef
        }) => checkboxRef.checked = Array.isArray(value) ? !!value.find(data => data === checkboxRef.value) : value === checkboxRef.value) : options[0].ref.checked = !!value;
      } else {
        ref.value = value;
      }
    }, []);
    const isFormDirty = A$1((name, data) => {
      if (readFormStateRef.current.isDirty) {
        const formValues = getValues();
        name && data && set(formValues, name, data);
        return !deepEqual(formValues, defaultValuesRef.current);
      }

      return false;
    }, []);
    const updateAndGetDirtyState = A$1((name, shouldRender = true) => {
      if (readFormStateRef.current.isDirty || readFormStateRef.current.dirtyFields) {
        const isFieldDirty = !deepEqual(get(defaultValuesRef.current, name), getFieldValue(fieldsRef, name, shallowFieldsStateRef));
        const isDirtyFieldExist = get(formStateRef.current.dirtyFields, name);
        const previousIsDirty = formStateRef.current.isDirty;
        isFieldDirty ? set(formStateRef.current.dirtyFields, name, true) : unset(formStateRef.current.dirtyFields, name);
        const state = {
          isDirty: isFormDirty(),
          dirtyFields: formStateRef.current.dirtyFields
        };
        const isChanged = readFormStateRef.current.isDirty && previousIsDirty !== state.isDirty || readFormStateRef.current.dirtyFields && isDirtyFieldExist !== get(formStateRef.current.dirtyFields, name);
        isChanged && shouldRender && updateFormState(state);
        return isChanged ? state : {};
      }

      return {};
    }, []);
    const executeValidation = A$1(async (name, skipReRender) => {
      {
        if (!fieldsRef.current[name]) {
          console.warn('📋 Field is missing with `name` attribute: ', name);
          return false;
        }
      }

      const error = (await validateField(fieldsRef, isValidateAllFieldCriteria, fieldsRef.current[name], shallowFieldsStateRef))[name];
      shouldRenderBaseOnError(name, error, skipReRender);
      return isUndefined(error);
    }, [shouldRenderBaseOnError, isValidateAllFieldCriteria]);
    const executeSchemaOrResolverValidation = A$1(async names => {
      const {
        errors
      } = await resolverRef.current(getValues(), contextRef.current, isValidateAllFieldCriteria);
      const previousFormIsValid = formStateRef.current.isValid;

      if (Array.isArray(names)) {
        const isInputsValid = names.map(name => {
          const error = get(errors, name);
          error ? set(formStateRef.current.errors, name, error) : unset(formStateRef.current.errors, name);
          return !error;
        }).every(Boolean);
        updateFormState({
          isValid: isEmptyObject(errors),
          isValidating: false
        });
        return isInputsValid;
      } else {
        const error = get(errors, names);
        shouldRenderBaseOnError(names, error, previousFormIsValid !== isEmptyObject(errors), {}, isEmptyObject(errors));
        return !error;
      }
    }, [shouldRenderBaseOnError, isValidateAllFieldCriteria]);
    const trigger = A$1(async name => {
      const fields = name || Object.keys(fieldsRef.current);
      updateIsValidating();

      if (resolverRef.current) {
        return executeSchemaOrResolverValidation(fields);
      }

      if (Array.isArray(fields)) {
        !name && (formStateRef.current.errors = {});
        const result = await Promise.all(fields.map(async data => await executeValidation(data, null)));
        updateFormState({
          isValidating: false
        });
        return result.every(Boolean);
      }

      return await executeValidation(fields);
    }, [executeSchemaOrResolverValidation, executeValidation]);
    const setInternalValues = A$1((name, value, {
      shouldDirty,
      shouldValidate
    }) => {
      const data = {};
      set(data, name, value);

      for (const fieldName of getPath(name, value)) {
        if (fieldsRef.current[fieldName]) {
          setFieldValue(fieldName, get(data, fieldName));
          shouldDirty && updateAndGetDirtyState(fieldName);
          shouldValidate && trigger(fieldName);
        }
      }
    }, [trigger, setFieldValue, updateAndGetDirtyState]);
    const setInternalValue = A$1((name, value, config) => {
      !shouldUnregister && !isPrimitive(value) && set(shallowFieldsStateRef.current, name, Array.isArray(value) ? [...value] : Object.assign({}, value));

      if (fieldsRef.current[name]) {
        setFieldValue(name, value);
        config.shouldDirty && updateAndGetDirtyState(name);
        config.shouldValidate && trigger(name);
      } else if (!isPrimitive(value)) {
        setInternalValues(name, value, config);

        if (fieldArrayNamesRef.current.has(name)) {
          const parentName = getFieldArrayParentName(name) || name;
          set(fieldArrayDefaultValuesRef.current, name, value);
          resetFieldArrayFunctionRef.current[parentName]({
            [parentName]: get(fieldArrayDefaultValuesRef.current, parentName)
          });

          if ((readFormStateRef.current.isDirty || readFormStateRef.current.dirtyFields) && config.shouldDirty) {
            set(formStateRef.current.dirtyFields, name, setFieldArrayDirtyFields(value, get(defaultValuesRef.current, name, []), get(formStateRef.current.dirtyFields, name, [])));
            updateFormState({
              isDirty: !deepEqual(Object.assign(Object.assign({}, getValues()), {
                [name]: value
              }), defaultValuesRef.current)
            });
          }
        }
      }

      !shouldUnregister && set(shallowFieldsStateRef.current, name, value);
    }, [updateAndGetDirtyState, setFieldValue, setInternalValues]);

    const isFieldWatched = name => isWatchAllRef.current || watchFieldsRef.current.has(name) || watchFieldsRef.current.has((name.match(/\w+/) || [])[0]);

    const renderWatchedInputs = name => {
      let found = true;

      if (!isEmptyObject(useWatchFieldsRef.current)) {
        for (const key in useWatchFieldsRef.current) {
          if (!name || !useWatchFieldsRef.current[key].size || useWatchFieldsRef.current[key].has(name) || useWatchFieldsRef.current[key].has(getFieldArrayParentName(name))) {
            useWatchRenderFunctionsRef.current[key]();
            found = false;
          }
        }
      }

      return found;
    };

    function setValue(name, value, config) {
      setInternalValue(name, value, config || {});
      isFieldWatched(name) && updateFormState();
      renderWatchedInputs(name);
    }

    handleChangeRef.current = handleChangeRef.current ? handleChangeRef.current : async ({
      type,
      target
    }) => {
      let name = target.name;
      const field = fieldsRef.current[name];
      let error;
      let isValid;

      if (field) {
        const isBlurEvent = type === EVENTS.BLUR;
        const shouldSkipValidation = skipValidation(Object.assign({
          isBlurEvent,
          isReValidateOnChange,
          isReValidateOnBlur,
          isTouched: !!get(formStateRef.current.touched, name),
          isSubmitted: formStateRef.current.isSubmitted
        }, modeRef.current));
        let state = updateAndGetDirtyState(name, false);
        let shouldRender = !isEmptyObject(state) || !isBlurEvent && isFieldWatched(name);

        if (isBlurEvent && !get(formStateRef.current.touched, name) && readFormStateRef.current.touched) {
          set(formStateRef.current.touched, name, true);
          state = Object.assign(Object.assign({}, state), {
            touched: formStateRef.current.touched
          });
        }

        if (!shouldUnregister && isCheckBoxInput(target)) {
          set(shallowFieldsStateRef.current, name, getFieldValue(fieldsRef, name));
        }

        if (shouldSkipValidation) {
          !isBlurEvent && renderWatchedInputs(name);
          return (!isEmptyObject(state) || shouldRender && isEmptyObject(state)) && updateFormState(state);
        }

        updateIsValidating();

        if (resolverRef.current) {
          const {
            errors
          } = await resolverRef.current(getValues(), contextRef.current, isValidateAllFieldCriteria);
          const previousFormIsValid = formStateRef.current.isValid;
          error = get(errors, name);

          if (isCheckBoxInput(target) && !error && resolverRef.current) {
            const parentNodeName = getFieldArrayParentName(name);
            const currentError = get(errors, parentNodeName, {});
            currentError.type && currentError.message && (error = currentError);

            if (parentNodeName && (currentError || get(formStateRef.current.errors, parentNodeName))) {
              name = parentNodeName;
            }
          }

          isValid = isEmptyObject(errors);
          previousFormIsValid !== isValid && (shouldRender = true);
        } else {
          error = (await validateField(fieldsRef, isValidateAllFieldCriteria, field, shallowFieldsStateRef))[name];
        }

        !isBlurEvent && renderWatchedInputs(name);
        shouldRenderBaseOnError(name, error, shouldRender, state, isValid);
      }
    };

    function setFieldArrayDefaultValues(data) {
      if (!shouldUnregister) {
        let copy = cloneObject(data);

        for (const value of fieldArrayNamesRef.current) {
          if (isKey(value) && !copy[value]) {
            copy = Object.assign(Object.assign({}, copy), {
              [value]: []
            });
          }
        }

        return copy;
      }

      return data;
    }

    function getValues(payload) {
      if (isString(payload)) {
        return getFieldValue(fieldsRef, payload, shallowFieldsStateRef);
      }

      if (Array.isArray(payload)) {
        const data = {};

        for (const name of payload) {
          set(data, name, getFieldValue(fieldsRef, name, shallowFieldsStateRef));
        }

        return data;
      }

      return setFieldArrayDefaultValues(getFieldsValues(fieldsRef, cloneObject(shallowFieldsStateRef.current), shouldUnregister));
    }

    const validateResolver = A$1(async (values = {}) => {
      const newDefaultValues = isEmptyObject(fieldsRef.current) ? defaultValuesRef.current : {};
      const {
        errors
      } = (await resolverRef.current(Object.assign(Object.assign(Object.assign({}, newDefaultValues), getValues()), values), contextRef.current, isValidateAllFieldCriteria)) || {};
      const isValid = isEmptyObject(errors);
      formStateRef.current.isValid !== isValid && updateFormState({
        isValid
      });
    }, [isValidateAllFieldCriteria]);
    const removeFieldEventListener = A$1((field, forceDelete) => {
      findRemovedFieldAndRemoveListener(fieldsRef, handleChangeRef.current, field, shallowFieldsStateRef, shouldUnregister, forceDelete);

      if (shouldUnregister) {
        unset(validFieldsRef.current, field.ref.name);
        unset(fieldsWithValidationRef.current, field.ref.name);
      }
    }, [shouldUnregister]);
    const updateWatchedValue = A$1(name => {
      if (isWatchAllRef.current) {
        updateFormState();
      } else {
        for (const watchField of watchFieldsRef.current) {
          if (watchField.startsWith(name)) {
            updateFormState();
            break;
          }
        }

        renderWatchedInputs(name);
      }
    }, []);
    const removeFieldEventListenerAndRef = A$1((field, forceDelete) => {
      if (field) {
        removeFieldEventListener(field, forceDelete);

        if (shouldUnregister && !compact(field.options || []).length) {
          unset(formStateRef.current.errors, field.ref.name);
          set(formStateRef.current.dirtyFields, field.ref.name, true);
          updateFormState({
            isDirty: isFormDirty()
          });
          readFormStateRef.current.isValid && resolverRef.current && validateResolver();
          updateWatchedValue(field.ref.name);
        }
      }
    }, [validateResolver, removeFieldEventListener]);

    function clearErrors(name) {
      name && (Array.isArray(name) ? name : [name]).forEach(inputName => fieldsRef.current[inputName] && isKey(inputName) ? delete formStateRef.current.errors[inputName] : unset(formStateRef.current.errors, inputName));
      updateFormState({
        errors: name ? formStateRef.current.errors : {}
      });
    }

    function setError(name, error) {
      const ref = (fieldsRef.current[name] || {}).ref;
      set(formStateRef.current.errors, name, Object.assign(Object.assign({}, error), {
        ref
      }));
      updateFormState({
        isValid: false
      });
      error.shouldFocus && ref && ref.focus && ref.focus();
    }

    const watchInternal = A$1((fieldNames, defaultValue, watchId) => {
      const watchFields = watchId ? useWatchFieldsRef.current[watchId] : watchFieldsRef.current;
      let fieldValues = getFieldsValues(fieldsRef, cloneObject(shallowFieldsStateRef.current), shouldUnregister, false, fieldNames);

      if (isString(fieldNames)) {
        const parentNodeName = getFieldArrayParentName(fieldNames) || fieldNames;

        if (fieldArrayNamesRef.current.has(parentNodeName)) {
          fieldValues = Object.assign(Object.assign({}, fieldArrayValuesRef.current), fieldValues);
        }

        return assignWatchFields(fieldValues, fieldNames, watchFields, isUndefined(get(defaultValuesRef.current, fieldNames)) ? defaultValue : get(defaultValuesRef.current, fieldNames), true);
      }

      const combinedDefaultValues = isUndefined(defaultValue) ? defaultValuesRef.current : defaultValue;

      if (Array.isArray(fieldNames)) {
        return fieldNames.reduce((previous, name) => Object.assign(Object.assign({}, previous), {
          [name]: assignWatchFields(fieldValues, name, watchFields, combinedDefaultValues)
        }), {});
      }

      isWatchAllRef.current = isUndefined(watchId);
      return transformToNestObject(!isEmptyObject(fieldValues) && fieldValues || combinedDefaultValues);
    }, []);

    function watch(fieldNames, defaultValue) {
      return watchInternal(fieldNames, defaultValue);
    }

    function unregister(name) {
      for (const fieldName of Array.isArray(name) ? name : [name]) {
        removeFieldEventListenerAndRef(fieldsRef.current[fieldName], true);
      }
    }

    function registerFieldRef(ref, options = {}) {
      {
        if (!ref.name) {
          return console.warn('📋 Field is missing `name` attribute', ref, `https://react-hook-form.com/api#useForm`);
        }

        if (fieldArrayNamesRef.current.has(ref.name.split(/\[\d+\]$/)[0]) && !RegExp(`^${ref.name.split(/\[\d+\]$/)[0]}[\\d+].\\w+`.replace(/\[/g, '\\[').replace(/\]/g, '\\]')).test(ref.name)) {
          return console.warn('📋 `name` prop should be in object shape: name="test[index].name"', ref, 'https://react-hook-form.com/api#useFieldArray');
        }
      }

      const {
        name,
        type,
        value
      } = ref;
      const fieldRefAndValidationOptions = Object.assign({
        ref
      }, options);
      const fields = fieldsRef.current;
      const isRadioOrCheckbox = isRadioOrCheckboxFunction(ref);
      const isFieldArray = isNameInFieldArray(fieldArrayNamesRef.current, name);

      const compareRef = currentRef => isWeb && (!isHTMLElement(ref) || currentRef === ref);

      let field = fields[name];
      let isEmptyDefaultValue = true;
      let defaultValue;

      if (field && (isRadioOrCheckbox ? Array.isArray(field.options) && compact(field.options).find(option => {
        return value === option.ref.value && compareRef(option.ref);
      }) : compareRef(field.ref))) {
        fields[name] = Object.assign(Object.assign({}, field), options);
        return;
      }

      if (type) {
        field = isRadioOrCheckbox ? Object.assign({
          options: [...compact(field && field.options || []), {
            ref
          }],
          ref: {
            type,
            name
          }
        }, options) : Object.assign({}, fieldRefAndValidationOptions);
      } else {
        field = fieldRefAndValidationOptions;
      }

      fields[name] = field;
      const isEmptyUnmountFields = isUndefined(get(shallowFieldsStateRef.current, name));

      if (!isEmptyObject(defaultValuesRef.current) || !isEmptyUnmountFields) {
        defaultValue = get(isEmptyUnmountFields ? defaultValuesRef.current : shallowFieldsStateRef.current, name);
        isEmptyDefaultValue = isUndefined(defaultValue);

        if (!isEmptyDefaultValue && !isFieldArray) {
          setFieldValue(name, defaultValue);
        }
      }

      if (!isEmptyObject(options)) {
        set(fieldsWithValidationRef.current, name, true);

        if (!isOnSubmit && readFormStateRef.current.isValid) {
          validateField(fieldsRef, isValidateAllFieldCriteria, field, shallowFieldsStateRef).then(error => {
            const previousFormIsValid = formStateRef.current.isValid;
            isEmptyObject(error) ? set(validFieldsRef.current, name, true) : unset(validFieldsRef.current, name);
            previousFormIsValid !== isEmptyObject(error) && updateFormState();
          });
        }
      }

      if (shouldUnregister && !(isFieldArray && isEmptyDefaultValue)) {
        !isFieldArray && unset(formStateRef.current.dirtyFields, name);
      }

      if (type) {
        attachEventListeners(isRadioOrCheckbox && field.options ? field.options[field.options.length - 1] : field, isRadioOrCheckbox || isSelectInput(ref), handleChangeRef.current);
      }
    }

    function register(refOrRegisterOptions, options) {
      if (!isWindowUndefined) {
        if (isString(refOrRegisterOptions)) {
          registerFieldRef({
            name: refOrRegisterOptions
          }, options);
        } else if (isObject(refOrRegisterOptions) && 'name' in refOrRegisterOptions) {
          registerFieldRef(refOrRegisterOptions, options);
        } else {
          return ref => ref && registerFieldRef(ref, refOrRegisterOptions);
        }
      }
    }

    const handleSubmit = A$1((onValid, onInvalid) => async e => {
      if (e && e.preventDefault) {
        e.preventDefault();
        e.persist();
      }

      let fieldErrors = {};
      let fieldValues = setFieldArrayDefaultValues(getFieldsValues(fieldsRef, cloneObject(shallowFieldsStateRef.current), shouldUnregister, true));
      readFormStateRef.current.isSubmitting && updateFormState({
        isSubmitting: true
      });

      try {
        if (resolverRef.current) {
          const {
            errors,
            values
          } = await resolverRef.current(fieldValues, contextRef.current, isValidateAllFieldCriteria);
          formStateRef.current.errors = fieldErrors = errors;
          fieldValues = values;
        } else {
          for (const field of Object.values(fieldsRef.current)) {
            if (field) {
              const {
                name
              } = field.ref;
              const fieldError = await validateField(fieldsRef, isValidateAllFieldCriteria, field, shallowFieldsStateRef);

              if (fieldError[name]) {
                set(fieldErrors, name, fieldError[name]);
                unset(validFieldsRef.current, name);
              } else if (get(fieldsWithValidationRef.current, name)) {
                unset(formStateRef.current.errors, name);
                set(validFieldsRef.current, name, true);
              }
            }
          }
        }

        if (isEmptyObject(fieldErrors) && Object.keys(formStateRef.current.errors).every(name => name in fieldsRef.current)) {
          updateFormState({
            errors: {},
            isSubmitting: true
          });
          await onValid(fieldValues, e);
        } else {
          formStateRef.current.errors = Object.assign(Object.assign({}, formStateRef.current.errors), fieldErrors);
          onInvalid && (await onInvalid(formStateRef.current.errors, e));
          shouldFocusError && focusOnErrorField(fieldsRef.current, formStateRef.current.errors);
        }
      } finally {
        formStateRef.current.isSubmitting = false;
        updateFormState({
          isSubmitted: true,
          isSubmitting: false,
          isSubmitSuccessful: isEmptyObject(formStateRef.current.errors),
          submitCount: formStateRef.current.submitCount + 1
        });
      }
    }, [shouldFocusError, isValidateAllFieldCriteria]);

    const resetRefs = ({
      errors,
      isDirty,
      isSubmitted,
      touched,
      isValid,
      submitCount,
      dirtyFields
    }) => {
      if (!isValid) {
        validFieldsRef.current = {};
        fieldsWithValidationRef.current = {};
      }

      fieldArrayDefaultValuesRef.current = {};
      watchFieldsRef.current = new Set();
      isWatchAllRef.current = false;
      updateFormState({
        submitCount: submitCount ? formStateRef.current.submitCount : 0,
        isDirty: isDirty ? formStateRef.current.isDirty : false,
        isSubmitted: isSubmitted ? formStateRef.current.isSubmitted : false,
        isValid: isValid ? formStateRef.current.isValid : false,
        dirtyFields: dirtyFields ? formStateRef.current.dirtyFields : {},
        touched: touched ? formStateRef.current.touched : {},
        errors: errors ? formStateRef.current.errors : {},
        isSubmitting: false,
        isSubmitSuccessful: false
      });
    };

    const reset = (values, omitResetState = {}) => {
      if (isWeb) {
        for (const field of Object.values(fieldsRef.current)) {
          if (field) {
            const {
              ref,
              options
            } = field;
            const inputRef = isRadioOrCheckboxFunction(ref) && Array.isArray(options) ? options[0].ref : ref;

            if (isHTMLElement(inputRef)) {
              try {
                inputRef.closest('form').reset();
                break;
              } catch (_a) {}
            }
          }
        }
      }

      fieldsRef.current = {};
      defaultValuesRef.current = Object.assign({}, values || defaultValuesRef.current);
      values && renderWatchedInputs('');
      Object.values(resetFieldArrayFunctionRef.current).forEach(resetFieldArray => isFunction(resetFieldArray) && resetFieldArray());
      shallowFieldsStateRef.current = shouldUnregister ? {} : cloneObject(values || defaultValuesRef.current);
      resetRefs(omitResetState);
    };

    y(() => {
      resolver && readFormStateRef.current.isValid && validateResolver();
      observerRef.current = observerRef.current || !isWeb ? observerRef.current : onDomRemove(fieldsRef, removeFieldEventListenerAndRef);
    }, [removeFieldEventListenerAndRef, defaultValuesRef.current]);
    y(() => () => {
      observerRef.current && observerRef.current.disconnect();
      isUnMount.current = true;

      {
        return;
      }
    }, []);

    if (!resolver && readFormStateRef.current.isValid) {
      formState.isValid = deepEqual(validFieldsRef.current, fieldsWithValidationRef.current) && isEmptyObject(formStateRef.current.errors);
    }

    const commonProps = {
      trigger,
      setValue: A$1(setValue, [setInternalValue, trigger]),
      getValues: A$1(getValues, []),
      register: A$1(register, [defaultValuesRef.current]),
      unregister: A$1(unregister, []),
      formState: isProxyEnabled ? new Proxy(formState, {
        get: (obj, prop) => {
          {
            if (prop === 'isValid' && isOnSubmit) {
              console.warn('📋 `formState.isValid` is applicable with `onTouched`, `onChange` or `onBlur` mode. https://react-hook-form.com/api#formState');
            }
          }

          if (prop in obj) {
            readFormStateRef.current[prop] = true;
            return obj[prop];
          }

          return undefined;
        }
      }) : formState
    };
    const control = d$1(() => Object.assign({
      isFormDirty,
      updateWatchedValue,
      shouldUnregister,
      updateFormState,
      removeFieldEventListener,
      watchInternal,
      mode: modeRef.current,
      reValidateMode: {
        isReValidateOnBlur,
        isReValidateOnChange
      },
      validateResolver: resolver ? validateResolver : undefined,
      fieldsRef,
      resetFieldArrayFunctionRef,
      useWatchFieldsRef,
      useWatchRenderFunctionsRef,
      fieldArrayDefaultValuesRef,
      validFieldsRef,
      fieldsWithValidationRef,
      fieldArrayNamesRef,
      readFormStateRef,
      formStateRef,
      defaultValuesRef,
      shallowFieldsStateRef,
      fieldArrayValuesRef
    }, commonProps), [defaultValuesRef.current, updateWatchedValue, shouldUnregister, removeFieldEventListener, watchInternal]);
    return Object.assign({
      watch,
      control,
      handleSubmit,
      reset: A$1(reset, []),
      clearErrors: A$1(clearErrors, []),
      setError: A$1(setError, []),
      errors: formState.errors
    }, commonProps);
  }

  const FormContext = D$1(null);
  FormContext.displayName = 'RHFContext';

  var minLength = 5;
  function FeedbackTextInput(props) {
    var _getBaseProps = getBaseProps(props, TAG_FEEDBACK_FORM),
        apiUrl = _getBaseProps.apiUrl;

    var _useFeedbackApi = useFeedbackApi(apiUrl, props[ATTR_SOURCE_UUID], props[ATTR_TARGET_UUID], props[ATTR_TRANSACTION_ID]),
        sendFeedback = _useFeedbackApi.sendFeedback;

    var _useForm = useForm(),
        register = _useForm.register,
        errors = _useForm.errors,
        handleSubmit = _useForm.handleSubmit;

    function onSubmit(feedback) {
      sendFeedback(feedback);
    }

    return v$2(BaseComponent, {
      className: "utu-feedback-form utu-section",
      excludeBootstrap: true,
      excludeFonts: true
    }, v$2("form", {
      onSubmit: handleSubmit(onSubmit),
      className: "d-flex flex-column align-items-stretch"
    }, v$2("textarea", {
      ref: register({
        required: true,
        minLength: minLength
      }),
      name: "review",
      placeholder: "Your text review"
    }), errors.review && v$2("div", {
      className: "error mt-1"
    }, "* Feedback should be at least ", minLength, " characters long"), v$2("button", {
      className: "btn mt-3",
      type: "submit"
    }, "Submit")));
  }

  /** @license React v16.13.1
   * react-is.development.js
   *
   * Copyright (c) Facebook, Inc. and its affiliates.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE file in the root directory of this source tree.
   */

  var reactIs_development = createCommonjsModule(function (module, exports) {

  {
    (function () {
      // nor polyfill, then a plain number is used for performance.

      var hasSymbol = typeof Symbol === 'function' && Symbol.for;
      var REACT_ELEMENT_TYPE = hasSymbol ? Symbol.for('react.element') : 0xeac7;
      var REACT_PORTAL_TYPE = hasSymbol ? Symbol.for('react.portal') : 0xeaca;
      var REACT_FRAGMENT_TYPE = hasSymbol ? Symbol.for('react.fragment') : 0xeacb;
      var REACT_STRICT_MODE_TYPE = hasSymbol ? Symbol.for('react.strict_mode') : 0xeacc;
      var REACT_PROFILER_TYPE = hasSymbol ? Symbol.for('react.profiler') : 0xead2;
      var REACT_PROVIDER_TYPE = hasSymbol ? Symbol.for('react.provider') : 0xeacd;
      var REACT_CONTEXT_TYPE = hasSymbol ? Symbol.for('react.context') : 0xeace; // TODO: We don't use AsyncMode or ConcurrentMode anymore. They were temporary
      // (unstable) APIs that have been removed. Can we remove the symbols?

      var REACT_ASYNC_MODE_TYPE = hasSymbol ? Symbol.for('react.async_mode') : 0xeacf;
      var REACT_CONCURRENT_MODE_TYPE = hasSymbol ? Symbol.for('react.concurrent_mode') : 0xeacf;
      var REACT_FORWARD_REF_TYPE = hasSymbol ? Symbol.for('react.forward_ref') : 0xead0;
      var REACT_SUSPENSE_TYPE = hasSymbol ? Symbol.for('react.suspense') : 0xead1;
      var REACT_SUSPENSE_LIST_TYPE = hasSymbol ? Symbol.for('react.suspense_list') : 0xead8;
      var REACT_MEMO_TYPE = hasSymbol ? Symbol.for('react.memo') : 0xead3;
      var REACT_LAZY_TYPE = hasSymbol ? Symbol.for('react.lazy') : 0xead4;
      var REACT_BLOCK_TYPE = hasSymbol ? Symbol.for('react.block') : 0xead9;
      var REACT_FUNDAMENTAL_TYPE = hasSymbol ? Symbol.for('react.fundamental') : 0xead5;
      var REACT_RESPONDER_TYPE = hasSymbol ? Symbol.for('react.responder') : 0xead6;
      var REACT_SCOPE_TYPE = hasSymbol ? Symbol.for('react.scope') : 0xead7;

      function isValidElementType(type) {
        return typeof type === 'string' || typeof type === 'function' || // Note: its typeof might be other than 'symbol' or 'number' if it's a polyfill.
        type === REACT_FRAGMENT_TYPE || type === REACT_CONCURRENT_MODE_TYPE || type === REACT_PROFILER_TYPE || type === REACT_STRICT_MODE_TYPE || type === REACT_SUSPENSE_TYPE || type === REACT_SUSPENSE_LIST_TYPE || typeof type === 'object' && type !== null && (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE || type.$$typeof === REACT_FUNDAMENTAL_TYPE || type.$$typeof === REACT_RESPONDER_TYPE || type.$$typeof === REACT_SCOPE_TYPE || type.$$typeof === REACT_BLOCK_TYPE);
      }

      function typeOf(object) {
        if (typeof object === 'object' && object !== null) {
          var $$typeof = object.$$typeof;

          switch ($$typeof) {
            case REACT_ELEMENT_TYPE:
              var type = object.type;

              switch (type) {
                case REACT_ASYNC_MODE_TYPE:
                case REACT_CONCURRENT_MODE_TYPE:
                case REACT_FRAGMENT_TYPE:
                case REACT_PROFILER_TYPE:
                case REACT_STRICT_MODE_TYPE:
                case REACT_SUSPENSE_TYPE:
                  return type;

                default:
                  var $$typeofType = type && type.$$typeof;

                  switch ($$typeofType) {
                    case REACT_CONTEXT_TYPE:
                    case REACT_FORWARD_REF_TYPE:
                    case REACT_LAZY_TYPE:
                    case REACT_MEMO_TYPE:
                    case REACT_PROVIDER_TYPE:
                      return $$typeofType;

                    default:
                      return $$typeof;
                  }

              }

            case REACT_PORTAL_TYPE:
              return $$typeof;
          }
        }

        return undefined;
      } // AsyncMode is deprecated along with isAsyncMode


      var AsyncMode = REACT_ASYNC_MODE_TYPE;
      var ConcurrentMode = REACT_CONCURRENT_MODE_TYPE;
      var ContextConsumer = REACT_CONTEXT_TYPE;
      var ContextProvider = REACT_PROVIDER_TYPE;
      var Element = REACT_ELEMENT_TYPE;
      var ForwardRef = REACT_FORWARD_REF_TYPE;
      var Fragment = REACT_FRAGMENT_TYPE;
      var Lazy = REACT_LAZY_TYPE;
      var Memo = REACT_MEMO_TYPE;
      var Portal = REACT_PORTAL_TYPE;
      var Profiler = REACT_PROFILER_TYPE;
      var StrictMode = REACT_STRICT_MODE_TYPE;
      var Suspense = REACT_SUSPENSE_TYPE;
      var hasWarnedAboutDeprecatedIsAsyncMode = false; // AsyncMode should be deprecated

      function isAsyncMode(object) {
        {
          if (!hasWarnedAboutDeprecatedIsAsyncMode) {
            hasWarnedAboutDeprecatedIsAsyncMode = true; // Using console['warn'] to evade Babel and ESLint

            console['warn']('The ReactIs.isAsyncMode() alias has been deprecated, ' + 'and will be removed in React 17+. Update your code to use ' + 'ReactIs.isConcurrentMode() instead. It has the exact same API.');
          }
        }
        return isConcurrentMode(object) || typeOf(object) === REACT_ASYNC_MODE_TYPE;
      }

      function isConcurrentMode(object) {
        return typeOf(object) === REACT_CONCURRENT_MODE_TYPE;
      }

      function isContextConsumer(object) {
        return typeOf(object) === REACT_CONTEXT_TYPE;
      }

      function isContextProvider(object) {
        return typeOf(object) === REACT_PROVIDER_TYPE;
      }

      function isElement(object) {
        return typeof object === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
      }

      function isForwardRef(object) {
        return typeOf(object) === REACT_FORWARD_REF_TYPE;
      }

      function isFragment(object) {
        return typeOf(object) === REACT_FRAGMENT_TYPE;
      }

      function isLazy(object) {
        return typeOf(object) === REACT_LAZY_TYPE;
      }

      function isMemo(object) {
        return typeOf(object) === REACT_MEMO_TYPE;
      }

      function isPortal(object) {
        return typeOf(object) === REACT_PORTAL_TYPE;
      }

      function isProfiler(object) {
        return typeOf(object) === REACT_PROFILER_TYPE;
      }

      function isStrictMode(object) {
        return typeOf(object) === REACT_STRICT_MODE_TYPE;
      }

      function isSuspense(object) {
        return typeOf(object) === REACT_SUSPENSE_TYPE;
      }

      exports.AsyncMode = AsyncMode;
      exports.ConcurrentMode = ConcurrentMode;
      exports.ContextConsumer = ContextConsumer;
      exports.ContextProvider = ContextProvider;
      exports.Element = Element;
      exports.ForwardRef = ForwardRef;
      exports.Fragment = Fragment;
      exports.Lazy = Lazy;
      exports.Memo = Memo;
      exports.Portal = Portal;
      exports.Profiler = Profiler;
      exports.StrictMode = StrictMode;
      exports.Suspense = Suspense;
      exports.isAsyncMode = isAsyncMode;
      exports.isConcurrentMode = isConcurrentMode;
      exports.isContextConsumer = isContextConsumer;
      exports.isContextProvider = isContextProvider;
      exports.isElement = isElement;
      exports.isForwardRef = isForwardRef;
      exports.isFragment = isFragment;
      exports.isLazy = isLazy;
      exports.isMemo = isMemo;
      exports.isPortal = isPortal;
      exports.isProfiler = isProfiler;
      exports.isStrictMode = isStrictMode;
      exports.isSuspense = isSuspense;
      exports.isValidElementType = isValidElementType;
      exports.typeOf = typeOf;
    })();
  }
  });

  var reactIs = createCommonjsModule(function (module) {

  {
    module.exports = reactIs_development;
  }
  });

  /*
  object-assign
  (c) Sindre Sorhus
  @license MIT
  */
  /* eslint-disable no-unused-vars */

  var getOwnPropertySymbols = Object.getOwnPropertySymbols;
  var hasOwnProperty = Object.prototype.hasOwnProperty;
  var propIsEnumerable = Object.prototype.propertyIsEnumerable;

  function toObject(val) {
    if (val === null || val === undefined) {
      throw new TypeError('Object.assign cannot be called with null or undefined');
    }

    return Object(val);
  }

  function shouldUseNative() {
    try {
      if (!Object.assign) {
        return false;
      } // Detect buggy property enumeration order in older V8 versions.
      // https://bugs.chromium.org/p/v8/issues/detail?id=4118


      var test1 = new String('abc'); // eslint-disable-line no-new-wrappers

      test1[5] = 'de';

      if (Object.getOwnPropertyNames(test1)[0] === '5') {
        return false;
      } // https://bugs.chromium.org/p/v8/issues/detail?id=3056


      var test2 = {};

      for (var i = 0; i < 10; i++) {
        test2['_' + String.fromCharCode(i)] = i;
      }

      var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
        return test2[n];
      });

      if (order2.join('') !== '0123456789') {
        return false;
      } // https://bugs.chromium.org/p/v8/issues/detail?id=3056


      var test3 = {};
      'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
        test3[letter] = letter;
      });

      if (Object.keys(Object.assign({}, test3)).join('') !== 'abcdefghijklmnopqrst') {
        return false;
      }

      return true;
    } catch (err) {
      // We don't expect any of the above to throw, but better to be safe.
      return false;
    }
  }

  var objectAssign = shouldUseNative() ? Object.assign : function (target, source) {
    var from;
    var to = toObject(target);
    var symbols;

    for (var s = 1; s < arguments.length; s++) {
      from = Object(arguments[s]);

      for (var key in from) {
        if (hasOwnProperty.call(from, key)) {
          to[key] = from[key];
        }
      }

      if (getOwnPropertySymbols) {
        symbols = getOwnPropertySymbols(from);

        for (var i = 0; i < symbols.length; i++) {
          if (propIsEnumerable.call(from, symbols[i])) {
            to[symbols[i]] = from[symbols[i]];
          }
        }
      }
    }

    return to;
  };

  /**
   * Copyright (c) 2013-present, Facebook, Inc.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE file in the root directory of this source tree.
   */

  var ReactPropTypesSecret$1 = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';
  var ReactPropTypesSecret_1 = ReactPropTypesSecret$1;

  /**
   * Copyright (c) 2013-present, Facebook, Inc.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE file in the root directory of this source tree.
   */

  var printWarning$1 = function () {};

  {
    var ReactPropTypesSecret = ReactPropTypesSecret_1;

    var loggedTypeFailures = {};
    var has$1 = Function.call.bind(Object.prototype.hasOwnProperty);

    printWarning$1 = function (text) {
      var message = 'Warning: ' + text;

      if (typeof console !== 'undefined') {
        console.error(message);
      }

      try {
        // --- Welcome to debugging React ---
        // This error was thrown as a convenience so that you can use this stack
        // to find the callsite that caused this warning to fire.
        throw new Error(message);
      } catch (x) {}
    };
  }
  /**
   * Assert that the values match with the type specs.
   * Error messages are memorized and will only be shown once.
   *
   * @param {object} typeSpecs Map of name to a ReactPropType
   * @param {object} values Runtime values that need to be type-checked
   * @param {string} location e.g. "prop", "context", "child context"
   * @param {string} componentName Name of the component for error messages.
   * @param {?Function} getStack Returns the component stack.
   * @private
   */


  function checkPropTypes(typeSpecs, values, location, componentName, getStack) {
    {
      for (var typeSpecName in typeSpecs) {
        if (has$1(typeSpecs, typeSpecName)) {
          var error; // Prop type validation may throw. In case they do, we don't want to
          // fail the render phase where it didn't fail before. So we log it.
          // After these have been cleaned up, we'll let them throw.

          try {
            // This is intentionally an invariant that gets caught. It's the same
            // behavior as without this statement except with a better message.
            if (typeof typeSpecs[typeSpecName] !== 'function') {
              var err = Error((componentName || 'React class') + ': ' + location + ' type `' + typeSpecName + '` is invalid; ' + 'it must be a function, usually from the `prop-types` package, but received `' + typeof typeSpecs[typeSpecName] + '`.');
              err.name = 'Invariant Violation';
              throw err;
            }

            error = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, ReactPropTypesSecret);
          } catch (ex) {
            error = ex;
          }

          if (error && !(error instanceof Error)) {
            printWarning$1((componentName || 'React class') + ': type specification of ' + location + ' `' + typeSpecName + '` is invalid; the type checker ' + 'function must return `null` or an `Error` but returned a ' + typeof error + '. ' + 'You may have forgotten to pass an argument to the type checker ' + 'creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and ' + 'shape all require an argument).');
          }

          if (error instanceof Error && !(error.message in loggedTypeFailures)) {
            // Only monitor this failure once because there tends to be a lot of the
            // same error.
            loggedTypeFailures[error.message] = true;
            var stack = getStack ? getStack() : '';
            printWarning$1('Failed ' + location + ' type: ' + error.message + (stack != null ? stack : ''));
          }
        }
      }
    }
  }
  /**
   * Resets warning cache when testing.
   *
   * @private
   */


  checkPropTypes.resetWarningCache = function () {
    {
      loggedTypeFailures = {};
    }
  };

  var checkPropTypes_1 = checkPropTypes;

  /**
   * Copyright (c) 2013-present, Facebook, Inc.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE file in the root directory of this source tree.
   */









  var has = Function.call.bind(Object.prototype.hasOwnProperty);

  var printWarning = function () {};

  {
    printWarning = function (text) {
      var message = 'Warning: ' + text;

      if (typeof console !== 'undefined') {
        console.error(message);
      }

      try {
        // --- Welcome to debugging React ---
        // This error was thrown as a convenience so that you can use this stack
        // to find the callsite that caused this warning to fire.
        throw new Error(message);
      } catch (x) {}
    };
  }

  function emptyFunctionThatReturnsNull() {
    return null;
  }

  var factoryWithTypeCheckers = function (isValidElement, throwOnDirectAccess) {
    /* global Symbol */
    var ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
    var FAUX_ITERATOR_SYMBOL = '@@iterator'; // Before Symbol spec.

    /**
     * Returns the iterator method function contained on the iterable object.
     *
     * Be sure to invoke the function with the iterable as context:
     *
     *     var iteratorFn = getIteratorFn(myIterable);
     *     if (iteratorFn) {
     *       var iterator = iteratorFn.call(myIterable);
     *       ...
     *     }
     *
     * @param {?object} maybeIterable
     * @return {?function}
     */

    function getIteratorFn(maybeIterable) {
      var iteratorFn = maybeIterable && (ITERATOR_SYMBOL && maybeIterable[ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL]);

      if (typeof iteratorFn === 'function') {
        return iteratorFn;
      }
    }
    /**
     * Collection of methods that allow declaration and validation of props that are
     * supplied to React components. Example usage:
     *
     *   var Props = require('ReactPropTypes');
     *   var MyArticle = React.createClass({
     *     propTypes: {
     *       // An optional string prop named "description".
     *       description: Props.string,
     *
     *       // A required enum prop named "category".
     *       category: Props.oneOf(['News','Photos']).isRequired,
     *
     *       // A prop named "dialog" that requires an instance of Dialog.
     *       dialog: Props.instanceOf(Dialog).isRequired
     *     },
     *     render: function() { ... }
     *   });
     *
     * A more formal specification of how these methods are used:
     *
     *   type := array|bool|func|object|number|string|oneOf([...])|instanceOf(...)
     *   decl := ReactPropTypes.{type}(.isRequired)?
     *
     * Each and every declaration produces a function with the same signature. This
     * allows the creation of custom validation functions. For example:
     *
     *  var MyLink = React.createClass({
     *    propTypes: {
     *      // An optional string or URI prop named "href".
     *      href: function(props, propName, componentName) {
     *        var propValue = props[propName];
     *        if (propValue != null && typeof propValue !== 'string' &&
     *            !(propValue instanceof URI)) {
     *          return new Error(
     *            'Expected a string or an URI for ' + propName + ' in ' +
     *            componentName
     *          );
     *        }
     *      }
     *    },
     *    render: function() {...}
     *  });
     *
     * @internal
     */


    var ANONYMOUS = '<<anonymous>>'; // Important!
    // Keep this list in sync with production version in `./factoryWithThrowingShims.js`.

    var ReactPropTypes = {
      array: createPrimitiveTypeChecker('array'),
      bool: createPrimitiveTypeChecker('boolean'),
      func: createPrimitiveTypeChecker('function'),
      number: createPrimitiveTypeChecker('number'),
      object: createPrimitiveTypeChecker('object'),
      string: createPrimitiveTypeChecker('string'),
      symbol: createPrimitiveTypeChecker('symbol'),
      any: createAnyTypeChecker(),
      arrayOf: createArrayOfTypeChecker,
      element: createElementTypeChecker(),
      elementType: createElementTypeTypeChecker(),
      instanceOf: createInstanceTypeChecker,
      node: createNodeChecker(),
      objectOf: createObjectOfTypeChecker,
      oneOf: createEnumTypeChecker,
      oneOfType: createUnionTypeChecker,
      shape: createShapeTypeChecker,
      exact: createStrictShapeTypeChecker
    };
    /**
     * inlined Object.is polyfill to avoid requiring consumers ship their own
     * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
     */

    /*eslint-disable no-self-compare*/

    function is(x, y) {
      // SameValue algorithm
      if (x === y) {
        // Steps 1-5, 7-10
        // Steps 6.b-6.e: +0 != -0
        return x !== 0 || 1 / x === 1 / y;
      } else {
        // Step 6.a: NaN == NaN
        return x !== x && y !== y;
      }
    }
    /*eslint-enable no-self-compare*/

    /**
     * We use an Error-like object for backward compatibility as people may call
     * PropTypes directly and inspect their output. However, we don't use real
     * Errors anymore. We don't inspect their stack anyway, and creating them
     * is prohibitively expensive if they are created too often, such as what
     * happens in oneOfType() for any type before the one that matched.
     */


    function PropTypeError(message) {
      this.message = message;
      this.stack = '';
    } // Make `instanceof Error` still work for returned errors.


    PropTypeError.prototype = Error.prototype;

    function createChainableTypeChecker(validate) {
      {
        var manualPropTypeCallCache = {};
        var manualPropTypeWarningCount = 0;
      }

      function checkType(isRequired, props, propName, componentName, location, propFullName, secret) {
        componentName = componentName || ANONYMOUS;
        propFullName = propFullName || propName;

        if (secret !== ReactPropTypesSecret_1) {
          if (throwOnDirectAccess) {
            // New behavior only for users of `prop-types` package
            var err = new Error('Calling PropTypes validators directly is not supported by the `prop-types` package. ' + 'Use `PropTypes.checkPropTypes()` to call them. ' + 'Read more at http://fb.me/use-check-prop-types');
            err.name = 'Invariant Violation';
            throw err;
          } else if (typeof console !== 'undefined') {
            // Old behavior for people using React.PropTypes
            var cacheKey = componentName + ':' + propName;

            if (!manualPropTypeCallCache[cacheKey] && // Avoid spamming the console because they are often not actionable except for lib authors
            manualPropTypeWarningCount < 3) {
              printWarning('You are manually calling a React.PropTypes validation ' + 'function for the `' + propFullName + '` prop on `' + componentName + '`. This is deprecated ' + 'and will throw in the standalone `prop-types` package. ' + 'You may be seeing this warning due to a third-party PropTypes ' + 'library. See https://fb.me/react-warning-dont-call-proptypes ' + 'for details.');
              manualPropTypeCallCache[cacheKey] = true;
              manualPropTypeWarningCount++;
            }
          }
        }

        if (props[propName] == null) {
          if (isRequired) {
            if (props[propName] === null) {
              return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required ' + ('in `' + componentName + '`, but its value is `null`.'));
            }

            return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required in ' + ('`' + componentName + '`, but its value is `undefined`.'));
          }

          return null;
        } else {
          return validate(props, propName, componentName, location, propFullName);
        }
      }

      var chainedCheckType = checkType.bind(null, false);
      chainedCheckType.isRequired = checkType.bind(null, true);
      return chainedCheckType;
    }

    function createPrimitiveTypeChecker(expectedType) {
      function validate(props, propName, componentName, location, propFullName, secret) {
        var propValue = props[propName];
        var propType = getPropType(propValue);

        if (propType !== expectedType) {
          // `propValue` being instance of, say, date/regexp, pass the 'object'
          // check, but we can offer a more precise error message here rather than
          // 'of type `object`'.
          var preciseType = getPreciseType(propValue);
          return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + preciseType + '` supplied to `' + componentName + '`, expected ') + ('`' + expectedType + '`.'));
        }

        return null;
      }

      return createChainableTypeChecker(validate);
    }

    function createAnyTypeChecker() {
      return createChainableTypeChecker(emptyFunctionThatReturnsNull);
    }

    function createArrayOfTypeChecker(typeChecker) {
      function validate(props, propName, componentName, location, propFullName) {
        if (typeof typeChecker !== 'function') {
          return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside arrayOf.');
        }

        var propValue = props[propName];

        if (!Array.isArray(propValue)) {
          var propType = getPropType(propValue);
          return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an array.'));
        }

        for (var i = 0; i < propValue.length; i++) {
          var error = typeChecker(propValue, i, componentName, location, propFullName + '[' + i + ']', ReactPropTypesSecret_1);

          if (error instanceof Error) {
            return error;
          }
        }

        return null;
      }

      return createChainableTypeChecker(validate);
    }

    function createElementTypeChecker() {
      function validate(props, propName, componentName, location, propFullName) {
        var propValue = props[propName];

        if (!isValidElement(propValue)) {
          var propType = getPropType(propValue);
          return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement.'));
        }

        return null;
      }

      return createChainableTypeChecker(validate);
    }

    function createElementTypeTypeChecker() {
      function validate(props, propName, componentName, location, propFullName) {
        var propValue = props[propName];

        if (!reactIs.isValidElementType(propValue)) {
          var propType = getPropType(propValue);
          return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement type.'));
        }

        return null;
      }

      return createChainableTypeChecker(validate);
    }

    function createInstanceTypeChecker(expectedClass) {
      function validate(props, propName, componentName, location, propFullName) {
        if (!(props[propName] instanceof expectedClass)) {
          var expectedClassName = expectedClass.name || ANONYMOUS;
          var actualClassName = getClassName(props[propName]);
          return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + actualClassName + '` supplied to `' + componentName + '`, expected ') + ('instance of `' + expectedClassName + '`.'));
        }

        return null;
      }

      return createChainableTypeChecker(validate);
    }

    function createEnumTypeChecker(expectedValues) {
      if (!Array.isArray(expectedValues)) {
        {
          if (arguments.length > 1) {
            printWarning('Invalid arguments supplied to oneOf, expected an array, got ' + arguments.length + ' arguments. ' + 'A common mistake is to write oneOf(x, y, z) instead of oneOf([x, y, z]).');
          } else {
            printWarning('Invalid argument supplied to oneOf, expected an array.');
          }
        }

        return emptyFunctionThatReturnsNull;
      }

      function validate(props, propName, componentName, location, propFullName) {
        var propValue = props[propName];

        for (var i = 0; i < expectedValues.length; i++) {
          if (is(propValue, expectedValues[i])) {
            return null;
          }
        }

        var valuesString = JSON.stringify(expectedValues, function replacer(key, value) {
          var type = getPreciseType(value);

          if (type === 'symbol') {
            return String(value);
          }

          return value;
        });
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of value `' + String(propValue) + '` ' + ('supplied to `' + componentName + '`, expected one of ' + valuesString + '.'));
      }

      return createChainableTypeChecker(validate);
    }

    function createObjectOfTypeChecker(typeChecker) {
      function validate(props, propName, componentName, location, propFullName) {
        if (typeof typeChecker !== 'function') {
          return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside objectOf.');
        }

        var propValue = props[propName];
        var propType = getPropType(propValue);

        if (propType !== 'object') {
          return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an object.'));
        }

        for (var key in propValue) {
          if (has(propValue, key)) {
            var error = typeChecker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);

            if (error instanceof Error) {
              return error;
            }
          }
        }

        return null;
      }

      return createChainableTypeChecker(validate);
    }

    function createUnionTypeChecker(arrayOfTypeCheckers) {
      if (!Array.isArray(arrayOfTypeCheckers)) {
        printWarning('Invalid argument supplied to oneOfType, expected an instance of array.') ;
        return emptyFunctionThatReturnsNull;
      }

      for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
        var checker = arrayOfTypeCheckers[i];

        if (typeof checker !== 'function') {
          printWarning('Invalid argument supplied to oneOfType. Expected an array of check functions, but ' + 'received ' + getPostfixForTypeWarning(checker) + ' at index ' + i + '.');
          return emptyFunctionThatReturnsNull;
        }
      }

      function validate(props, propName, componentName, location, propFullName) {
        for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
          var checker = arrayOfTypeCheckers[i];

          if (checker(props, propName, componentName, location, propFullName, ReactPropTypesSecret_1) == null) {
            return null;
          }
        }

        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`.'));
      }

      return createChainableTypeChecker(validate);
    }

    function createNodeChecker() {
      function validate(props, propName, componentName, location, propFullName) {
        if (!isNode(props[propName])) {
          return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`, expected a ReactNode.'));
        }

        return null;
      }

      return createChainableTypeChecker(validate);
    }

    function createShapeTypeChecker(shapeTypes) {
      function validate(props, propName, componentName, location, propFullName) {
        var propValue = props[propName];
        var propType = getPropType(propValue);

        if (propType !== 'object') {
          return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
        }

        for (var key in shapeTypes) {
          var checker = shapeTypes[key];

          if (!checker) {
            continue;
          }

          var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);

          if (error) {
            return error;
          }
        }

        return null;
      }

      return createChainableTypeChecker(validate);
    }

    function createStrictShapeTypeChecker(shapeTypes) {
      function validate(props, propName, componentName, location, propFullName) {
        var propValue = props[propName];
        var propType = getPropType(propValue);

        if (propType !== 'object') {
          return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
        } // We need to check all keys in case some are required but missing from
        // props.


        var allKeys = objectAssign({}, props[propName], shapeTypes);

        for (var key in allKeys) {
          var checker = shapeTypes[key];

          if (!checker) {
            return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` key `' + key + '` supplied to `' + componentName + '`.' + '\nBad object: ' + JSON.stringify(props[propName], null, '  ') + '\nValid keys: ' + JSON.stringify(Object.keys(shapeTypes), null, '  '));
          }

          var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret_1);

          if (error) {
            return error;
          }
        }

        return null;
      }

      return createChainableTypeChecker(validate);
    }

    function isNode(propValue) {
      switch (typeof propValue) {
        case 'number':
        case 'string':
        case 'undefined':
          return true;

        case 'boolean':
          return !propValue;

        case 'object':
          if (Array.isArray(propValue)) {
            return propValue.every(isNode);
          }

          if (propValue === null || isValidElement(propValue)) {
            return true;
          }

          var iteratorFn = getIteratorFn(propValue);

          if (iteratorFn) {
            var iterator = iteratorFn.call(propValue);
            var step;

            if (iteratorFn !== propValue.entries) {
              while (!(step = iterator.next()).done) {
                if (!isNode(step.value)) {
                  return false;
                }
              }
            } else {
              // Iterator will provide entry [k,v] tuples rather than values.
              while (!(step = iterator.next()).done) {
                var entry = step.value;

                if (entry) {
                  if (!isNode(entry[1])) {
                    return false;
                  }
                }
              }
            }
          } else {
            return false;
          }

          return true;

        default:
          return false;
      }
    }

    function isSymbol(propType, propValue) {
      // Native Symbol.
      if (propType === 'symbol') {
        return true;
      } // falsy value can't be a Symbol


      if (!propValue) {
        return false;
      } // 19.4.3.5 Symbol.prototype[@@toStringTag] === 'Symbol'


      if (propValue['@@toStringTag'] === 'Symbol') {
        return true;
      } // Fallback for non-spec compliant Symbols which are polyfilled.


      if (typeof Symbol === 'function' && propValue instanceof Symbol) {
        return true;
      }

      return false;
    } // Equivalent of `typeof` but with special handling for array and regexp.


    function getPropType(propValue) {
      var propType = typeof propValue;

      if (Array.isArray(propValue)) {
        return 'array';
      }

      if (propValue instanceof RegExp) {
        // Old webkits (at least until Android 4.0) return 'function' rather than
        // 'object' for typeof a RegExp. We'll normalize this here so that /bla/
        // passes PropTypes.object.
        return 'object';
      }

      if (isSymbol(propType, propValue)) {
        return 'symbol';
      }

      return propType;
    } // This handles more types than `getPropType`. Only used for error messages.
    // See `createPrimitiveTypeChecker`.


    function getPreciseType(propValue) {
      if (typeof propValue === 'undefined' || propValue === null) {
        return '' + propValue;
      }

      var propType = getPropType(propValue);

      if (propType === 'object') {
        if (propValue instanceof Date) {
          return 'date';
        } else if (propValue instanceof RegExp) {
          return 'regexp';
        }
      }

      return propType;
    } // Returns a string that is postfixed to a warning about an invalid type.
    // For example, "undefined" or "of type array"


    function getPostfixForTypeWarning(value) {
      var type = getPreciseType(value);

      switch (type) {
        case 'array':
        case 'object':
          return 'an ' + type;

        case 'boolean':
        case 'date':
        case 'regexp':
          return 'a ' + type;

        default:
          return type;
      }
    } // Returns class name of the object, if any.


    function getClassName(propValue) {
      if (!propValue.constructor || !propValue.constructor.name) {
        return ANONYMOUS;
      }

      return propValue.constructor.name;
    }

    ReactPropTypes.checkPropTypes = checkPropTypes_1;
    ReactPropTypes.resetWarningCache = checkPropTypes_1.resetWarningCache;
    ReactPropTypes.PropTypes = ReactPropTypes;
    return ReactPropTypes;
  };

  /**
   * Copyright (c) 2013-present, Facebook, Inc.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE file in the root directory of this source tree.
   */

  var propTypes = createCommonjsModule(function (module) {
  {
    var ReactIs = reactIs; // By explicitly using `prop-types` you are opting into new development behavior.
    // http://fb.me/prop-types-in-prod


    var throwOnDirectAccess = true;
    module.exports = factoryWithTypeCheckers(ReactIs.isElement, throwOnDirectAccess);
  }
  });

  var require$$0 = /*@__PURE__*/getAugmentedNamespace(compat$1);

  var _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();



  var _react2 = _interopRequireDefault(require$$0);



  var _propTypes2 = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  var parentStyles = {
    overflow: 'hidden',
    position: 'relative'
  };
  var defaultStyles = {
    position: 'relative',
    overflow: 'hidden',
    cursor: 'pointer',
    display: 'block',
    float: 'left'
  };

  var getHalfStarStyles = function getHalfStarStyles(color, uniqueness) {
    return '\n    .react-stars-' + uniqueness + ':before {\n      position: absolute;\n      overflow: hidden;\n      display: block;\n      z-index: 1;\n      top: 0; left: 0;\n      width: 50%;\n      content: attr(data-forhalf);\n      color: ' + color + ';\n  }';
  };

  var ReactStars = function (_Component) {
    _inherits(ReactStars, _Component);

    function ReactStars(props) {
      _classCallCheck(this, ReactStars); // set defaults


      var _this = _possibleConstructorReturn(this, (ReactStars.__proto__ || Object.getPrototypeOf(ReactStars)).call(this, props));

      props = _extends({}, props);
      _this.state = {
        uniqueness: (Math.random() + '').replace('.', ''),
        value: props.value || 0,
        stars: [],
        halfStar: {
          at: Math.floor(props.value),
          hidden: props.half && props.value % 1 < 0.5
        }
      };
      _this.state.config = {
        count: props.count,
        size: props.size,
        char: props.char,
        // default color of inactive star
        color1: props.color1,
        // color of an active star
        color2: props.color2,
        half: props.half,
        edit: props.edit
      };
      return _this;
    }

    _createClass(ReactStars, [{
      key: 'componentDidMount',
      value: function componentDidMount() {
        this.setState({
          stars: this.getStars(this.state.value)
        });
      }
    }, {
      key: 'componentWillReceiveProps',
      value: function componentWillReceiveProps(props) {
        this.setState({
          stars: this.getStars(props.value),
          value: props.value,
          halfStar: {
            at: Math.floor(props.value),
            hidden: this.state.config.half && props.value % 1 < 0.5
          }
        });
      }
    }, {
      key: 'isDecimal',
      value: function isDecimal(value) {
        return value % 1 !== 0;
      }
    }, {
      key: 'getRate',
      value: function getRate() {
        var stars = void 0;

        if (this.state.config.half) {
          stars = Math.floor(this.state.value);
        } else {
          stars = Math.round(this.state.value);
        }

        return stars;
      }
    }, {
      key: 'getStars',
      value: function getStars(activeCount) {
        if (typeof activeCount === 'undefined') {
          activeCount = this.getRate();
        }

        var stars = [];

        for (var i = 0; i < this.state.config.count; i++) {
          stars.push({
            active: i <= activeCount - 1
          });
        }

        return stars;
      }
    }, {
      key: 'mouseOver',
      value: function mouseOver(event) {
        var _state = this.state,
            config = _state.config,
            halfStar = _state.halfStar;
        if (!config.edit) return;
        var index = Number(event.target.getAttribute('data-index'));

        if (config.half) {
          var isAtHalf = this.moreThanHalf(event, config.size);
          halfStar.hidden = isAtHalf;
          if (isAtHalf) index = index + 1;
          halfStar.at = index;
        } else {
          index = index + 1;
        }

        this.setState({
          stars: this.getStars(index)
        });
      }
    }, {
      key: 'moreThanHalf',
      value: function moreThanHalf(event, size) {
        var target = event.target;
        var mouseAt = event.clientX - target.getBoundingClientRect().left;
        mouseAt = Math.round(Math.abs(mouseAt));
        return mouseAt > size / 2;
      }
    }, {
      key: 'mouseLeave',
      value: function mouseLeave() {
        var _state2 = this.state,
            value = _state2.value,
            halfStar = _state2.halfStar,
            config = _state2.config;
        if (!config.edit) return;

        if (config.half) {
          halfStar.hidden = !this.isDecimal(value);
          halfStar.at = Math.floor(this.state.value);
        }

        this.setState({
          stars: this.getStars()
        });
      }
    }, {
      key: 'clicked',
      value: function clicked(event) {
        var _state3 = this.state,
            config = _state3.config,
            halfStar = _state3.halfStar;
        if (!config.edit) return;
        var index = Number(event.target.getAttribute('data-index'));
        var value = void 0;

        if (config.half) {
          var isAtHalf = this.moreThanHalf(event, config.size);
          halfStar.hidden = isAtHalf;
          if (isAtHalf) index = index + 1;
          value = isAtHalf ? index : index + .5;
          halfStar.at = index;
        } else {
          value = index = index + 1;
        }

        this.setState({
          value: value,
          stars: this.getStars(index)
        });
        this.props.onChange(value);
      }
    }, {
      key: 'renderHalfStarStyleElement',
      value: function renderHalfStarStyleElement() {
        var _state4 = this.state,
            config = _state4.config,
            uniqueness = _state4.uniqueness;
        return _react2.default.createElement('style', {
          dangerouslySetInnerHTML: {
            __html: getHalfStarStyles(config.color2, uniqueness)
          }
        });
      }
    }, {
      key: 'renderStars',
      value: function renderStars() {
        var _this2 = this;

        var _state5 = this.state,
            halfStar = _state5.halfStar,
            stars = _state5.stars,
            uniqueness = _state5.uniqueness,
            config = _state5.config;
        var color1 = config.color1,
            color2 = config.color2,
            size = config.size,
            char = config.char,
            half = config.half,
            edit = config.edit;
        return stars.map(function (star, i) {
          var starClass = '';

          if (half && !halfStar.hidden && halfStar.at === i) {
            starClass = 'react-stars-' + uniqueness;
          }

          var style = _extends({}, defaultStyles, {
            color: star.active ? color2 : color1,
            cursor: edit ? 'pointer' : 'default',
            fontSize: size + 'px'
          });

          return _react2.default.createElement('span', {
            className: starClass,
            style: style,
            key: i,
            'data-index': i,
            'data-forhalf': char,
            onMouseOver: _this2.mouseOver.bind(_this2),
            onMouseMove: _this2.mouseOver.bind(_this2),
            onMouseLeave: _this2.mouseLeave.bind(_this2),
            onClick: _this2.clicked.bind(_this2)
          }, char);
        });
      }
    }, {
      key: 'render',
      value: function render() {
        var className = this.props.className;
        return _react2.default.createElement('div', {
          className: className,
          style: parentStyles
        }, this.state.config.half ? this.renderHalfStarStyleElement() : '', this.renderStars());
      }
    }]);

    return ReactStars;
  }(require$$0.Component);

  ReactStars.propTypes = {
    className: _propTypes2.default.string,
    edit: _propTypes2.default.bool,
    half: _propTypes2.default.bool,
    value: _propTypes2.default.number,
    count: _propTypes2.default.number,
    char: _propTypes2.default.string,
    size: _propTypes2.default.number,
    color1: _propTypes2.default.string,
    color2: _propTypes2.default.string
  };
  ReactStars.defaultProps = {
    edit: true,
    half: true,
    value: 0,
    count: 5,
    char: '★',
    size: 15,
    color1: 'gray',
    color2: '#ffd700',
    onChange: function onChange() {}
  };
  var _default = ReactStars;

  function StarRatingInput(props) {
    var _getBaseProps = getBaseProps(props, TAG_FEEDBACK_FORM),
        apiUrl = _getBaseProps.apiUrl;

    var _useFeedbackApi = useFeedbackApi(apiUrl, props[ATTR_SOURCE_UUID], props[ATTR_TARGET_UUID], props[ATTR_TRANSACTION_ID]),
        sendFeedback = _useFeedbackApi.sendFeedback;

    var ratingChanged = function ratingChanged(newRating) {
      sendFeedback({
        stars: newRating
      });
    };

    return v$2("div", {
      className: "utu-section"
    }, v$2(BaseComponent, {
      className: "d-flex justify-content-center",
      excludeBootstrap: true,
      excludeFonts: true
    }, v$2(_default, {
      count: 5,
      half: false,
      onChange: ratingChanged,
      size: 30,
      color2: "#ffd700"
    })));
  }

  var o$1;!function(e){e.AbortError="media_aborted",e.NotAllowedError="permission_denied",e.NotFoundError="no_specified_media_found",e.NotReadableError="media_in_use",e.OverconstrainedError="invalid_media_constraints",e.TypeError="no_constraints",e.NONE="",e.NO_RECORDER="recorder_error";}(o$1||(o$1={}));var i$1=function(){function e(){this.url=null,this.blob=new Blob,this.mediaChunks=[];}var r=e.prototype;return r.setBlobProperties=function(e){this.blobProperties=e;},r.storeChunk=function(e){this.mediaChunks.push(e);},r.stop=function(){var e=new Blob(this.mediaChunks,this.blobProperties),r=URL.createObjectURL(e);this.blob=e,this.url=r;},r.getUrl=function(){return this.url},r.getBlob=function(){return this.blob},e}();function u$1(u){var c=u.audio,a=void 0===c||c,s=u.video,d=void 0!==s&&s,l=u.onStop,f=void 0===l?function(){return null}:l,p=u.blobPropertyBag,v=u.screen,h=void 0!==v&&v,m=u.mediaRecorderOptions,b=void 0===m?null:m,g=u.videoStorageFactory,w=void 0===g?function(){return new i$1}:g,y$1=u.timeslice,R=void 0===y$1?void 0:y$1,E=s$1(null),P=s$1(null),M=s$1(null),_=l$1("idle"),k=_[0],T=_[1],O=l$1(!1),N=O[0],B=O[1],D=l$1(null),U=D[0],j=D[1],C=l$1("NONE"),A=C[0],S=C[1],F=!1,L=A$1(function(){try{T("acquiring_media");var e={audio:"boolean"==typeof a?!!a:a,video:"boolean"==typeof d?!!d:d},r=function(r,n){try{var t=function(){function r(){T("idle");}var n=h?Promise.resolve(window.navigator.mediaDevices.getDisplayMedia({video:d||!0})).then(function(e){function r(){M.current=e;}var n=function(){if(a)return Promise.resolve(window.navigator.mediaDevices.getUserMedia({audio:a})).then(function(r){r.getAudioTracks().forEach(function(r){return e.addTrack(r)});})}();return n&&n.then?n.then(r):r()}):Promise.resolve(window.navigator.mediaDevices.getUserMedia(e)).then(function(e){M.current=e;});return n&&n.then?n.then(r):r()}();}catch(e){return n(e)}return t&&t.then?t.then(void 0,n):t}(0,function(e){S(e.name),T("idle");});return Promise.resolve(r&&r.then?r.then(function(){}):void 0)}catch(e){return Promise.reject(e)}},[a,d,h]);y(function(){if(!window.MediaRecorder)throw new Error("Unsupported Browser");if(h&&!window.navigator.mediaDevices.getDisplayMedia)throw new Error("This browser doesn't support screen capturing");var e=function(e){var r=navigator.mediaDevices.getSupportedConstraints(),n=Object.keys(e).filter(function(e){return !r[e]});n.length>0&&console.error("The constraints "+n.join(",")+" doesn't support on this browser. Please check your ReactMediaRecorder component.");};"object"==typeof a&&e(a),"object"==typeof d&&e(d),b&&b.mimeType&&(MediaRecorder.isTypeSupported(b.mimeType)||console.error("The specified MIME type you supplied for MediaRecorder doesn't support this browser")),M.current||L();},[a,h,d,L,b]);var q=function(e){var r,n=e.data;if(!F){var t,o=Object.assign({type:n.type},p||(d?{type:"video/mp4"}:{type:"audio/wav"}));null==(t=E.current)||t.setBlobProperties(o),F=!0;}null==(r=E.current)||r.storeChunk(n);},x=function(){var e,r,n,t;null==(e=E.current)||e.stop();var o=null!=(r=null==(n=E.current)?void 0:n.getUrl())?r:null;T("stopped"),j(o),f(o,null==(t=E.current)?void 0:t.getBlob());},I=function(e){B(e),M.current&&M.current.getAudioTracks().forEach(function(r){return r.enabled=!e});};return {error:o$1[A],muteAudio:function(){return I(!0)},unMuteAudio:function(){return I(!1)},startRecording:function(){try{var e=function(){var e=function(){if(M.current){var e=function(){E.current=w(),F=!1,P.current=new MediaRecorder(M.current),P.current.ondataavailable=q,P.current.onstop=x,P.current.onerror=function(){S("NO_RECORDER"),T("idle");},P.current.start(R),T("recording");},r=M.current.getTracks().some(function(e){return "ended"===e.readyState}),n=function(){if(r)return Promise.resolve(L()).then(function(){})}();return n&&n.then?n.then(e):e()}}();if(e&&e.then)return e.then(function(){})};S("NONE");var r=function(){if(!M.current)return Promise.resolve(L()).then(function(){})}();return Promise.resolve(r&&r.then?r.then(e):e())}catch(e){return Promise.reject(e)}},pauseRecording:function(){P.current&&"recording"===P.current.state&&P.current.pause();},resumeRecording:function(){P.current&&"paused"===P.current.state&&P.current.resume();},stopRecording:function(){P.current&&"inactive"!==P.current.state&&(T("stopping"),P.current.stop(),M.current&&M.current.getTracks().forEach(function(e){return e.stop()}));},mediaBlobUrl:U,status:k,isAudioMuted:N,previewStream:M.current?new MediaStream(M.current.getVideoTracks()):null,clearBlobUrl:function(){return j(null)}}}var c$1=function(e){return e.render(u$1(e))};

  var e=0;function t(t){return "__private_"+e+++"_"+t}function i(e,t){if(!Object.prototype.hasOwnProperty.call(e,t))throw new TypeError("attempted to use private field on non-instance");return e}var r=function(e){this.value=e;},n=t("head"),o=t("end"),u=function(){function e(){Object.defineProperty(this,n,{writable:!0,value:void 0}),Object.defineProperty(this,o,{writable:!0,value:void 0});}var t=e.prototype;return t.enqueue=function(e){var t=new r(e);i(this,o)[o]?(i(this,o)[o].next=t,i(this,o)[o]=t):i(this,n)[n]=i(this,o)[o]=t;},t.dequeue=function(){var e=i(this,n)[n];return i(this,n)[n]=null==e?void 0:e.next,i(this,n)[n]||(i(this,o)[o]=void 0),null==e?void 0:e.value},e}();function s(e){return {done:!1,value:e}}var h={done:!0},a=t("chunkQueue"),l=t("resolveQueue"),v=t("isInProgress"),c=t("isLocked"),d=function(){function e(){Object.defineProperty(this,a,{writable:!0,value:void 0}),Object.defineProperty(this,l,{writable:!0,value:void 0}),Object.defineProperty(this,v,{writable:!0,value:void 0}),Object.defineProperty(this,c,{writable:!0,value:void 0}),this.url=null,i(this,a)[a]=new u,i(this,l)[l]=new u,i(this,v)[v]=!0,i(this,c)[c]=!1;}var t=e.prototype;return t.setUrl=function(e){this.url=e;},t.setBlobProperties=function(e){this.blobProperties=e;},t.storeChunk=function(e){i(this,v)[v]=!0;var t=i(this,l)[l].dequeue();t?t(s(e)):i(this,a)[a].enqueue(e);},t.stop=function(){i(this,v)[v]=!1;},t.reset=function(){i(this,v)[v]=!0,i(this,a)[a]=new u,i(this,l)[l]=new u,i(this,c)[c]=!1;},t.getUrl=function(){return this.url},t.getBlob=function(){},t.getReader=function(){if(i(this,c)[c])throw new Error("ReadableStorage is locked.");i(this,c)[c]=!0;var e=this;return {read:function(){var t=i(e,a)[a].dequeue();return t?Promise.resolve(s(t)):i(e,v)[v]?new Promise(function(t){return i(e,l)[l].enqueue(t)}):Promise.resolve(h)}}},e}();

  var Circles_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Circles = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var Circles = function Circles(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      width: props.width,
      height: props.height,
      viewBox: "0 0 135 135",
      xmlns: "http://www.w3.org/2000/svg",
      fill: props.color,
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("path", {
      d: "M67.447 58c5.523 0 10-4.477 10-10s-4.477-10-10-10-10 4.477-10 10 4.477 10 10 10zm9.448 9.447c0 5.523 4.477 10 10 10 5.522 0 10-4.477 10-10s-4.478-10-10-10c-5.523 0-10 4.477-10 10zm-9.448 9.448c-5.523 0-10 4.477-10 10 0 5.522 4.477 10 10 10s10-4.478 10-10c0-5.523-4.477-10-10-10zM58 67.447c0-5.523-4.477-10-10-10s-10 4.477-10 10 4.477 10 10 10 10-4.477 10-10z"
    }, /*#__PURE__*/_react["default"].createElement("animateTransform", {
      attributeName: "transform",
      type: "rotate",
      from: "0 67 67",
      to: "-360 67 67",
      dur: "2.5s",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("path", {
      d: "M28.19 40.31c6.627 0 12-5.374 12-12 0-6.628-5.373-12-12-12-6.628 0-12 5.372-12 12 0 6.626 5.372 12 12 12zm30.72-19.825c4.686 4.687 12.284 4.687 16.97 0 4.686-4.686 4.686-12.284 0-16.97-4.686-4.687-12.284-4.687-16.97 0-4.687 4.686-4.687 12.284 0 16.97zm35.74 7.705c0 6.627 5.37 12 12 12 6.626 0 12-5.373 12-12 0-6.628-5.374-12-12-12-6.63 0-12 5.372-12 12zm19.822 30.72c-4.686 4.686-4.686 12.284 0 16.97 4.687 4.686 12.285 4.686 16.97 0 4.687-4.686 4.687-12.284 0-16.97-4.685-4.687-12.283-4.687-16.97 0zm-7.704 35.74c-6.627 0-12 5.37-12 12 0 6.626 5.373 12 12 12s12-5.374 12-12c0-6.63-5.373-12-12-12zm-30.72 19.822c-4.686-4.686-12.284-4.686-16.97 0-4.686 4.687-4.686 12.285 0 16.97 4.686 4.687 12.284 4.687 16.97 0 4.687-4.685 4.687-12.283 0-16.97zm-35.74-7.704c0-6.627-5.372-12-12-12-6.626 0-12 5.373-12 12s5.374 12 12 12c6.628 0 12-5.373 12-12zm-19.823-30.72c4.687-4.686 4.687-12.284 0-16.97-4.686-4.686-12.284-4.686-16.97 0-4.687 4.686-4.687 12.284 0 16.97 4.686 4.687 12.284 4.687 16.97 0z"
    }, /*#__PURE__*/_react["default"].createElement("animateTransform", {
      attributeName: "transform",
      type: "rotate",
      from: "0 67 67",
      to: "360 67 67",
      dur: "8s",
      repeatCount: "indefinite"
    })));
  };

  exports.Circles = Circles;
  Circles.propTypes = {
    height: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    width: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    color: _propTypes["default"].string,
    label: _propTypes["default"].string
  };
  Circles.defaultProps = {
    height: 80,
    width: 80,
    color: "green",
    label: "audio-loading"
  };
  });

  var Watch_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Watch = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var Watch = function Watch(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      width: props.width,
      height: props.height,
      version: "1.1",
      id: "L2",
      xmlns: "http://www.w3.org/2000/svg",
      x: "0px",
      y: "0px",
      viewBox: "0 0 100 100",
      enableBackground: "new 0 0 100 100",
      xmlSpace: "preserve",
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("circle", {
      fill: "none",
      stroke: props.color,
      strokeWidth: "4",
      strokeMiterlimit: "10",
      cx: "50",
      cy: "50",
      r: props.radius
    }), /*#__PURE__*/_react["default"].createElement("line", {
      fill: "none",
      strokeLinecap: "round",
      stroke: props.color,
      strokeWidth: "4",
      strokeMiterlimit: "10",
      x1: "50",
      y1: "50",
      x2: "85",
      y2: "50.5"
    }, /*#__PURE__*/_react["default"].createElement("animateTransform", {
      attributeName: "transform",
      dur: "2s",
      type: "rotate",
      from: "0 50 50",
      to: "360 50 50",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("line", {
      fill: "none",
      strokeLinecap: "round",
      stroke: props.color,
      strokeWidth: "4",
      strokeMiterlimit: "10",
      x1: "50",
      y1: "50",
      x2: "49.5",
      y2: "74"
    }, /*#__PURE__*/_react["default"].createElement("animateTransform", {
      attributeName: "transform",
      dur: "15s",
      type: "rotate",
      from: "0 50 50",
      to: "360 50 50",
      repeatCount: "indefinite"
    })));
  };

  exports.Watch = Watch;
  Watch.propTypes = {
    height: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    width: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    color: _propTypes["default"].string,
    label: _propTypes["default"].string,
    radius: _propTypes["default"].number
  };
  Watch.defaultProps = {
    height: 80,
    width: 80,
    color: "green",
    label: "audio-loading",
    radius: 48
  };
  });

  var Audio_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Audio = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var Audio = function Audio(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      height: props.height,
      width: props.width,
      fill: props.color,
      viewBox: "0 0 55 80",
      xmlns: "http://www.w3.org/2000/svg",
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("g", {
      transform: "matrix(1 0 0 -1 0 80)"
    }, /*#__PURE__*/_react["default"].createElement("rect", {
      width: "10",
      height: "20",
      rx: "3"
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "height",
      begin: "0s",
      dur: "4.3s",
      values: "20;45;57;80;64;32;66;45;64;23;66;13;64;56;34;34;2;23;76;79;20",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("rect", {
      x: "15",
      width: "10",
      height: "80",
      rx: "3"
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "height",
      begin: "0s",
      dur: "2s",
      values: "80;55;33;5;75;23;73;33;12;14;60;80",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("rect", {
      x: "30",
      width: "10",
      height: "50",
      rx: "3"
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "height",
      begin: "0s",
      dur: "1.4s",
      values: "50;34;78;23;56;23;34;76;80;54;21;50",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("rect", {
      x: "45",
      width: "10",
      height: "30",
      rx: "3"
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "height",
      begin: "0s",
      dur: "2s",
      values: "30;45;13;80;56;72;45;76;34;23;67;30",
      calcMode: "linear",
      repeatCount: "indefinite"
    }))));
  };

  exports.Audio = Audio;
  Audio.propTypes = {
    height: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    width: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    color: _propTypes["default"].string,
    label: _propTypes["default"].string
  };
  Audio.defaultProps = {
    height: 80,
    width: 80,
    color: "green",
    label: "audio-loading"
  };
  });

  var BallTriangle_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.BallTriangle = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var BallTriangle = function BallTriangle(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      height: props.height,
      width: props.width,
      stroke: props.color,
      viewBox: "0 0 57 57",
      xmlns: "http://www.w3.org/2000/svg",
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("g", {
      fill: "none",
      fillRule: "evenodd"
    }, /*#__PURE__*/_react["default"].createElement("g", {
      transform: "translate(1 1)",
      strokeWidth: "2"
    }, /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "5",
      cy: "50",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "cy",
      begin: "0s",
      dur: "2.2s",
      values: "50;5;50;50",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "cx",
      begin: "0s",
      dur: "2.2s",
      values: "5;27;49;5",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "27",
      cy: "5",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "cy",
      begin: "0s",
      dur: "2.2s",
      from: "5",
      to: "5",
      values: "5;50;50;5",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "cx",
      begin: "0s",
      dur: "2.2s",
      from: "27",
      to: "27",
      values: "27;49;5;27",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "49",
      cy: "50",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "cy",
      begin: "0s",
      dur: "2.2s",
      values: "50;50;5;50",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "cx",
      from: "49",
      to: "49",
      begin: "0s",
      dur: "2.2s",
      values: "49;5;27;49",
      calcMode: "linear",
      repeatCount: "indefinite"
    })))));
  };

  exports.BallTriangle = BallTriangle;
  BallTriangle.propTypes = {
    height: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    width: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    color: _propTypes["default"].string,
    label: _propTypes["default"].string,
    radius: _propTypes["default"].number
  };
  BallTriangle.defaultProps = {
    height: 80,
    width: 80,
    color: "green",
    radius: 5,
    label: "audio-loading"
  };
  });

  var Bars_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Bars = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var Bars = function Bars(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      width: props.width,
      height: props.height,
      fill: props.color,
      viewBox: "0 0 135 140",
      xmlns: "http://www.w3.org/2000/svg",
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("rect", {
      y: "10",
      width: "15",
      height: "120",
      rx: "6"
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "height",
      begin: "0.5s",
      dur: "1s",
      values: "120;110;100;90;80;70;60;50;40;140;120",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "y",
      begin: "0.5s",
      dur: "1s",
      values: "10;15;20;25;30;35;40;45;50;0;10",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("rect", {
      x: "30",
      y: "10",
      width: "15",
      height: "120",
      rx: "6"
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "height",
      begin: "0.25s",
      dur: "1s",
      values: "120;110;100;90;80;70;60;50;40;140;120",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "y",
      begin: "0.25s",
      dur: "1s",
      values: "10;15;20;25;30;35;40;45;50;0;10",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("rect", {
      x: "60",
      width: "15",
      height: "140",
      rx: "6"
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "height",
      begin: "0s",
      dur: "1s",
      values: "120;110;100;90;80;70;60;50;40;140;120",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "y",
      begin: "0s",
      dur: "1s",
      values: "10;15;20;25;30;35;40;45;50;0;10",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("rect", {
      x: "90",
      y: "10",
      width: "15",
      height: "120",
      rx: "6"
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "height",
      begin: "0.25s",
      dur: "1s",
      values: "120;110;100;90;80;70;60;50;40;140;120",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "y",
      begin: "0.25s",
      dur: "1s",
      values: "10;15;20;25;30;35;40;45;50;0;10",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("rect", {
      x: "120",
      y: "10",
      width: "15",
      height: "120",
      rx: "6"
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "height",
      begin: "0.5s",
      dur: "1s",
      values: "120;110;100;90;80;70;60;50;40;140;120",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "y",
      begin: "0.5s",
      dur: "1s",
      values: "10;15;20;25;30;35;40;45;50;0;10",
      calcMode: "linear",
      repeatCount: "indefinite"
    })));
  };

  exports.Bars = Bars;
  Bars.propTypes = {
    height: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    width: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    color: _propTypes["default"].string,
    label: _propTypes["default"].string
  };
  Bars.defaultProps = {
    height: 80,
    width: 80,
    color: "green",
    label: "audio-loading"
  };
  });

  var CradleLoader_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.CradleLoader = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var CradleLoader = function CradleLoader(props) {
    return /*#__PURE__*/_react["default"].createElement("div", {
      "aria-label": props.label,
      role: "presentation",
      className: "container"
    }, /*#__PURE__*/_react["default"].createElement("div", {
      className: "react-spinner-loader-swing"
    }, /*#__PURE__*/_react["default"].createElement("div", {
      className: "react-spinner-loader-swing-l"
    }), /*#__PURE__*/_react["default"].createElement("div", null), /*#__PURE__*/_react["default"].createElement("div", null), /*#__PURE__*/_react["default"].createElement("div", null), /*#__PURE__*/_react["default"].createElement("div", null), /*#__PURE__*/_react["default"].createElement("div", null), /*#__PURE__*/_react["default"].createElement("div", {
      className: "react-spinner-loader-swing-r"
    })), /*#__PURE__*/_react["default"].createElement("div", {
      className: "react-spinner-loader-shadow"
    }, /*#__PURE__*/_react["default"].createElement("div", {
      className: "react-spinner-loader-shadow-l"
    }), /*#__PURE__*/_react["default"].createElement("div", null), /*#__PURE__*/_react["default"].createElement("div", null), /*#__PURE__*/_react["default"].createElement("div", null), /*#__PURE__*/_react["default"].createElement("div", null), /*#__PURE__*/_react["default"].createElement("div", null), /*#__PURE__*/_react["default"].createElement("div", {
      className: "react-spinner-loader-shadow-r"
    })));
  };

  exports.CradleLoader = CradleLoader;
  CradleLoader.propTypes = {
    label: _propTypes["default"].string
  };
  CradleLoader.defaultProps = {
    label: "audio-loading"
  };
  });

  var Grid_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Grid = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var Grid = function Grid(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      width: props.width,
      height: props.height,
      viewBox: "0 0 105 105",
      fill: props.color,
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "12.5",
      cy: "12.5",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "fill-opacity",
      begin: "0s",
      dur: "1s",
      values: "1;.2;1",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "12.5",
      cy: "52.5",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "fill-opacity",
      begin: "100ms",
      dur: "1s",
      values: "1;.2;1",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "52.5",
      cy: "12.5",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "fill-opacity",
      begin: "300ms",
      dur: "1s",
      values: "1;.2;1",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "52.5",
      cy: "52.5",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "fill-opacity",
      begin: "600ms",
      dur: "1s",
      values: "1;.2;1",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "92.5",
      cy: "12.5",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "fill-opacity",
      begin: "800ms",
      dur: "1s",
      values: "1;.2;1",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "92.5",
      cy: "52.5",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "fill-opacity",
      begin: "400ms",
      dur: "1s",
      values: "1;.2;1",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "12.5",
      cy: "92.5",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "fill-opacity",
      begin: "700ms",
      dur: "1s",
      values: "1;.2;1",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "52.5",
      cy: "92.5",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "fill-opacity",
      begin: "500ms",
      dur: "1s",
      values: "1;.2;1",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "92.5",
      cy: "92.5",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "fill-opacity",
      begin: "200ms",
      dur: "1s",
      values: "1;.2;1",
      calcMode: "linear",
      repeatCount: "indefinite"
    })));
  };

  exports.Grid = Grid;
  Grid.propTypes = {
    height: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    width: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    color: _propTypes["default"].string,
    label: _propTypes["default"].string,
    radius: _propTypes["default"].number
  };
  Grid.defaultProps = {
    height: 80,
    width: 80,
    color: "green",
    radius: 12.5,
    label: "audio-loading"
  };
  });

  var Hearts_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Hearts = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var Hearts = function Hearts(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      width: props.width,
      height: props.height,
      viewBox: "0 0 140 64",
      xmlns: "http://www.w3.org/2000/svg",
      fill: props.color,
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("path", {
      d: "M30.262 57.02L7.195 40.723c-5.84-3.976-7.56-12.06-3.842-18.063 3.715-6 11.467-7.65 17.306-3.68l4.52 3.76 2.6-5.274c3.717-6.002 11.47-7.65 17.305-3.68 5.84 3.97 7.56 12.054 3.842 18.062L34.49 56.118c-.897 1.512-2.793 1.915-4.228.9z",
      attributeName: "fill-opacity",
      from: "0",
      to: ".5"
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "fill-opacity",
      begin: "0s",
      dur: "1.4s",
      values: "0.5;1;0.5",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("path", {
      d: "M105.512 56.12l-14.44-24.272c-3.716-6.008-1.996-14.093 3.843-18.062 5.835-3.97 13.588-2.322 17.306 3.68l2.6 5.274 4.52-3.76c5.84-3.97 13.592-2.32 17.307 3.68 3.718 6.003 1.998 14.088-3.842 18.064L109.74 57.02c-1.434 1.014-3.33.61-4.228-.9z",
      attributeName: "fill-opacity",
      from: "0",
      to: ".5"
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "fill-opacity",
      begin: "0.7s",
      dur: "1.4s",
      values: "0.5;1;0.5",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("path", {
      d: "M67.408 57.834l-23.01-24.98c-5.864-6.15-5.864-16.108 0-22.248 5.86-6.14 15.37-6.14 21.234 0L70 16.168l4.368-5.562c5.863-6.14 15.375-6.14 21.235 0 5.863 6.14 5.863 16.098 0 22.247l-23.007 24.98c-1.43 1.556-3.757 1.556-5.188 0z"
    }));
  };

  exports.Hearts = Hearts;
  Hearts.propTypes = {
    height: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    width: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    color: _propTypes["default"].string,
    label: _propTypes["default"].string
  };
  Hearts.defaultProps = {
    height: 80,
    width: 80,
    color: "green",
    label: "audio-loading"
  };
  });

  var MutatingDots_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.MutatingDots = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var MutatingDots = function MutatingDots(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      id: "goo-loader",
      width: props.width,
      height: props.height,
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("filter", {
      id: "fancy-goo"
    }, /*#__PURE__*/_react["default"].createElement("feGaussianBlur", {
      "in": "SourceGraphic",
      stdDeviation: "6",
      result: "blur"
    }), /*#__PURE__*/_react["default"].createElement("feColorMatrix", {
      "in": "blur",
      mode: "matrix",
      values: "1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9",
      result: "goo"
    }), /*#__PURE__*/_react["default"].createElement("feComposite", {
      "in": "SourceGraphic",
      in2: "goo",
      operator: "atop"
    })), /*#__PURE__*/_react["default"].createElement("g", {
      filter: "url(#fancy-goo)"
    }, /*#__PURE__*/_react["default"].createElement("animateTransform", {
      id: "mainAnim",
      attributeName: "transform",
      attributeType: "XML",
      type: "rotate",
      from: "0 50 50",
      to: "359 50 50",
      dur: "1.2s",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "50%",
      cy: "40",
      r: props.radius,
      fill: props.color
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      id: "cAnim1",
      attributeType: "XML",
      attributeName: "cy",
      dur: "0.6s",
      begin: "0;cAnim1.end+0.2s",
      calcMode: "spline",
      values: "40;20;40",
      keyTimes: "0;0.3;1",
      keySplines: "0.09, 0.45, 0.16, 1;0.09, 0.45, 0.16, 1"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "50%",
      cy: "60",
      r: props.radius,
      fill: props.secondaryColor
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      id: "cAnim2",
      attributeType: "XML",
      attributeName: "cy",
      dur: "0.6s",
      begin: "0.4s;cAnim2.end+0.2s",
      calcMode: "spline",
      values: "60;80;60",
      keyTimes: "0;0.3;1",
      keySplines: "0.09, 0.45, 0.16, 1;0.09, 0.45, 0.16, 1"
    }))));
  };

  exports.MutatingDots = MutatingDots;
  MutatingDots.propTypes = {
    width: _propTypes["default"].number,
    secondaryColor: _propTypes["default"].string,
    height: _propTypes["default"].number,
    color: _propTypes["default"].string,
    radius: _propTypes["default"].number,
    label: _propTypes["default"].string
  };
  MutatingDots.defaultProps = {
    width: 80,
    height: 90,
    color: "green",
    radius: 11,
    secondaryColor: "green",
    label: "audio-loading"
  };
  });

  var Oval_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Oval = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var Oval = function Oval(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      width: props.width,
      height: props.height,
      viewBox: "0 0 38 38",
      xmlns: "http://www.w3.org/2000/svg",
      stroke: props.color,
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("g", {
      fill: "none",
      fillRule: "evenodd"
    }, /*#__PURE__*/_react["default"].createElement("g", {
      transform: "translate(1 1)",
      strokeWidth: "2"
    }, /*#__PURE__*/_react["default"].createElement("circle", {
      strokeOpacity: ".5",
      cx: "18",
      cy: "18",
      r: props.radius
    }), /*#__PURE__*/_react["default"].createElement("path", {
      d: "M36 18c0-9.94-8.06-18-18-18"
    }, /*#__PURE__*/_react["default"].createElement("animateTransform", {
      attributeName: "transform",
      type: "rotate",
      from: "0 18 18",
      to: "360 18 18",
      dur: "1s",
      repeatCount: "indefinite"
    })))));
  };

  exports.Oval = Oval;
  Oval.propTypes = {
    height: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    width: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    color: _propTypes["default"].string,
    label: _propTypes["default"].string,
    radius: _propTypes["default"].number
  };
  Oval.defaultProps = {
    height: 80,
    width: 80,
    color: "green",
    label: "audio-loading",
    radius: 18
  };
  });

  var Plane_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Plane = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var Plane = function Plane(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      className: "react-spinner-loader-svg-calLoader",
      xmlns: "http://www.w3.org/2000/svg",
      width: "230",
      height: "230",
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("desc", null, "Plane animation. Loading "), /*#__PURE__*/_react["default"].createElement("path", {
      className: "react-spinner-loader-cal-loader__path",
      style: {
        stroke: props.secondaryColor
      },
      d: "M86.429 40c63.616-20.04 101.511 25.08 107.265 61.93 6.487 41.54-18.593 76.99-50.6 87.643-59.46 19.791-101.262-23.577-107.142-62.616C29.398 83.441 59.945 48.343 86.43 40z",
      fill: "none",
      stroke: "#0099cc",
      strokeWidth: "4",
      strokeLinecap: "round",
      strokeLinejoin: "round",
      strokeDasharray: "10 10 10 10 10 10 10 432",
      strokeDashoffset: "77"
    }), /*#__PURE__*/_react["default"].createElement("path", {
      className: "cal-loader__plane",
      style: {
        fill: props.color
      },
      d: "M141.493 37.93c-1.087-.927-2.942-2.002-4.32-2.501-2.259-.824-3.252-.955-9.293-1.172-4.017-.146-5.197-.23-5.47-.37-.766-.407-1.526-1.448-7.114-9.773-4.8-7.145-5.344-7.914-6.327-8.976-1.214-1.306-1.396-1.378-3.79-1.473-1.036-.04-2-.043-2.153-.002-.353.1-.87.586-1 .952-.139.399-.076.71.431 2.22.241.72 1.029 3.386 1.742 5.918 1.644 5.844 2.378 8.343 2.863 9.705.206.601.33 1.1.275 1.125-.24.097-10.56 1.066-11.014 1.032a3.532 3.532 0 0 1-1.002-.276l-.487-.246-2.044-2.613c-2.234-2.87-2.228-2.864-3.35-3.309-.717-.287-2.82-.386-3.276-.163-.457.237-.727.644-.737 1.152-.018.39.167.805 1.916 4.373 1.06 2.166 1.964 4.083 1.998 4.27.04.179.004.521-.076.75-.093.228-1.109 2.064-2.269 4.088-1.921 3.34-2.11 3.711-2.123 4.107-.008.25.061.557.168.725.328.512.72.644 1.966.676 1.32.029 2.352-.236 3.05-.762.222-.171 1.275-1.313 2.412-2.611 1.918-2.185 2.048-2.32 2.45-2.505.241-.111.601-.232.82-.271.267-.058 2.213.201 5.912.8 3.036.48 5.525.894 5.518.914 0 .026-.121.306-.27.638-.54 1.198-1.515 3.842-3.35 9.021-1.029 2.913-2.107 5.897-2.4 6.62-.703 1.748-.725 1.833-.594 2.286.137.46.45.833.872 1.012.41.177 3.823.24 4.37.085.852-.25 1.44-.688 2.312-1.724 1.166-1.39 3.169-3.948 6.771-8.661 5.8-7.583 6.561-8.49 7.387-8.702.233-.065 2.828-.056 5.784.011 5.827.138 6.64.09 8.62-.5 2.24-.67 4.035-1.65 5.517-3.016 1.136-1.054 1.135-1.014.207-1.962-.357-.38-.767-.777-.902-.893z",
      fill: "#000033"
    }));
  };

  exports.Plane = Plane;
  Plane.propTypes = {
    secondaryColor: _propTypes["default"].string,
    color: _propTypes["default"].string,
    label: _propTypes["default"].string
  };
  Plane.defaultProps = {
    secondaryColor: "grey",
    color: "#FFA500",
    label: "async-loading"
  };
  });

  var Puff_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Puff = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var Puff = function Puff(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      width: props.width,
      height: props.height,
      viewBox: "0 0 44 44",
      xmlns: "http://www.w3.org/2000/svg",
      stroke: props.color,
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("g", {
      fill: "none",
      fillRule: "evenodd",
      strokeWidth: "2"
    }, /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "22",
      cy: "22",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "r",
      begin: "0s",
      dur: "1.8s",
      values: "1; 20",
      calcMode: "spline",
      keyTimes: "0; 1",
      keySplines: "0.165, 0.84, 0.44, 1",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "strokeOpacity",
      begin: "0s",
      dur: "1.8s",
      values: "1; 0",
      calcMode: "spline",
      keyTimes: "0; 1",
      keySplines: "0.3, 0.61, 0.355, 1",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "22",
      cy: "22",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "r",
      begin: "-0.9s",
      dur: "1.8s",
      values: "1; 20",
      calcMode: "spline",
      keyTimes: "0; 1",
      keySplines: "0.165, 0.84, 0.44, 1",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "strokeOpacity",
      begin: "-0.9s",
      dur: "1.8s",
      values: "1; 0",
      calcMode: "spline",
      keyTimes: "0; 1",
      keySplines: "0.3, 0.61, 0.355, 1",
      repeatCount: "indefinite"
    }))));
  };

  exports.Puff = Puff;
  Puff.propTypes = {
    height: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    width: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    color: _propTypes["default"].string,
    label: _propTypes["default"].string,
    radius: _propTypes["default"].number
  };
  Puff.defaultProps = {
    height: 80,
    width: 80,
    color: "green",
    label: "audio-loading",
    radius: 1
  };
  });

  var RevolvingDot_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.RevolvingDot = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var RevolvingDot = function RevolvingDot(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      version: "1.1",
      width: props.width,
      height: props.height,
      xmlns: "http://www.w3.org/2000/svg",
      x: "0px",
      y: "0px",
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("circle", {
      fill: "none",
      stroke: props.color,
      strokeWidth: "4",
      cx: "50",
      cy: "50",
      r: props.radius + 38,
      style: {
        opacity: 0.5
      }
    }), /*#__PURE__*/_react["default"].createElement("circle", {
      fill: props.color,
      stroke: props.color,
      strokeWidth: "3",
      cx: "8",
      cy: "54",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animateTransform", {
      attributeName: "transform",
      dur: "2s",
      type: "rotate",
      from: "0 50 48",
      to: "360 50 52",
      repeatCount: "indefinite"
    })));
  };

  exports.RevolvingDot = RevolvingDot;
  RevolvingDot.propTypes = {
    height: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    width: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    color: _propTypes["default"].string,
    label: _propTypes["default"].string,
    radius: _propTypes["default"].number
  };
  RevolvingDot.defaultProps = {
    height: 80,
    width: 80,
    color: "green",
    label: "audio-loading",
    radius: 6
  };
  });

  var Rings_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Rings = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var Rings = function Rings(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      width: props.width,
      height: props.height,
      viewBox: "0 0 45 45",
      xmlns: "http://www.w3.org/2000/svg",
      stroke: props.color,
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("g", {
      fill: "none",
      fillRule: "evenodd",
      transform: "translate(1 1)",
      strokeWidth: "2"
    }, /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "22",
      cy: "22",
      r: props.radius,
      strokeOpacity: "0"
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "r",
      begin: "1.5s",
      dur: "3s",
      values: "6;22",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "stroke-opacity",
      begin: "1.5s",
      dur: "3s",
      values: "1;0",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "stroke-width",
      begin: "1.5s",
      dur: "3s",
      values: "2;0",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "22",
      cy: "22",
      r: props.radius,
      strokeOpacity: "0"
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "r",
      begin: "3s",
      dur: "3s",
      values: "6;22",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "strokeOpacity",
      begin: "3s",
      dur: "3s",
      values: "1;0",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "strokeWidth",
      begin: "3s",
      dur: "3s",
      values: "2;0",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "22",
      cy: "22",
      r: props.radius + 2
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "r",
      begin: "0s",
      dur: "1.5s",
      values: "6;1;2;3;4;5;6",
      calcMode: "linear",
      repeatCount: "indefinite"
    }))));
  };

  exports.Rings = Rings;
  Rings.propTypes = {
    height: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    width: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    color: _propTypes["default"].string,
    label: _propTypes["default"].string,
    radius: _propTypes["default"].number
  };
  Rings.defaultProps = {
    height: 80,
    width: 80,
    color: "green",
    radius: 6,
    label: "audio-loading"
  };
  });

  var TailSpin_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.TailSpin = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var TailSpin = function TailSpin(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      width: props.width,
      height: props.height,
      viewBox: "0 0 38 38",
      xmlns: "http://www.w3.org/2000/svg",
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("defs", null, /*#__PURE__*/_react["default"].createElement("linearGradient", {
      x1: "8.042%",
      y1: "0%",
      x2: "65.682%",
      y2: "23.865%",
      id: "a"
    }, /*#__PURE__*/_react["default"].createElement("stop", {
      stopColor: props.color,
      stopOpacity: "0",
      offset: "0%"
    }), /*#__PURE__*/_react["default"].createElement("stop", {
      stopColor: props.color,
      stopOpacity: ".631",
      offset: "63.146%"
    }), /*#__PURE__*/_react["default"].createElement("stop", {
      stopColor: props.color,
      offset: "100%"
    }))), /*#__PURE__*/_react["default"].createElement("g", {
      fill: "none",
      fillRule: "evenodd"
    }, /*#__PURE__*/_react["default"].createElement("g", {
      transform: "translate(1 1)"
    }, /*#__PURE__*/_react["default"].createElement("path", {
      d: "M36 18c0-9.94-8.06-18-18-18",
      id: "Oval-2",
      stroke: props.color,
      strokeWidth: "2"
    }, /*#__PURE__*/_react["default"].createElement("animateTransform", {
      attributeName: "transform",
      type: "rotate",
      from: "0 18 18",
      to: "360 18 18",
      dur: "0.9s",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      fill: "#fff",
      cx: "36",
      cy: "18",
      r: props.radius
    }, /*#__PURE__*/_react["default"].createElement("animateTransform", {
      attributeName: "transform",
      type: "rotate",
      from: "0 18 18",
      to: "360 18 18",
      dur: "0.9s",
      repeatCount: "indefinite"
    })))));
  };

  exports.TailSpin = TailSpin;
  TailSpin.propTypes = {
    height: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    width: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    color: _propTypes["default"].string,
    label: _propTypes["default"].string,
    radius: _propTypes["default"].number
  };
  TailSpin.defaultProps = {
    height: 80,
    width: 80,
    color: "green",
    radius: 1,
    label: "audio-loading"
  };
  });

  var ThreeDots_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.ThreeDots = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var ThreeDots = function ThreeDots(props) {
    return /*#__PURE__*/_react["default"].createElement("svg", {
      width: props.width,
      height: props.height,
      viewBox: "0 0 120 30",
      xmlns: "http://www.w3.org/2000/svg",
      fill: props.color,
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "15",
      cy: "15",
      r: props.radius + 6
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "r",
      from: "15",
      to: "15",
      begin: "0s",
      dur: "0.8s",
      values: "15;9;15",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "fillOpacity",
      from: "1",
      to: "1",
      begin: "0s",
      dur: "0.8s",
      values: "1;.5;1",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "60",
      cy: "15",
      r: props.radius,
      attributeName: "fillOpacity",
      from: "1",
      to: "0.3"
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "r",
      from: "9",
      to: "9",
      begin: "0s",
      dur: "0.8s",
      values: "9;15;9",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "fillOpacity",
      from: "0.5",
      to: "0.5",
      begin: "0s",
      dur: "0.8s",
      values: ".5;1;.5",
      calcMode: "linear",
      repeatCount: "indefinite"
    })), /*#__PURE__*/_react["default"].createElement("circle", {
      cx: "105",
      cy: "15",
      r: props.radius + 6
    }, /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "r",
      from: "15",
      to: "15",
      begin: "0s",
      dur: "0.8s",
      values: "15;9;15",
      calcMode: "linear",
      repeatCount: "indefinite"
    }), /*#__PURE__*/_react["default"].createElement("animate", {
      attributeName: "fillOpacity",
      from: "1",
      to: "1",
      begin: "0s",
      dur: "0.8s",
      values: "1;.5;1",
      calcMode: "linear",
      repeatCount: "indefinite"
    })));
  };

  exports.ThreeDots = ThreeDots;
  ThreeDots.propTypes = {
    height: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    width: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    color: _propTypes["default"].string,
    label: _propTypes["default"].string,
    radius: _propTypes["default"].number
  };
  ThreeDots.defaultProps = {
    height: 80,
    width: 80,
    color: "green",
    label: "audio-loading",
    radius: 9
  };
  });

  var Triangle_1 = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Triangle = void 0;

  var _react = _interopRequireDefault(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  var Triangle = function Triangle(props) {
    return /*#__PURE__*/_react["default"].createElement("div", {
      className: "react-spinner-loader-svg"
    }, /*#__PURE__*/_react["default"].createElement("svg", {
      id: "triangle",
      width: props.width,
      height: props.height,
      viewBox: "-3 -4 39 39",
      "aria-label": props.label
    }, /*#__PURE__*/_react["default"].createElement("polygon", {
      fill: "transparent",
      stroke: props.color,
      strokeWidth: "1",
      points: "16,0 32,32 0,32"
    })));
  };

  exports.Triangle = Triangle;
  Triangle.propTypes = {
    height: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    width: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
    color: _propTypes["default"].string,
    label: _propTypes["default"].string
  };
  Triangle.defaultProps = {
    height: 80,
    width: 80,
    color: "green",
    label: "audio-loading"
  };
  });

  var loader = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Spinner = void 0;



































  var Spinner = {
    Circles: Circles_1.Circles,
    Audio: Audio_1.Audio,
    BallTriangle: BallTriangle_1.BallTriangle,
    Bars: Bars_1.Bars,
    CradleLoader: CradleLoader_1.CradleLoader,
    Grid: Grid_1.Grid,
    Hearts: Hearts_1.Hearts,
    MutatingDots: MutatingDots_1.MutatingDots,
    Oval: Oval_1.Oval,
    Plane: Plane_1.Plane,
    Puff: Puff_1.Puff,
    RevolvingDot: RevolvingDot_1.RevolvingDot,
    Rings: Rings_1.Rings,
    TailSpin: TailSpin_1.TailSpin,
    ThreeDots: ThreeDots_1.ThreeDots,
    Triangle: Triangle_1.Triangle,
    Watch: Watch_1.Watch
  };
  exports.Spinner = Spinner;
  });

  var dist = createCommonjsModule(function (module, exports) {

  function _typeof(obj) {
    "@babel/helpers - typeof";

    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function _typeof(obj) {
        return typeof obj;
      };
    } else {
      _typeof = function _typeof(obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof(obj);
  }

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports["default"] = Loader;

  var _react = _interopRequireWildcard(require$$0);

  var _propTypes = _interopRequireDefault(propTypes);



  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      "default": obj
    };
  }

  function _getRequireWildcardCache() {
    if (typeof WeakMap !== "function") return null;
    var cache = new WeakMap();

    _getRequireWildcardCache = function _getRequireWildcardCache() {
      return cache;
    };

    return cache;
  }

  function _interopRequireWildcard(obj) {
    if (obj && obj.__esModule) {
      return obj;
    }

    if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
      return {
        "default": obj
      };
    }

    var cache = _getRequireWildcardCache();

    if (cache && cache.has(obj)) {
      return cache.get(obj);
    }

    var newObj = {};
    var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

        if (desc && (desc.get || desc.set)) {
          Object.defineProperty(newObj, key, desc);
        } else {
          newObj[key] = obj[key];
        }
      }
    }

    newObj["default"] = obj;

    if (cache) {
      cache.set(obj, newObj);
    }

    return newObj;
  }

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(Object(source), true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(Object(source)).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
  }

  function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;

    for (var i = 0, arr2 = new Array(len); i < len; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }

  function _iterableToArrayLimit(arr, i) {
    if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  var componentNames = ["Audio", "BallTriangle", "Bars", "Circles", "Grid", "Hearts", "Oval", "Puff", "Rings", "TailSpin", "ThreeDots", "Watch", "RevolvingDot", "Triangle", "Plane", "MutatingDots", "CradleLoader"];

  function componentName(type) {
    if (componentNames.includes(type)) {
      return loader.Spinner[type];
    }

    return loader.Spinner.Audio;
  }
  /**
   * @return {null}
   */


  function Loader(props) {
    var _useState = (0, _react.useState)(true),
        _useState2 = _slicedToArray(_useState, 2),
        display = _useState2[0],
        setDisplay = _useState2[1];

    (0, _react.useEffect)(function () {
      var timer;

      if (props.timeout && props.timeout > 0) {
        timer = setTimeout(function () {
          setDisplay(false);
        }, props.timeout);
      }

      return function () {
        if (timer) clearTimeout(timer);
      };
    });

    if (!props.visible || props.visible === "false") {
      return null;
    }

    return display ? /*#__PURE__*/_react["default"].createElement("div", {
      "aria-busy": "true",
      className: props.className,
      style: props.style
    }, /*#__PURE__*/_react["default"].createElement(componentName(props.type), _objectSpread({}, props))) : null;
  }

  Loader.propTypes = {
    type: _propTypes["default"].oneOf([].concat(componentNames)),
    style: _propTypes["default"].objectOf(_propTypes["default"].string),
    className: _propTypes["default"].string,
    visible: _propTypes["default"].oneOfType([_propTypes["default"].bool, _propTypes["default"].string]),
    timeout: _propTypes["default"].number
  };
  Loader.defaultProps = {
    type: "Audio",
    style: {},
    className: "",
    visible: true,
    timeout: 0
  };
  });

  var Loader = /*@__PURE__*/getDefaultExportFromCjs(dist);

  var css_248z$3 = "@import url(https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css);\n:host {\n  margin: 0;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";\n  font-size: 14px;\n  font-weight: 300;\n  line-height: 1.5;\n  color: #000;\n  -webkit-text-size-adjust: 100%;\n  -webkit-tap-highlight-color: rgba(0, 0, 0, 0); }\n\n.utu-section {\n  border-bottom: 1px solid rgba(0, 0, 0, 0.15);\n  background: #fff;\n  padding: 18px; }\n\nh3 {\n  text-transform: capitalize;\n  font-weight: 500;\n  font-size: 14px; }\n\n.btn {\n  background-color: #FFDD33;\n  padding: 12px;\n  border-radius: 10px;\n  border: none;\n  font-size: 14px;\n  font-weight: 500;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\"; }\n\n.icon-btn {\n  border: none;\n  background: none;\n  padding: 0;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";\n  font-weight: 500;\n  font-size: 14px; }\n  .icon-btn svg {\n    width: 60px;\n    height: 60px; }\n\ntextarea {\n  resize: vertical;\n  min-height: 189px;\n  border-radius: 10px;\n  border: none;\n  background-color: rgba(255, 221, 51, 0.15);\n  padding: 16px;\n  font-weight: 300; }\n\n.error {\n  color: #e80054; }\n\n.trust-video {\n  top: 0;\n  left: 0;\n  border: none;\n  border-radius: 20px;\n  max-width: 100%;\n  min-width: 315px; }\n  .trust-video-wrapper {\n    position: relative;\n    padding: 0;\n    margin-top: 30px; }\n    .trust-video-wrapper:hover .trust-video-controls {\n      visibility: visible;\n      opacity: 1; }\n  .trust-video-controls {\n    position: absolute;\n    bottom: 14px;\n    padding-left: 36px;\n    left: 14px;\n    width: calc(100% - 28px);\n    right: 0;\n    border-radius: 10px;\n    height: 4em;\n    align-items: center;\n    justify-content: space-between;\n    visibility: hidden;\n    opacity: 0;\n    transition: all .25s ease-out;\n    display: none; }\n    .trust-video-controls.show {\n      display: flex; }\n  .trust-video-play {\n    border-color: transparent;\n    transition: 0.3s;\n    top: 50%;\n    left: 50%;\n    transform: translate(-50%, -50%);\n    font-size: 3em;\n    line-height: 1.5em;\n    width: 3em;\n    height: 3em;\n    display: block;\n    position: absolute;\n    padding: 0;\n    cursor: pointer;\n    opacity: 1;\n    background-color: #FFDD33;\n    border-radius: 50%;\n    font: normal normal normal 18px/1 FontAwesome; }\n    .trust-video-play:before {\n      content: \"\\f04b\"; }\n    .trust-video-play.paused:before {\n      content: \"\\f04c\"; }\n  .trust-video-volume {\n    border-color: transparent;\n    color: rgba(43, 51, 63, 0.7);\n    font-size: 3em;\n    line-height: 1.5em;\n    width: 2em;\n    height: 1.5em;\n    display: block;\n    position: absolute;\n    font: normal normal normal 18px/1 FontAwesome;\n    background-color: rgba(255, 221, 51, 0.7);\n    left: 20px; }\n    .trust-video-volume:before {\n      content: \"\\f028\"; }\n    .trust-video-volume.off:before {\n      content: \"\\f026\"; }\n  .trust-video-fullscreen {\n    border-color: transparent;\n    color: rgba(43, 51, 63, 0.7);\n    font-size: 3em;\n    line-height: 1.5em;\n    width: 3em;\n    height: 1.5em;\n    display: block;\n    position: absolute;\n    font: normal normal normal 18px/1 FontAwesome;\n    background-color: rgba(255, 221, 51, 0.7);\n    right: 20px; }\n    .trust-video-fullscreen:before {\n      content: \"\\f065\"; }\n\n.record_btn {\n  margin: 5px 0;\n  width: 40%; }\n\n.spinner_wrapper {\n  position: relative; }\n\n.loading_spinner {\n  position: absolute;\n  bottom: 14%;\n  left: 39%; }\n\n.response_message {\n  height: 3rem;\n  line-height: 3rem;\n  width: 100%; }\n";
  styleInject(css_248z$3);

  var aPossiblePrototype = function (it) {
    if (!isObject$2(it) && it !== null) {
      throw TypeError("Can't set " + String(it) + ' as a prototype');
    }

    return it;
  };

  /* eslint-disable no-proto -- safe */

  // `Object.setPrototypeOf` method
  // https://tc39.es/ecma262/#sec-object.setprototypeof
  // Works with __proto__ only. Old v8 can't work with null proto objects.
  // eslint-disable-next-line es/no-object-setprototypeof -- safe


  var objectSetPrototypeOf = Object.setPrototypeOf || ('__proto__' in {} ? function () {
    var CORRECT_SETTER = false;
    var test = {};
    var setter;

    try {
      // eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
      setter = Object.getOwnPropertyDescriptor(Object.prototype, '__proto__').set;
      setter.call(test, []);
      CORRECT_SETTER = test instanceof Array;
    } catch (error) {
      /* empty */
    }

    return function setPrototypeOf(O, proto) {
      anObject(O);
      aPossiblePrototype(proto);
      if (CORRECT_SETTER) setter.call(O, proto);else O.__proto__ = proto;
      return O;
    };
  }() : undefined);

  // makes subclassing work correct for wrapped built-ins


  var inheritIfRequired = function ($this, dummy, Wrapper) {
    var NewTarget, NewTargetPrototype;
    if ( // it can work only with native `setPrototypeOf`
    objectSetPrototypeOf && // we haven't completely correct pre-ES6 way for getting `new.target`, so use this
    typeof (NewTarget = dummy.constructor) == 'function' && NewTarget !== Wrapper && isObject$2(NewTargetPrototype = NewTarget.prototype) && NewTargetPrototype !== Wrapper.prototype) objectSetPrototypeOf($this, NewTargetPrototype);
    return $this;
  };

  // a string of all valid unicode whitespaces
  var whitespaces = '\u0009\u000A\u000B\u000C\u000D\u0020\u00A0\u1680\u2000\u2001\u2002' + '\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF';

  var whitespace = '[' + whitespaces + ']';
  var ltrim = RegExp('^' + whitespace + whitespace + '*');
  var rtrim = RegExp(whitespace + whitespace + '*$'); // `String.prototype.{ trim, trimStart, trimEnd, trimLeft, trimRight }` methods implementation

  var createMethod$1 = function (TYPE) {
    return function ($this) {
      var string = toString$2(requireObjectCoercible($this));
      if (TYPE & 1) string = string.replace(ltrim, '');
      if (TYPE & 2) string = string.replace(rtrim, '');
      return string;
    };
  };

  var stringTrim = {
    // `String.prototype.{ trimLeft, trimStart }` methods
    // https://tc39.es/ecma262/#sec-string.prototype.trimstart
    start: createMethod$1(1),
    // `String.prototype.{ trimRight, trimEnd }` methods
    // https://tc39.es/ecma262/#sec-string.prototype.trimend
    end: createMethod$1(2),
    // `String.prototype.trim` method
    // https://tc39.es/ecma262/#sec-string.prototype.trim
    trim: createMethod$1(3)
  };

  var getOwnPropertyNames = objectGetOwnPropertyNames.f;

  var getOwnPropertyDescriptor = objectGetOwnPropertyDescriptor.f;

  var defineProperty = objectDefineProperty.f;

  var trim = stringTrim.trim;

  var NUMBER = 'Number';
  var NativeNumber = global$1[NUMBER];
  var NumberPrototype = NativeNumber.prototype; // Opera ~12 has broken Object#toString

  var BROKEN_CLASSOF = classofRaw(objectCreate(NumberPrototype)) == NUMBER; // `ToNumber` abstract operation
  // https://tc39.es/ecma262/#sec-tonumber

  var toNumber = function (argument) {
    if (isSymbol$1(argument)) throw TypeError('Cannot convert a Symbol value to a number');
    var it = toPrimitive(argument, 'number');
    var first, third, radix, maxCode, digits, length, index, code;

    if (typeof it == 'string' && it.length > 2) {
      it = trim(it);
      first = it.charCodeAt(0);

      if (first === 43 || first === 45) {
        third = it.charCodeAt(2);
        if (third === 88 || third === 120) return NaN; // Number('+0x1') should be NaN, old V8 fix
      } else if (first === 48) {
        switch (it.charCodeAt(1)) {
          case 66:
          case 98:
            radix = 2;
            maxCode = 49;
            break;
          // fast equal of /^0b[01]+$/i

          case 79:
          case 111:
            radix = 8;
            maxCode = 55;
            break;
          // fast equal of /^0o[0-7]+$/i

          default:
            return +it;
        }

        digits = it.slice(2);
        length = digits.length;

        for (index = 0; index < length; index++) {
          code = digits.charCodeAt(index); // parseInt parses a string to a first unavailable symbol
          // but ToNumber should return NaN if a string contains unavailable symbols

          if (code < 48 || code > maxCode) return NaN;
        }

        return parseInt(digits, radix);
      }
    }

    return +it;
  }; // `Number` constructor
  // https://tc39.es/ecma262/#sec-number-constructor


  if (isForced_1(NUMBER, !NativeNumber(' 0o1') || !NativeNumber('0b1') || NativeNumber('+0x1'))) {
    var NumberWrapper = function Number(value) {
      var it = arguments.length < 1 ? 0 : value;
      var dummy = this;
      return dummy instanceof NumberWrapper // check on 1..constructor(foo) case
      && (BROKEN_CLASSOF ? fails(function () {
        NumberPrototype.valueOf.call(dummy);
      }) : classofRaw(dummy) != NUMBER) ? inheritIfRequired(new NativeNumber(toNumber(it)), dummy, NumberWrapper) : toNumber(it);
    };

    for (var keys = descriptors ? getOwnPropertyNames(NativeNumber) : ( // ES3:
    'MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,' + // ES2015 (in case, if modules with ES2015 Number statics required before):
    'EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,' + 'MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger,' + // ESNext
    'fromString,range').split(','), j = 0, key; keys.length > j; j++) {
      if (has$8(NativeNumber, key = keys[j]) && !has$8(NumberWrapper, key)) {
        defineProperty(NumberWrapper, key, getOwnPropertyDescriptor(NativeNumber, key));
      }
    }

    NumberWrapper.prototype = NumberPrototype;
    NumberPrototype.constructor = NumberWrapper;
    redefine(global$1, NUMBER, NumberWrapper);
  }

  // `thisNumberValue` abstract operation
  // https://tc39.es/ecma262/#sec-thisnumbervalue


  var thisNumberValue = function (value) {
    if (typeof value != 'number' && classofRaw(value) != 'Number') {
      throw TypeError('Incorrect invocation');
    }

    return +value;
  };

  // `String.prototype.repeat` method implementation
  // https://tc39.es/ecma262/#sec-string.prototype.repeat


  var stringRepeat = function repeat(count) {
    var str = toString$2(requireObjectCoercible(this));
    var result = '';
    var n = toInteger(count);
    if (n < 0 || n == Infinity) throw RangeError('Wrong number of repetitions');

    for (; n > 0; (n >>>= 1) && (str += str)) if (n & 1) result += str;

    return result;
  };

  var nativeToFixed = 1.0.toFixed;
  var floor = Math.floor;

  var pow = function (x, n, acc) {
    return n === 0 ? acc : n % 2 === 1 ? pow(x, n - 1, acc * x) : pow(x * x, n / 2, acc);
  };

  var log = function (x) {
    var n = 0;
    var x2 = x;

    while (x2 >= 4096) {
      n += 12;
      x2 /= 4096;
    }

    while (x2 >= 2) {
      n += 1;
      x2 /= 2;
    }

    return n;
  };

  var multiply = function (data, n, c) {
    var index = -1;
    var c2 = c;

    while (++index < 6) {
      c2 += n * data[index];
      data[index] = c2 % 1e7;
      c2 = floor(c2 / 1e7);
    }
  };

  var divide = function (data, n) {
    var index = 6;
    var c = 0;

    while (--index >= 0) {
      c += data[index];
      data[index] = floor(c / n);
      c = c % n * 1e7;
    }
  };

  var dataToString = function (data) {
    var index = 6;
    var s = '';

    while (--index >= 0) {
      if (s !== '' || index === 0 || data[index] !== 0) {
        var t = String(data[index]);
        s = s === '' ? t : s + stringRepeat.call('0', 7 - t.length) + t;
      }
    }

    return s;
  };

  var FORCED = nativeToFixed && (0.00008.toFixed(3) !== '0.000' || 0.9.toFixed(0) !== '1' || 1.255.toFixed(2) !== '1.25' || 1000000000000000128.0.toFixed(0) !== '1000000000000000128') || !fails(function () {
    // V8 ~ Android 4.3-
    nativeToFixed.call({});
  }); // `Number.prototype.toFixed` method
  // https://tc39.es/ecma262/#sec-number.prototype.tofixed

  _export({
    target: 'Number',
    proto: true,
    forced: FORCED
  }, {
    toFixed: function toFixed(fractionDigits) {
      var number = thisNumberValue(this);
      var fractDigits = toInteger(fractionDigits);
      var data = [0, 0, 0, 0, 0, 0];
      var sign = '';
      var result = '0';
      var e, z, j, k;
      if (fractDigits < 0 || fractDigits > 20) throw RangeError('Incorrect fraction digits'); // eslint-disable-next-line no-self-compare -- NaN check

      if (number != number) return 'NaN';
      if (number <= -1e21 || number >= 1e21) return String(number);

      if (number < 0) {
        sign = '-';
        number = -number;
      }

      if (number > 1e-21) {
        e = log(number * pow(2, 69, 1)) - 69;
        z = e < 0 ? number * pow(2, -e, 1) : number / pow(2, e, 1);
        z *= 0x10000000000000;
        e = 52 - e;

        if (e > 0) {
          multiply(data, 0, z);
          j = fractDigits;

          while (j >= 7) {
            multiply(data, 1e7, 0);
            j -= 7;
          }

          multiply(data, pow(10, j, 1), 0);
          j = e - 1;

          while (j >= 23) {
            divide(data, 1 << 23);
            j -= 23;
          }

          divide(data, 1 << j);
          multiply(data, 1, 1);
          divide(data, 2);
          result = dataToString(data);
        } else {
          multiply(data, 0, z);
          multiply(data, 1 << -e, 0);
          result = dataToString(data) + stringRepeat.call('0', fractDigits);
        }
      }

      if (fractDigits > 0) {
        k = result.length;
        result = sign + (k <= fractDigits ? '0.' + stringRepeat.call('0', fractDigits - k) + result : result.slice(0, k - fractDigits) + '.' + result.slice(k - fractDigits));
      } else {
        result = sign + result;
      }

      return result;
    }
  });

  var Player = (function (videoUrl, preview, previewStream) {
    var _useState = l$1(false),
        _useState2 = _slicedToArray(_useState, 2),
        controlsVisible = _useState2[0],
        setControlsVisible = _useState2[1]; // const [length, setLength] = useState(null);
    // const [formattedLength, setFormattedLength] = useState(null);


    var videoRef = s$1(null);
    var playBtnRef = s$1(null);
    var muteBtnRef = s$1(null);

    var _useState3 = l$1({
      width: 0,
      height: 0
    }),
        _useState4 = _slicedToArray(_useState3, 2),
        size = _useState4[0],
        setSize = _useState4[1];

    h$1(function () {
      function updateSize() {
        setSize({
          width: window.innerWidth / 2,
          height: window.innerHeight
        });
      }

      window.addEventListener("resize", updateSize);
      updateSize();
      return function () {
        return window.removeEventListener("resize", updateSize);
      };
    }, []);

    var play = /*#__PURE__*/function () {
      var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                getDuration();

                if (!videoRef.current.paused) {
                  _context.next = 7;
                  break;
                }

                _context.next = 4;
                return videoRef.current.play();

              case 4:
                playBtnRef.current.classList.add("paused");
                _context.next = 9;
                break;

              case 7:
                videoRef.current.pause();
                playBtnRef.current.classList.remove("paused");

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function play() {
        return _ref.apply(this, arguments);
      };
    }();

    var getDuration = function getDuration() {
      var dur = videoRef.current.duration;
      dur = Number(dur.toFixed()); // const formatted = "";
      // setLength(dur);
      // setFormattedLength(formatted);

      return dur;
    };

    var mute = function mute() {
      videoRef.current.muted = !videoRef.current.muted;

      if (videoRef.current.muted) {
        muteBtnRef.current.classList.add("off");
      } else {
        muteBtnRef.current.classList.remove("off");
      }
    };

    var fullScreen = /*#__PURE__*/function () {
      var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return videoRef.current.requestFullscreen();

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      return function fullScreen() {
        return _ref2.apply(this, arguments);
      };
    }();

    y(function () {
      if (videoRef.current) {
        setControlsVisible(true);
      }
    }, [videoUrl]);

    var Preview = function Preview(_ref3) {
      var stream = _ref3.stream;
      var previewRef = s$1(null);
      y(function () {
        if (previewRef.current && stream && preview) {
          previewRef.current.srcObject = stream;
        }
      }, [stream]);

      if (!stream) {
        return null;
      }

      if (!stream.active) {
        return v$2("div", {
          style: {
            paddingTop: "50%"
          }
        });
      }

      return v$2("video", {
        className: "trust-video preview-video",
        id: "trustVideoPreview",
        ref: previewRef,
        autoPlay: true
      }, v$2("track", {
        kind: "captions"
      }));
    };

    var VideoSrc = function VideoSrc() {
      if (videoUrl) {
        return v$2("video", {
          className: "trust-video",
          id: "trustVideo",
          ref: videoRef,
          autoPlay: true
        }, v$2("source", {
          src: videoUrl,
          type: "video/mp4"
        }), v$2("source", {
          src: videoUrl,
          type: "video/webm"
        }), v$2("track", {
          kind: "captions"
        }));
      }

      return v$2(Preview, {
        stream: previewStream
      });
    };

    return v$2("div", {
      className: "trust-video-wrapper"
    }, v$2(VideoSrc, null), v$2("div", {
      className: "trust-video-controls ".concat(controlsVisible && "show"),
      style: {
        maxWidth: size.width
      }
    }, v$2("button", {
      ref: muteBtnRef,
      type: "button",
      className: "trust-video-volume trust-video-control ".concat(videoRef.current && videoRef.current.muted && "off"),
      onClick: mute
    }, " "), !preview && v$2("button", {
      ref: playBtnRef,
      type: "button",
      className: "trust-video-play trust-video-control paused",
      onClick: play
    }, " "), v$2("div", {
      className: "trust-video-progress"
    }), !preview && v$2("button", {
      type: "button",
      className: "trust-video-fullscreen trust-video-control",
      onClick: fullScreen
    }, " ")));
  });

  function RecordVideo(props) {
    /**
     * Holds the active reader retrieved from ReadableStorage.getReader() while a video is recording, or null while no
     * video is recording.
     */
    var _useState = l$1(null),
        _useState2 = _slicedToArray(_useState, 2),
        activeReader = _useState2[0],
        setActiveReader = _useState2[1];

    var _getBaseProps = getBaseProps(props, TAG_FEEDBACK_FORM),
        apiUrl = _getBaseProps.apiUrl;

    var constraints = {
      // set user env var
      height: 400,
      width: 500,
      frameRate: 24,
      facingMode: "user"
    };

    var _useUploadVideoApi = useUploadVideoApi(activeReader),
        publishedVideoUrl = _useUploadVideoApi.publishedVideoUrl,
        uploading = _useUploadVideoApi.uploading,
        successMessage = _useUploadVideoApi.successMessage,
        errorMessage = _useUploadVideoApi.errorMessage;

    var _useFeedbackApi = useFeedbackApi(apiUrl, props[ATTR_SOURCE_UUID], props[ATTR_TARGET_UUID], props[ATTR_TRANSACTION_ID]),
        sendFeedback = _useFeedbackApi.sendFeedback;

    var sendFeedbackOnce = A$1(function () {
      return publishedVideoUrl && sendFeedback({
        video: publishedVideoUrl
      });
    }, [sendFeedback, publishedVideoUrl]);
    sendFeedbackOnce();

    var VideoSrc = function VideoSrc(status, previewStream) {
      if (publishedVideoUrl) {
        return Player(publishedVideoUrl, false);
      }

      return Player(null, true, previewStream);
    };

    function videoStorageFactory() {
      var storage = new d();
      setActiveReader(storage.getReader());
      return storage;
    }

    return v$2(BaseComponent, {
      style: css_248z$3,
      className: "py-3"
    }, v$2("div", null, v$2("div", {
      className: "d-flex justify-content-center mt-4"
    }, v$2(c$1, {
      video: constraints,
      videoStorageFactory: videoStorageFactory,
      timeslice: 1000,
      render: function render(_ref) {
        var status = _ref.status,
            startRecording = _ref.startRecording,
            stopRecording = _ref.stopRecording,
            previewStream = _ref.previewStream;
        return v$2("div", {
          className: "text-center "
        }, v$2("div", null, status !== "recording" && v$2("button", {
          type: "button",
          style: {
            height: "50px"
          },
          className: " btn btn-outline-primary record_btn m-1",
          onClick: startRecording
        }, "Start Recording"), status === "recording" && v$2("button", {
          type: "button",
          style: {
            height: "50px"
          },
          className: " btn btn-outline-primary record_btn m-1",
          onClick: function onClick() {
            stopRecording();
            setActiveReader(null);
          }
        }, "Stop Recording")), v$2("div", null, successMessage && v$2("h3", {
          className: "response_message my-4 text-white bg-success"
        }, successMessage), errorMessage && v$2("h3", {
          className: "response_message my-4 text-white bg-danger"
        }, errorMessage), uploading ? v$2("p", {
          className: "response_message my-4 text-white bg-success"
        }, "uploading...") : null), v$2("div", {
          className: "spinner_wrapper"
        }, VideoSrc(status, previewStream), v$2("p", {
          className: ""
        }, uploading ? v$2("div", {
          className: "mt-4 loading_spinner"
        }, v$2(Loader, {
          type: "MutatingDots",
          color: "#FFDD33",
          secondaryColor: "#FFDD33",
          height: 80,
          width: 80
        })) : null)));
      }
    }))));
  }

  function FeedbackForm(props) {
    /* eslint-disable react/jsx-props-no-spreading */
    return v$2(BaseComponent, {
      style: css_248z$5,
      className: "utu-feedback-form utu-section d-flex flex-column align-items-stretch"
    }, v$2(StarRatingInput, props), v$2(Badges, props), v$2(FeedbackTextInput, props), v$2(RecordVideo, props));
  }

  var css_248z$2 = ":host {\n  margin: 0;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";\n  font-size: 14px;\n  font-weight: 300;\n  line-height: 1.5;\n  color: #000;\n  -webkit-text-size-adjust: 100%;\n  -webkit-tap-highlight-color: rgba(0, 0, 0, 0); }\n\n.utu-section {\n  border-bottom: 1px solid rgba(0, 0, 0, 0.15);\n  background: #fff;\n  padding: 18px; }\n\nh3 {\n  text-transform: capitalize;\n  font-weight: 500;\n  font-size: 14px; }\n\n.btn {\n  background-color: #FFDD33;\n  padding: 12px;\n  border-radius: 10px;\n  border: none;\n  font-size: 14px;\n  font-weight: 500;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\"; }\n\n.icon-btn {\n  border: none;\n  background: none;\n  padding: 0;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";\n  font-weight: 500;\n  font-size: 14px; }\n  .icon-btn svg {\n    width: 60px;\n    height: 60px; }\n\ntextarea {\n  resize: vertical;\n  min-height: 189px;\n  border-radius: 10px;\n  border: none;\n  background-color: rgba(255, 221, 51, 0.15);\n  padding: 16px;\n  font-weight: 300; }\n\n.error {\n  color: #e80054; }\n\n.popup {\n  position: fixed;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  z-index: 1000; }\n  .popup .background {\n    position: fixed;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    right: 0;\n    background-color: rgba(236, 236, 236, 0.95); }\n  .popup .popup-container {\n    overflow-y: auto;\n    max-height: 100%;\n    background: white;\n    width: 700px;\n    z-index: 1001; }\n    .popup .popup-container .icon-btn {\n      padding: 10px;\n      font-size: 1.2em; }\n";
  styleInject(css_248z$2);

  function PopUp(_ref) {
    var onClose = _ref.onClose,
        children = _ref.children;
    // return <BaseComponent style={style} className="popup d-flex justify-content-center align-items-center">
    return v$2(BaseComponent, {
      style: css_248z$2,
      className: "popup d-flex justify-content-center align-items-center"
    }, v$2("div", {
      className: "popup-container d-flex flex-column align-items-stretch justify-content-left border-component"
    }, v$2("div", {
      className: "d-flex flex-row-reverse"
    }, v$2("button", {
      type: "button",
      className: "icon-btn",
      onClick: onClose
    }, "\xD7")), children), v$2("div", {
      onClick: onClose,
      className: "background"
    }));
  }

  var FAILS_ON_PRIMITIVES = fails(function () {
    objectKeys(1);
  }); // `Object.keys` method
  // https://tc39.es/ecma262/#sec-object.keys

  _export({
    target: 'Object',
    stat: true,
    forced: FAILS_ON_PRIMITIVES
  }, {
    keys: function keys(it) {
      return objectKeys(toObject$1(it));
    }
  });

  var propertyIsEnumerable = objectPropertyIsEnumerable.f; // `Object.{ entries, values }` methods implementation


  var createMethod = function (TO_ENTRIES) {
    return function (it) {
      var O = toIndexedObject(it);
      var keys = objectKeys(O);
      var length = keys.length;
      var i = 0;
      var result = [];
      var key;

      while (length > i) {
        key = keys[i++];

        if (!descriptors || propertyIsEnumerable.call(O, key)) {
          result.push(TO_ENTRIES ? [key, O[key]] : O[key]);
        }
      }

      return result;
    };
  };

  var objectToArray = {
    // `Object.entries` method
    // https://tc39.es/ecma262/#sec-object.entries
    entries: createMethod(true),
    // `Object.values` method
    // https://tc39.es/ecma262/#sec-object.values
    values: createMethod(false)
  };

  var $values = objectToArray.values; // `Object.values` method
  // https://tc39.es/ecma262/#sec-object.values


  _export({
    target: 'Object',
    stat: true
  }, {
    values: function values(O) {
      return $values(O);
    }
  });

  /* eslint max-len: 0 */
  function Logo() {
    return v$2("svg", {
      version: "1.1",
      id: "Capa_1",
      xmlns: "http://www.w3.org/2000/svg",
      xmlnsXlink: "http://www.w3.org/1999/xlink",
      x: "0px",
      y: "0px",
      width: "6rem",
      viewBox: "0 0 841.89 401.471",
      xmlSpace: "preserve"
    }, v$2("g", {
      id: "Layer_2"
    }, v$2("g", {
      id: "Layer_1-2"
    }, v$2("path", {
      fill: "#FFFFFF",
      d: "M565.858,0c263.37,4.676,263.323,395.802,0,400.243C302.487,395.709,302.532,4.397,565.858,0z"
    }), v$2("path", {
      fill: "#BBBBBB",
      d: "M0.005,224.144v-45.228c-0.005-2.54,2.051-4.603,4.591-4.607c0.019,0,0.038,0,0.057,0 c7.535,0.416,21.79-1.872,27.35,4.176c6.247,4.983,6.239,19.43,0,24.454c-4.912,5.359-15.871,4.127-22.758,4.215v16.991 C9.443,230.176-0.14,230.119,0.005,224.144z M9.26,198.834c6.479-0.119,18.55,1.96,17.854-8.119 c0.695-10.07-11.415-7.999-17.854-8.087V198.834z"
    }), v$2("path", {
      fill: "#BBBBBB",
      d: "M40.041,209.985c-1.848-25.798,36.885-25.901,34.957,0C76.878,235.775,38.241,235.671,40.041,209.985z M48.84,209.985c-1.208,15.847,18.742,15.839,17.534,0c1.232-15.99-18.782-15.99-17.574,0H48.84z"
    }), v$2("path", {
      fill: "#BBBBBB",
      d: "M77.149,195.475c-0.192-4.712,6.936-5.911,8.399-1.479l6.64,19.646l5.352-19.334 c0.498-1.867,2.202-3.156,4.135-3.128c1.851-0.022,3.506,1.146,4.104,2.896l5.352,19.574l6.56-19.646 c0.912-2.96,5.12-3.712,7.199-1.64c4.496,2.079-9.991,30.637-10.11,33.709c-1.112,3.823-7.144,3.384-8.168-0.32l-4.959-17.534 l-4.936,17.43c-0.72,2.308-3.173,3.595-5.479,2.875c-0.419-0.13-0.815-0.322-1.176-0.57 C87.868,226.512,77.678,197.931,77.149,195.475z"
    }), v$2("path", {
      fill: "#BBBBBB",
      d: "M128.201,209.649c-2.473-23.021,33.517-26.27,33.356-3.199c0.288,3.399-0.896,6.831-4.911,6.559h-19.814 c0.392,11.487,13.039,8.592,19.91,4.96C174.181,225.784,126.593,242.11,128.201,209.649z M137.063,205.649h15.854 C153.502,195.499,137.048,195.698,137.063,205.649z"
    }), v$2("path", {
      fill: "#BBBBBB",
      d: "M167.453,224.456c0.472-1.896-1.264-31.637,1.264-31.997c2.704-2.863,7.855-0.416,7.367,3.504 c2.288-6.655,16.919-7.04,11.535,1.823c-5.88,2.776-11.199,0.801-11.535,10.096v16.614 C176.212,230.039,167.325,230.048,167.453,224.456z"
    }), v$2("path", {
      fill: "#BBBBBB",
      d: "M190.963,209.649c-2.464-23.021,33.524-26.27,33.356-3.199c0.296,3.399-0.896,6.831-4.903,6.559h-19.822 c0.4,11.487,13.047,8.592,19.918,4.96C236.95,225.784,189.355,242.11,190.963,209.649z M199.834,205.649h15.854 C216.272,195.499,199.818,195.698,199.834,205.649z"
    }), v$2("path", {
      fill: "#BBBBBB",
      d: "M228.543,209.985c-1.544-14.935,13.255-24.861,25.03-16.11v-18.398c-0.152-3.728,4.848-5.76,7.375-3.048 c2.584-0.136,0.744,39.236,1.264,40.74c0,5.157-1.506,9.116-4.52,11.879C245.75,234.767,226.888,227.248,228.543,209.985z M237.175,209.985c-1.984,13.143,15.998,16.902,16.406,3.199v-11.062C246.79,193.891,236.07,198.986,237.175,209.985 L237.175,209.985z"
    }), v$2("path", {
      fill: "#BBBBBB",
      d: "M287.834,213.169v-37.692c-0.16-3.735,4.84-5.751,7.367-3.048c2.479,1.32,0.8,19.158,1.264,21.446 c5.464-5.088,16.526-3.696,20.838,1.688c5.703,5.728,5.768,23.198-0.191,28.798C307.92,233.959,286.426,228.976,287.834,213.169z M296.465,213.169c-0.855,8.104,9.807,11.679,14.287,5.655c6.839-10.398-2-29.524-14.287-16.727V213.169z"
    }), v$2("path", {
      fill: "#BBBBBB",
      d: "M323.175,243.046c-2.584-2.399-0.681-7.199,2.951-6.983c6.151-0.296,7.447-4.799,10.151-10.631 l-12.303-28.293c-0.211-0.535-0.317-1.105-0.312-1.68c-0.2-4.656,6.848-5.855,8.319-1.601l8.791,22.19l8.8-22.182 c0.928-2.744,5.031-3.656,7.071-1.44c5.095,2.2-13.775,35.325-13.943,38.356C338.413,240.198,331.006,246.95,323.175,243.046z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M710.811,214.345v-18.67c-0.152-3.728,4.871-5.76,7.383-3.048c4.903,6.264-5.039,29.693,8.479,28.941 c4.735,0,7.105-2.399,7.111-7.199v-18.687c-0.136-5.6,8.799-5.6,8.631,0c-0.695,7.199,2.096,23.198-3.031,29.077 C730.688,234.063,708.843,229.567,710.811,214.345z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M766.469,198.259c-2.928-3.288,0.552-7.111,4.376-6.256c0.144-3.455-1.264-10.943,4.264-10.814 c5.495-0.137,4.071,7.391,4.216,10.814c2.76-0.176,7.287-0.248,7.199,3.656c0.104,3.903-4.408,3.879-7.199,3.688 c0.376,1.399-0.96,21.118,0.936,21.462c2.191,1.304,6.008,0.8,5.855,4.472c-0.488,6.151-9.679,3.392-12.143,0.655 c-5.399-3.999-2.4-20.894-3.128-26.589C769.373,199.362,767.533,199.53,766.469,198.259z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M810.137,214.345v-18.67c-0.151-3.728,4.872-5.76,7.384-3.048c4.903,6.264-5.04,29.693,8.479,28.941 c4.736,0,7.106-2.399,7.111-7.199v-18.687c-0.136-5.6,8.8-5.6,8.632,0c-0.696,7.199,2.096,23.198-3.032,29.077 C830.015,234.063,808.169,229.567,810.137,214.345z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M531.155,40.865c45.052,0.8,45.044,67.729,0,68.489C486.104,108.579,486.111,41.665,531.155,40.865z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M444.163,148.855c45.052,0.8,45.044,67.705,0,68.465C399.111,216.545,399.119,149.607,444.163,148.855z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M529.835,248.934c39.845,0.672,39.845,59.898,0,60.57C489.982,308.84,489.991,249.646,529.835,248.934z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M465.257,230.447c31.197,0.528,31.197,46.916,0,47.443C434.084,277.371,434.092,230.991,465.257,230.447z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M511.389,130.457c29.47,0.504,29.461,44.268,0,44.796C481.911,174.677,481.92,130.921,511.389,130.457z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M582.566,101.42c29.469,0.504,29.461,44.308,0,44.795C553.097,145.72,553.104,101.915,582.566,101.42z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M637.921,93.524c29.445,0.504,29.445,44.292,0,44.796C608.628,138.176,608.628,93.66,637.921,93.524z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M656.367,146.224c26.006,0.447,26.006,39.052,0,39.492C630.346,185.268,630.346,146.671,656.367,146.224z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M593.109,264.732c22.525,0.384,22.518,33.86,0,34.236C570.583,298.585,570.583,265.116,593.109,264.732z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M651.096,198.866c22.534,0.384,22.534,33.854,0,34.229C628.554,232.711,628.562,199.25,651.096,198.866z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M437.563,101.42c22.51,0.384,22.51,33.893,0,34.276C415.166,135.593,415.166,101.516,437.563,101.42z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M603.66,56.655c22.526,0.385,22.518,33.854,0,34.237C581.254,90.788,581.254,56.752,603.66,56.655z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M500.838,196.251c22.534,0.384,22.534,33.828,0,34.213C478.296,230.071,478.304,196.627,500.838,196.251z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M632.642,240.99c19.046,0.328,19.046,28.678,0,29.006C613.604,269.644,613.604,241.318,632.642,240.99z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M474.464,64.567c19.062,0.327,19.062,28.637,0,28.957C455.506,93.437,455.506,64.655,474.464,64.567z"
    }), v$2("path", {
      fill: "#2B2B2B",
      d: "M479.744,104.06c19.054,0.319,19.054,28.677,0,28.997C460.762,132.969,460.762,104.14,479.744,104.06z"
    }), v$2("rect", {
      x: "474.385",
      y: "78.803",
      transform: "matrix(-0.9944 0.1058 -0.1058 -0.9944 961.3718 138.2227)",
      fill: "#2B2B2B",
      width: "5.272",
      height: "31.605"
    }), v$2("rect", {
      x: "437.48",
      y: "126.203",
      transform: "matrix(-0.9944 0.1058 -0.1058 -0.9944 892.7841 236.662)",
      fill: "#2B2B2B",
      width: "5.272",
      height: "31.606"
    }), v$2("rect", {
      x: "490.251",
      y: "181.519",
      transform: "matrix(-0.0772 0.997 -0.997 -0.0772 728.7499 -306.1609)",
      fill: "#2B2B2B",
      width: "31.605",
      height: "5.272"
    }), v$2("rect", {
      x: "501.348",
      y: "116.825",
      transform: "matrix(-0.349 0.9371 -0.9371 -0.349 809.6375 -323.5298)",
      fill: "#2B2B2B",
      width: "31.702",
      height: "5.288"
    }), v$2("polygon", {
      fill: "#2B2B2B",
      points: "500.006,156.966 469.969,166.869 468.312,161.87 498.35,151.959 500.006,156.966 \t\t"
    }), v$2("polygon", {
      fill: "#2B2B2B",
      points: "474.744,120.346 443.115,121.018 443.003,115.754 474.632,115.082 474.744,120.346 \t\t"
    }), v$2("polygon", {
      fill: "#2B2B2B",
      points: "591.813,71.87 560.265,72.462 560.152,67.782 591.701,67.183 591.813,71.87 \t\t"
    }), v$2("polygon", {
      fill: "#2B2B2B",
      points: "499.438,223.992 476.472,245.614 473.248,242.223 496.206,220.601 499.438,223.992 \t\t"
    }), v$2("polygon", {
      fill: "#2B2B2B",
      points: "625.714,262.309 601.301,282.282 598.316,278.667 622.73,258.701 625.714,262.309 \t\t"
    }), v$2("polygon", {
      fill: "#2B2B2B",
      points: "587.094,241.286 595.189,271.748 590.669,272.972 582.566,242.511 \t\t"
    }), v$2("polygon", {
      fill: "#2B2B2B",
      points: "643.904,215.889 612.739,210.985 613.451,206.354 644.616,211.257 643.904,215.889 \t\t"
    }), v$2("polygon", {
      fill: "#2B2B2B",
      points: "642.568,172.725 612.188,181.22 610.907,176.717 641.289,168.222 642.568,172.725 \t\t"
    }), v$2("polygon", {
      fill: "#2B2B2B",
      points: "583.342,137.208 579.742,168.518 575.079,168.005 578.679,136.696 \t\t"
    }), v$2("polygon", {
      fill: "#2B2B2B",
      points: "545.922,177.221 519.204,160.454 521.676,156.479 548.394,173.245 545.922,177.221 \t\t"
    }), v$2("polygon", {
      fill: "#2B2B2B",
      points: "567.008,111.379 540.29,94.612 542.762,90.637 569.479,107.403 567.008,111.379 \t\t"
    }), v$2("polygon", {
      fill: "#2B2B2B",
      points: "584.438,112.122 594.733,82.325 599.173,83.829 588.878,113.626 584.438,112.122 \t\t"
    }), v$2("polygon", {
      fill: "#2B2B2B",
      points: "528.579,261.717 543.498,233.943 547.642,236.143 532.715,263.908 528.579,261.717 \t\t"
    }), v$2("polygon", {
      fill: "#2B2B2B",
      points: "633.905,106.683 609.972,86.141 613.012,82.573 636.945,103.115 633.905,106.683 \t\t"
    }), v$2("path", {
      fill: "#FFE535",
      d: "M575.951,156.759c62.394,1.063,62.394,93.759,0,94.807C513.604,250.502,513.62,157.814,575.951,156.759z"
    }))));
  }

  var css_248z$1 = ".badge-details {\n  padding: 20px;\n  border-bottom: 1px solid #000000; }\n\n.badge-img {\n  border-radius: 50%;\n  height: 5rem; }\n\n.avatar-img {\n  border-radius: 50%;\n  height: 3rem;\n  border: 1px white solid; }\n\n.recommendation-images {\n  margin-bottom: 5rem; }\n\n.avatar-img-top {\n  border-radius: 50%;\n  height: 4.5rem;\n  border: 4px solid gold; }\n\n.avatar-img-top:not(:first-child) {\n  margin-left: -15px; }\n\n.border {\n  border: 1px black solid; }\n\n.bottom-line-yellow {\n  border-bottom: 18px gold solid; }\n\n.badge-counter {\n  border-radius: 50%;\n  border: 1px black solid;\n  height: 1.3rem;\n  width: 1.3rem;\n  background-color: white;\n  color: black;\n  font-size: smaller; }\n\n.img {\n  max-width: 100%;\n  max-height: 100%;\n  object-fit: fill; }\n\n.badge {\n  position: relative; }\n\n.position-parent {\n  position: relative; }\n\n.position-child {\n  position: absolute;\n  left: 4.5rem;\n  right: 0;\n  bottom: 0;\n  top: 3.3rem; }\n\n.review-text {\n  text-align: left;\n  padding-left: 1rem; }\n\n.badge-title {\n  padding-bottom: 0.5rem; }\n\n.left {\n  width: 100%; }\n\n.right {\n  width: 80%; }\n\n.border-bottom {\n  border-bottom: 1px black solid; }\n\n.border-top {\n  border-top: 1px black solid; }\n\n.bottom-line {\n  height: 1rem;\n  width: 100%;\n  background-color: #ffe600; }\n\n.border-component {\n  border: 0.1px #9b9898 solid;\n  border-radius: 2%; }\n\n.spacing {\n  padding-top: 2rem;\n  padding-bottom: 2rem; }\n\n.spacing-extra {\n  padding-top: 4rem;\n  padding-bottom: 3em; }\n\n.spacing-top-img {\n  padding-top: 3rem;\n  padding-bottom: 4rem; }\n\n.spacing-y {\n  margin-right: 1.5rem; }\n\n.logo-position {\n  position: relative;\n  top: 0.8rem;\n  left: 2rem; }\n";
  styleInject(css_248z$1);

  function FeedbackDetails(props) {
    var _feedbackSummary$revi, _feedbackSummary$revi2;

    var _getBaseProps = getBaseProps(props, TAG_FEEDBACK_DETAILS_POPUP),
        apiUrl = _getBaseProps.apiUrl;

    var targetUuid = props[ATTR_TARGET_UUID];

    var _useFeedbackSummaryAp = useFeedbackSummaryApi(apiUrl, targetUuid),
        feedbackSummary = _useFeedbackSummaryAp.feedbackSummary;

    if (!feedbackSummary) {
      return v$2(d$2, null);
    } // video
    // const video = feedbackSummary.video.url
    // fet badges and keys


    var fbBadges = feedbackSummary.badges;
    var keys = Object.keys(fbBadges); // returns the badge with image, counter and title

    var badgeGroup = keys.map(function (key, i) {
      var badges = fbBadges[key];
      var badgeKeys = Object.keys(badges);
      return badgeKeys.map(function (badgeKey) {
        return v$2("div", {
          key: badgeKey,
          className: "d-flex flex-column pt-4 pb-4 mr-4 spacing-y"
        }, v$2("div", {
          className: "px-6 py-6 badge-title"
        }, keys[i]), v$2("div", {
          className: "d-flex position-parent pt-6"
        }, v$2("img", {
          className: "badge-img mx-2",
          src: badges[badgeKey].badge.image,
          alt: "person face"
        }), v$2("div", {
          className: "counter badge-counter position-child"
        }, badges[badgeKey].count)));
      });
    }); // star rating

    var stars = feedbackSummary.stars.avg; // review

    var fbReviewImg = (_feedbackSummary$revi = feedbackSummary.review) === null || _feedbackSummary$revi === void 0 ? void 0 : _feedbackSummary$revi.image;
    var fbReviewContent = (_feedbackSummary$revi2 = feedbackSummary.review) === null || _feedbackSummary$revi2 === void 0 ? void 0 : _feedbackSummary$revi2.content; // top image

    var topImg = feedbackSummary.video.image;
    Object.values(topImg).map(function (image) {
      return v$2("img", {
        className: "avatar-img-top",
        src: image,
        alt: "person face"
      });
    });
    return v$2("div", null, v$2(BaseComponent, {
      style: css_248z$1,
      className: "text-center d-flex flex-column justify-content-left align-items-center bottom-line-yellow"
    }, v$2("section", {
      className: "d-flex justify-content-center pl-4 pr-4 spacing-top-img"
    }, v$2("img", {
      className: "avatar-img-top",
      src: topImg,
      alt: "person face"
    })), v$2("section", {
      className: "d-flex justify-content-center left border-bottom border-top spacing"
    }, v$2(_default, {
      count: 5,
      value: stars,
      size: 30,
      color2: "#ffd700",
      edit: false
    })), v$2("section", {
      className: "d-flex justify-content-center pl-4 pr-4 left border-bottom spacing"
    }, badgeGroup), fbReviewContent && v$2("section", {
      className: "d-flex justify-content-center align-items-center mx-6 pl-4 pr-4 left spacing-extra"
    }, fbReviewImg ? v$2("img", {
      className: "avatar-img mx-2 mr-2",
      src: fbReviewImg,
      alt: "person face"
    }) : null, v$2("div", {
      className: "d-flex mx-2 review-text"
    }, fbReviewContent)), v$2("section", {
      className: "d-flex justify-content-end logo-position right"
    }, v$2(Logo, null))));
  }

  var css_248z = ":host {\n  margin: 0;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";\n  font-size: 14px;\n  font-weight: 300;\n  line-height: 1.5;\n  color: #000;\n  -webkit-text-size-adjust: 100%;\n  -webkit-tap-highlight-color: rgba(0, 0, 0, 0); }\n\n.utu-section {\n  border-bottom: 1px solid rgba(0, 0, 0, 0.15);\n  background: #fff;\n  padding: 18px; }\n\nh3 {\n  text-transform: capitalize;\n  font-weight: 500;\n  font-size: 14px; }\n\n.btn {\n  background-color: #FFDD33;\n  padding: 12px;\n  border-radius: 10px;\n  border: none;\n  font-size: 14px;\n  font-weight: 500;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\"; }\n\n.icon-btn {\n  border: none;\n  background: none;\n  padding: 0;\n  font-family: Roboto, system-ui, -apple-system, \"Segoe UI\", \"Helvetica Neue\", Arial, \"Noto Sans\", \"Liberation Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";\n  font-weight: 500;\n  font-size: 14px; }\n  .icon-btn svg {\n    width: 60px;\n    height: 60px; }\n\ntextarea {\n  resize: vertical;\n  min-height: 189px;\n  border-radius: 10px;\n  border: none;\n  background-color: rgba(255, 221, 51, 0.15);\n  padding: 16px;\n  font-weight: 300; }\n\n.error {\n  color: #e80054; }\n";
  styleInject(css_248z);

  function FeedbackPopup(props) {
    var _useState = l$1(false),
        _useState2 = _slicedToArray(_useState, 2),
        popUpVisible = _useState2[0],
        setPopUpVisible = _useState2[1];

    return v$2(BaseComponent, {
      style: css_248z,
      className: "utu-section d-flex flex-column align-items-strath"
    }, v$2("button", {
      type: "button",
      className: "btn",
      onClick: function onClick() {
        return setPopUpVisible(true);
      }
    }, "Show Feedback Details"), popUpVisible && v$2(PopUp, {
      onClose: function onClose() {
        return setPopUpVisible(false);
      }
    }, v$2(FeedbackDetails, props)));
  }

  function FeedbackFormPopup(props) {
    var _useState = l$1(false),
        _useState2 = _slicedToArray(_useState, 2),
        popUpVisible = _useState2[0],
        setPopUpVisible = _useState2[1];

    return v$2(BaseComponent, {
      style: css_248z$5,
      className: "utu-feedback-details-popup utu-section d-flex flex-column align-items-stretch"
    }, v$2("button", {
      type: "button",
      className: "btn",
      onClick: function onClick() {
        return setPopUpVisible(true);
      }
    }, "Give feedback"), popUpVisible && v$2(PopUp, {
      onClose: function onClose() {
        return setPopUpVisible(false);
      }
    }, v$2(FeedbackForm, props)));
  }

  window.addEventListener(EVENT_UTU_IDENTITY_DATA_READY, function (event) {
    localStorage.setItem(LOCAL_STORAGE_KEY_UTU_IDENTITY_DATA, JSON.stringify(event.detail));
  });
  register(Root, TAG_ROOT, [ATTR_API_URL, ATTR_SOURCE_UUID, ATTR_TARGET_TYPE, ATTR_TARGET_UUIDS], {
    shadow: true
  });
  register(Recommendation, TAG_RECOMMENDATION, [ATTR_TARGET_UUID], {
    shadow: true
  });
  register(FeedbackForm, TAG_FEEDBACK_FORM, [ATTR_API_URL, ATTR_SOURCE_UUID, ATTR_TARGET_UUID], {
    shadow: true
  });
  register(FeedbackFormPopup, TAG_FEEDBACK_FORM_POPUP, [ATTR_API_URL, ATTR_SOURCE_UUID, ATTR_TARGET_UUID], {
    shadow: true
  });
  register(FeedbackPopup, TAG_FEEDBACK_DETAILS_POPUP, [ATTR_API_URL], {
    shadow: true
  });

})));
//# sourceMappingURL=index.js.map
