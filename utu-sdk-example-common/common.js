const UTU_API_BASE_URL_TEST = "https://stage-api.ututrust.com";
export const UTU_CORE_API_URL_TEST = UTU_API_BASE_URL_TEST + "/core-api";
export const UTU_ID_API_URL_TEST = UTU_API_BASE_URL_TEST + "/identity-api";

// Identity API service paths
export const ID_USERS = "/users";

// Trust API service paths
export const API_ENTITY = "/entity";
export const API_RELATIONSHIP = "/relationship";
export const API_FEEDBACK = "/feedback";

