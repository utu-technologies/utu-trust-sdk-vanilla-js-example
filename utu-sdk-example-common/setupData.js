import axios from "axios";
import {UTU_CORE_API_URL_TEST, API_ENTITY, API_RELATIONSHIP, API_FEEDBACK} from "./common.js";

const ENTITIES = [
    {
        "name": "Paul",
        "type": "provider",
        "ids": {
            "uuid": "provider-1"
        },
        "image": "https://utu.io/wp-content/uploads/2021/07/P1.png"
    },
    {
        "name": "Jane",
        "type": "provider",
        "ids": {
            "uuid": "provider-2"
        },
        "image": "https://utu.io/wp-content/uploads/2021/07/P2.png"
    },
    {
        "name": "Ali",
        "type": "provider",
        "ids": {
            "uuid": "provider-3"
        },
        "image": "https://utu.io/wp-content/uploads/2021/07/P3-1.png"
    },
    {
        "name": "James",
        "type": "user",
        "ids": {
            "uuid": "user-1"
        },
        "image": "https://utu.io/wp-content/uploads/2021/07/P4.png"
    },
    {
        "name": "Esther",
        "type": "user",
        "ids": {
            "uuid": "user-2"
        },
        "image": "https://utu.io/wp-content/uploads/2021/07/P5.png"
    },
    {
        "name": "Octavia",
        "type": "user",
        "ids": {
            "uuid": "user-3"
        },
        "image": "https://utu.io/wp-content/uploads/2021/07/P6.png"
    }
];

const RELATIONSHIPS = [
    {
        "type": "social",
        "sourceCriteria": {
            "type": "user",
            "ids": {
                "uuid": "user-1"
            }
        },
        "targetCriteria": {
            "type": "user",
            "ids": {
                "uuid": "user-2"
            }
        },
        "bidirectional": true,
        "properties": {
            "kind": "facebook",
            "source": "facebook"
        }
    },
    {
        "type": "social",
        "sourceCriteria": {
            "type": "user",
            "ids": {
                "uuid": "user-1"
            }
        },
        "targetCriteria": {
            "type": "user",
            "ids": {
                "uuid": "user-3"
            }
        },
        "bidirectional": false,
        "properties": {
            "kind": "phonebook"
        }
    }
];

const FEEDBACK = [
    {
        "sourceCriteria": {
            "ids": {
                "uuid": "user-2"
            }
        },
        "targetCriteria": {
            "ids": {
                "uuid": "provider-1"
            }
        },
        "transactionId": "tx1",
        "data": {
            "badges": {
                "Experience": "positive",
                "Quality": "positive"
            },
            "review": "Always a great choice.",
            "stars": 5
        }
    },
    {
        "sourceCriteria": {
            "ids": {
                "uuid": "user-3"
            }
        },
        "targetCriteria": {
            "ids": {
                "uuid": "provider-1"
            }
        },
        "transactionId": "tx2",
        "data": {
            "badges": {
                "Experience": "positive",
                "Punctuality": "positive"
            },
            "stars": 5
        }
    },
    {
        "sourceCriteria": {
            "ids": {
                "uuid": "user-3"
            }
        },
        "targetCriteria": {
            "ids": {
                "uuid": "provider-3"
            }
        },
        "transactionId": "tx3",
        "data": {
            "badges": {
                "Experience": "negative"
            },
            "stars": 1
        }
    }
];

async function apiCall(path, datas, token) {
    const url = UTU_CORE_API_URL_TEST + path;
    for(const data of datas) {
        await axios.post(url, data, withAuthorizationHeader(token))
            .then(response => console.log("POST", url, data, ":", response.data))
            .catch(error => console.log(error));
    }
}

function createEntities(token) {
    return apiCall(API_ENTITY, ENTITIES, token);
}

function createRelationships(token) {
    return apiCall(API_RELATIONSHIP, RELATIONSHIPS, token);
}

function createFeedback(token) {
    return apiCall(API_FEEDBACK, FEEDBACK, token);
}

export function setup(token) {
    return createEntities(token).then(() => createRelationships(token)).then(() => createFeedback(token));
}

function withAuthorizationHeader(token, config = {}) {
    return {
        ...config,
        headers: {
            ...(config.headers || {}),
            "Authorization": `Bearer ${token}`
        }
    };
}

