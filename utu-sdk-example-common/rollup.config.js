import {nodeResolve} from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import {UTU_CORE_API_URL_TEST} from "./common";

export default {
    input: 'auth.js',
    output: {
        dir: 'dist/bundle',
        format: 'iife',
        name: 'getToken'
    },
    plugins: [nodeResolve({ preferBuiltins: true, browser: true}), commonjs()]
};
