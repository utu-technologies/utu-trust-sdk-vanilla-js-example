import axios from "axios";
import {UTU_ID_API_URL_TEST, ID_USERS} from "./common.js";
import { setup } from "./setupData.js";

const usersUrl = `${UTU_ID_API_URL_TEST}${ID_USERS}`

export function getToken(apiKey, userId) {
    const data = { "userId": userId }
    return axios.post(usersUrl, data, withAuthorizationHeader(apiKey))
        .catch(error => {
            // ignore error if 409 conflict, meaning the user already exists.
            if(error.response && error.response.status !== 409) throw error;
        })
        .then(() => axios.get(`${usersUrl}/${userId}/tokens`, withAuthorizationHeader(apiKey)))
        .then(response => setup(response.data.access_token).then(() => response))
        .then(response => response.data)
        .catch(error => console.log(error));
}

function withAuthorizationHeader(apiKey, config = {}) {
    return {
        ...config,
        headers: {
            ...(config.headers || {}),
            "Authorization": `apiKey ${apiKey}`
        }
    };
}
